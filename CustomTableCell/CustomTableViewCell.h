//
//  CustomTableViewCell.h
//  Timex
//
//  Created by Rahul Lekurwale on 24/01/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
{
    UIImageView *img,*subTitleImg;
    UILabel *lbl_SubTitle;

}

@property (strong, nonatomic) IBOutlet UILabel *lbl_SubTitle;


@property(nonatomic,retain)UIImageView *subTitleImg;
@property (weak, nonatomic) IBOutlet UIView *homeBackView;
@property (weak, nonatomic) IBOutlet UIView * viewAddRemove;
@property (weak, nonatomic) IBOutlet UIImageView *homeImageView;
@property (weak, nonatomic) IBOutlet UILabel *homeName;

@property (weak, nonatomic) IBOutlet UIView *viewPlusMinus;

@property (weak, nonatomic) IBOutlet UIButton *btn_popMinus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_popQty;
@property (weak, nonatomic) IBOutlet UIButton *btn_popPlus;

//@property (weak, nonatomic) IBOutlet UIImageView *img_product;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_prod_name;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_sku;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_size;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_qty;
//@property (weak, nonatomic) IBOutlet UIButton *btn_edit;
//@property (weak, nonatomic) IBOutlet UIButton *btn_delete;
//- (IBAction)deleteMethod:(id)sender;
//- (IBAction)editMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *contentBackView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_shortDesc;
@property (weak, nonatomic) IBOutlet UIImageView *img_product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_price;
@property (weak, nonatomic) IBOutlet UIView *qtyBackView;
@property (weak, nonatomic) IBOutlet UIButton *btn_sub;
@property (weak, nonatomic) IBOutlet UIButton *btn_add;
@property (weak, nonatomic) IBOutlet UILabel *lbl_qty;
@property (weak, nonatomic) IBOutlet UIButton *btn_addCart;
@property (weak, nonatomic) IBOutlet UILabel *lbl_brandName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shortDescHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartWeightHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblOutOfStock;

@property (weak, nonatomic) IBOutlet UILabel *lbl_cut;
@property (weak, nonatomic) IBOutlet UIButton *btn_cut;
@property (weak, nonatomic) IBOutlet UILabel *lbl_weight;
@property (weak, nonatomic) IBOutlet UIButton *btn_weight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_type;
@property (weak, nonatomic) IBOutlet UIButton *btn_type;
@property (weak, nonatomic) IBOutlet UILabel *lbl_noAttrWeight;
@property (strong, nonatomic) IBOutlet UILabel *weight_Lbl;
@property (strong, nonatomic) IBOutlet UIButton *btn_wish;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_cutHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_cutHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_weightHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_weightHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_typeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_typeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_noAttrWeightHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *weightLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *outOFStockHeightConstraints;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblPriceHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *qtyViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnCartHeight;

//@property (weak, nonatomic) IBOutlet UITextField *txt_msg;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_underline;
//@property (weak, nonatomic) IBOutlet UIImageView *img_spices;
//@property (weak, nonatomic) IBOutlet UIButton *btn_spices;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spiceBtnContraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spiceImgConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *underlineConstraint;
//
////////Cart Table Cell
//@property (weak, nonatomic) IBOutlet UILabel *lbl_cartItemName;
//@property (weak, nonatomic) IBOutlet UIImageView *img_cartVegMark;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_cartQtyPrice;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_cartSubtotal;
//@property (weak, nonatomic) IBOutlet UIImageView *img_cartItemImage;
//
//
//// History list
//
//@property (strong, nonatomic) IBOutlet UILabel *lbl_orderId;
//@property (strong, nonatomic) IBOutlet UILabel *lbl_OrderTime;
//@property (strong, nonatomic) IBOutlet UILabel *lbl_ShipTo;
//@property (strong, nonatomic) IBOutlet UILabel *lbl_Amount;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_orderName;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_orderStatus;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_orderDate;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_orderAddress;
//@property (weak, nonatomic) IBOutlet UIView *orderBackView;
//
//// History Detail list
//
//@property (strong, nonatomic) IBOutlet UILabel *lbl_DishName;
//@property (strong, nonatomic) IBOutlet UILabel *lbl_Qty;
//@property (strong, nonatomic) IBOutlet UILabel *lbl_Price;
//
//// Order Address list
//
//@property (strong, nonatomic) IBOutlet UILabel *lbl_Address;
//@property (strong, nonatomic) IBOutlet UIButton *btn_RadioButton;

@property (weak, nonatomic) IBOutlet UIView *cart_contentBackView;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_name;
@property (weak, nonatomic) IBOutlet UIImageView *cart_img_product;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_brand;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_weight;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_cut;
@property (weak, nonatomic) IBOutlet UIButton *cart_btn_cut;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_weightRange;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_type;
@property (weak, nonatomic) IBOutlet UIButton *cart_btn_weightRange;
@property (weak, nonatomic) IBOutlet UIButton *cart_btn_type;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_qtyPrice;
@property (weak, nonatomic) IBOutlet UILabel *cart_lbl_price;



//Address cell
@property (strong, nonatomic) IBOutlet UILabel *addressLbl;
@property (strong, nonatomic) IBOutlet UIButton *selectedAddresChkBox;


//History View
@property (strong, nonatomic) IBOutlet UIView *orderBackView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_orderId;
@property (strong, nonatomic) IBOutlet UILabel *lbl_orderStatus;
@property (strong, nonatomic) IBOutlet UILabel *lbl_order_Time;
@property (strong, nonatomic) IBOutlet UILabel *lbl_orderAmount;
@property (strong, nonatomic) IBOutlet UIImageView *forwarderImgView;


//History Details View
@property (strong, nonatomic) IBOutlet UILabel *Lbl_DishName;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_Qty;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_Price;

@end
