//
//  CustomTableViewCell.m
//  Timex
//
//  Created by Rahul Lekurwale on 24/01/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

@synthesize subTitleImg,lbl_SubTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        
        subTitleImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.origin.x+35, self.bounds.origin.y+7,25, 25)];
        [self addSubview:subTitleImg];
        
        lbl_SubTitle = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.origin.x+75, self.bounds.origin.y+5,self.bounds.size.width-50,30)];
        lbl_SubTitle.backgroundColor =[UIColor clearColor];
        lbl_SubTitle.text = @"";
        lbl_SubTitle.textAlignment = NSTextAlignmentLeft;
        lbl_SubTitle.textColor =[UIColor blackColor];
        //lbl_SubTitle.font=[UIFont systemFontOfSize:15.0f];
        NSString *fontName=[NSString stringWithFormat:@"ebrima"];
        lbl_SubTitle.font=[UIFont fontWithName:fontName size:15.0f];
        lbl_SubTitle.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_SubTitle.numberOfLines = 0;
        lbl_SubTitle.userInteractionEnabled = NO;
        [self addSubview:lbl_SubTitle];

        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteMethod:(id)sender {
}

- (IBAction)editMethod:(id)sender {
}
@end
