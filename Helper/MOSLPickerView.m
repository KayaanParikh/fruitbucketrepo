//
//  MOSLPicerView.m
//  MOSL
//
//  Created by MacMINI1 on 02/02/16.
//  Copyright © 2016, Motilal Oswal Securities Limited
//

#import "MOSLPickerView.h"

@interface MOSLPickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UILabel *pickerViewLabel;
@property (nonatomic, strong) UIView *pickerContainerView;
@property (nonatomic, strong) UIView *pickerViewContainerView;
@property (nonatomic, strong) UIView *pickerTopBarView;
@property (nonatomic, strong) UIToolbar *pickerViewToolBar;
@property (nonatomic, strong) UIBarButtonItem *pickerViewBarButtonItem;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) NSArray *pickerViewArray;
@property (nonatomic, strong) UIDatePicker *datePickerView;

@property (copy) void (^onDismissCompletionObject)(int);
@property (copy) void (^onDismissCompletionDate)(NSDate *);

@end


@implementation MOSLPickerView

bool isDatePickerSelected;

#pragma mark - Singleton

+ (MOSLPickerView*)sharedView
{
    static dispatch_once_t once;
    static MOSLPickerView *sharedView;
    dispatch_once(&once, ^{
        sharedView = [[self alloc] init];
    });
    return sharedView;
}

#pragma mark - Initialize PickerView

//- (void)initializePickerViewInViewForHome:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)array withSelectedIndex:(int)index
//{
//    _pickerViewArray = array;
//    
//    [self setFrame:view.bounds];
//    [self setBackgroundColor:[UIColor clearColor]];
//    
//    //Whole screen with PickerView and a dimmed background
//    
//    _pickerViewContainerView = [[UIView alloc] initWithFrame:view.bounds];
//    [_pickerViewContainerView setBackgroundColor: [UIColor clearColor]];
//    [self addSubview:_pickerViewContainerView];
//    
//    //PickerView Container with top bar
//    _pickerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _pickerViewContainerView.bounds.size.height - 260.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
//    
//    [_pickerViewContainerView addSubview:_pickerContainerView];
//    
//    //Content of pickerContainerView
//    
//    //Top bar view
//    _pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _pickerContainerView.frame.size.width, 44.0)];
//    [_pickerContainerView addSubview:_pickerTopBarView];
//    [_pickerTopBarView setBackgroundColor:[UIColor whiteColor]];
//    
//    _pickerViewToolBar = [[UIToolbar alloc] initWithFrame:_pickerTopBarView.frame];
//    [_pickerContainerView addSubview:_pickerViewToolBar];
//    
//    [_pickerViewToolBar setBackgroundColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8]];
//    _pickerViewToolBar.barTintColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8];
//    
//    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
//    [cancelButton setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
//    
//    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    
//    _pickerViewBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
//    
//    [_pickerViewBarButtonItem setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
//    
//    _pickerViewToolBar.items = @[flexibleSpace, _pickerViewBarButtonItem];
//    
//    //Add pickerView
//    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, [UIScreen mainScreen].bounds.size.width, 260.0)];
//    [_pickerView setDelegate:self];
//    [_pickerView setDataSource:self];
//    [_pickerView setBackgroundColor:[UIColor whiteColor]];
//    [_pickerView setShowsSelectionIndicator: true];
//    [_pickerContainerView addSubview:_pickerView];
//    
//    [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
//    
//    [_pickerView selectRow:index inComponent:0 animated:YES];
//    
//    self.onDismissCompletionObject ([self selectedObjectObject_inComponent:index]);
//}


- (void)initializePickerViewInView:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)array withSelectedIndex:(int)index
{
    _pickerViewArray = array;
    
    [self setFrame:view.bounds];
    [self setBackgroundColor:[UIColor clearColor]];
    [_pickerViewContainerView setBackgroundColor: [UIColor clearColor]];
    [_pickerView removeFromSuperview];
    [_pickerViewToolBar removeFromSuperview];
    [_pickerTopBarView removeFromSuperview];
    //Whole screen with PickerView and a dimmed background
    
    _pickerViewContainerView = [[UIView alloc] initWithFrame:view.bounds];
    [_pickerViewContainerView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.7]];
    [self addSubview:_pickerViewContainerView];
    
    //PickerView Container with top bar
    _pickerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _pickerViewContainerView.bounds.size.height - 260.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    [_pickerViewContainerView addSubview:_pickerContainerView];
    
    //Content of pickerContainerView
    
    //Top bar view
    _pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _pickerContainerView.frame.size.width, 44.0)];
    [_pickerContainerView addSubview:_pickerTopBarView];
    [_pickerTopBarView setBackgroundColor:[UIColor whiteColor]];
    
    _pickerViewToolBar = [[UIToolbar alloc] initWithFrame:_pickerTopBarView.frame];
    [_pickerContainerView addSubview:_pickerViewToolBar];
    
    [_pickerViewToolBar setBackgroundColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8]];
    _pickerViewToolBar.barTintColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    [cancelButton setTintColor:[UIColor redColor]]; //[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    _pickerViewBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    
    [_pickerViewBarButtonItem setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    _pickerViewToolBar.items = @[cancelButton,flexibleSpace, _pickerViewBarButtonItem];
    
    //Add pickerView
    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, [UIScreen mainScreen].bounds.size.width, 260.0)];
    [_pickerView setDelegate:self];
    [_pickerView setDataSource:self];
    [_pickerView setBackgroundColor:[UIColor whiteColor]];
    [_pickerView setShowsSelectionIndicator: true];
    [_pickerContainerView addSubview:_pickerView];
    
    [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
    
    [_pickerView selectRow:index inComponent:0 animated:YES];
    
    
//    self.onDismissCompletionObject ([self selectedObjectObject_inComponent:index]);
}

#pragma mark - Initialize DatePicker

- (void)initializeDatePickerViewInView:(UIView *)view withDelegate:(id)VCDelegate forSelectedYear:(int)year forStartDate:(bool)isStart
{
    [self setFrame:view.bounds];
    [self setBackgroundColor:[UIColor clearColor]];
    
    //Whole screen with PickerView and a dimmed background
    _pickerViewContainerView = [[UIView alloc] initWithFrame:view.bounds];
    [_pickerViewContainerView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.7]];
    [self addSubview:_pickerViewContainerView];
    
    //PickerView Container with top bar
    _pickerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _pickerViewContainerView.bounds.size.height - 260.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    [_pickerViewContainerView addSubview:_pickerContainerView];
    
    //Content of pickerContainerView
    
    //Top bar view
    _pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _pickerContainerView.frame.size.width, 44.0)];
    [_pickerContainerView addSubview:_pickerTopBarView];
    [_pickerTopBarView setBackgroundColor:[UIColor whiteColor]];
    
    _pickerViewToolBar = [[UIToolbar alloc] initWithFrame:_pickerTopBarView.frame];
    [_pickerContainerView addSubview:_pickerViewToolBar];
    
    [_pickerViewToolBar setBackgroundColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8]];
    _pickerViewToolBar.barTintColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    [cancelButton setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    _pickerViewBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    
    [_pickerViewBarButtonItem setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    _pickerViewToolBar.items = @[flexibleSpace, _pickerViewBarButtonItem];
    
    //Add pickerView
    _datePickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, [UIScreen mainScreen].bounds.size.width, 260.0)];
    [_datePickerView setDatePickerMode:UIDatePickerModeDate];
    [_datePickerView setBackgroundColor:[UIColor whiteColor]];
    [_datePickerView setMinimumDate:[self generateDate:year minimum:true]];
    [_datePickerView setMaximumDate:[self generateDate:year minimum:false]];
    [_datePickerView setDate:[self generateDate:year minimum:isStart]];
    
    [_datePickerView addTarget:self action:@selector(changeDatePicker:) forControlEvents:UIControlEventValueChanged];
    [_pickerContainerView addSubview:_datePickerView];
    
    [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
}

//[_datePickerView setMinimumDate:[NSDate date]];
- (void)initializeDatePickerViewForFrom:(UIView *)view withDelegate:(id)VCDelegate withYear:(int)year withToDate:(NSDate *)toDate withSelectDate:(NSDate *)selectDate withTitle:(NSString *)title {
    
    [self setFrame:view.bounds];
    [self setBackgroundColor:[UIColor clearColor]];
    
    //Whole screen with PickerView and a dimmed background
    _pickerViewContainerView = [[UIView alloc] initWithFrame:view.bounds];
    [_pickerViewContainerView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.7]];
    [self addSubview:_pickerViewContainerView];
    
    //PickerView Container with top bar
    _pickerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _pickerViewContainerView.bounds.size.height - 260.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    [_pickerViewContainerView addSubview:_pickerContainerView];
    
    //Content of pickerContainerView
    
    //Top bar view
    _pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _pickerContainerView.frame.size.width, 44.0)];
    [_pickerContainerView addSubview:_pickerTopBarView];
    [_pickerTopBarView setBackgroundColor:[UIColor whiteColor]];
    
    _pickerViewToolBar = [[UIToolbar alloc] initWithFrame:_pickerTopBarView.frame];
    [_pickerContainerView addSubview:_pickerViewToolBar];
    
    [_pickerViewToolBar setBackgroundColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8]];
    _pickerViewToolBar.barTintColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    [cancelButton setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *btnTemp = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:nil];
    [btnTemp setTintColor:[UIColor clearColor]];
   
    UIBarButtonItem *btnTitle = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:nil];
    [btnTitle setTintColor:[UIColor colorWithWhite:0.6 alpha:1.0]];
    
    _pickerViewBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    
    [_pickerViewBarButtonItem setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    _pickerViewToolBar.items = @[btnTemp, flexibleSpace, btnTitle, flexibleSpace, _pickerViewBarButtonItem];
    
    
    NSDateComponents* components = [[NSDateComponents alloc] init];;
    
    [components setDay:1];
    [components setMonth:1];
    [components setYear:year];
    
    //Add pickerView
    _datePickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, [UIScreen mainScreen].bounds.size.width, 260.0)];
    [_datePickerView setDatePickerMode:UIDatePickerModeDate];
    [_datePickerView setBackgroundColor:[UIColor whiteColor]];
    [_datePickerView setMinimumDate:( toDate == nil ) ? [[NSCalendar currentCalendar] dateFromComponents:components] : toDate];
    [_datePickerView setMaximumDate:[[NSDate date] dateByAddingTimeInterval: -86400.0]];
    [_datePickerView setDate:( selectDate == nil ) ? [NSDate date] : selectDate];
    
    [_datePickerView addTarget:self action:@selector(changeDatePicker:) forControlEvents:UIControlEventValueChanged];
    [_pickerContainerView addSubview:_datePickerView];
    
    [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
}

- (void)initializeDatePickerViewForQMSForFrom:(UIView *)view withDelegate:(id)VCDelegate withYear:(int)year withToDate:(NSDate *)toDate withSelectDate:(NSDate *)selectDate withTitle:(NSString *)title {
    
    [self setFrame:view.bounds];
    [self setBackgroundColor:[UIColor clearColor]];
    
    //Whole screen with PickerView and a dimmed background
    _pickerViewContainerView = [[UIView alloc] initWithFrame:view.bounds];
    [_pickerViewContainerView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.7]];
    [self addSubview:_pickerViewContainerView];
    
    //PickerView Container with top bar
    _pickerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _pickerViewContainerView.bounds.size.height - 260.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    [_pickerViewContainerView addSubview:_pickerContainerView];
    
    //Content of pickerContainerView
    
    //Top bar view
    _pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _pickerContainerView.frame.size.width, 44.0)];
    [_pickerContainerView addSubview:_pickerTopBarView];
    [_pickerTopBarView setBackgroundColor:[UIColor whiteColor]];
    
    _pickerViewToolBar = [[UIToolbar alloc] initWithFrame:_pickerTopBarView.frame];
    [_pickerContainerView addSubview:_pickerViewToolBar];
    
    [_pickerViewToolBar setBackgroundColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8]];
    _pickerViewToolBar.barTintColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    [cancelButton setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *btnTemp = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:nil];
    [btnTemp setTintColor:[UIColor clearColor]];
    
    UIBarButtonItem *btnTitle = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:nil];
    [btnTitle setTintColor:[UIColor colorWithWhite:0.6 alpha:1.0]];
    
    _pickerViewBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    
    [_pickerViewBarButtonItem setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    _pickerViewToolBar.items = @[btnTemp, flexibleSpace, btnTitle, flexibleSpace, _pickerViewBarButtonItem];
    
    
    NSDateComponents* components = [[NSDateComponents alloc] init];;
    
    [components setDay:1];
    [components setMonth:1];
    [components setYear:year];
    
    //Add pickerView
    _datePickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, [UIScreen mainScreen].bounds.size.width, 260.0)];
    [_datePickerView setDatePickerMode:UIDatePickerModeDate];
    [_datePickerView setBackgroundColor:[UIColor whiteColor]];
//    [_datePickerView setMinimumDate:( toDate == nil ) ? [[NSCalendar currentCalendar] dateFromComponents:components] : toDate];
//    [_datePickerView setMaximumDate:[[NSDate date] dateByAddingTimeInterval: -86400.0]];
    [_datePickerView setMinimumDate:[NSDate date]];
    [_datePickerView setDate:( selectDate == nil ) ? [NSDate date] : selectDate];
    
    [_datePickerView addTarget:self action:@selector(changeDatePicker:) forControlEvents:UIControlEventValueChanged];
    [_pickerContainerView addSubview:_datePickerView];
    
    [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
}

+ (void)showDatePickerViewForFromDate:(UIView *)view withDelegate:(id)VCDelegate withFromData:(NSDate *)fromDate withToDate:(NSDate *)toDate withSelectDate:(NSDate *)selectDate withTitle:(NSString *)title completion:(void(^)(NSDate* selectedDate))completion {
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:fromDate];
    
    int year = [@([components year]) intValue];
    
    year = year - 50;
    
    isDatePickerSelected = true;
    
    [self sharedView].onDismissCompletionDate = completion;
    
    title = ( title == nil ) ? @"" : title;
    
    [[self sharedView] initializeDatePickerViewForFrom:view withDelegate:VCDelegate withYear:year withToDate:toDate withSelectDate:( selectDate == nil ) ? [[NSDate date] dateByAddingTimeInterval: -86400.0] : selectDate withTitle:title];
    
    [[self sharedView] setPickerHiddenObject:NO callBack:nil];
    [view addSubview:[self sharedView]];
}

+ (void)showDatePickerViewForQMSFromDate:(UIView *)view withDelegate:(id)VCDelegate withFromData:(NSDate *)fromDate withToDate:(NSDate *)toDate withSelectDate:(NSDate *)selectDate withTitle:(NSString *)title completion:(void(^)(NSDate* selectedDate))completion {
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:fromDate];
    
    int year = [@([components year]) intValue];
    
    year = year - 50;
    
    isDatePickerSelected = true;
    
    [self sharedView].onDismissCompletionDate = completion;
    
    title = ( title == nil ) ? @"" : title;
    
    [[self sharedView] initializeDatePickerViewForQMSForFrom:view withDelegate:VCDelegate withYear:year withToDate:toDate withSelectDate:( selectDate == nil ) ? [[NSDate date] dateByAddingTimeInterval: -86400.0] : selectDate withTitle:title];
    
    [[self sharedView] setPickerHiddenObject:NO callBack:nil];
    [view addSubview:[self sharedView]];
}

#pragma mark - Generate Data

- (NSDate *)generateDate:(int)year minimum:(bool)min
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:min ? 1 : 31];
    [comps setMonth:min ? 4 : 3];
    [comps setYear:min ? year : year + 1];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

#pragma mark - Date Picker values

- (IBAction)changeDatePicker:(id)sender
{
//    self.onDismissCompletionDate(_datePickerView.date);
}

#pragma mark - Show Methods

+ (void)showPickerViewInView:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)arr withSelectedIndex:(int)index completion:(void(^)(int selectedValue))completion
{
    isDatePickerSelected = false;
    
    [self sharedView].onDismissCompletionObject = completion;
    
    [[self sharedView] initializePickerViewInView:view withDelegate:VCDelegate withArray:arr withSelectedIndex:index];
    
    [[self sharedView] setPickerHiddenObject:NO callBack:nil];
    [view addSubview:[self sharedView]];
}

//+ (void)showPickerViewInHomeView:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)arr withSelectedIndex:(int)index completion:(void(^)(int selectedValue))completion
//{
//    [self sharedView].onDismissCompletionObject = completion;
//    
//    [[self sharedView] initializePickerViewInViewForHome:view withDelegate:VCDelegate withArray:arr withSelectedIndex:index ];
//    
//    [[self sharedView] setPickerHiddenObject:NO callBack:nil];
//    [view addSubview:[self sharedView]];
//}

+ (void)showPickerViewInView1:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)arr withSelectedIndex:(int)index completion:(void(^)(int selectedValue))completion
{
    isDatePickerSelected = false;
    
    [self sharedView].onDismissCompletionObject = completion;
    
    [[self sharedView] initializePickerViewInView1:view withDelegate:VCDelegate withArray:arr withSelectedIndex:index];
    
    [[self sharedView] setPickerHiddenObject:NO callBack:nil];
    [view addSubview:[self sharedView]];
}

- (void)initializePickerViewInView1:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)array withSelectedIndex:(int)index
{
    _pickerViewArray = array;
    
    [self setFrame:view.bounds];
    [self setBackgroundColor:[UIColor clearColor]];
    [_pickerViewContainerView setBackgroundColor: [UIColor clearColor]];
    [_pickerView removeFromSuperview];
    [_pickerViewToolBar removeFromSuperview];
    [_pickerTopBarView removeFromSuperview];
    //Whole screen with PickerView and a dimmed background
    
    _pickerViewContainerView = [[UIView alloc] initWithFrame:view.bounds];
    [_pickerViewContainerView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.7]];
    [self addSubview:_pickerViewContainerView];
    
    //PickerView Container with top bar
    _pickerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 200, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    _pickerContainerView.backgroundColor=[UIColor whiteColor];
    [_pickerViewContainerView addSubview:_pickerContainerView];
    
    //Content of pickerContainerView
    
    //Top bar view
    _pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(20,0, _pickerContainerView.frame.size.width-40, 44.0)];
    [_pickerContainerView addSubview:_pickerTopBarView];
    [_pickerTopBarView setBackgroundColor:[UIColor whiteColor]];
    
    _pickerViewToolBar = [[UIToolbar alloc] initWithFrame:_pickerTopBarView.frame];
    [_pickerContainerView addSubview:_pickerViewToolBar];
    
    [_pickerViewToolBar setBackgroundColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8]];
    _pickerViewToolBar.barTintColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    [cancelButton setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    _pickerViewBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    
    [_pickerViewBarButtonItem setTintColor:[UIColor colorWithRed:0.000 green:0.486 blue:0.976 alpha:1]];
    
    _pickerViewToolBar.items = @[cancelButton,flexibleSpace, _pickerViewBarButtonItem];
    
    //Add pickerView
    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 40, [UIScreen mainScreen].bounds.size.width, 260.0)];
    [_pickerView setDelegate:self];
    [_pickerView setDataSource:self];
    [_pickerView setBackgroundColor:[UIColor whiteColor]];
    [_pickerView setShowsSelectionIndicator: true];
    [_pickerContainerView addSubview:_pickerView];
    
    [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
    
    [_pickerView selectRow:index inComponent:0 animated:YES];
}



+ (void)showDatePickerViewInView:(UIView *)view withDelegate:(id)VCDelegate withYear:(int)year forStartDate :(bool)isStart completion:(void(^)(NSDate* selectedDate))completion
{
    isDatePickerSelected = true;
    
    [self sharedView].onDismissCompletionDate = completion;
    
    [[self sharedView] initializeDatePickerViewInView:view withDelegate:VCDelegate forSelectedYear:year forStartDate:isStart];
    
    [[self sharedView] setPickerHiddenObject:NO callBack:nil];
    [view addSubview:[self sharedView]];
}

#pragma mark - Dismiss Methods

- (void)doneAction
{
    if (isDatePickerSelected) {
        
        if (self.onDismissCompletionDate)
        {
            
            self.onDismissCompletionDate(_datePickerView.date);
            [MOSLPickerView dismissWithCompletionDate:self.onDismissCompletionDate];
        }
    }
    else {
        
        if (self.onDismissCompletionObject)
        {
            self.onDismissCompletionObject ((int)[self.pickerView selectedRowInComponent:0]);
            [MOSLPickerView dismissWithCompletionObject:self.onDismissCompletionObject];
        }
    }
}

- (void)cancelAction
{
    
    [MOSLPickerView dismissWithCompletionObject:nil];
}

+ (void)dismissWithCompletionObject:(void (^)(int))completion
{
    [[self sharedView] setPickerHiddenObject:YES callBack:completion];
}

+ (void)dismissWithCompletionDate:(void (^)(NSDate *))completion
{
    [[self sharedView] setDatePickerHiddenObject:YES callBack:completion];
}

#pragma mark - Show / Hide PickerView methods

- (void)setPickerHiddenObject:(BOOL)hidden callBack: (void(^)(int))callBack
{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         if (hidden) {
                             [_pickerViewContainerView setAlpha:0.0];
                             [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
                         } else {
                             [_pickerViewContainerView setAlpha:1.0];
                             [_pickerContainerView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if (completed && hidden) {
                             [MOSLPickerView removePickerView];
                         }
                     }];
}

- (void)setDatePickerHiddenObject:(BOOL)hidden callBack: (void(^)(NSDate *))callBack
{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         if (hidden) {
                             [_pickerViewContainerView setAlpha:0.0];
                             [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
                         } else {
                             [_pickerViewContainerView setAlpha:1.0];
                             [_pickerContainerView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if (completed && hidden) {
                             [MOSLPickerView removePickerView];
                         }
                     }];
}

#pragma mark - Remove method

+ (void)removePickerView
{
    [[self sharedView] removeFromSuperview];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [_pickerViewArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_pickerViewArray objectAtIndex: [self.pickerView selectedRowInComponent:0]];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UIView *customPickerView = view;
    
    UILabel *pickerViewLabel;
    
    if (customPickerView == nil)
    {
        CGRect frame = CGRectMake(0.0, 0.0, 292.0, 44.0);
        customPickerView = [[UIView alloc] initWithFrame: frame];
        
        [_pickerView setBackgroundColor:[UIColor whiteColor]];
        CGRect labelFrame = CGRectMake(0.0, 3.0, 292.0, 35); // 35 or 44
        
        pickerViewLabel = [[UILabel alloc] initWithFrame:labelFrame];
        [pickerViewLabel setTextAlignment: NSTextAlignmentCenter];
        [pickerViewLabel setBackgroundColor:[UIColor clearColor]];
        [pickerViewLabel setTextColor:[UIColor blackColor]];
        [pickerViewLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightLight]];
        [pickerViewLabel setLineBreakMode : NSLineBreakByWordWrapping];
        [pickerViewLabel setNumberOfLines : 0];
        [customPickerView addSubview:pickerViewLabel];
    }
    else
    {
        for (UIView *view in customPickerView.subviews)
        {
            if (view.tag == 1)
            {
                pickerViewLabel = (UILabel *)view;
                break;
            }
        }
    }
    
    [pickerViewLabel setText:[_pickerViewArray objectAtIndex:row]];
    return customPickerView;
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    self.onDismissCompletionObject ([self selectedObjectObject_inComponent:component]);
    _pickerView.showsSelectionIndicator = true;
}

- (int)selectedObjectObject_inComponent:(NSInteger)component 
{ 
    return (int)[self.pickerView selectedRowInComponent:0];
}

@end
