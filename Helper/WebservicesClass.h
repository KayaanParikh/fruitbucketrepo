//
//  WebservicesClass.h
//  MagentoSkeletonApp
//
//  Created by Rahul Lekurwale on 04/03/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperation.h"
#import "RESTConnector.h"
#import "MBProgressHUD.h"
#import "HelperClass.h"
#import "AFHTTPRequestOperationManager+Synchronous.h"

@interface WebservicesClass : NSObject
{
    MBProgressHUD *HUD;
}

@property (nonatomic, retain) RESTConnector *connectReq;



//-(NSArray *) initWithURL: (NSString*) url RequestType: (NSString*) requestType PostDataValuesAndKeys: (NSDictionary*) postData  UrlParameters: (NSDictionary*) urlParameters;


@property(nonatomic,retain)NSMutableArray *responseData;
//-(NSMutableArray *) fetchUrl: (NSString*) url RequestType: (NSString*) requestType PostDataValuesAndKeys: (NSDictionary*) postData  UrlParameters: (NSDictionary*) urlParameters;

-(NSDictionary *) fetchUrl: (NSString*) url PostDataValuesAndKeys: (NSString *) postData;

//-(NSMutableArray *) fetchUrl: (NSString*) url RequestType: (NSString*) requestType UrlParameters: (NSDictionary*) urlParameters;
-(NSDictionary *) fetchUrl: (NSString*) url;
-(NSDictionary *)GetTopviewProductWithUrl:(NSString*) tempUrl;
-(NSDictionary *)PostTopviewProductWithUrl:(NSString*) tempUrl withParameter:(NSMutableDictionary *)parameters;

@end
