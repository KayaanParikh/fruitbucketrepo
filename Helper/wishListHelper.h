//
//  wishListHelper.h
//  TSMC
//
//  Created by Rahul Lekurwale on 08/09/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface wishListHelper : NSObject
{
    NSMutableDictionary *wishDictionary;


}


+ (instancetype)sharedInstance;
-(NSMutableDictionary *)getAllWishData;
-(NSArray *)getAllWishProductId;
-(id)insertIntoProductList:(NSString *)prodId;
-(id)deleteFromWishList:(NSString *)prodId;
@end
