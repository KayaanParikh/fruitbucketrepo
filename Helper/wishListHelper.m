//
//  wishListHelper.m
//  TSMC
//
//  Created by Rahul Lekurwale on 08/09/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "wishListHelper.h"
#import "HelperClass.h"
#import "WebservicesClass.h"
#import "AppConstant.h"
@implementation wishListHelper


//- (id) initWithCustId:(NSString *)custId{
//    
//    if (self = [super init])
//    {
//        wishDictionary=[[NSMutableDictionary alloc] init];
//    }
//    
//    return self;
//}

+ (instancetype)sharedInstance
{
    static wishListHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[wishListHelper alloc] init];
        // Perform other initialisation...
    });
    return sharedInstance;
}
-(wishListHelper *)init
{
    wishDictionary=[[NSMutableDictionary alloc] init];
    return self;
}

-(NSMutableDictionary *)getAllWishData{
    wishDictionary=[[NSMutableDictionary alloc] init];
    
    NSString *userid=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:userid forKey:@"customer_id"];

    NSString *tempUrl;
    tempUrl=[NSString stringWithFormat:@"%@",API_GET_ALL_WISHLIST];
    WebservicesClass *web=[[WebservicesClass alloc] init];
    //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
    NSDictionary *jsonDict=[web PostTopviewProductWithUrl:tempUrl withParameter:dict];
    NSLog(@"%@",jsonDict);
    wishDictionary=(NSMutableDictionary *)jsonDict;
    return (NSMutableDictionary *)jsonDict;
}

-(NSArray *)getAllWishProductId{
    NSArray *productIdArray=[wishDictionary valueForKey:@"entity_id"];
    return productIdArray;
}


-(id)insertIntoProductList:(NSString *)prodId{
    NSString *userid=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:userid forKey:@"customer_id"];
    [dict setObject:prodId forKey:@"product_id"];

    NSString *tempUrl;
    tempUrl=[NSString stringWithFormat:@"%@",API_INSERT_INTO_WISHLIST];
    WebservicesClass *web=[[WebservicesClass alloc] init];
    //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
    id jsonDict=[web PostTopviewProductWithUrl:tempUrl withParameter:dict];
    NSLog(@"%@",jsonDict);
    return jsonDict;

}
-(id)deleteFromWishList:(NSString *)prodId{
    NSString *userid=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:userid forKey:@"customer_id"];
    [dict setObject:prodId forKey:@"product_id"];
    
    NSString *tempUrl;
    tempUrl=[NSString stringWithFormat:@"%@",API_DELETE_FROM_WISHLIST];
    WebservicesClass *web=[[WebservicesClass alloc] init];
    //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
    id jsonDict=[web PostTopviewProductWithUrl:tempUrl withParameter:dict];
    NSLog(@"%@",jsonDict);
    return jsonDict;



}

@end
