//
//  WebservicesClass.m
//  MagentoSkeletonApp
//
//  Created by Rahul Lekurwale on 04/03/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "WebservicesClass.h"
#import "ProgressHUD.h"
@implementation WebservicesClass

@synthesize responseData;

-(NSDictionary *) fetchUrl: (NSString*) url PostDataValuesAndKeys: (NSString *) postData{
    NSData *post = [postData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    NSDictionary *dictObj=[NSDictionary dictionary];
    _connectReq = [[RESTConnector alloc]init];
    HelperClass *s=[[HelperClass alloc]init];
    BOOL connection=s.getInternetStatus;
    
    if (!connection)
    {
        
        
        NSLog(@"Hello Net connectiion is not present....");
        
        [HUD hide:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                        message:@"Please check your internet connection!"
                                                       delegate:self
                                              cancelButtonTitle:@"Close App"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        //NSDictionary *dictObj = [_connectReq createRequest:[NSString stringWithFormat:@"%@setting/setting",url] body:nil methodType:@"GET" headers:nil synchronous:YES useCache:NO];
        
        dictObj = [_connectReq createRequest:url body:post methodType:@"POST" headers:nil synchronous:YES useCache:NO];
        NSLog(@"%@",dictObj);
        
        if ([dictObj count] >0)
        {
            
            NSLog(@"success");
        }
        
    }
    return dictObj;
}




-(NSDictionary *)GetTopviewProductWithUrl:(NSString*) tempUrl
{
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
    
    NSError *error = nil;
    
    dict = [manager syncGET:tempUrl
            
                 parameters:nil
            
                  operation:NULL
            
                      error:&error];
    
    if (error) {
  
        //[ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];

    }
    return dict;
}

-(NSDictionary *)PostTopviewProductWithUrl:(NSString*) tempUrl withParameter:(NSMutableDictionary *)parameters
{
    
//    NSURL *u=[NSURL URLWithString:parameters];
//    NSDictionary *urlDict=[NSDictionary dictionaryWithContentsOfURL:u];
    
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
    
    NSError *error = nil;
    
    dict = [manager syncPOST:tempUrl
            
                 parameters:parameters
            
                  operation:NULL
            
                      error:&error];
    
    if (error) {

        //[ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
        
    }
    return dict;
}



- (NSDictionary *)processResponseUsingData:(NSData*)data {
    NSError *parseJsonError = nil;
    
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingAllowFragments error:&parseJsonError];
    if (!parseJsonError) {
        NSLog(@"json data = %@", jsonDict);
        
            
            
        
            
            
            
        }
    return jsonDict;
}


@end
