//
//  HelperClass.h
//  Timex
//
//  Created by Rahul Lekurwale on 23/01/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface HelperClass : NSObject
{
    NSUserDefaults *defaults;

}
@property(strong,nonatomic)NSString *url;



- (BOOL)getInternetStatus;
-(NSString *)Get_Base_url;
-(NSString *)Get_url;
-(NSString *)Get_Website_url;
-(void)addFCMTokenToServer;
@end
