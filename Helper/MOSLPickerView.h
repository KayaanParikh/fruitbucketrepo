//
//  MOSLPicerView.h
//  MOSL
//
//  Created by MacMINI1 on 02/02/16.
//  Copyright © 2016, Motilal Oswal Securities Limited
//

#import <UIKit/UIKit.h>

@interface MOSLPickerView : UIView

+ (void)showPickerViewInView:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)arr withSelectedIndex:(int)index completion:(void(^)(int selectedValue))completion;

+ (void)showDatePickerViewInView:(UIView *)view withDelegate:(id)VCDelegate withYear:(int)year forStartDate :(bool)isStart completion:(void(^)(NSDate* selectedDate))completion;
+ (void)showDatePickerViewForFromDate:(UIView *)view withDelegate:(id)VCDelegate withFromData:(NSDate *)fromDate withToDate:(NSDate *)toDate withSelectDate:(NSDate *)selectDate withTitle:(NSString *)title completion:(void(^)(NSDate* selectedDate))completion;

//+ (void)showPickerViewInHomeView:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)arr withSelectedIndex:(int)index completion:(void(^)(int selectedValue))completion;
+ (void)showPickerViewInView1:(UIView *)view withDelegate:(id)VCDelegate withArray:(NSArray *)arr withSelectedIndex:(int)index completion:(void(^)(int selectedValue))completion;

+ (void)dismissWithCompletionObject:(void(^)(int))completion;

+ (void)removePickerView;

+ (void)showDatePickerViewForQMSFromDate:(UIView *)view withDelegate:(id)VCDelegate withFromData:(NSDate *)fromDate withToDate:(NSDate *)toDate withSelectDate:(NSDate *)selectDate withTitle:(NSString *)title completion:(void(^)(NSDate* selectedDate))completion;

@end
