//
//  HelperClass.m
//  Timex
//
//  Created by Rahul Lekurwale on 23/01/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "HelperClass.h"
#import "Reachability.h"
#import "AppConstant.h"
#import "WebservicesClass.h"
@implementation HelperClass

@synthesize url;

- (BOOL)getInternetStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(NSString *)Get_Base_url{
    url=@"https://earlofhindh.com/order/";
   // url=@"http://order.earlofhindh.com/";
    return url;

}
-(NSString *)Get_url
{
    NSString *base=[self Get_Base_url];
    url=[NSString stringWithFormat:@"%@api/",base];
    return url;
}
-(NSString *)Get_Website_url{
    
    url=@"https://earlofhindh.com/";
    return url;
    
}

-(void)addFCMTokenToServer{
    NSString *userid=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
    NSString *tok=[[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"];

    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:userid forKey:@"customer_id"];
    [dict setObject:tok forKey:@"fcm_id"];

    NSString *tempUrl;
    tempUrl=[NSString stringWithFormat:@"%@",API_NOTIFICATION];
    WebservicesClass *web=[[WebservicesClass alloc] init];
    //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
    id jsonDict=[web PostTopviewProductWithUrl:tempUrl withParameter:dict];
    NSLog(@"%@",jsonDict);

    
}


@end
