//
//  RESTConnector.h
//  SampleEcommerce
//
//  Created by Gaurav Joshi on 22/07/15.
//  Copyright (c) 2015 GAURAV. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kDataLoadedNotification         @"DataLoaded"
#define kDataFailedNotification         @"DataFailed"

@interface RESTConnector : NSObject
{
    //NSString *url;
    NSURL* URL;
    NSString *methodType;
    NSData *body;
    NSDictionary *headers;
    NSMutableData *downloadedData;
    NSMutableDictionary* responseDict;
}

@property (nonatomic, strong) NSString *methodType;
@property (nonatomic, strong) NSData *body;
@property (nonatomic, strong) NSDictionary *headers;

//-(NSMutableURLRequest *)createRequest:(NSString *)url body:(NSData *)httpBody methodType:(NSString *)metType headers:(NSDictionary *)head;

-(NSMutableDictionary *)createRequest:(NSString *)url body:(NSData *)httpBody methodType:(NSString *)metType headers:(NSDictionary *)head synchronous:(BOOL)sync useCache:(BOOL)cache;

-(NSMutableDictionary*) ExecuteRequest:(NSMutableURLRequest*)request;

-(void)ExecuteAsyncRequest:(NSMutableURLRequest*)request;

@end

