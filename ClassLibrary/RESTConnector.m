//
//  RESTConnector.m
//  SampleEcommerce
//
//  Created by Gaurav Joshi on 22/07/15.
//  Copyright (c) 2015 GAURAV. All rights reserved.
//

#import "RESTConnector.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonDigest.h>

@implementation RESTConnector
@synthesize methodType;
@synthesize body;
@synthesize headers;

//60 sec = 1 min cache duration
double cacheDuration = 60;

//-(NSMutableURLRequest *)createRequest:(NSString *)url body:(NSData *)httpBody methodType:(NSString *)metType headers:(NSDictionary *)head
//{
//    NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    URL = [NSURL URLWithString:webStringURL];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
//    
//    if (httpBody != nil)
//    {
//        [request setHTTPBody:httpBody];
//    }
//    if (metType !=nil)
//    {
//        [request setHTTPMethod:metType];
//    }
//    
//    if ([head count]==0)
//    {
//        head = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded",@"Content-Type", nil];
//    }
//    
//    NSArray *keys = [head allKeys];
//    for (NSString* key in keys)
//    {
//        NSString *headerField = [head objectForKey:key];
//        [request setValue:headerField forHTTPHeaderField:key];
//    }
//    
//    NSLog(@"Request URL:%@", [request description]);
//    return request;
//
//}


-(NSMutableDictionary *)createRequest:(NSString *)url body:(NSData *)httpBody methodType:(NSString *)metType headers:(NSDictionary *)head synchronous:(BOOL)sync useCache:(BOOL)cache
{
    NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    URL = [NSURL URLWithString:webStringURL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    
    if (httpBody != nil)
    {
        [request setHTTPBody:httpBody];
    }
    if (metType !=nil)
    {
        [request setHTTPMethod:metType];
    }
    
    if ([head count]==0)
    {
        head = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded",@"Content-Type", nil];
        
    }
    
    NSArray *keys = [head allKeys];
    for (NSString* key in keys)
    {
        NSString *headerField = [head objectForKey:key];
        NSLog(@"%@",headerField);
        [request setValue:headerField forHTTPHeaderField:key];
    }
    
    NSLog(@"Request URL:%@", [request description]);
    
    responseDict = [[NSMutableDictionary alloc] init];
    
    //Checking for Cache Data
    
    if (cache)
    {
        NSDate *date = [NSDate date];
        NSLog(@"Date: %f", date.timeIntervalSince1970);
        double time = date.timeIntervalSince1970 * 1000;
        NSString *timeStamp = [NSString stringWithFormat: @"%.0f", time];
        
        const char *cStr = [[URL absoluteString] UTF8String];
        unsigned char digest[CC_MD5_DIGEST_LENGTH];
        CC_MD5(cStr, (CC_LONG)strlen(cStr), digest); // This is the md5 call
        
        NSMutableString *md5URL = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
        
        for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        {
            [md5URL appendFormat:@"%02x", digest[i]];
        }
        
        NSString *storedTimestamp = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_timestamp", md5URL]];
        
        double timeValue = [timeStamp doubleValue] - [storedTimestamp doubleValue];
        
        if (cacheDuration > timeValue)
        {
            responseDict = [[NSUserDefaults standardUserDefaults] objectForKey:md5URL];
            return responseDict;
        }
    }
    
    NSDate *date = [NSDate date];
    NSLog(@"Date: %f", date.timeIntervalSince1970);
    double time = date.timeIntervalSince1970 * 1000;
    NSString *timeStamp = [NSString stringWithFormat: @"%.0f", time];
    
    NSLog(@"Time Stamp: %@", timeStamp);
    
    
    if (sync)
    {
        responseDict = [self ExecuteRequest:request];
        
        //Encyrption for Storing response in NSUserDefaults
        const char *cStr = [[URL absoluteString] UTF8String];
        unsigned char digest[CC_MD5_DIGEST_LENGTH];
        CC_MD5(cStr, (CC_LONG)strlen(cStr), digest); // This is the md5 call
        
        NSMutableString *md5URL = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
        
        for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        {
            [md5URL appendFormat:@"%02x", digest[i]];
        }
        
//        NSString *tempStr = @"This is response to enccrypt";
//        NSString *encryptedString = [md5URL AES256EncryptWithKey:md5URL];
//        NSLog( @"Encrypted String: %@", encryptedString );
//        
//        NSLog( @"Decrypted String: %@", [encryptedString AES256DecryptWithKey:key] );
//        
//        Storing in NSUserDefaults
//        [[NSUserDefaults standardUserDefaults] setObject:responseDict forKey:md5URL];
//        [[NSUserDefaults standardUserDefaults] setObject:timeStamp forKey:[NSString stringWithFormat:@"%@_timestamp", md5URL]];
    }
    else
    {
        [self ExecuteAsyncRequest:request];
    }
    
    return responseDict;
}


-(NSMutableDictionary*) ExecuteRequest:(NSMutableURLRequest*)request
{
    NSHTTPURLResponse *resp =nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&resp error:nil];
    
    NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Str Data : %@",strData);

    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    if(data != nil)
    {
         jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    }
    
    
    
    NSLog(@"jsonDict: %@", [jsonDict description]);
    return jsonDict;
}


-(void)ExecuteAsyncRequest:(NSMutableURLRequest*)request
{
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (theConnection)
    {
        downloadedData = [NSMutableData data];
    }
    else
    {
        NSLog(@"connection failed...");
    }
    
}


#pragma mark Connection Delegate methods
// Implement Connection delegate
-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [downloadedData appendData:data];
    
}

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"in didReceiveResponse");
    
}

-(void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"Success in connection: %@",[downloadedData description]);
    NSError* error;
    [[NSNotificationCenter defaultCenter] postNotificationName:kDataLoadedNotification object:downloadedData];
    
    responseDict = [NSJSONSerialization JSONObjectWithData:downloadedData
                                                         options:kNilOptions
                                                           error:&error];
    
    //Encyrption for Storing response in NSUserDefaults
    const char *cStr = [[URL absoluteString] UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest); // This is the md5 call
    
    NSMutableString *md5URL = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        [md5URL appendFormat:@"%02x", digest[i]];
    }
    
    NSDate *date = [NSDate date];
    NSLog(@"Date: %f", date.timeIntervalSince1970);
    double time = date.timeIntervalSince1970 * 1000;
    NSString *timeStamp = [NSString stringWithFormat: @"%.0f", time];
    
    //Storing in NSUserDefaults
    [[NSUserDefaults standardUserDefaults] setObject:downloadedData forKey:md5URL];
    [[NSUserDefaults standardUserDefaults] setObject:timeStamp forKey:[NSString stringWithFormat:@"%@_timestamp", md5URL]];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kDataFailedNotification object:error];
}

@end
