//
//  OfflineDb.h
//  Timex
//
//  Created by Rahul Lekurwale on 23/01/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface OfflineDb : NSObject
@property(nonatomic,strong)NSString *category_id,*parent_id,*category_name,*category_include_in_menu,*category_image ,*is_active;

@property(nonatomic,strong)NSString *location_id,*location_name;
@property(nonatomic,strong)NSString *shipping_id,*shipping_value,*shipping_label;
@property(nonatomic,strong)NSString *attribute_id,*attribute_type,*attribute_value,*attribute_label;
@property(nonatomic,strong)NSString *cart_id,*cart_category_id,*cart_name,*cart_image_url,*cart_image_thumb,*cart_image_small,*cart_image_main,*cart_price,*cart_qty,*cart_description,*cart_short_desc,*cart_food_type,*cart_total,*cart_brand,*cart_weight,*cart_origin,*cart_option_id,*cart_cut_option_id,*cart_cut_option_value,*cart_cut_option_price,*cart_weight_option_id,*cart_weight_option_value,*cart_weight_option_price,*cart_type_option_id,*cart_type_option_value,*cart_type_option_price,*qnty_cnt,*cart_cut_option_label,*cart_weight_option_label,*cart_type_option_label,*total_cnt;
@property(nonatomic,strong)NSString *cm_cart_id,*cm_category_id,*cm_all_cut_values,*cm_all_cut_labels,*cm_all_cut_prices,*cm_all_weight_values,*cm_all_weight_labels,*cm_all_weight_prices,*cm_all_type_values,*cm_all_type_labels,*cm_all_type_prices;
//Old



@property(nonatomic,strong)NSMutableArray *categoryArray;

@property(nonatomic,strong)NSMutableArray *locationDataArray;
@property(nonatomic,strong)NSMutableArray *shipping_Order_Array;
@property(nonatomic,strong)NSMutableArray *attributes_Array;
@property(nonatomic,strong)NSMutableArray *brand_Array;
@property(nonatomic,strong)NSMutableArray *cartMasterArray;


//old
@property(nonatomic,strong)NSMutableArray *cartDataArray;

@property(nonatomic,strong)NSMutableArray *fetched_cart_Quantity;


@property(nonatomic,strong)NSMutableArray *quntity_count;
@property(nonatomic,strong)NSMutableArray *totalAmount;



#pragma mark Categories
-(void)insert_CategoryDetails_Data:(NSArray *)TestDetailArray;

-(NSMutableArray *)fetch_parentWiseCategory:(NSString *)parentId;

-(BOOL)Delete_Categories_Data;

-(NSMutableArray *)fetch_categoryName:(NSString *)catId;


#pragma mark Locations
-(void)insert_LocationsDetails_Data:(NSArray *)LocationDetailArray;

-(NSMutableArray *)fetch_Locations;

-(BOOL)Delete_Locations_Data;

#pragma mark Shipping method
-(BOOL)insert_ShippingOrder:(NSArray *)ShippingOrderArray;

-(NSMutableArray *)fetch_ShippingOrderValue;

-(BOOL)Delete_Shipping_Data;


#pragma mark Attributes
-(BOOL)insert_Attributes:(NSArray *)attributesOrderArray;

-(NSMutableArray *)fetch_AttributeValue:(NSString *)attr_type;

-(BOOL)Delete_Attributes_Data;

-(NSMutableArray *)fetch_BrandValue:(NSString *)type AndValue:(NSString *)value;

#pragma mark Cart Master
-(BOOL)insert_Into_CartMaster:(NSArray *)CartDetailArray;


-(NSMutableArray *)fetch_CartMasterOfCartId:(NSString *)cartid;


//Old
#pragma mark Cart operations
-(void)checkCartEntry:(NSArray *)CartDetailArray;

-(BOOL)insert_Cart_Details:(NSArray *)CartDetailArray;

-(NSMutableArray *)fetch_CartDetails;

-(NSMutableArray *)fetch_Cart_QuantityOfCartIds:(NSString *)cId WithCutLabel:(NSString *)cutlab WithWeightLabel:(NSString *)weightlab WithTypeLabel:(NSString *)typelab;

-(BOOL)Update_Cart_Data :(NSString *)cartId WithQuntity:(NSString *)qty WithTotal:(NSString *)total forCutlabel:(NSString *)cutLab forWeightlabel:(NSString *)weightLab forTypelabel:(NSString *)typeLab;

-(NSMutableArray *)fetch_customized_Data:(NSString *) foodVal;



-(BOOL)Delete_Cart_Data :(NSString *)cartId AndCutLab:(NSString *)cutLab AndWeightlab:(NSString *)weightLab AndTypeLab:(NSString *)typeLab;


-(NSMutableArray *)fetch_Quantity_Count;
-(NSMutableArray *)fetch_Total_Amount;




@end
