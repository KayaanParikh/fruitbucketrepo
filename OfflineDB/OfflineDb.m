//
//  OfflineDb.m
//  Timex
//
//  Created by Rahul Lekurwale on 23/01/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "OfflineDb.h"
#import "AppDelegate.h"
#import <sqlite3.h>


@implementation OfflineDb
@synthesize category_id,parent_id,category_name,category_include_in_menu,category_image,is_active;
@synthesize location_id,location_name,locationDataArray;
@synthesize shipping_id,shipping_value,shipping_label,shipping_Order_Array;
@synthesize attribute_id,attribute_type,attribute_value,attribute_label,attributes_Array;
@synthesize cart_id,cart_category_id,cart_name,cart_image_url,cart_image_thumb,cart_image_small,cart_image_main,cart_price,cart_qty,cart_description,cart_short_desc,cart_food_type,cart_total,cart_brand,cart_weight,cart_origin,cart_option_id,cart_cut_option_id,cart_cut_option_value,cart_cut_option_price,cart_weight_option_id,cart_weight_option_value,cart_weight_option_price,cart_type_option_id,cart_type_option_value,cart_type_option_price,qnty_cnt,total_cnt;
@synthesize cm_cart_id,cm_category_id,cm_all_cut_values,cm_all_cut_labels,cm_all_cut_prices,cm_all_weight_values,cm_all_weight_labels,cm_all_weight_prices,cm_all_type_values,cm_all_type_labels,cm_all_type_prices,cartMasterArray;

@synthesize brand_Array;
//old
@synthesize categoryArray,cartDataArray;
@synthesize fetched_cart_Quantity,quntity_count,totalAmount;


#pragma mark Categories
-(void)insert_CategoryDetails_Data:(NSArray *)CategoryDetailArray
{
    sqlite3_stmt *insert_statement = nil;
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if (sqlite3_open([dbpath UTF8String], &database) == SQLITE_OK) {
        if (insert_statement == nil)
        {
            
            OfflineDb *testDetail_Obj=[[OfflineDb alloc]init];
            testDetail_Obj=[CategoryDetailArray objectAtIndex:0];
            
            static char *staffRecords_insert = "INSERT OR REPLACE INTO category(category_id,parent_id,category_name,category_include_in_menu,category_image,is_active) VALUES(?,?,?,?,?,?)";
            
            if (sqlite3_prepare_v2(database, staffRecords_insert, -1, &insert_statement, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
            
            sqlite3_bind_text(insert_statement, 1, [testDetail_Obj.category_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 2, [testDetail_Obj.parent_id UTF8String], -1, SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 3, [testDetail_Obj.category_name UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 4, [testDetail_Obj.category_include_in_menu UTF8String], -1, SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 5, [testDetail_Obj.category_image UTF8String], -1, SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 6, [testDetail_Obj.is_active UTF8String], -1, SQLITE_TRANSIENT );
            
            
        }
        int success = sqlite3_step(insert_statement);
        
        sqlite3_reset(insert_statement);
        if (success != SQLITE_ERROR)
        {
            //        NSLog(@"error");
        }
    }
    sqlite3_close(database);
}

-(NSMutableArray *)fetch_parentWiseCategory:(NSString *)parentId
{
    categoryArray=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT * FROM category WHERE parent_id = \"%@\"" ,parentId];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    NSMutableString *str_categoryId=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    NSMutableString *str_ParentId=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    NSMutableString *str_categoryName=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 2)];
                    
                    NSMutableString *str_categoryInclude_In=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 3)];
                    
                    NSMutableString *str_categoryImage=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 4)];
                    
                    NSMutableString *str_isActive=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 5)];
                    
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.category_id=str_categoryId;
                    TestDB_Obj.parent_id=str_ParentId;
                    TestDB_Obj.category_name=str_categoryName;
                    TestDB_Obj.category_include_in_menu=str_categoryInclude_In;
                    TestDB_Obj.category_image=str_categoryImage;
                    TestDB_Obj.is_active=str_isActive;
                    
                    
                    [categoryArray addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return categoryArray;
}



//////Delete All Categories
-(BOOL)Delete_Categories_Data
{
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if(sqlite3_open([dbpath UTF8String], &database)!=SQLITE_OK)
        return NO;
    NSString *q;
        q=[NSString stringWithFormat:@"DELETE FROM category"];
        
    
    if(sqlite3_exec(database, [q UTF8String], NULL, NULL, NULL)==SQLITE_OK)
    {
        NSLog(@"Deleted..");
    }
    else
    {
        NSLog(@"fail to delete");
        return NO;
    }
    
    
    sqlite3_close(database);
    return YES;
}



-(NSMutableArray *)fetch_categoryName:(NSString *)catId
{
    categoryArray=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT category_name FROM category WHERE category_id = \"%@\"" ,catId];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    
                    NSMutableString *str_categoryName=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.category_name=str_categoryName;
                    [categoryArray addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return categoryArray;
}


#pragma mark Locations
-(void)insert_LocationsDetails_Data:(NSArray *)LocationDetailArray
{
    sqlite3_stmt *insert_statement = nil;
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if (sqlite3_open([dbpath UTF8String], &database) == SQLITE_OK) {
        if (insert_statement == nil)
        {
            
            OfflineDb *testDetail_Obj=[[OfflineDb alloc]init];
            testDetail_Obj=[LocationDetailArray objectAtIndex:0];
            
            static char *staffRecords_insert = "INSERT OR REPLACE INTO LocationTable(location_id,location_name) VALUES(?,?)";
            
            if (sqlite3_prepare_v2(database, staffRecords_insert, -1, &insert_statement, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
            
//            sqlite3_bind_text(insert_statement, 1, [testDetail_Obj.location_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 2, [testDetail_Obj.location_name UTF8String], -1, SQLITE_TRANSIENT );
            
            
        }
        int success = sqlite3_step(insert_statement);
        
        sqlite3_reset(insert_statement);
        if (success != SQLITE_ERROR)
        {
            //        NSLog(@"error");
        }
    }
    sqlite3_close(database);
}

-(NSMutableArray *)fetch_Locations
{
    locationDataArray=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT * FROM LocationTable"];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    NSMutableString *str_locId=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    NSMutableString *str_locName=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.location_id=str_locId;
                    TestDB_Obj.location_name=str_locName;
                    
                    
                    [locationDataArray addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return locationDataArray;
}



//////Delete All Categories
-(BOOL)Delete_Locations_Data
{
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if(sqlite3_open([dbpath UTF8String], &database)!=SQLITE_OK)
        return NO;
    NSString *q;
    q=[NSString stringWithFormat:@"DELETE FROM LocationTable"];
    
    
    if(sqlite3_exec(database, [q UTF8String], NULL, NULL, NULL)==SQLITE_OK)
    {
        NSLog(@"Deleted..");
    }
    else
    {
        NSLog(@"fail to delete");
        return NO;
    }
    
    
    sqlite3_close(database);
    return YES;
}


#pragma mark ShippingMethod
-(BOOL)insert_ShippingOrder:(NSArray *)ShippingOrderArray
{
    sqlite3_stmt *insert_statement = nil;
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if (sqlite3_open([dbpath UTF8String], &database) == SQLITE_OK) {
        if (insert_statement == nil)
        {
            
            OfflineDb *testDetail_Obj=[[OfflineDb alloc]init];
            testDetail_Obj=[ShippingOrderArray objectAtIndex:0];
            /*(cart_id INT PRIMARY KEY,category_id text,sku text,name text,image_url text,price text,qty text,type text,total text
             label text,value text
             */
            static char *staffRecords_insert = "INSERT OR REPLACE INTO ShippingMethods(shipping_id,value,label) VALUES(?,?,?)";
            
            if (sqlite3_prepare_v2(database, staffRecords_insert, -1, &insert_statement, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
            
            sqlite3_bind_text(insert_statement, 2, [testDetail_Obj.shipping_value UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 3, [testDetail_Obj.shipping_label UTF8String], -1, SQLITE_TRANSIENT );
            
        }
        int success = sqlite3_step(insert_statement);
        
        sqlite3_reset(insert_statement);
        if (success != SQLITE_ERROR)
        {
            //        NSLog(@"error");
        }
        else{
            return NO;
            
        }
    }
    sqlite3_close(database);
    return YES;
    
}

-(NSMutableArray *)fetch_ShippingOrderValue
{
    shipping_Order_Array=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT * FROM ShippingMethods"];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    NSMutableString *str_shipId=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    NSMutableString *str_shipValue=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    NSMutableString *str_shipLabel=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 2)];

                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.shipping_id=str_shipId;
                    TestDB_Obj.shipping_label=str_shipLabel;
                    TestDB_Obj.shipping_value=str_shipValue;
                    
                    
                    [shipping_Order_Array addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return shipping_Order_Array;
}

//////Delete All Categories
-(BOOL)Delete_Shipping_Data
{
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if(sqlite3_open([dbpath UTF8String], &database)!=SQLITE_OK)
        return NO;
    NSString *q;
    q=[NSString stringWithFormat:@"DELETE FROM ShippingMethods"];
    
    
    if(sqlite3_exec(database, [q UTF8String], NULL, NULL, NULL)==SQLITE_OK)
    {
        NSLog(@"Deleted..");
    }
    else
    {
        NSLog(@"fail to delete");
        return NO;
    }
    
    
    sqlite3_close(database);
    return YES;
}

#pragma mark Attributes
-(BOOL)insert_Attributes:(NSArray *)attributesOrderArray
{
    sqlite3_stmt *insert_statement = nil;
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if (sqlite3_open([dbpath UTF8String], &database) == SQLITE_OK) {
        if (insert_statement == nil)
        {
            
            OfflineDb *testDetail_Obj=[[OfflineDb alloc]init];
            testDetail_Obj=[attributesOrderArray objectAtIndex:0];
            /*(cart_id INT PRIMARY KEY,category_id text,sku text,name text,image_url text,price text,qty text,type text,total text
             label text,value text
             */
            static char *staffRecords_insert = "INSERT OR REPLACE INTO Attributes(attribute_id,type,value,label) VALUES(?,?,?,?)";
            
            if (sqlite3_prepare_v2(database, staffRecords_insert, -1, &insert_statement, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
            
            sqlite3_bind_text(insert_statement, 2, [testDetail_Obj.attribute_type UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 3, [testDetail_Obj.attribute_value UTF8String], -1, SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 4, [testDetail_Obj.attribute_label UTF8String], -1, SQLITE_TRANSIENT );

            
        }
        int success = sqlite3_step(insert_statement);
        
        sqlite3_reset(insert_statement);
        if (success != SQLITE_ERROR)
        {
            //        NSLog(@"error");
        }
        else{
            return NO;
            
        }
    }
    sqlite3_close(database);
    return YES;
    
}

-(NSMutableArray *)fetch_AttributeValue:(NSString *)attr_type
{
    attributes_Array=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT * FROM Attributes WHERE type = \"%@\"" ,attr_type];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    NSMutableString *str_attrId=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    NSMutableString *str_attrType=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    NSMutableString *str_attrValue=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 2)];
                    
                    NSMutableString *str_attrLabel=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 3)];

                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.attribute_id=str_attrId;
                    TestDB_Obj.attribute_type=str_attrType;
                    TestDB_Obj.attribute_value=str_attrValue;
                    TestDB_Obj.attribute_label=str_attrLabel;

                    
                    [attributes_Array addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return attributes_Array;
}


-(NSMutableArray *)fetch_BrandValue:(NSString *)type AndValue:(NSString *)value
{
    attributes_Array=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
           // attribute_id INTEGER PRIMARY KEY   AUTOINCREMENT,type text,value text,label text
            NSString *sql = [NSString stringWithFormat:@"SELECT * FROM Attributes WHERE type = \"%@\" AND value = \"%@\"" ,type,value];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    NSMutableString *str_attrId=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    NSMutableString *str_attrType=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    NSMutableString *str_attrValue=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 2)];
                    
                    NSMutableString *str_attrLabel=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 3)];
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.attribute_id=str_attrId;
                    TestDB_Obj.attribute_type=str_attrType;
                    TestDB_Obj.attribute_value=str_attrValue;
                    TestDB_Obj.attribute_label=str_attrLabel;
                    
                    
                    [attributes_Array addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return attributes_Array;
}

//////Delete All Categories
-(BOOL)Delete_Attributes_Data
{
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if(sqlite3_open([dbpath UTF8String], &database)!=SQLITE_OK)
        return NO;
    NSString *q;
    q=[NSString stringWithFormat:@"DELETE FROM Attributes"];
    
    
    if(sqlite3_exec(database, [q UTF8String], NULL, NULL, NULL)==SQLITE_OK)
    {
        NSLog(@"Deleted..");
    }
    else
    {
        NSLog(@"fail to delete");
        return NO;
    }
    
    
    sqlite3_close(database);
    return YES;
}

#pragma mark CartOperations
-(void)checkCartEntry:(NSArray *)CartDetailArray
{
    
    OfflineDb *testDetail_Obj=[[OfflineDb alloc]init];
    testDetail_Obj=[CartDetailArray objectAtIndex:0];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM cartDetails WHERE cart_id=\"%@\" AND cut_option_label=\"%@\" AND weight_option_label=\"%@\" AND type_option_label=\"%@\"",testDetail_Obj.cart_id,testDetail_Obj.cart_cut_option_label,testDetail_Obj.cart_weight_option_label,testDetail_Obj.cart_type_option_label];
    
    NSLog(@"query : %@",query);
    BOOL recordExist = [self recordExistOrNot:query:CartDetailArray];
    
    if (!recordExist) {
        // Insert your data
        [self insert_Cart_Details:CartDetailArray];
        
    }
    else{
        NSArray *getDetailsArr=[self fetch_Cart_QuantityOfCartIds:testDetail_Obj.cart_id WithCutLabel:testDetail_Obj.cart_cut_option_label WithWeightLabel:testDetail_Obj.cart_weight_option_label WithTypeLabel:testDetail_Obj.cart_type_option_label];
        
        NSArray *cartQty=[getDetailsArr valueForKey:@"cart_qty"];
        NSArray *carttot=[getDetailsArr valueForKey:@"cart_total"];
        
        int qty=[[cartQty objectAtIndex:0] intValue]+[testDetail_Obj.cart_qty intValue];
        float tot=[[carttot objectAtIndex:0] floatValue] + [testDetail_Obj.cart_total floatValue];
        NSString *strQty=[NSString stringWithFormat:@"%d",qty];
        NSString *strtot=[NSString stringWithFormat:@"%.2f",tot];
        
        //        [self Update_History_Data:testDetail_Obj.history_id WithQuntity:strQty WithTotal:strtot OfTable:testDetail_Obj.history_table];
        
        [self Update_Cart_Data:testDetail_Obj.cart_id WithQuntity:strQty WithTotal:strtot forCutlabel:testDetail_Obj.cart_cut_option_label forWeightlabel:testDetail_Obj.cart_weight_option_label forTypelabel:testDetail_Obj.cart_type_option_label];
        
    }
}

-(BOOL)recordExistOrNot:(NSString *)query :(NSArray *)HistoryDetailArray{
    BOOL recordExist=NO;
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            sqlite3_stmt *statement;
            if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil)==SQLITE_OK)
            {
                if(sqlite3_step(statement)==SQLITE_ROW)
                {
                    
                    recordExist=YES;
                    
                }
                else if (!(sqlite3_step(statement)==SQLITE_ROW))
                {
                    recordExist=NO;
                    
                }
                
                sqlite3_finalize(statement);
                sqlite3_close(database);
            }
        }
    }
    return recordExist;
}




-(BOOL)insert_Cart_Details:(NSArray *)CartDetailArray
{
    sqlite3_stmt *insert_statement = nil;
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if (sqlite3_open([dbpath UTF8String], &database) == SQLITE_OK) {
        if (insert_statement == nil)
        {
            
            OfflineDb *testDetail_Obj=[[OfflineDb alloc]init];
            testDetail_Obj=[CartDetailArray objectAtIndex:0];
            static char *staffRecords_insert = "INSERT OR REPLACE INTO cartDetails(cid,cart_id,category_id,name,image_url,image_thumb,image_small,image_main,price,qty,description,short_desc,food_type,total,brand,weight,origin,option_id,cut_option_id,cut_option_value,cut_option_price,weight_option_id,weight_option_value,weight_option_price,type_option_id,type_option_value,type_option_price,cut_option_label,weight_option_label,type_option_label,all_cut_values,all_cut_labels,all_cut_prices,all_weight_values,all_weight_labels,all_weight_prices,all_type_values,all_type_labels,all_type_prices) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            if (sqlite3_prepare_v2(database, staffRecords_insert, -1, &insert_statement, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
            sqlite3_bind_text(insert_statement, 2, [testDetail_Obj.cart_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 3, [testDetail_Obj.cart_category_id UTF8String], -1, SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 4, [testDetail_Obj.cart_name UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 5, [testDetail_Obj.cart_image_url UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 6, [testDetail_Obj.cart_image_thumb UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 7, [testDetail_Obj.cart_image_small UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 8, [testDetail_Obj.cart_image_main UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 9, [testDetail_Obj.cart_price UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 10, [testDetail_Obj.cart_qty UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 11, [testDetail_Obj.cart_description UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 12, [testDetail_Obj.cart_short_desc UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 13, [testDetail_Obj.cart_food_type UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 14, [testDetail_Obj.cart_total UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 15, [testDetail_Obj.cart_brand UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 16, [testDetail_Obj.cart_weight UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 17, [testDetail_Obj.cart_origin UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 18, [testDetail_Obj.cart_option_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 19, [testDetail_Obj.cart_cut_option_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 20, [testDetail_Obj.cart_cut_option_value UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 21, [testDetail_Obj.cart_cut_option_price UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 22, [testDetail_Obj.cart_weight_option_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 23, [testDetail_Obj.cart_weight_option_value UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 24, [testDetail_Obj.cart_weight_option_price UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 25, [testDetail_Obj.cart_type_option_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 26, [testDetail_Obj.cart_type_option_value UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 27, [testDetail_Obj.cart_type_option_price UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 28, [testDetail_Obj.cart_cut_option_label UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 29, [testDetail_Obj.cart_weight_option_label UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 30, [testDetail_Obj.cart_type_option_label UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 31, [testDetail_Obj.cm_all_cut_values UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 32, [testDetail_Obj.cm_all_cut_labels UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 33, [testDetail_Obj.cm_all_cut_prices UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 34, [testDetail_Obj.cm_all_weight_values UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 35, [testDetail_Obj.cm_all_weight_labels UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 36, [testDetail_Obj.cm_all_weight_prices UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 37, [testDetail_Obj.cm_all_type_values UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 38, [testDetail_Obj.cm_all_type_labels UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 39, [testDetail_Obj.cm_all_type_prices UTF8String], -1,  SQLITE_TRANSIENT );

            
        }
        int success = sqlite3_step(insert_statement);
        
        sqlite3_reset(insert_statement);
        if (success != SQLITE_ERROR)
        {
            
//            [self insert_Into_CartMaster:CartDetailArray];
            //        NSLog(@"error");
        }
        else{
            return NO;
            
        }
    }
    sqlite3_close(database);
    return YES;
    
}

////Fetch Cart Details

-(NSMutableArray *)fetch_CartDetails
{
    cartDataArray=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT * FROM cartDetails"];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    // ,,,,,,,,,,,,,,,
                    
                    NSMutableString *str_cart_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    NSMutableString *str_cart_category_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 2)];
                    
                    NSMutableString *str_cart_name=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 3)];
                    
                    NSMutableString *str_cart_image_url=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 4)];
                    
                    NSMutableString *str_cart_image_thumb=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 5)];
                    
                    NSMutableString *str_cart_image_small=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 6)];
                    
                    NSMutableString *str_cart_image_main=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 7)];
                    
                    NSMutableString *str_cart_price=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 8)];
                    
                    NSMutableString *str_cart_qty=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 9)];
                    
                    NSMutableString *str_cart_description=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 10)];
                    
                    NSMutableString *str_cart_short_desc=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 11)];
                    
                    NSMutableString *str_cart_food_type=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 12)];
                    
                    NSMutableString *str_cart_total=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 13)];
                    
                    NSMutableString *str_cart_brand=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 14)];
            
                    NSMutableString *str_cart_cart_weight=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 15)];

                    NSMutableString *str_cart_origin=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 16)];
                    
                    NSMutableString *str_cart_option_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 17)];

                    NSMutableString *str_cart_cut_option_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 18)];

                    NSMutableString *str_cart_cut_option_value=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 19)];

                    NSMutableString *str_cart_cut_option_price=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 20)];

                    NSMutableString *str_cart_weight_option_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 21)];

                    NSMutableString *str_cart_weight_option_value=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 22)];

                    NSMutableString *str_cart_weight_option_price=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 23)];

                    NSMutableString *str_cart_type_option_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 24)];

                    NSMutableString *str_cart_type_option_value=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 25)];
                   
                    NSMutableString *str_cart_type_option_price=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 26)];

                    NSMutableString *str_cart_cut_option_label=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 27)];

                    NSMutableString *str_cart_weight_option_label=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 28)];

                    NSMutableString *str_cart_type_option_label=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 29)];

                    NSMutableString *str_cm_all_cut_values=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 30)];
                    
                    NSMutableString *str_cm_all_cut_labels=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 31)];
                    
                    NSMutableString *str_cm_all_cut_prices=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 32)];
                    
                    NSMutableString *str_cm_all_weight_values=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 33)];
                    
                    NSMutableString *str_cm_all_weight_labels=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 34)];
                    
                    NSMutableString *str_cm_all_weight_prices=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 35)];
                    
                    NSMutableString *str_cm_all_type_values=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 36)];
                    
                    NSMutableString *str_cm_all_type_labels=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 37)];
                    
                    NSMutableString *str_cm_all_type_prices=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 38)];

                    
                    
                    //                     ,,,,,,,,,

                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.cart_id=str_cart_id;
                    TestDB_Obj.cart_category_id=str_cart_category_id;
                    TestDB_Obj.cart_name=str_cart_name;
                    TestDB_Obj.cart_image_url=str_cart_image_url;
                    TestDB_Obj.cart_image_thumb=str_cart_image_thumb;
                    TestDB_Obj.cart_image_small=str_cart_image_small;
                    TestDB_Obj.cart_image_main=str_cart_image_main;
                    TestDB_Obj.cart_price=str_cart_price;
                    TestDB_Obj.cart_qty=str_cart_qty;
                    
                    TestDB_Obj.cart_description=str_cart_description;
                    TestDB_Obj.cart_short_desc=str_cart_short_desc;
                    TestDB_Obj.cart_food_type=str_cart_food_type;
                    TestDB_Obj.cart_total=str_cart_total;
                    TestDB_Obj.cart_brand=str_cart_brand;
                    TestDB_Obj.cart_weight=str_cart_cart_weight;
                    TestDB_Obj.cart_origin=str_cart_origin;

                    TestDB_Obj.cart_option_id=str_cart_option_id;
                    TestDB_Obj.cart_cut_option_id=str_cart_cut_option_id;
                    TestDB_Obj.cart_cut_option_value=str_cart_cut_option_value;
                    TestDB_Obj.cart_cut_option_price=str_cart_cut_option_price;
                    TestDB_Obj.cart_weight_option_id=str_cart_weight_option_id;
                    TestDB_Obj.cart_weight_option_value=str_cart_weight_option_value;
                    TestDB_Obj.cart_weight_option_price=str_cart_weight_option_price;
                    TestDB_Obj.cart_type_option_id=str_cart_type_option_id;
                    TestDB_Obj.cart_type_option_value=str_cart_type_option_value;
                    TestDB_Obj.cart_type_option_price=str_cart_type_option_price;
                    TestDB_Obj.cart_cut_option_label=str_cart_cut_option_label;
                    TestDB_Obj.cart_weight_option_label=str_cart_weight_option_label;
                    TestDB_Obj.cart_type_option_label=str_cart_type_option_label;
                    TestDB_Obj.cm_all_cut_values=str_cm_all_cut_values;
                    TestDB_Obj.cm_all_cut_labels=str_cm_all_cut_labels;
                    TestDB_Obj.cm_all_cut_prices=str_cm_all_cut_prices;
                    TestDB_Obj.cm_all_weight_values=str_cm_all_weight_values;
                    TestDB_Obj.cm_all_weight_labels=str_cm_all_weight_labels;
                    TestDB_Obj.cm_all_weight_prices=str_cm_all_weight_prices;
                    TestDB_Obj.cm_all_type_values=str_cm_all_type_values;
                    
                    TestDB_Obj.cm_all_type_labels=str_cm_all_type_labels;
                    TestDB_Obj.cm_all_type_prices=str_cm_all_type_prices;

                    [cartDataArray addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return cartDataArray;
}

//old
////////////Insert Cart Details

////Fetch CategoryWise cart data

-(NSMutableArray *)fetch_Cart_QuantityOfCartIds:(NSString *)cId WithCutLabel:(NSString *)cutlab WithWeightLabel:(NSString *)weightlab WithTypeLabel:(NSString *)typelab
{
    fetched_cart_Quantity=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT qty,total FROM cartDetails WHERE cart_id=\"%@\" AND cut_option_label=\"%@\" AND weight_option_label=\"%@\" AND type_option_label=\"%@\"",cId,cutlab,weightlab,typelab];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    // cart_id,cart_categoryId,cart_sku,cart_name,cart_imageUrl,cart_price,cart_qty,cart_type,cart_total
                    
                    
                    NSMutableString *str_cart_qty=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    NSMutableString *str_cart_total=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];


                    
                    
                    // cart_id,cart_categoryId,cart_sku,cart_name,cart_imageUrl,cart_price,cart_qty,cart_type,cart_total
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.cart_qty=str_cart_qty;
                    TestDB_Obj.cart_total=str_cart_total;

                    [fetched_cart_Quantity addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return fetched_cart_Quantity;
}


//////update cart
-(BOOL)Update_Cart_Data :(NSString *)cartId WithQuntity:(NSString *)qty WithTotal:(NSString *)total forCutlabel:(NSString *)cutLab forWeightlabel:(NSString *)weightLab forTypelabel:(NSString *)typeLab
{
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if(sqlite3_open([dbpath UTF8String], &database)!=SQLITE_OK)
        return NO;

    NSString *q=[NSString stringWithFormat:@"UPDATE cartDetails SET qty=\"%@\",total=\"%@\" WHERE cart_id=\"%@\" AND cut_option_label=\"%@\" AND weight_option_label=\"%@\" AND type_option_label=\"%@\"",qty,total,cartId,cutLab,weightLab,typeLab];
    if(sqlite3_exec(database, [q UTF8String], NULL, NULL, NULL)==SQLITE_OK)
    {
        NSLog(@"Updated..");
        
    }
    else
    {
        NSLog(@"fail to delete");
        return NO;
    }
    
    sqlite3_close(database);
    return YES;
}


//////Delete cart
-(BOOL)Delete_Cart_Data :(NSString *)cartId AndCutLab:(NSString *)cutLab AndWeightlab:(NSString *)weightLab AndTypeLab:(NSString *)typeLab
{
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if(sqlite3_open([dbpath UTF8String], &database)!=SQLITE_OK)
        return NO;
    NSString *q;
    if ([cartId isEqualToString:@"ALL"]) {
        q=[NSString stringWithFormat:@"DELETE FROM cartDetails"];
        
    }
    else{
        q=[NSString stringWithFormat:@"DELETE FROM cartDetails WHERE cart_id=\"%@\" AND cut_option_label=\"%@\" AND weight_option_label=\"%@\" AND type_option_label=\"%@\"",cartId,cutLab,weightLab,typeLab];
        
    }
    if(sqlite3_exec(database, [q UTF8String], NULL, NULL, NULL)==SQLITE_OK)
    {
        NSLog(@"Deleted..");
    }
    else
    {
        NSLog(@"fail to delete");
        return NO;
    }
    
    
    sqlite3_close(database);
    return YES;
}

////Insert Shipping Order
//SELECT sum(qty) FROM cartDetails;
////Fetch CategoryWise cart data
-(NSMutableArray *)fetch_Quantity_Count
{
    quntity_count=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT sum(qty) FROM cartDetails"];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    // cart_id,cart_categoryId,cart_sku,cart_name,cart_imageUrl,cart_price,cart_qty,cart_type,cart_total
                    
                    
                    NSMutableString *str_cnt=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    
                    
                    // cart_id,cart_categoryId,cart_sku,cart_name,cart_imageUrl,cart_price,cart_qty,cart_type,cart_total
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.qnty_cnt=str_cnt;
                    
                    [quntity_count addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return quntity_count;
}


-(NSMutableArray *)fetch_customized_Data:(NSString *) foodVal
{
    cartDataArray=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT cart_id,name,food_type FROM cartDetails WHERE food_type=\"%@\"",foodVal];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    // ,,,,,,,,,,,,,,,
                    
                    NSMutableString *str_cart_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    
                    NSMutableString *str_cart_name=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    NSMutableString *str_cart_food=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 2)];

                    
                    //                     ,,,,,,,,,
                    
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.cart_id=str_cart_id;
                    TestDB_Obj.cart_name=str_cart_name;
                    TestDB_Obj.cart_food_type=str_cart_food;
                    
                    [cartDataArray addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return cartDataArray;
}


-(NSMutableArray *)fetch_Total_Amount
{
    totalAmount=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT sum(total) FROM cartDetails"];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    
                    NSMutableString *str_cnt=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 0)];
                    
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.total_cnt=str_cnt;
                    
                    [totalAmount addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return totalAmount;
}


/*
#pragma mark CartMaster Operations
-(BOOL)insert_Into_CartMaster:(NSArray *)CartDetailArray
{
    sqlite3_stmt *insert_statement = nil;
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    if([f fileExistsAtPath:dbpath])
    {
        NSLog(@"database already exist");
    }
    
    sqlite3 *database;
    
    if (sqlite3_open([dbpath UTF8String], &database) == SQLITE_OK) {
        if (insert_statement == nil)
        {
            
            OfflineDb *testDetail_Obj=[[OfflineDb alloc]init];
            testDetail_Obj=[CartDetailArray objectAtIndex:0];
            static char *staffRecords_insert = "INSERT OR REPLACE INTO cartMaster(cmid,cart_id,category_id,all_cut_values,all_cut_labels,all_cut_prices,all_weight_values,all_weight_labels,all_weight_prices,all_type_values,all_type_labels,all_type_prices) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
            
            if (sqlite3_prepare_v2(database, staffRecords_insert, -1, &insert_statement, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
          //  ,,,,,,,,,,
            sqlite3_bind_text(insert_statement, 2, [testDetail_Obj.cm_cart_id UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 3, [testDetail_Obj.cm_category_id UTF8String], -1, SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 4, [testDetail_Obj.cm_all_cut_values UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 5, [testDetail_Obj.cm_all_cut_labels UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 6, [testDetail_Obj.cm_all_cut_prices UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 7, [testDetail_Obj.cm_all_weight_values UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 8, [testDetail_Obj.cm_all_weight_labels UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 9, [testDetail_Obj.cm_all_weight_prices UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 10, [testDetail_Obj.cm_all_type_values UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 11, [testDetail_Obj.cm_all_type_labels UTF8String], -1,  SQLITE_TRANSIENT );
            sqlite3_bind_text(insert_statement, 12, [testDetail_Obj.cm_all_type_prices UTF8String], -1,  SQLITE_TRANSIENT );
           
            
            
        }
        int success = sqlite3_step(insert_statement);
        
        sqlite3_reset(insert_statement);
        if (success != SQLITE_ERROR)
        {
            //        NSLog(@"error");
        }
        else{
            return NO;
            
        }
    }
    sqlite3_close(database);
    return YES;
    
}
-(NSMutableArray *)fetch_CartMasterOfCartId:(NSString *)cartid
{
    cartMasterArray=[[NSMutableArray alloc]init];
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dbpath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",dbpath);
    
    NSFileManager *f=[[NSFileManager alloc]init];
    
    if([f fileExistsAtPath:dbpath])
    {
        //NSLog(@"database already exist");
        
        
        sqlite3 *database;
        
        
        if(sqlite3_open([dbpath UTF8String], &database)==SQLITE_OK)
        {
            
            NSString *sql = [NSString stringWithFormat:@"SELECT * FROM cartMaster WHERE cart_id=\"%@\"",cartid];
            
            const char *query_stmt = [sql UTF8String];
            
            sqlite3_stmt *statement;
            
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil)==SQLITE_OK)
            {
                while(sqlite3_step(statement)==SQLITE_ROW)
                {
                    // ,,,,,,,,,,
                    
                    NSMutableString *str_cm_cart_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 1)];
                    
                    NSMutableString *str_cm_category_id=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 2)];
                    
                    NSMutableString *str_cm_all_cut_values=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 3)];
                    
                    NSMutableString *str_cm_all_cut_labels=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 4)];
                    
                    NSMutableString *str_cm_all_cut_prices=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 5)];
                    
                    NSMutableString *str_cm_all_weight_values=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 6)];
                    
                    NSMutableString *str_cm_all_weight_labels=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 7)];
                    
                    NSMutableString *str_cm_all_weight_prices=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 8)];
                    
                    NSMutableString *str_cm_all_type_values=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 9)];
                    
                    NSMutableString *str_cm_all_type_labels=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 10)];
                    
                    NSMutableString *str_cm_all_type_prices=[NSMutableString stringWithFormat:@"%s",sqlite3_column_text(statement, 11)];
                    
                    
                    
                    OfflineDb *TestDB_Obj=[[OfflineDb alloc]init];
                    TestDB_Obj.cm_cart_id=str_cm_cart_id;
                    TestDB_Obj.cm_category_id=str_cm_category_id;
                    TestDB_Obj.cm_all_cut_values=str_cm_all_cut_values;
                    TestDB_Obj.cm_all_cut_labels=str_cm_all_cut_labels;
                    TestDB_Obj.cm_all_cut_prices=str_cm_all_cut_prices;
                    TestDB_Obj.cm_all_weight_values=str_cm_all_weight_values;
                    TestDB_Obj.cm_all_weight_labels=str_cm_all_weight_labels;
                    TestDB_Obj.cm_all_weight_prices=str_cm_all_weight_prices;
                    TestDB_Obj.cm_all_type_values=str_cm_all_type_values;
                    
                    TestDB_Obj.cm_all_type_labels=str_cm_all_type_labels;
                    TestDB_Obj.cm_all_type_prices=str_cm_all_type_prices;
                   
                    [cartMasterArray addObject:TestDB_Obj];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return cartMasterArray;
}

*/

@end
