//
//  LoginViewController.m
//  TSMC
//
//  Created by user on 01/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "LoginViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "SWRevealViewController.h"
#import "ViewController.h"
#import "OrderInformationViewController.h"
#import "ProfileUpdateViewController.h"
#import "SignUpViewController.h"
#import "CartController.h"
#import "ShippingAddressController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>



#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define iPadPro12 (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height == 2732)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define TEXT_FIELD_TAG 9999

#define ACTION_SHEET_TAG 8888


@interface LoginViewController ()<FBSDKLoginButtonDelegate>
{
    FBSDKLoginManager *loginManager;
    NSString *fb_email,*fb_pass,*fb_fname,*fb_lname;
    


}
@end

@implementation LoginViewController
CGRect backToOriginal;
UIView *popup;

@synthesize goBackTo,comingFrom;

- (void)viewDidLoad
{
    [super viewDidLoad];
       // Do any additional setup after loading the view.
    loginManager = [[FBSDKLoginManager alloc] init];
    
    UNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    PasswordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    forgotPasswordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];


//    [self getBackgroundImage];
    [self InitAttributeFrame];
   
    btn_fbLogin.delegate=self;
    btn_fbLogin.readPermissions =
    @[@"public_profile", @"email", @"user_friends"];
    
    /*Add shadow to a backview*/
    [loginBackView.layer setShadowOpacity:0.0];
    [loginBackView.layer setShadowRadius:1.0];
    [loginBackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];

    
    
    
    /*To check user logged in or not*/
//    if ([FBSDKAccessToken currentAccessToken]) {
//        // User is logged in, do work such as go to next view controller.
//    }
//
//
//    btn_fbLogin.readPermissions =
//    @[@"public_profile", @"email", @"user_friends"];
   
}




-(void)getBackgroundImage{
    if (IS_IPHONE_5) {
        img_background.image=[UIImage imageNamed:@"splash_1136.png"];
    }
    else if(IS_IPHONE_6){
        img_background.image=[UIImage imageNamed:@"splash_750.png"];
        
    }
    else if(IS_IPHONE_6P){
        img_background.image=[UIImage imageNamed:@"splash_1080png.png"];
        
    }
    
    
}




#pragma mark facebook Integration


-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    
    self.tabBarController.tabBar.hidden=YES;
    if (error == nil)
    {
        NSLog(@"LoggedIn");
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"fb user info : %@",result);
                 
                 
//                 userId = [result valueForKey:@"id"];                  // For client-side use only!
//                 idToken = [result valueForKey:@"id"]; // Safe to send to the server
//                 fb_fname =[result valueForKey:@"name"];
                 fb_fname = [result valueForKey:@"first_name"];
                 fb_lname = [result valueForKey:@"last_name"];
                 fb_email = [result valueForKey:@"email"];
//
//                 [[NSUserDefaults standardUserDefaults]setObject:userId forKey:@"fbid"];
//                 [[NSUserDefaults standardUserDefaults]setObject:email forKey:@"fbemail"];
//                 [[NSUserDefaults standardUserDefaults]setObject:fullName forKey:@"fbfullName"];
//                 
//                 chkSignIn=@"FB";
                 HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                 HUD.dimBackground = YES;
                 HUD.labelText = @"Please wait...";
                 [HUD showWhileExecuting:@selector(SignupAPI) onTarget:self withObject:Nil animated:YES];
                 
                 
             }
             else
             {
                 NSLog(@"error : %@",error);
             }
         }];
    }
    else{
        NSLog(@"Error");
    }
    
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    NSLog(@"Logged Out");
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)InitAttributeFrame
{
    subView_ForgotPassword.hidden=YES;
    
    [scrollView setContentSize:CGSizeMake(375, 603)];
    
     self.navigationController.navigationBarHidden=NO;
//    _titleBack.backgroundColor=TopBarColor;
    self.title = @"Sign In".uppercaseString;
//    _titleText.font=TopBarFont;
//    _titleText.textColor=TopBarTextColor;
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"New user? Click to sign up?"];
    
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    [Btn_SignUp setAttributedTitle:titleString forState: UIControlStateNormal];
    
    NSMutableAttributedString *titleString2 = [[NSMutableAttributedString alloc] initWithString:@"Forgot Password?"];
    
    [titleString2 addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString2 length])];
    
    [Btn_Forgot setAttributedTitle:titleString2 forState: UIControlStateNormal];
//    if ([comingFrom isEqualToString:@"CART"]) {
//        [btn_Back setBackgroundImage:[UIImage imageNamed:@"arrow_left_white.png"] forState:UIControlStateNormal];
//    }
//    else
//    {
//        [btn_Back setBackgroundImage:[UIImage imageNamed:@"arrow_left_white.png"] forState:UIControlStateNormal];
//        
//    }


}


#pragma mark - Button Clicked methods

- (IBAction)Back_Btn_Clicked:(id)sender
{
   // [self.navigationController popViewControllerAnimated:YES];
//    [self loadStoryBoardWithIdentifier:@"ViewController"];
    
    
    [UNameTF resignFirstResponder];
    [PasswordTF resignFirstResponder];

    if ([comingFrom isEqualToString:@"PROFILE"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if([comingFrom isEqualToString:@"CART"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        [self loadStoryBoardWithIdentifier:@"ViewController"];
//        [self.revealViewController revealToggle:nil];
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
    }

    
}

- (IBAction)Cancel_Btn_Clicked:(id)sender
{
    [PasswordTF resignFirstResponder];
}

- (IBAction)SignIn_Btn_Clicked:(id)sender
{
    if(UNameTF.text.length>0)
    {
       if(PasswordTF.text.length>0)
       {
           [PasswordTF resignFirstResponder];
           [self LoginAPI];
       }
       else
       {
           [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Password"];
          //pswrd empty
       }
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Username"];
       //name empty
    }
}

- (IBAction)SignUpBtnClicked:(id)sender
{
//    [self loadStoryBoardWithIdentifier:@"SignUpVC"];
    [PasswordTF resignFirstResponder];
    [UNameTF resignFirstResponder];

    SignUpViewController *sign=[self.storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
    sign.comingFrom=comingFrom;
    [self.navigationController pushViewController:sign animated:YES];

}

- (IBAction)ForgotPass_Btn_Clicked:(id)sender
{
    [PasswordTF resignFirstResponder];
    [UNameTF resignFirstResponder];

    [self.view bringSubviewToFront:subView_ForgotPassword];
    subView_ForgotPassword.hidden=NO;
    subView_ForgotPassword.hidden=NO;
    contactBackView.hidden=YES;
//    [[NSUserDefaults standardUserDefaults]setObject:@"forgotPassword" forKey:@"CMSURLToLoad"];
//    [self loadStoryBoardWithIdentifier:@"CMSVC"];
}


#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - API Call Methods

-(void)LoginAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }
        else
        {
            @try
            {
                NSString *email=UNameTF.text;
                NSString *pass=PasswordTF.text;
                
                NSString *url=API_SIGN_IN;
                
                NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:email,@"email",pass,@"password", nil];
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                HUD.hidden=YES;
                
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    if (success)
                    {
                        HUD.hidden=YES;
                        
                        defaults=[NSUserDefaults standardUserDefaults];
                        
                        NSDictionary *userDict=[getJsonData valueForKey:@"data"];
                    
                        [[NSUserDefaults standardUserDefaults]setObject:@"ZeroAddress" forKey:@"chkAddressCount"];
                        
                        
                        [defaults setObject:[userDict valueForKey:@"email"] forKey:USEREMAIL];;
                        [defaults setObject:[userDict valueForKey:@"firstname"] forKey:USERFIRSTNAME];
                        [defaults setObject:[userDict valueForKey:@"lastname"] forKey:USERLASTNAME];
                        [defaults setObject:[userDict valueForKey:@"password_hash"] forKey:@"userPassword"];
                        [defaults setObject:[userDict valueForKey:@"entity_id"] forKey:USERID];
                        [defaults setObject:@"YES" forKey:ISLOGIN];
                        
                        
                        
                        //adding fcm id against userid

                            NSString *tok=[[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"];
                            if (tok !=nil) {
                                HelperClass *helpObj=[[HelperClass alloc] init];
                                [helpObj addFCMTokenToServer];
                            }
                            else{
                    
                                NSLog(@"EORROR in Fetching token.");
                            }
                            
                        
//                        [self loadStoryBoardWithIdentifier:@"ViewController"];
                       
                        [ProgressHUD showSuccess:@"Login Successful" Interaction:NO];
                        
                        if ([comingFrom isEqualToString:@"PROFILE"])
                        {
                            ProfileUpdateViewController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileUpdateVC"];
//                            vie.goBackTo=goBackTo;
                            [self.navigationController pushViewController:vie animated:YES];
                        }
                        else if([comingFrom isEqualToString:@"CART"])
                        {
                            ShippingAddressController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingAddressController"];
                            vie.comingFrom=@"LoginVC";
                            [self.navigationController pushViewController:vie animated:YES];
                            
                           /* CartController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
                            vie.comingFrom=@"LoginVC";
                            [self.navigationController pushViewController:vie animated:YES];*/
                        }
                        else
                        {
                            ViewController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                            [self.navigationController pushViewController:vie animated:YES];
                        }

                       // [self UIAlertViewControllerMethodTitle:@"Success" message:@"Login success"];
                        
                    }
                    
                    else
                    {
                        NSString *msg=[NSString stringWithFormat:@"%@",[getJsonData valueForKey:@"message"]];
                        [self UIAlertViewControllerMethodTitle:@"Login Error" message:msg];
                        
                    }
                    
                    
                    
                }
                
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
                
            }
        }
        
    });
}

-(void)ForgotPasswordAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }
        else
        {
            @try
            {
                NSString *forgotpasswordEmail=forgotPasswordTF.text;
                
                NSString *url = API_FORGOT_PASSWORD;
                
                NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:forgotpasswordEmail,@"email", nil];
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                HUD.hidden=YES;
                
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if([getJsonData count]>0)
                {
                    if(success)
                    {
                        
                        [ProgressHUD showSuccess:@"Check email id for Password Reset link" Interaction:NO];
                    }
                }
                else
                {
                    NSString *msg=[getJsonData valueForKey:@"message"];
                   [ProgressHUD showError:msg Interaction:NO];
                }
                
                    
                HUD.hidden=YES;
                subView_ForgotPassword.hidden=YES;
                
                
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
                
            }
        }
        
    });
  
}


-(void)SignupAPI
{
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }
        else
        {
            @try
            {
                
//                fb_pass=[self genRandStringLength:8];
                NSString *email=fb_email;
//                NSString *pass=fb_pass;
                NSString *fname=fb_fname;
                NSString *lname=fb_lname;
                
                NSString *url=API_SOCIAL_LOGIN;
                
                NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:email,@"email",fname,@"firstname",lname,@"lastname", nil];
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                
                
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    if (success)
                    {
                        defaults=[NSUserDefaults standardUserDefaults];
                        
                        NSDictionary *userDict=[getJsonData valueForKey:@"data"];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:@"ZeroAddress" forKey:@"chkAddressCount"];
                        [defaults setObject:[userDict valueForKey:@"email"] forKey:USEREMAIL];;
                        [defaults setObject:[userDict valueForKey:@"firstname"] forKey:USERFIRSTNAME];
                        [defaults setObject:[userDict valueForKey:@"lastname"] forKey:USERLASTNAME];
                        [defaults setObject:[userDict valueForKey:@"password_hash"] forKey:@"userPassword"];
                        [defaults setObject:[userDict valueForKey:@"entity_id"] forKey:USERID];
                        [defaults setObject:@"YES" forKey:ISLOGIN];
                        
                        
                        //adding fcm id against userid
                        NSString *tok=[[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"];
                        if (tok !=nil) {
                            HelperClass *helpObj=[[HelperClass alloc] init];
                            [helpObj addFCMTokenToServer];
                        }
                        else{
                            
                            NSLog(@"EORROR in Fetching token.");
                        }
                        
                        
                        if ([comingFrom isEqualToString:@"PROFILE"]) {
                            ProfileUpdateViewController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileUpdateVC"];
                            [self.navigationController pushViewController:vie animated:YES];
                        }
                        else if([comingFrom isEqualToString:@"CART"])
                        {
                            ShippingAddressController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingAddressController"];
                            vie.comingFrom=@"LoginVC";
                            [self.navigationController pushViewController:vie animated:YES];
                            
                        }
                        else
                        {
                            ViewController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                            [self.navigationController pushViewController:vie animated:YES];
                        }
                        
                        
                    }
                    
                    else
                    {
                        NSString *msg=[NSString stringWithFormat:@"%@",[getJsonData valueForKey:@"message"]];
                        [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:msg];
                        
                    }
                    HUD.hidden=YES;
                    
                    
                }
                
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
                
            }
        }
        
    });
}

#pragma mark GenerateRandomNumber
- (NSString *)genRandStringLength:(int)len {
    static NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    return randomString;
}


#pragma mark - Text field Delegates

- (IBAction)EmailTFDidEndEditing:(id)sender
{
   
}

- (IBAction)PasswordTFDidEndEditing:(id)sender
{
    [PasswordTF resignFirstResponder];
}

#pragma mark forgot password method
- (IBAction)cancelBtn_Clicked:(id)sender
{
    subView_ForgotPassword.hidden=YES;
    [self.view sendSubviewToBack:subView_ForgotPassword];
}

- (IBAction)submitBtn_Clicked:(id)sender
{
    [forgotPasswordTF resignFirstResponder];
    if(forgotPasswordTF.text.length>0)
    {
      if([self validEmail:forgotPasswordTF.text])
      {
          [self ForgotPasswordAPI];
      }
      else
      {
          [ProgressHUD showError:@"Enter Valid Email Id" Interaction:NO];
      }
    }
    else
    {
        [ProgressHUD showError:@"Enter Email Id" Interaction:NO];
    }
}

- (IBAction)fbLoginMethod:(id)sender {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           HUD.hidden=YES;
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               HUD.hidden=YES;
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

#pragma mark - ValidationMethods
- (BOOL) validEmail:(NSString*) emailString
{
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}


- (IBAction)facebookLoginMehothod:(id)sender {
}

- (IBAction)contactSubmitMethod:(id)sender {
    subView_ForgotPassword.hidden=YES;

}

- (IBAction)contactCancelMethod:(id)sender {
    subView_ForgotPassword.hidden=YES;
    [loginManager logOut];

}




#pragma mark POPUP
-(void) CreateSlideOut
{
    
    CGRect frame=CGRectMake(0, CGRectGetMaxY(self.view.bounds), 200, 200);
    
    backToOriginal=frame;
    
    popup=[[UIView alloc]initWithFrame:frame];
    
    popup.backgroundColor = [UIColor orangeColor];
    
    [self.view addSubview:popup];
    
}


-(void) slidePopup
{
    
    UIButton *button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame=CGRectMake(60, 190, 100,40);
    button.backgroundColor=[UIColor clearColor];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button setTitle:@"Dismiss" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(DismissClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txt_contact = [[UITextField alloc] initWithFrame:CGRectMake(20, 28, 100, 40)];
    txt_contact.delegate=self;
    txt_contact.autocorrectionType = UITextAutocorrectionTypeNo;
    [txt_contact setBackgroundColor:[UIColor clearColor]];
    [txt_contact setBorderStyle:UITextBorderStyleRoundedRect];
    
    [popup addSubview:txt_contact];
    [popup addSubview:button];
    
    CGRect frame=CGRectMake(200, 200, 200, 200);
    
    [UIView beginAnimations:nil context:nil];
    
    [popup setFrame:frame];
    
    [UIView commitAnimations];
    
    
}

-(void) removePopUp

{
    [UIView beginAnimations:nil context:nil];
    
    [popup setFrame:backToOriginal];
    
    [UIView commitAnimations];
    
    [txt_contact resignFirstResponder];
}

-(IBAction) DismissClicked : (id) sender
{
    
    [self removePopUp];
}




@end
