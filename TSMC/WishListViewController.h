//
//  ItemsViewController2.h
//  TSMC
//
//  Created by user on 13/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "ProgressHUD.h"
#import "CustomTableViewCell.h"


@interface WishListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    
    MBProgressHUD *HUD;
    UITextField *globalvar;
    
    NSString *chkCellClicked,*recogniseGesture;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btn_Back;
- (IBAction)backMethod:(id)sender;
@property(nonatomic)NSString *fetchedId,*fetchedName;



- (IBAction)addCartMethod:(id)sender;
- (IBAction)addQtyMethod:(id)sender;
- (IBAction)subQtyMethod:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *pageTableView;
@property (weak, nonatomic) IBOutlet UIView *tableBackView;
@property (strong,nonatomic)UIPickerView *spicesPicker;
//@property (weak, nonatomic) IBOutlet UIButton *btn_profile;
@property (weak, nonatomic) IBOutlet UIButton *btn_cart;
@property (weak, nonatomic) IBOutlet UILabel *lbl_badgeCount;
@property (weak, nonatomic) IBOutlet UILabel *titleBack;
@property (strong, nonatomic) IBOutlet UIButton *callUsBtn;
- (IBAction)callUsBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleText;

@property (weak, nonatomic) IBOutlet UIButton *btn_search;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

//- (IBAction)spiceMethod:(id)sender;
//- (IBAction)profileMethod:(id)sender;

- (IBAction)wishMethod:(id)sender;

- (IBAction)cutMethod:(id)sender;
- (IBAction)weightMethod:(id)sender;
- (IBAction)typeMethod:(id)sender;

- (IBAction)searchMethod:(id)sender;
- (IBAction)filterMethod:(id)sender;
- (IBAction)cartMethod:(id)sender;
@end
