//
//  HistoryViewController.h
//  TSMC
//
//  Created by user on 05/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface HistoryViewController : UIViewController
{
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *titleText;
    IBOutlet UITableView *historyTblView;
    
    MBProgressHUD *HUD;
    
    NSMutableArray *historyArr,*customerArr,*orderIdArr,*orderStatusArr,*orderDateTimeArr,*orderAmount,*orderAmount2,*shippingCharges,*subTotal,*comment,*orderDateArr2,*orderTimeArr2,*customDeliveryDateArr,*customDeliveryTimeArr,*couponCodeNameArr,*couponDiscountArr;
}
- (IBAction)backBtnClicked:(id)sender;
@end
