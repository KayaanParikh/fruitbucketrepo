//
//  updateAddressViewController.m
//  TSMC
//
//  Created by user on 03/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "ShippingAddressController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "CustomTableViewCell.h"
#import "OrderInformationViewController.h"
#import "CartController.h"
#import "ObjStateData.h"
#import "UIView+Toast.h"
#import "ProfileUpdateViewController.h"

@interface ShippingAddressController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UISearchResultsUpdating>
{
    
    NSMutableArray *searchedLocationArray,*searchedLocationKeyArray,*locationStateArray;
    BOOL isSearching;
    NSString *cityStr,*country_idStr,*entity_idStr,*firstnameStr,*lastnameStr,*postcodeStr,*regionStr,*streetStr,*telephoneStr,*entity_type_idStr,*street1,*street2,*selectedCountryKey,*regionId,*countryName;
    NSArray *seperateSreet;
    BOOL isEdited;
    NSString *selectedBox,*billingScreen;
    NSDictionary *dictCountry,*dictState;
    
}
@end

@implementation ShippingAddressController
@synthesize locationPicker,chkAddressCount,comingFrom,arrayRedeeemShip;
@synthesize shipUserId,shipAddressId,shipfname,shiplname,shipTelephone,shipStreet1,shipStreet2,shipCity,shipPostcode;

-(void)viewWillAppear:(BOOL)animated
{
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isSearching=NO;
    _shieldView.hidden=YES;
    
    firstNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* First Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    lastNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Last Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    telephoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Telephone" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    StreetLine1TF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Street Address Line 1" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    streetLine2TF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Street Address Line 2" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    _locationTableView.backgroundColor = [UIColor whiteColor];
    
    [self initObjects];
    isEdited=NO;
    
    if ([comingFrom isEqualToString:@"SHIPPING"]) {
        billingScreen=@"YES";
        [self InitAttributeFrameForBilling];
        [self OutletsInteractionEnabledYES];
        
    }
    else if ([comingFrom isEqualToString:@"CART"] || [comingFrom isEqualToString:@"LoginVC"])
    {
        billingScreen=@"NO";
//        [self OutletsInteractionEnabledNo];
        [self InitAttributeFrameForShipping];
        
//        // toast with a specific duration and position
//        [self.view makeToast:@"To add/edit this information please click profile update icon on right corner and select your address and go to edit section..!!!!"
//                    duration:5.0
//                    position:CSToastPositionTop];


        HelperClass *s=[[HelperClass alloc]init];
        BOOL connection=s.getInternetStatus;
        
        if(connection){
            [self getAddressesAPI];
        }else{
            HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.hidden=NO;
            HUD.dimBackground = YES;
            HUD.labelText = @"Please wait...";
            
            for (int i =0 ; i<5; i++) {
                BOOL connection=s.getInternetStatus;
                
                if(connection){
                    [self getAddressesAPI];
                    break;
                }else if(i == 4){
                    [self alertControllerWithAction:@"Connection Lost" message:@"Please check your internet connection!"];
                }
                sleep(1);
            }
        }
    }
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark API

-(void)getAddressesAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    ChkAPICall = @"getAddress";
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection){
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }else{
            @try
            {
                NSString *userId=[defaults objectForKey:USERID];
                
                NSString *url=API_GET_ADDRESSES;
                
                NSMutableDictionary *parameterDict;
                
                parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id", nil];
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                HUD.hidden=YES;
                BOOL success = [[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    NSArray *addressArr = [[getJsonData valueForKey:@"addresses"] valueForKey:@"addresses"];
                    NSArray *defaultArr = [[getJsonData valueForKey:@"addresses"] valueForKey:@"default"];
                    
                    if (success)
                    {
                        for (int i=0; i<[defaultArr count]; i++)
                        {
                            BOOL isDefault = [[defaultArr objectAtIndex:i] boolValue];
                            
                            if (isDefault){
                                cityStr = [[addressArr valueForKey:@"city"] objectAtIndex:i];
                                countryName = [[addressArr valueForKey:@"country"] objectAtIndex:i];
                                country_idStr = [[addressArr valueForKey:@"country_id"] objectAtIndex:i];
                                entity_idStr = [[addressArr valueForKey:@"entity_id"] objectAtIndex:i];
                                entity_type_idStr = [[addressArr valueForKey:@"entity_type_id"] objectAtIndex:i];
                                firstnameStr = [[addressArr valueForKey:@"firstname"] objectAtIndex:i];
                                lastnameStr = [[addressArr valueForKey:@"lastname"] objectAtIndex:i];
                                postcodeStr = [[addressArr valueForKey:@"postcode"] objectAtIndex:i];
                                regionStr = [[addressArr valueForKey:@"region"] objectAtIndex:i];
                                regionId = [[addressArr valueForKey:@"region_id"] objectAtIndex:i];
                                streetStr = [[addressArr valueForKey:@"street"] objectAtIndex:i];
                                telephoneStr = [[addressArr valueForKey:@"telephone"] objectAtIndex:i];
                            }
                        }
                        NSUserDefaults *userDFT=[NSUserDefaults standardUserDefaults];
                        
                        seperateSreet =[streetStr componentsSeparatedByString:@"\n"];
                        if (seperateSreet.count>1)
                        {
                            streetLine2TF.text=[NSString stringWithFormat:@"%@",[seperateSreet objectAtIndex:1]];
                            StreetLine1TF.text=[NSString stringWithFormat:@"%@",[seperateSreet objectAtIndex:0]];
                            street1=[NSString stringWithFormat:@"%@",[seperateSreet objectAtIndex:0]];
                            street2=[NSString stringWithFormat:@"%@",[seperateSreet objectAtIndex:1]];
                            
                        }else{
                            street1=[NSString stringWithFormat:@"%@",[seperateSreet objectAtIndex:0]];
                            street2=@"";
                            StreetLine1TF.text=[NSString stringWithFormat:@"%@",[seperateSreet objectAtIndex:0]];
                        }
                        
                        firstNameTF.text=[NSString stringWithFormat:@"%@",firstnameStr];
                        lastNameTF.text=[NSString stringWithFormat:@"%@",lastnameStr];
                        telephoneTF.text=[NSString stringWithFormat:@"%@",telephoneStr];
                        [selectLocationBtn setTitle:countryName forState:UIControlStateNormal];
                        [cityBtn setTitle:regionStr  forState:UIControlStateNormal];
                        [cityStateBtn setTitle:cityStr forState:UIControlStateNormal];
                        
                        if([postcodeStr isEqualToString:@"India"]){
                            txtPostCode.text = @"";
                        }else{
                            txtPostCode.text = [NSString stringWithFormat:@"%@",postcodeStr];
                        }
                        
//                        [cityStateBtn setTitle:cityStr forState:UIControlStateNormal];
                        
                        [userDFT setObject:firstnameStr forKey:@"fname"];
                        [userDFT setObject:lastnameStr forKey:@"lname"];
                        [userDFT setObject:postcodeStr forKey:@"postcode"];
                        [userDFT setObject:street1 forKey:@"street1"];
                        [userDFT setObject:street2 forKey:@"street2"];
                        [userDFT setObject:telephoneStr forKey:@"telephone"];
                        [userDFT setObject:entity_idStr forKey:@"shipid"];
                        [userDFT setObject:cityStr forKey:@"city"];
                        [userDFT setObject:regionId forKey:@"region_id"];
                        [userDFT setObject:country_idStr forKey:@"country_id"];
                        
                    }
                }
            } @catch (NSException *exception){
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
    });
}

-(void)EditAddressesAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    ChkAPICall = @"getAddress";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }else{
            @try
            {
                NSDictionary *getJsonData;
                NSString *userId=[defaults objectForKey:USERID];
                NSString *addressId=entity_idStr;
                NSString *fname=firstNameTF.text;
                NSString *lname=lastNameTF.text;
                NSString *telephone=telephoneTF.text;
                NSString *street=StreetLine1TF.text;
                NSString *area=streetLine2TF.text;
                NSString *city=cityBtn.titleLabel.text;
                NSString *postcode = txtPostCode.text;
                NSString *countryId = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"countryId"];
                NSString *regionId = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"stateValue"];
                NSString *regionName = cityStateBtn.titleLabel.text;
                
                NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"id",fname,@"firstname",lname,@"lastname",telephone,@"telephone",street,@"street",area,@"area",postcode,@"postcode",city,@"city",addressId,@"addressId",@"1",@"default",countryId,@"country_id",regionName,@"region",regionId,@"region_id",nil];
                NSString *url=API_EDIT_ADDRESSES;
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];

                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    if(success)
                    {
                        NSString *msg=[getJsonData valueForKey:@"message"];
                        [ProgressHUD showSuccess:msg Interaction:NO];
                        
                        NSUserDefaults *userDFT=[NSUserDefaults standardUserDefaults];
                        [userDFT setObject:fname forKey:@"fname"];
                        [userDFT setObject:lname forKey:@"lname"];
                        [userDFT setObject:postcode forKey:@"postcode"];
                        [userDFT setObject:street forKey:@"street1"];
                        [userDFT setObject:area forKey:@"street2"];
                        [userDFT setObject:telephone forKey:@"telephone"];
                        [userDFT setObject:addressId forKey:@"shipid"];
                        [userDFT setObject:city forKey:@"city"];
                        [HUD hide:YES];
                        ShippingAddressController *shpivc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingAddressController"];
                        shpivc.comingFrom=@"SHIPPING";
                        [self.navigationController pushViewController:shpivc animated:YES];
                    
                    }else{
                        [HUD hide:YES];
                        NSString *msg=[getJsonData valueForKey:@"message"];
                        [ProgressHUD showError:msg Interaction:NO];
                    }
                }
            } @catch (NSException *exception){
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
    });
}

#pragma mark - InitObjects
-(void)OutletsInteractionEnabledNo
{
    [firstNameTF setUserInteractionEnabled:NO];
    [lastNameTF setUserInteractionEnabled:NO];
    [telephoneTF setUserInteractionEnabled:NO];
    [StreetLine1TF setUserInteractionEnabled:NO];
    [streetLine2TF setUserInteractionEnabled:NO];
    [selectLocationBtn setUserInteractionEnabled:NO];
    [defaultAddressBtn setUserInteractionEnabled:NO];
    [cityBtn setUserInteractionEnabled:NO];
    [cityStateBtn setUserInteractionEnabled:NO];
    [txtPostCode setUserInteractionEnabled:NO];

}
-(void)OutletsInteractionEnabledYES
{
    [firstNameTF setUserInteractionEnabled:YES];
    [lastNameTF setUserInteractionEnabled:YES];
    [telephoneTF setUserInteractionEnabled:YES];
    [StreetLine1TF setUserInteractionEnabled:YES];
    [streetLine2TF setUserInteractionEnabled:YES];
    [selectLocationBtn setUserInteractionEnabled:YES];
    [cityBtn setUserInteractionEnabled:YES];
    [cityStateBtn setUserInteractionEnabled:YES];
    [txtPostCode setUserInteractionEnabled:YES];

}

-(void)initObjects
{
    defaults=[NSUserDefaults standardUserDefaults];
    firstNameTF.tag=104;
    lastNameTF.tag=105;
    telephoneTF.tag=106;
    chkBoxImgView.tag=1;
    selectLocationBtn.tag=0;
    chkBoxImgView.image=[UIImage imageNamed:@"checked.png"];
    selectedBox=@"YES";
    
    locationArr=[[NSMutableArray alloc]init];
    locationKeyArr = [[NSMutableArray alloc]init];
    selectedLocationArr=[[NSMutableArray alloc]init];
    locationStateArray =[[NSMutableArray alloc]init];
}

-(void)InitAttributeFrameForShipping
{
    [scrollView setContentSize:CGSizeMake(375, 1000)];
    
    self.navigationController.navigationBarHidden=NO;
//    titleLbl.backgroundColor=TopBarColor;
    self.title = @"Shipping Address Preview".uppercaseString;
//    titleText.font=TopBarFont;
//    titleText.textColor=TopBarTextColor;
    selectLocationBtn.layer.borderWidth=0.7f;
    cityStateBtn.layer.borderWidth=0.7f;
    cityBtn.layer.borderWidth=0.7f;
    
    chkBoxImgView.hidden=NO;
    defaultAddressBtn.hidden=NO;
    lbl_useSameForBill.hidden=NO;
    
    
}
-(void)InitAttributeFrameForBilling
{
    [scrollView setContentSize:CGSizeMake(375, 1000)];
    
    self.navigationController.navigationBarHidden=NO;
    titleLbl.backgroundColor=TopBarColor;
    titleText.text=@"Billing Address";
    titleText.font=TopBarFont;
    titleText.textColor=cartIconTextColor;
    
    
    selectLocationBtn.layer.borderWidth=0.7f;
    cityStateBtn.layer.borderWidth=0.7f;
    cityBtn.layer.borderWidth=0.7f;
    
    NSUserDefaults *getUserDFT=[NSUserDefaults standardUserDefaults];
    
    NSString *chck=[[NSUserDefaults standardUserDefaults] valueForKey:@"billingfname"];
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"billingfname"]==nil || [[[NSUserDefaults standardUserDefaults] valueForKey:@"billingfname"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] valueForKey:@"billingfname"] isEqualToString:@""]) {
        firstNameTF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"fname"]];
        lastNameTF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"lname"]];
        telephoneTF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"telephone"]];
        //        StreetLine1TF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"street1"]];
        //        streetLine2TF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"street2"]];
        [selectLocationBtn setTitle:@"Select Location" forState:UIControlStateNormal];
        [cityBtn setTitle:@"Mumbai" forState:UIControlStateNormal];
        
    }else{
        
        firstNameTF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"billingfname"]];
        lastNameTF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"billinglname"]];
        telephoneTF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"billingtelephone"]];
        StreetLine1TF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"billingstreet1"]];
        streetLine2TF.text=[NSString stringWithFormat:@"%@",[getUserDFT objectForKey:@"billingstreet2"]];
        [selectLocationBtn setTitle:[getUserDFT objectForKey:@"billingpostcode"] forState:UIControlStateNormal];
        [cityBtn setTitle:[getUserDFT objectForKey:@"billingcity"] forState:UIControlStateNormal];
        
    }
    
    chkBoxImgView.hidden=YES;
    defaultAddressBtn.hidden=YES;
    lbl_useSameForBill.hidden=YES;
    
}

#pragma mark - Button Clicked Methods
- (IBAction)backBtn_Clicked:(id)sender
{
    //    [self.navigationController popViewControllerAnimated:YES];
    if ([comingFrom isEqualToString:@"LoginVC"]) {
        for (UIViewController* vc in self.navigationController.viewControllers) {
            
            if ([vc isKindOfClass:[CartController class]] )
            {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                CartController *groupViewController = (CartController*)vc;
                [self.navigationController popToViewController:groupViewController animated:YES];
            }
        }
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

//- (IBAction)profileBtn_Clicked:(id)sender
//{
//    ProfileUpdateViewController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileUpdateVC"];
//    //                            vie.goBackTo=goBackTo;
//    [self.navigationController pushViewController:vie animated:YES];
//
//}

- (IBAction)selectLocationBtn_Clicked:(id)sender
{
    selectLocationBtn.tag=1;
    ChkAPICall=@"selectLocation";
    
    [cityBtn setTitle:@"Select State" forState:UIControlStateNormal];
    [cityStateBtn setTitle:@"Select City" forState:UIControlStateNormal];
    
    [self getUseraddressesAPI];
}

- (IBAction)selectStateBtn_Clicked:(id)sender{
    
    [cityStateBtn setTitle:@"Select City" forState:UIControlStateNormal];
    
    if ([selectLocationBtn.currentTitle isEqual: @"Select Country"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select Country"];
    }else{
        ChkAPICall=@"selectStateLocation";
        [self getUseraddressesAPI];
    }
    
}
- (IBAction)selectCityBtn_Clicked:(id)sender{
    if ([selectLocationBtn.currentTitle isEqual: @"Select Country"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select Country"];
    }else if ([cityBtn.currentTitle isEqual: @"Select State"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select State"];
    }else{
        ChkAPICall=@"selectCityLocation";
        [self getUseraddressesAPI];
    }
    
}

- (IBAction)defaultAddressBtn_Clicked:(id)sender
{
    if(chkBoxImgView.tag==0)
    {
        chkBoxImgView.image=[UIImage imageNamed:@"checked.png"];
        chkBoxImgView.tag=1;
        selectedBox=@"YES";
        //[self getUseraddressesAPI];
    }else if(chkBoxImgView.tag==1){
        
        chkBoxImgView.image=[UIImage imageNamed:@"square.png"];
        chkBoxImgView.tag=0;
        selectedBox=@"NO";
        
    }
}


-(BOOL)checkEditedOrNot
{
    if (!([cityStr isEqualToString:cityBtn.titleLabel.text])) {
        return YES;
    }else if (!([firstnameStr isEqualToString:firstNameTF.text])){
        
        return YES;
    }else if (!([lastnameStr isEqualToString:lastNameTF.text])){
        
        return YES;
    }else if (!([telephoneStr isEqualToString:telephoneTF.text])){
        
        return YES;
    }else if (!([street1 isEqualToString:StreetLine1TF.text])){
        return YES;
    }else if (!([street2 isEqualToString:streetLine2TF.text])){
        return YES;
    }else if (!([postcodeStr isEqualToString:txtPostCode.text])){
        return YES;
    }
    
    return NO;
}

- (IBAction)EditBtnClicked:(id)sender
{
    if ([self validation]) {
        if ([billingScreen isEqualToString:@"YES"]) {
            
            [self addSelectedAddressAPI];
            
        }else{
            
            if ([selectedBox isEqualToString:@"YES"]) {
                [HUD hide:YES];
                
                if (country_idStr ==nil || [country_idStr isEqualToString:@""]) {
                    [self addSelectedAddressAPI];
                }
                else{
                    [self addSelectedAddressAPI];
//                    OrderInformationViewController *shpivc=[self.storyboard instantiateViewControllerWithIdentifier:@"OrderInformationVC"];
//                    shpivc.isBillAddressSame=@"YES";
//                    shpivc.arrayRedeemOrder = arrayRedeeemShip;
//                    [self.navigationController pushViewController:shpivc animated:YES];
                    
                }
            }else{
                if ([self checkEditedOrNot]) {
                    ////Call update api
                    
                    if (country_idStr ==nil || [country_idStr isEqualToString:@""]) {
                        [self addSelectedAddressAPI];
                    }
                    else{
                        [self EditAddressesAPI];
                    }
                }else{
                    [HUD hide:YES];
                    ShippingAddressController *shpivc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingAddressController"];
                    shpivc.comingFrom=@"SHIPPING";
                    [self.navigationController pushViewController:shpivc animated:YES];
                    
                }
            }
        }
    }
}

#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}



#pragma mark - API Call Methods

-(void)getUseraddressesAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }else{
            @try
            {
                NSDictionary *getJsonData;
                
                if([ChkAPICall isEqualToString:@"selectLocation"])
                {
//                    NSString *url=API_SELECT_LOCATIONS;
                    NSString *url = API_GET_COUNTRYLIST;
                    // NSString *userId=[defaults objectForKey:@"userID"];
                    
                    NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    if(getJsonData == nil || getJsonData.count<=0 || [getJsonData isEqual:@"(null)"])
                    {
                        locationPicker.hidden=YES;
                        [ProgressHUD showError:@"No Addresses Found" Interaction:NO];
                    }
                    else if(getJsonData.count > 0)
                    {
//                        NSDictionary * tempArray = [getJsonData valueForKey:@"list"];
                        
                        dictCountry = [[NSDictionary alloc]initWithDictionary:[getJsonData objectForKey:@"list"]];
                        locationKeyArr = [[NSMutableArray alloc] initWithArray:[dictCountry allKeys]];
                        locationArr = [[NSMutableArray alloc] initWithArray:[dictCountry allValues]];
                        _lbl_availability.text = @"Please select country";
                        _shieldView.hidden=NO;
                        //                        locationPicker.hidden=NO;
                        [_locationTableView reloadData];
                    }
                    
                }else if([ChkAPICall isEqualToString:@"selectStateLocation"]){
//                    NSString *url=API_GET_STATELIST;
                    NSString *strCountryId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"countryId"];
                    if (strCountryId.length == 0){
                        strCountryId = country_idStr;
                    }
                    NSString *url =  [API_GET_STATELIST stringByAppendingString:strCountryId];
                    // NSString *userId=[defaults objectForKey:@"userID"];
                    
                    NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    if(getJsonData == nil || getJsonData.count<=0 || [getJsonData isEqual:@"(null)"])
                    {
                        locationPicker.hidden=YES;
                        [ProgressHUD showError:@"No Addresses Found" Interaction:NO];
                    }
                    else if(getJsonData.count > 0)
                    {
                        
                        NSArray *arraytsik = [getJsonData allValues];
                        
                        [arraytsik[1] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            
                            ObjStateData *event = [ObjStateData modelObjectWithDictionary:obj];
                            [locationStateArray addObject:event];
                        }];
                        
//                        dictState = [[NSDictionary alloc]initWithDictionary:arraytsik[1]];
                        _lbl_availability.text = @"Please select state";
                        _shieldView.hidden=NO;
                        //                        locationPicker.hidden=NO;
                        [_locationTableView reloadData];
                    }
                    
                }else if([ChkAPICall isEqualToString:@"selectCityLocation"]){
                    
                    NSString *stateId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"stateId"];
                    if (stateId.length == 0){
                        stateId = regionId;
                    }
                    
    //                    NSString *url=API_SELECT_LOCATIONS;
                    NSString *url =  [API_GET_CITY stringByAppendingString:stateId];
                        // NSString *userId=[defaults objectForKey:@"userID"];
                        
                        NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
                        
                        WebservicesClass *web=[[WebservicesClass alloc] init];
                        
                        getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                        
                        if(getJsonData == nil || getJsonData.count<=0 || [getJsonData isEqual:@"(null)"])
                        {
                            locationPicker.hidden=YES;
                            [ProgressHUD showError:@"No Addresses Found" Interaction:NO];
                        }
                        else if(getJsonData.count > 0)
                        {
    //                        NSDictionary * tempArray = [getJsonData valueForKey:@"list"];
                            
                            NSDictionary *dict = [[NSDictionary alloc]initWithDictionary:[getJsonData objectForKey:@"list"]];
                            locationKeyArr = [[NSMutableArray alloc] initWithArray:[dict allKeys]];
                            locationArr = [[NSMutableArray alloc] initWithArray:[dict allValues]];
                            _lbl_availability.text = @"Please select City";
                            _shieldView.hidden=NO;
                            //                        locationPicker.hidden=NO;
                            [_locationTableView reloadData];
                        }
                                        
                }else if([ChkAPICall isEqualToString:@"SelectDefault"]){
                    NSString *userId=[defaults objectForKey:USERID];
                    NSString *addressId=[[NSUserDefaults standardUserDefaults] objectForKey:@"Address_ID"];
                    
                    NSString *url=API_SET_DEFAULT_ADDRESSES;
                    
                    NSMutableDictionary *parameterDict;
                    
                    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"AddressView"] isEqualToString:@"AddAddress"])
                    {
                        parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id",@"",@"address_id", nil];
                    }else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"AddressView"] isEqualToString:@"EditAddress"]){
                        
                        parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id",addressId,@"address_id", nil];
                    }
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    NSLog(@"%@",getJsonData);
                    
                    BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                    
                    if ([getJsonData count] >0)
                    {
                        
                        if (success){
                            [ProgressHUD showSuccess:@"Customer address default set successfully" Interaction:NO];
                            // [self UIAlertViewControllerMethodTitle:@"Done" message:@"Customer address default set successfully"];
                        }else{
                            [ProgressHUD showError:@"Customer address not set as default" Interaction:NO];
                            // [self UIAlertViewControllerMethodTitle:@"Error" message:@"Customer address not set as default"];
                        }
                    }
                }
                HUD.hidden=YES;
            }
            @catch (NSException *exception)
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Error" message:exc];
            }
        }
    });
}

-(void)addSelectedAddressAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection){
            NSLog(@"Hello Net connectiion is not present....");
            
            //[HUD hide:YES];
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }else{
            @try
            {
                NSDictionary *getJsonData;
                NSString *userId=[defaults objectForKey:USERID];
                //                NSString *addressId=[[NSUserDefaults standardUserDefaults] objectForKey:@"Address_ID"];
                NSString *fname=firstNameTF.text;
                NSString *lname=lastNameTF.text;
                NSString *telephone=telephoneTF.text;
                NSString *street=StreetLine1TF.text;
                NSString *area=streetLine2TF.text;
                NSString *city=cityStateBtn.titleLabel.text;
                NSString *countryId = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"countryId"];
                NSString *regionLocalId = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"stateValue"];
                
                if (regionLocalId.length == 0){
                    regionLocalId = regionId;
                }
                
                if (countryId.length == 0){
                    countryId = country_idStr;
                }
                
                NSString *regionName = cityBtn.titleLabel.text;
                
                
                NSString *location=[NSString stringWithFormat:@"%@",selectLocationBtn.titleLabel.text];
                // NSString *encodedUrlStr = [self urlencode:location];
                NSString *postcode= txtPostCode.text;
                
                NSString *url=API_ADD_ADDRESS;
                NSMutableDictionary *parameterDict;
                
                if ([billingScreen isEqualToString:@"YES"]) {
                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   userId,@"id",
                                   fname,@"firstname",
                                   lname,@"lastname",
                                   telephone,@"telephone",
                                   street,@"street",
                                   area,@"area",
                                   postcode,@"postcode",
                                   city,@"city",
                                   @"0",@"default",
                                   countryId,@"country_id",
                                   regionName,@"region",
                                   regionLocalId,@"region_id", nil];
                    
                }else{
                    
//                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"id",fname,@"firstname",lname,@"lastname",telephone,@"telephone",street,@"street",area,@"area",postcode,@"postcode",city,@"city",@"1",@"default", nil];
                    
                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   userId,@"id",
                                   fname,@"firstname",
                                   lname,@"lastname",
                                   telephone,@"telephone",
                                   street,@"street",
                                   area,@"area",
                                   postcode,@"postcode",
                                   city,@"city",
                                   @"0",@"default",
                                   countryId,@"country_id",
                                   regionName,@"region",
                                   regionLocalId,@"region_id", nil];
                    
                }
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    if(success)
                    {
                        NSString *msg=[getJsonData valueForKey:@"message"];
                        [ProgressHUD showSuccess:msg Interaction:NO];
                        NSString *addr=[getJsonData valueForKey:@"addressId"];
                        
                        if ([billingScreen isEqualToString:@"YES"]) {
                            
                            NSUserDefaults *userDFT=[NSUserDefaults standardUserDefaults];
                            [userDFT setObject:fname forKey:@"billingfname"];
                            [userDFT setObject:lname forKey:@"billinglname"];
                            [userDFT setObject:postcode forKey:@"billingpostcode"];
                            [userDFT setObject:street forKey:@"billingstreet1"];
                            [userDFT setObject:area forKey:@"billingstreet2"];
                            [userDFT setObject:telephone forKey:@"billingtelephone"];
                            [userDFT setObject:addr forKey:@"billingid"];
                            [userDFT setObject:city forKey:@"billingcity"];
                            OrderInformationViewController *ovc=[self.storyboard instantiateViewControllerWithIdentifier:@"OrderInformationVC"];
                            [self.navigationController pushViewController:ovc animated:YES];
                            
                        }else{
                            
                            NSUserDefaults *userDFT=[NSUserDefaults standardUserDefaults];
                            
                            [userDFT setObject:fname forKey:@"fname"];
                            [userDFT setObject:lname forKey:@"lname"];
                            [userDFT setObject:postcode forKey:@"postcode"];
                            [userDFT setObject:street forKey:@"street1"];
                            [userDFT setObject:area forKey:@"street2"];
                            [userDFT setObject:telephone forKey:@"telephone"];
                            [userDFT setObject:addr forKey:@"shipid"];
                            [userDFT setObject:city forKey:@"city"];
                            
                            if ([selectedBox isEqualToString:@"YES"]) {
                                [HUD hide:YES];
                                OrderInformationViewController *ovc=[self.storyboard instantiateViewControllerWithIdentifier:@"OrderInformationVC"];
                                ovc.isBillAddressSame=@"YES";
                                ovc.arrayRedeemOrder = arrayRedeeemShip;
                                [self.navigationController pushViewController:ovc animated:YES];
                            }
                            else{
                                ShippingAddressController *shpivc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingAddressController"];
                                shpivc.comingFrom=@"SHIPPING";
                                [self.navigationController pushViewController:shpivc animated:YES];
                            }
                        }
                        [HUD hide:YES];
                    }else{
                        [HUD hide:YES];
                        [ProgressHUD showError:@"address not added" Interaction:NO];
                    }
                }
                HUD.hidden=YES;
            }
            @catch (NSException *exception){
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Error" message:exc];
            }
        }
        
    });
}

#pragma mark - Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    /*Restrict user to enter numbers in First Name and Last Name*/
    if (textField.tag==104) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    if (textField.tag==105) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    
    /*Restrict user to enter Charaters in Mobile textfield*/
    if (textField.tag==106) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return YES;
            }
            else{
                return NO;
            }
        }
    }
    
    
    return YES;
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

-(void)alertControllerWithAction:(NSString*)title message:(NSString*)msg
{
    HUD.hidden=YES;
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           if ([comingFrom isEqualToString:@"LoginVC"]) {
                               for (UIViewController* vc in self.navigationController.viewControllers) {
                                   
                                   if ([vc isKindOfClass:[CartController class]] ){
                                       
                                       // Here viewController is a reference of UIViewController base class of MyGroupViewController
                                       // but viewController holds MyGroupViewController  object so we can type cast it here
                                       CartController *groupViewController = (CartController*)vc;
                                       [self.navigationController popToViewController:groupViewController animated:YES];
                                   }
                               }
                           }else{
                               [self.navigationController popViewControllerAnimated:YES];
                           }
                       }];
    
    [alrt addAction:ok];
    [self presentViewController:alrt animated:YES completion:nil];
}

/*
 -(void)checkInternetConnection{
 int retry = 5;
 int count = 0;
 
 while (count < retry) {
 <#statements#>
 }
 }*/

#pragma mark Picker View Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if([ChkAPICall isEqualToString:@"selectStateLocation"]){
        if(locationStateArray.count>0){
            return locationStateArray.count;
        }else{
            return 0;
        }
    }else{
        if(locationArr.count>0){
            return locationArr.count;
        }else{
            return 0;
        }
    }
    return  0;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,locationPicker.bounds.size.width, 44.0f)];
    
    NSString *rowItem = [locationArr objectAtIndex:row];
    
    [locationLbl setTextAlignment:NSTextAlignmentCenter];
    [locationLbl setTextColor: [UIColor blackColor]];
    [locationLbl setText:rowItem];
    [locationLbl setBackgroundColor:[UIColor clearColor]];
    
    return locationLbl;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    selectedLocationIndex=[locationArr objectAtIndex:row];
}

#pragma mark - URLEncodedString
- (NSString *)urlencode:(NSString *)str
{
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[str UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i)
    {
        const unsigned char thisChar = source[i];
        if (thisChar == ' ')
        {
            [output appendString:@"%20"];
        }
        else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                 (thisChar >= 'a' && thisChar <= 'z') ||
                 (thisChar >= 'A' && thisChar <= 'Z') ||
                 (thisChar >= '0' && thisChar <= '9'))
        {
            [output appendFormat:@"%c", thisChar];
        }
        else
        {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

#pragma mark - Search delegate methods

- (void)filterContentForSearchText:(NSString*)searchText{
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF CONTAINS %@",
                                    searchText];
    
   
    
    if ([ChkAPICall isEqualToString:@"selectStateLocation"]){

//        ObjStateData *event = [locationStateArray objectAtIndex:indexPath.row];
//
////            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"stateName"];
//        [[NSUserDefaults standardUserDefaults] setdObject:event.label forKey:@"stateId"];
//        [[NSUserDefaults standardUserDefaults] setObject:event.value forKey:@"stateValue"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        [cityBtn setTitle:event.label forState:UIControlStateNormal];
        
        NSArray *sortedArray= [[locationStateArray valueForKey:@"label"] filteredArrayUsingPredicate:resultPredicate];
//        NSArray *sortedKeyArray= [locationKeyArr filteredArrayUsingPredicate:resultPredicate];
        searchedLocationArray=[[NSMutableArray alloc] init];
//        searchedLocationKeyArray=[[NSMutableArray alloc] init];
        [searchedLocationArray addObjectsFromArray:sortedArray];
//        [searchedLocationKeyArray addObjectsFromArray:sortedKeyArray];

    }else if ([ChkAPICall isEqualToString:@"selectCityLocation"]){
//        selectedLoc=[locationArr objectAtIndex:indexPath.row];
//        selectedCountryKey = [locationKeyArr objectAtIndex:indexPath.row];
////            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"cityName"];
//        [cityStateBtn setTitle:selectedLoc forState:UIControlStateNormal];
//        [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKey forKey:@"cityId"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
//            selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
//            selectedCountryKey = [searchedLocationKeyArray objectAtIndex:indexPath.row];

//        selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
//        selectedCountryKey = [locationKeyArr objectAtIndex:indexPath.row];
////            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"countryName"];
//        [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKey forKey:@"countryId"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//        [selectLocationBtn setTitle:selectedLoc forState:UIControlStateNormal];
        
        NSArray *sortedArray= [locationArr filteredArrayUsingPredicate:resultPredicate];
        NSArray *sortedKeyArray= [locationKeyArr filteredArrayUsingPredicate:resultPredicate];
        searchedLocationArray=[[NSMutableArray alloc] init];
        searchedLocationKeyArray=[[NSMutableArray alloc] init];
        [searchedLocationArray addObjectsFromArray:sortedArray];
        [searchedLocationKeyArray addObjectsFromArray:sortedKeyArray];
    }
    
    
    [_locationTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length == 0) {
        isSearching = NO;
        [self.locationTableView reloadData];
    }
    else {
        isSearching = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    isSearching = YES;
    [self filterContentForSearchText:searchBar.text];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{

    _shieldView.hidden=true;
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    isSearching = NO;
    
    
    [_locationTableView reloadData];
    
}

#pragma mark TableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableVw{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (isSearching) {
        if([ChkAPICall isEqualToString:@"selectStateLocation"]){
        
            return searchedLocationArray.count;
        }else{
            return searchedLocationArray.count;
        }
        
        
    }else{
        if([ChkAPICall isEqualToString:@"selectStateLocation"]){
            return locationStateArray.count;
        }else{
            return locationArr.count;
        }
        
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    if (isSearching) {
        
        
        if ([ChkAPICall isEqualToString:@"selectStateLocation"]){
//            ObjStateData *event
            cell.textLabel.text = [searchedLocationArray objectAtIndex:indexPath.row];
            //cell.textLabel.text= [NSString stringWithFormat:@"%@",event.label];
            
        }else{
            cell.textLabel.text=[searchedLocationArray objectAtIndex:indexPath.row];
        }
        
    }else if([ChkAPICall isEqualToString:@"selectStateLocation"]){
        ObjStateData *event = [locationStateArray objectAtIndex:indexPath.row];
        if (event.title.length > 0){
            cell.textLabel.text = event.title;
        }else{
            cell.textLabel.text = @"";
        }
        
    }else{
        cell.textLabel.text=[locationArr objectAtIndex:indexPath.row];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int height=50;
    return height;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //fetchedItemId,*fetchedItemName,*fetchedItemImage,*fetchedFoodType,*fetchedItemDesc,*fetchedSelectedStatus,*fetchedItemPrice,*fetchedCategoryId,*fetchedMsgId,*fetchedSpiceId,*fetchedLevelIds
    
    
    NSString *selectedLoc;
    if (isSearching) {
        
        [_searchBar resignFirstResponder];
        _searchBar.text = @"";
        
        if ([ChkAPICall isEqualToString:@"selectStateLocation"]){
            selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
            
            for (ObjStateData *event in locationStateArray) {
              if ([event.title isEqualToString:selectedLoc]) {
                  regionStr = [NSString stringWithFormat:@"%@",event.label];
                  regionId = [NSString stringWithFormat:@"%@",event.value];
                  [[NSUserDefaults standardUserDefaults] setObject:event.label forKey:@"stateId"];
                  [[NSUserDefaults standardUserDefaults] setObject:event.value forKey:@"stateValue"];
                  [[NSUserDefaults standardUserDefaults] synchronize];
                  [cityBtn setTitle:event.label forState:UIControlStateNormal];
              }
            }
            
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"stateName"];
            
            
        }else if ([ChkAPICall isEqualToString:@"selectCityLocation"]){
//            selectedLoc=[locationArr objectAtIndex:indexPath.row];
//            selectedCountryKey = [locationKeyArr objectAtIndex:indexPath.row];
////            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"cityName"];
//            [cityStateBtn setTitle:selectedLoc forState:UIControlStateNormal];
//            [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKey forKey:@"cityId"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
            NSArray *temp = [dictCountry allKeysForObject:selectedLoc];
            NSString *key = [temp lastObject];
//            selectedCountryKey = [searchedLocationKeyArray objectAtIndex:indexPath.row];
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"countryName"];
            [[NSUserDefaults standardUserDefaults] setObject:key forKey:@"countryId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [selectLocationBtn setTitle:selectedLoc forState:UIControlStateNormal];
        }
        
    }
    else{
        
        if ([ChkAPICall isEqualToString:@"selectLocation"]){
            selectedLoc=[locationArr objectAtIndex:indexPath.row];
            selectedCountryKey = [locationKeyArr objectAtIndex:indexPath.row];
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"countryName"];
            [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKey forKey:@"countryId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [selectLocationBtn setTitle:selectedLoc forState:UIControlStateNormal];
            
        }else if ([ChkAPICall isEqualToString:@"selectStateLocation"]){
            
            ObjStateData *event = [locationStateArray objectAtIndex:indexPath.row];
            
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"stateName"];
            
            regionStr = [NSString stringWithFormat:@"%@",event.label];
            regionId = [NSString stringWithFormat:@"%@",event.value];
            
            [[NSUserDefaults standardUserDefaults] setObject:event.label forKey:@"stateId"];
            [[NSUserDefaults standardUserDefaults] setObject:event.value forKey:@"stateValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [cityBtn setTitle:event.label forState:UIControlStateNormal];
            
        }else if ([ChkAPICall isEqualToString:@"selectCityLocation"]){
            selectedLoc=[locationArr objectAtIndex:indexPath.row];
            selectedCountryKey = [locationKeyArr objectAtIndex:indexPath.row];
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"cityName"];
            [cityStateBtn setTitle:selectedLoc forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKey forKey:@"cityId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    
    _shieldView.hidden=YES;
    
    //        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ShowLocationViewFirstTime"];
    
    //        [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:SELECTEDLOCATION];
    //        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //        HUD.dimBackground = YES;
    //        HUD.labelText = @"Please wait...";
    //        [self getLocationWisePrice:selectedLoc];
    
}

-(BOOL)validation{
    if ([firstNameTF.text isEqualToString:@""]) {
        [ProgressHUD showError:@"Please enter first name" Interaction:NO];
        return NO;
    }
    else if ([lastNameTF.text isEqualToString:@""]) {
        [ProgressHUD showError:@"Please enter last name" Interaction:NO];
        return NO;
    }
    else if (!([self validatePhone:telephoneTF.text])) {
        [ProgressHUD showError:@"Please enter valid phone number" Interaction:NO];
        return NO;
    }
    else if ([StreetLine1TF.text isEqualToString:@""]) {
        [ProgressHUD showError:@"Please enter street 1" Interaction:NO];
        return NO;
    }
    else if ([streetLine2TF.text isEqualToString:@""]) {
        [ProgressHUD showError:@"Please enter street 2" Interaction:NO];
        return NO;
    }
    else if ([txtPostCode.text isEqualToString:@""]) {
        [ProgressHUD showError:@"Please enter Pincode" Interaction:NO];
        return NO;
    }
//    else if ([selectLocationBtn.titleLabel.text isEqualToString:@"Select Location"] || [selectLocationBtn.titleLabel.text isEqualToString:@""]) {
//        [ProgressHUD showError:@"Please Select location" Interaction:NO];
//        return NO;
//    }
    else if ([selectLocationBtn.currentTitle isEqual: @"Select Country"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select Country"];
        return NO;
    }else if ([cityBtn.currentTitle isEqual: @"Select State"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select State"];
        return NO;
    }else if ([cityStateBtn.currentTitle isEqual: @"Select City"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select City"];
        return NO;
    }
    
    return YES;
}


-(BOOL)validatePhone:(NSString *)candidate

{
    
    NSString *phoneRegex = @"^+(?:[0-9] ?){9,9}[0-9]$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:candidate];
}

@end
