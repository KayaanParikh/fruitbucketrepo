//
//  CMSViewController.m
//  TSMC
//
//  Created by user on 10/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "CMSViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"

@interface CMSViewController ()<UIWebViewDelegate>
{
    NSString *str;

}

@end

@implementation CMSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self InitAttributeFrame];
    [self loadWebView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - InitObjects

-(void)InitAttributeFrame
{
    self.navigationController.navigationBarHidden=YES;
    
    _titleLbl.backgroundColor=TopBarColor;
    _titleText.font=TopBarFont;
    _titleText.textColor=TopBarTextColor;
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"aboutUs"])
    {
       _titleText.text=@"About Us".uppercaseString;
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"contactUs"])
    {
       _titleText.text=@"Contact Us".uppercaseString;
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"recipes"])
    {
        _titleText.text=@"Recipes".uppercaseString;
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"forgotPassword"])
    {
        _titleText.text=@"Forgot Password".uppercaseString;
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"qualityControl"])
    {
        _titleText.text=@"Quality Control".uppercaseString;
    }

    
}
- (IBAction)MenuBtn_Clicked:(id)sender
{
//    if ([webView canGoBack])
//    {
//        [webView goBack];
//    }
    
    
    if ([_titleText.text isEqualToString:@"Recipes"]) {
        if ([str rangeOfString:@"https://seafoodandmeatco.in/recipes?ismobile=true"].location == NSNotFound) {
            
            if ([webView canGoBack] ) {
                
//                urlStr=[NSString stringWithFormat:@"https://fresh-pick.in/recipes/"];
////                https://fresh-pick.in/recipes/
////                @"https://seafoodandmeatco.in/recipes?ismobile=true"
//                url=[NSURL URLWithString:urlStr];
//                NSURLRequest *webReq=[NSURLRequest requestWithURL:url];
//                [webView loadRequest:webReq];
                [self loadStoryBoardWithIdentifier:@"ViewController"];
                
            }
            
        }
        else{
            [self loadStoryBoardWithIdentifier:@"ViewController"];
            
        }
    }
    else{
        [self loadStoryBoardWithIdentifier:@"ViewController"];

    
    }
    
    
}

#pragma mark - Load Web View
-(void)loadWebView
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";

    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"aboutUs"])
    {
        urlStr=[NSString stringWithFormat:@"https://demo.myavenue.in/about-fresh-pick/"];
//        https://seafoodandmeatco.in/about-tsmc?ismobile=true
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"contactUs"])
    {
      urlStr=[NSString stringWithFormat:@"https://demo.myavenue.in/contacts?ismobile=true"];
//        https://seafoodandmeatco.in/contacts?ismobile=true
        //https://fresh-pick.in/contacts/
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"recipes"])
    {
        urlStr=[NSString stringWithFormat:@"https://demo.myavenue.in/recipes/"];
//        @"https://seafoodandmeatco.in/recipes?ismobile=true"
        //https://fresh-pick.in/recipes/
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"forgotPassword"])
    {
        urlStr=[NSString stringWithFormat:@"https://demo.myavenue.in/customer/account/forgotpassword?ismobile=true"];
    }
    else if([[[NSUserDefaults standardUserDefaults]objectForKey:@"CMSURLToLoad"] isEqualToString:@"qualityControl"])
    {
        urlStr=[NSString stringWithFormat:@"https://demo.myavenue.in/licenses-and-certificates"]; //Changed

        //https://fresh-pick.in/licenses-and-certificates
    }
    url=[NSURL URLWithString:urlStr];
    NSURLRequest *webReq=[NSURLRequest requestWithURL:url];
    [webView loadRequest:webReq];
}

-(BOOL)webView:(UIWebView *)webViw shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    [HUD hide:YES];

        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";

//    NSLog(@"%@",currentURL);
    
    return YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webVw
{
    [HUD hide:YES];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";

    

}

- (void)webViewDidFinishLoad:(UIWebView *)webVw
{
    NSString *currentURL=webVw.request.URL.absoluteString;
    str=currentURL;
    if (currentURL.length>0){
        if ([currentURL rangeOfString:@"ismobile=true"].location == NSNotFound)
        {
            currentURL=[NSString stringWithFormat:@"%@?ismobile=true",currentURL];
            currentURL=[currentURL stringByReplacingOccurrencesOfString:@"/?" withString:@"?"];
            url=[NSURL URLWithString:currentURL];
            NSURLRequest *webReq=[NSURLRequest requestWithURL:url];
            [webView loadRequest:webReq];
            
        }
    }

    HUD.hidden=YES;
}
#pragma mark - OtherMethods

-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}

@end
