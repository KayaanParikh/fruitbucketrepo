//
//  ViewController.m
//  TSMC
//
//  Created by Rahul Lekurwale on 21/06/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "ViewController.h"
#import "CustomTableViewCell.h"
#import "AppConstant.h"
#import "OfflineDb.h"
#import "UIImageView+WebCache.h"
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "ItemsViewController.h"
#import "SearchController.h"
#import "CartController.h"
#import <AddressBookUI/AddressBookUI.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UIView+Toast.h"
#import "UIBarButtonItem+Badge.h"



@import CoreLocation;

__unused static dispatch_once_t predicate;

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate,UISearchBarDelegate>
{
    NSMutableArray *nameArray,*idArray,*imagesArray;
    NSMutableArray *locationNameArray,*brandListArr,*brandIdArr,*searchedLocationArray;
    OfflineDb *object;
    BOOL isSearching;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D coordinate;
    NSString *currentTxt;

}
//@property (nonatomic)

@end

@implementation ViewController
@synthesize comingFrom,logOutWindow;


-(void)viewWillAppear:(BOOL)animated{
    [self checkProgressBar];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    
    self.title = HomeScreenTitleText.uppercaseString;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = TopBarColor;
    [self.navigationController.navigationBar setTitleTextAttributes:
    @{NSForegroundColorAttributeName:cartIconTextColor}];
    self.navigationController.navigationBar.translucent = NO;
    self.homeTableView.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSArray *str=[object fetch_CartDetails];
    NSLog(@"%@",str);
    NSString *cartCount=[NSString stringWithFormat:@"%lu",(unsigned long)[str count]];
    if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
        _lbl_cartCount.hidden=YES;
        self.navigationItem.rightBarButtonItem.badge.hidden = true;
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
        
    }
    else{
        _btnRightCart.badge.hidden = false;
        _lbl_cartCount.hidden=NO;
        [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
        _lbl_cartCount.text=cartCount;
        _lbl_cartCount.layer.masksToBounds=YES;
        _lbl_cartCount.layer.cornerRadius=_lbl_cartCount.frame.size.width/2;
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
            [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
        
    }

    //Show Location First time
    NSString *locVal=[[NSUserDefaults standardUserDefaults] valueForKey:@"ShowLocationViewFirstTime"];
    if (!([locVal isEqualToString:@"1"])) {
        
    }

    if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"ShowLocationViewFirstTime"] isEqualToString:@"1"])
    {
        
//        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        HUD.dimBackground = YES;
//        HUD.labelText = @"Fetching location..."; // CHANGED - 11Oct20
        
//        [HUD showWhileExecuting:@selector(CurrentLocationIdentifier:) onTarget:self withObject:nil animated:YES];
//        [self ABC];
        //[self CurrentLocationIdentifier]; // method to get current location
//        _shieldView.hidden=NO;


    }
    else
    {
        _shieldView.hidden=YES;
    }
    
    
    if ([comingFrom isEqualToString:LOCATION]) {
        _shieldView.hidden=NO;
        
        [_locationTableView bringSubviewToFront:self.shieldView];
    }
    else{
        _shieldView.hidden=YES;
        
    }


}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    isSearching=NO;
    _locationTableView.backgroundColor = [UIColor whiteColor];
    
    UIView *border = [UIView new];
    border.backgroundColor = ButtonBlueColor;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(0, 0, self.progressBackView.frame.size.width, 2.0);
    [self.progressBackView addSubview:border];
    
//    _progressBackView.hidden=YES;
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    locationNameArray=[[NSMutableArray alloc] init];
    searchedLocationArray=[[NSMutableArray alloc] init];
    object=[[OfflineDb alloc]init];
    
    
    
    
    
//    _lbl_titleBack.backgroundColor=TopBarColor;
//    _lbl_titleText.text=HomeScreenTitleText;
//    _lbl_titleText.textColor=TopBarTextColor;
//
//    _lbl_cartCount.textColor=cartIconTextColor;
//    _lbl_cartCount.backgroundColor=cartIconBackColor;

    
    brandListArr=[[NSMutableArray alloc] init];
    brandIdArr=[[NSMutableArray alloc] init];
    NSArray *brandData=[object fetch_AttributeValue:@"brand"];
    brandListArr=[brandData valueForKey:@"attribute_label"];
    brandIdArr=[brandData valueForKey:@"attribute_value"];
    
    // toast with a specific duration and position
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]stringForKey:@"startMessage"];
    
    [self.view makeToast:savedValue
                duration:3.0
                position:CSToastPositionBottom];

    
    if ([logOutWindow isEqualToString:@"YES"]) {
        
    
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:@"Are you sure you want to sign out?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
        {
            //Do Some action here
                                                       
            [ProgressHUD showSuccess:@"Sign out successfull" Interaction:NO];
            FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
            [loginManager logOut];

                                                       
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISLOGIN];;
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:USERID];;
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:USERFIRSTNAME];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:USERLASTNAME];
//          [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:USE];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:USEREMAIL];
            
            NSUserDefaults *userDFT=[NSUserDefaults standardUserDefaults];

            [userDFT setObject:@"" forKey:@"fname"];
            [userDFT setObject:@"" forKey:@"lname"];
            [userDFT setObject:@"" forKey:@"postcode"];
            [userDFT setObject:@"" forKey:@"street1"];
            [userDFT setObject:@"" forKey:@"street2"];
            [userDFT setObject:@"" forKey:@"telephone"];
            [userDFT setObject:@"" forKey:@"shipid"];
            [userDFT setObject:@"" forKey:@"city"];

            
            [userDFT setObject:@"" forKey:@"billingfname"];
            [userDFT setObject:@"" forKey:@"billinglname"];
            [userDFT setObject:@"" forKey:@"billingpostcode"];
            [userDFT setObject:@"" forKey:@"billingstreet1"];
            [userDFT setObject:@"" forKey:@"billingstreet2"];
            [userDFT setObject:@"" forKey:@"billingtelephone"];
            [userDFT setObject:@"" forKey:@"billingid"];
            [userDFT setObject:@"" forKey:@"billingcity"];
            
            [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
            }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           //Do Some action here
                                                           
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                           
                                                       }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(handleSingleTap:)];
//    [self.shieldView addGestureRecognizer:singleFingerTap];
//    [singleFingerTap setCancelsTouchesInView:NO];

    [self getDetails];
}

-(void)checkProgressBar{
    
    NSArray *getTotalAmountArray=[object fetch_Total_Amount];
    NSString *amt=[[getTotalAmountArray valueForKey:@"total_cnt"] objectAtIndex:0];
    if (amt.length<=0 || [amt isEqualToString:@"(null)"]) {
        NSLog(@"nill");
        _progressBackViewHeight.constant=0;
        _progressView.hidden=YES;
        _lbl_progressMax.hidden=YES;
        _lbl_progressMin.hidden=YES;
        
        _progressBackView.hidden=YES;
        
    }
    else{
        _progressBackViewHeight.constant=60;
        _progressView.hidden=NO;
        _lbl_progressMax.hidden=NO;
        _lbl_progressMin.hidden=NO;
        
        _progressBackView.hidden=NO;
        
        _lbl_progressMin.text=@"0";
        //total_cnt
        NSString *storedAmt=[[NSUserDefaults standardUserDefaults] valueForKey:MAXLIMIT];
        _lbl_progressMax.text=storedAmt;
        
        if ([amt floatValue]<[_lbl_progressMax.text floatValue]) {
            _lbl_progressMessage.text=FREEFORSHIPPING;
            
            
            _progressBackView.hidden=NO;
            float maxValue = 1.0;
            float minValue = 0.0;
            
            CGFloat diff = ([amt floatValue] - 0);
            CGFloat scope = ([_lbl_progressMax.text floatValue] - 0);
            CGFloat progress;
            
            if(diff != 0.0) {
                progress = diff / scope;
            } else {
                progress = 0.0f;
            }
            
            
            
            float myValueConstrained = MAX(minValue, MIN(maxValue, progress)); // 1.0
            NSLog(@"%f",myValueConstrained);
            _progressView.progress=myValueConstrained;
            
            
            
            
        }
        else{
            //            _progressBackView.hidden=YES;
            _lbl_progressMessage.text=ELIGIBLE;
            _lbl_progressMessage.textColor=[UIColor blackColor];
            _progressBackViewHeight.constant=30;
            _minHeight.constant=0;
            _maxHeight.constant=0;
            _barHeight.constant=0;
            _progressView.hidden=YES;
            _lbl_progressMax.hidden=YES;
            _lbl_progressMin.hidden=YES;
            
        }
        
        
    }
    
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    _shieldView.hidden=YES;
    
    
    //Do stuff here...
}



#pragma mark - Search delegate methods

- (void)filterContentForSearchText:(NSString*)searchText
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF CONTAINS %@",
                                    searchText];
    
     NSArray *sortedArray= [locationNameArray filteredArrayUsingPredicate:resultPredicate];
    searchedLocationArray=[[NSMutableArray alloc] init];
    [searchedLocationArray addObjectsFromArray:sortedArray];

    [_locationTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length == 0) {
        isSearching = NO;
        [self.locationTableView reloadData];
    }
    else {
        isSearching = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    isSearching = YES;
    [self filterContentForSearchText:searchBar.text];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    isSearching = NO;
    [_locationTableView reloadData];
    
}

-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
    }
    [locationManager startUpdatingLocation];
//
//    float currentLat=locationManager.location.coordinate.latitude;
//    float currentLongitude=locationManager.location.coordinate.longitude;
//    NSLog(@"%f,%f",currentLat,currentLongitude);
    
    //------
    
//    coordinate=[CLLocationCoordinate2DMake(CLLocationDegrees latitude, <#CLLocationDegrees longitude#>)];

    
}
#pragma mark - CLLocationManagerDelegate


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert"
                                  message:@"Failed to get your current location."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   
                                                   NSLog(@"HEllO");
                                                   [self dismissViewControllerAnimated:YES completion:nil];

                                                   _shieldView.hidden=NO;
                                                   [_locationTableView bringSubviewToFront:self.shieldView];
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {
        
    }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];

    [HUD hide:YES];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{

//    CLLocation * LocationAtual = [[CLLocation alloc] initWithLatitude:19.002157 longitude:72.841579];
    
    
//    currentLocation =LocationAtual;// [locations objectAtIndex:0];
    
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
//    dispatch_once(&predicate, ^{
        //your code here
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 NSLog(@"\nCurrent Location Detected\n");
                 NSLog(@"placemark %@",placemark);
//                 NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//                 NSString *Address = [[NSString alloc]initWithString:locatedAt];
//                    NSLog(@"placemark %@",placemark.region);
//                    NSLog(@"placemark %@",placemark.country);  // Give Country Name
                    NSLog(@"placemark %@",placemark.locality); // Extract the city name
//                    NSLog(@"location %@",placemark.name);
//                    NSLog(@"location %@",placemark.ocean);
//                    NSLog(@"location %@",placemark.postalCode);
//                    NSLog(@"location %@",placemark.subLocality);
//                    NSLog(@"location %@",placemark.thoroughfare);
//                    NSLog(@"location %@",placemark.subThoroughfare);
//                    NSLog(@"location %@",placemark.subThoroughfare);
//                    NSLog(@"location %@",placemark.subLocality);
//                    NSString *Country = [[NSString alloc]initWithString:placemark.country];
//                    NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
//                    NSLog(@"%@",CountryArea);
                 
                 
                 BOOL IsFound=NO;
                 
                 if ([placemark.locality isEqualToString:@"Mumbai"]){
                     NSString *subLocality = [[[NSString alloc]initWithString:placemark.subLocality] lowercaseString];
                 
                     NSLog(@"subLocality = %@",subLocality);
                 for (int i=0; i<[locationNameArray count]; i++) {
                     if (!([subLocality rangeOfString:[[locationNameArray objectAtIndex:i] lowercaseString]].location==NSNotFound)) {
                         
                         NSLog(@"string contains bla!");
                         
                         IsFound=YES;
                         NSString *selectedLoc=[locationNameArray objectAtIndex:i];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ShowLocationViewFirstTime"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:SELECTEDLOCATION];
                         [HUD hide:YES];
                         [locationManager stopUpdatingLocation];
                         
                         
                         [self getLocationWisePrice:selectedLoc];
                         _shieldView.hidden=YES;
                     }
                   }
                 }else{
//                     [self UIAlertViewControllerMethodTitle:@"TSMC" message:@"Detected location is outside of Mumbai,India area.Please select manual location from list."];
                 }
                 
                 if (!(IsFound)) {
                     _shieldView.hidden=NO;
                     [_locationTableView bringSubviewToFront:self.shieldView];
                 }
                 
                 [HUD hide:YES];
                 [locationManager stopUpdatingLocation];
                 
             }
             else
             {
                 NSLog(@"Geocode failed with error %@", error);
                 NSLog(@"\nCurrent Location Not Detected\n");
                 //return;
                 //             CountryArea = NULL;
             }
             //         ---- For more results
             //          placemark.region);
             //          placemark.country);
             //          placemark.locality);
             //          placemark.name);
             //          placemark.ocean);
             //          placemark.postalCode);
             //          placemark.subLocality);
             //          placemark.location);
             //          ------
         }];
//    });
    
}

-(void)getDetails{
    
    
    @try
    {
        NSString *parentId=PARENTID;
        NSArray *dataArray=[object fetch_parentWiseCategory:parentId];
        NSArray *nArr=[dataArray valueForKey:@"category_name"];
        NSArray *iArr=[dataArray valueForKey:@"category_id"];
        NSArray *imgArr=[dataArray valueForKey:@"category_image"];
        
        for (int j=0; j<[iArr count]; j++) {
            NSString *formattedStr=[[nArr objectAtIndex:j] stringByReplacingOccurrencesOfString:@" ‚Äì " withString:@"-"];
            [nameArray addObject:formattedStr];
            [idArray addObject:[iArr objectAtIndex:j]];
            [imagesArray addObject:[imgArr objectAtIndex:j]];
            
        }
        
        
        [self getLocationDetails];
        [self.homeTableView reloadData];
        
    } @catch (NSException *exception) {
        [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
        
    }
    
    
    
}

-(void)getLocationDetails{
    @try
    {
        NSArray *dataArray=[object fetch_Locations];
        NSArray *location_name=[dataArray valueForKey:@"location_name"];
        
        if (location_name.count>0) {
            for (int j=0; j<[location_name count]; j++) {
                [locationNameArray addObject:[location_name objectAtIndex:j]];
                
            }
        }
        [_locationTableView reloadData];
        
    } @catch (NSException *exception) {
        [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
        
    }
    
    
    
}

#pragma mark TableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableVw
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView==_homeTableView) {
        return idArray.count;

    }
    else{
        if (isSearching) {
            return searchedLocationArray.count;
        }
        else{
            return locationNameArray.count;

        }

    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if (tableView == _homeTableView) {
        [cell.homeBackView.layer setShadowOpacity:0.5];
        [cell.homeBackView.layer setShadowRadius:5.0];
        [cell.homeBackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        cell.homeName.font=HomeScreenLabelFont;
        cell.homeName.textColor=HomeScreenLabelColor;
        cell.homeName.text=[nameArray objectAtIndex:indexPath.row];
        
        
        NSString *image=[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]];
        [cell.homeImageView sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//            [HUD hide:YES];
            
            
        }];

    }
    else{
    
        cell.textLabel.textColor = [UIColor blackColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        if (isSearching) {
            cell.textLabel.text=[searchedLocationArray objectAtIndex:indexPath.row];

        }
        else{
            cell.textLabel.text=[locationNameArray objectAtIndex:indexPath.row];

        }
    }
    
    
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _homeTableView) {
        int height=(self.homeTableView.frame.size.height/3);
        return height;
        
    }
    else{
        int height=50;
        return height;
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //fetchedItemId,*fetchedItemName,*fetchedItemImage,*fetchedFoodType,*fetchedItemDesc,*fetchedSelectedStatus,*fetchedItemPrice,*fetchedCategoryId,*fetchedMsgId,*fetchedSpiceId,*fetchedLevelIds
    
    if (tableView == _homeTableView) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMINPRICE];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMAXPRICE];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MINPRICE];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MAXPRICE];

        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:BRANDVALUE];
        APP_DELEGATE.selectBarndArray=[[NSMutableArray alloc] init];
        [APP_DELEGATE.selectBarndArray addObjectsFromArray:brandIdArr];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISFILTER];
        
        

        ItemsViewController *it=[self.storyboard instantiateViewControllerWithIdentifier:@"ItemsViewController"];
        it.fetchedId=[idArray objectAtIndex:indexPath.row];
        it.fetchedName=[nameArray objectAtIndex:indexPath.row];
        it.view.backgroundColor = [UIColor whiteColor];
        [self.navigationController pushViewController:it animated:YES];

    }
    else{
        
        NSString *selectedLoc;
        if (isSearching) {
            selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
            [_searchBar resignFirstResponder];


        }
        else{
            selectedLoc=[locationNameArray objectAtIndex:indexPath.row];

        
        }
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ShowLocationViewFirstTime"];

        [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:SELECTEDLOCATION];
        [self getLocationWisePrice:selectedLoc];
        
        
    
    }
    
    
    
}
-(void)getLocationWisePrice:(NSString *)selectedLoc{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";


    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        
        
        
        /////Category Webservice
        NSLog(@"Hello Net connectiion is not present....");
        
        //        [HUD hide:YES];
        
        [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        
    }
    else
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *para=[selectedLoc stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
            NSString *url=[NSString stringWithFormat:@"%@&location=%@",API_SEND_LOCATION,para];

            
            NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer=[AFJSONResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
            
            NSError *error = nil;
            @try {
                dictObj = [manager syncPOST:url parameters:nil operation:NULL error:&error];
                
                if (error) {
                    
                    //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
                    //            [ProgressHUD showError:exc Interaction:NO];
                    
                    [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
                    
                    
                }
                else
                {
                    NSLog(@"%@",dictObj);
                    if (dictObj.count >0)
                    {
                        
                        //                     NSDictionary *dictObj=[responseObject valueForKey:@"categories"];
                        NSString *message=[dictObj valueForKey:@"message"];
                        NSString *value=[dictObj valueForKey:@"value"];
                        BOOL success=[dictObj valueForKey:@"success"];
                        

                        if (success) {
                            [[NSUserDefaults standardUserDefaults] setObject:value forKey:SELECTEDLOCATIONPRICE];
                            [ProgressHUD showSuccess:@"Location set successfully." Interaction:NO];
                            
                            
                        }
                        else{
                            [ProgressHUD showError:@"Something went wrong.Please try again." Interaction:NO];
                        
                        }
                        
                        _shieldView.hidden=YES;
                        [HUD hide:YES];
                        
                    }
                }
                
            } @catch (NSException *exception)
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
                
                
            }
            
            
        });
        
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)menuMethod:(id)sender {
    
    [self.revealViewController revealToggle:nil];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

}
- (IBAction)cartMethod:(id)sender {
    NSString *cartCount=[[NSUserDefaults standardUserDefaults] valueForKey:@"BADGECOUNT"];

    if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] )
    {
        [ProgressHUD showError:@"Cart Empty"];
    }
    else
    {
        CartController *crt=[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
        crt.comingFrom=@"ViewC";
        [self.navigationController pushViewController:crt animated:YES];
    }
    
}
- (IBAction)searchMethod:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMAXPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MAXPRICE];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:BRANDVALUE];
    APP_DELEGATE.selectBarndArray=[[NSMutableArray alloc] init];
    [APP_DELEGATE.selectBarndArray addObjectsFromArray:brandIdArr];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISFILTER];
    

    SearchController *crt=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchController"];
    [self.navigationController pushViewController:crt animated:YES];
}


- (IBAction)callUsBtnClicked:(id)sender
{
    NSString *phNo = @"+919699933330";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Alert" message:@"Call facility is not available!!!"];
    }
}


#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    //[alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
