//
//  ProfileUpdateViewController.h
//  TSMC
//
//  Created by user on 03/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppConstant.h"
#import "SWRevealViewController.h"
#import "CustomTableViewCell.h"
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface ProfileUpdateViewController : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UILabel *_titleLbl;
    IBOutlet UILabel *_titleText;
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *userInfoView;
    IBOutlet UIButton *editBtn;
    IBOutlet UITextField *EmailTF;
    IBOutlet UITextField *FirstNameTF;
    IBOutlet UITextField *LastNameTF;
    IBOutlet UITextField *PasswordTF;
    IBOutlet UITextField *ConfirmPasswordTF;
    IBOutlet UILabel *confirmPwrdLabel;
    IBOutlet UIButton *cancelBtn;
    IBOutlet UIButton *updateBtn;
    
    IBOutlet UIButton *btn_cart;
    IBOutlet UILabel *myAddressesBtn;
    IBOutlet UIButton *addAddressesBtn2;
    IBOutlet UITableView *addressesTblView;
    
    
    
    IBOutlet NSLayoutConstraint *userInfoViewHeight;
    IBOutlet NSLayoutConstraint *confirmPasswordHeight;
    IBOutlet NSLayoutConstraint *confirmPasswordLblHeight;
    IBOutlet NSLayoutConstraint *cancelBtnHeight;
    IBOutlet NSLayoutConstraint *updateBtnHeight;
    IBOutlet NSLayoutConstraint *myAddresesTop;
   
    IBOutlet NSLayoutConstraint *addressesTblViewHeight;
    IBOutlet UILabel *lbl_badgeCount;
    
    MBProgressHUD *HUD;
    
    NSUserDefaults *defaults;
    NSMutableArray *addressesArr,*addressesArr2,*addressesIDArr,*decodedAddressesArr,*streerAddressArr,*decodeStreetArr,*finalAddressArr,*cityArr,*telephoneArr,*ChkDefaultArr,*fNameArr,*lNameArr,*postCodeArray,*countryListArray,*stateListArray;
    
    NSString *checkedData,*ChkAPICall;
}
- (IBAction)backBtn_Clicked:(id)sender;
- (IBAction)editBtn_Clicked:(id)sender;
- (IBAction)cancelBtn_Clicked:(id)sender;
- (IBAction)updateBtn_Clicked:(id)sender;
- (IBAction)cartMethod:(id)sender;
- (IBAction)addAddressesBtn2_Clicked:(id)sender;

- (IBAction)EmailTFDidEndEditing:(id)sender;
- (IBAction)FirstNameTFDidEndEditing:(id)sender;
- (IBAction)LastNameTFDidEndEditing:(id)sender;
- (IBAction)passwordTFDidEndEditing:(id)sender;
- (IBAction)confirmPasswordTFDidEndEditing:(id)sender;
@end
