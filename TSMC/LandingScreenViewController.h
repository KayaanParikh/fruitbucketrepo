//
//  LandingScreenViewController.h
//  TSMC
//
//  Created by Rahul Lekurwale on 22/06/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressHUD.h"
#import "RESTConnector.h"

@interface LandingScreenViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *img_background;
@property (nonatomic, strong) RESTConnector *connectReq;

@end
