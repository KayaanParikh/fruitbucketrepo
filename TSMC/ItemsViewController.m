//
//  ItemsViewController2.m
//  TSMC
//
//  Created by user on 13/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "ItemsViewController.h"
#import "OfflineDb.h"
#import "HelperClass.h"
#import "WebservicesClass.h"
#import "HMSegmentedControl.h"
#import "ItemDetailsController.h"
#import "AppConstant.h"
#import "CartController.h"
#import "SWRevealViewController.h"
#import "FilterController.h"
#import "SearchController.h"
#import "CartController.h"
#import "wishListHelper.h"
#import "UIBarButtonItem+Badge.h"


@interface ItemsViewController ()
{
    
    UISwipeGestureRecognizer *gestureRight,*gestureLeft;
    wishListHelper *wishObj;
    
    
    NSMutableArray *nameArray,*idArray,*imagesArray,*categoryIdArr,*categoryNameArr,*imageMainArray,*imageThumbArray,*imageSmallArray,*weightArray,*brandIndexArr,*brandValueArr,*outOfStockTiminingArray;
    int offset,limit;
    NSMutableArray *descriptionArray,*skuArray,*shortDescArray,*priceArray,*idContainInLocal,*localQtyArr,*foodTypeArr,*mirchImagesArr,*mirchIndexArr;
    NSMutableArray *localCutOpIdArray,*localCutOpValArray,*localCutOpPriceArray,*localWeightOpIdArray,*localWeightOpValArray,*localWeightOpPriceArray,*localTypeOpIdArray,*localTypeOpValArray,*localTypeOpPriceArray,*optionIdArray,*localTypeOpLabelArray,*localWeightOpLabelArray,*localCutOpLabelArray;
    int quntity;
    ItemsViewController *items;
    NSString *yesrr;
    NSMutableArray *dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray,*spicelevelArray,*messagesArray;
    NSMutableArray *wishlistIdArray,*containInWishListArray;
    NSString *categoryId;
    int indexToShow;
    UILabel *lbl_line;
    OfflineDb *object;
    
    
    //Wight required array's
    NSMutableArray *weightlabelArray,*weightOptionIdArray;
    NSMutableArray *weightValueArray;
    NSMutableArray *weightpriceArray;
    NSMutableArray *pickerWeightLabelArr,*pickerWeightValueArr,*pickerWeightPriceArr;
    NSMutableArray *selectedWeightLabelIndex,*selectedWeightValueIndex,*selectedWeightPriceIndex;
    
    
    //Cut required array's
    NSMutableArray *cutlabelArray,*cutOptionIdArray;
    NSMutableArray *cutValueArray;
    NSMutableArray *cutpriceArray;
    NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
    NSMutableArray *selectedCutLabelIndex,*selectedCutValueIndex,*selectedCutPriceIndex;
    
    //Type required array's
    NSMutableArray *typelabelArray,*typeOptionIdArray;
    NSMutableArray *typeValueArray;
    NSMutableArray *typepriceArray;
    NSMutableArray *pickerTypeLabelArr,*pickerTypeValueArr,*pickerTypePriceArr;
    NSMutableArray *selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    NSString *selectedLabel,*selectedValue,*selectedPrice;
    
    
    NSString *outofstock;
    NSMutableArray *outofstockArray;
    
}
@property (nonatomic, strong) HMSegmentedControl *segmentedControl3;

@end

@implementation ItemsViewController
@synthesize fetchedId,fetchedName,spicesPicker;

-(void)viewWillAppear:(BOOL)animated{
    
    // Build your regular UIBarButtonItem with Custom View
//    UIImage *image = [UIImage imageNamed:@"CartNew"];
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    button.frame = CGRectMake(0,0,image.size.width, image.size.height);
//    [button addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchDown];
//    [button setBackgroundImage:image forState:UIControlStateNormal];
//
//    // Make BarButton Item
//    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
//    self.navigationItem.rightBarButtonItem = navLeftButton;
//    self.navigationItem.rightBarButtonItem.badgeValue = @"1";
    
    
    [_pageTableView setHidden:YES];
    [self checkProgressBar];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    // [_pageTableView setSeparatorColor:[UIColor clearColor]];
    
    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    categoryIdArr=[[NSMutableArray alloc] init];
    categoryNameArr=[[NSMutableArray alloc] init];
    wishlistIdArray=[[NSMutableArray alloc] init];
    containInWishListArray=[[NSMutableArray alloc] init];
    //    weightlabelArray=[[NSMutableArray alloc] init];
    //    weightValueArray=[[NSMutableArray alloc] init];
    //    weightpriceArray=[[NSMutableArray alloc] init];
    
    
    
    NSArray *str=[object fetch_CartDetails];
    NSLog(@"cart details = %@",str);
    NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
    if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
        _lbl_badgeCount.hidden=YES;
        self.btnRightCart.badge.hidden = YES;
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
        
    }
    else{
        self.navigationItem.rightBarButtonItem.badge.hidden = false;
        _lbl_badgeCount.hidden=NO;
        
    
        [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
        _lbl_badgeCount.text=cartCount;
        
        // Build your regular UIBarButtonItem with Custom View
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
        [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
        
        
    }
    
    self.lbl_badgeCount.layer.masksToBounds=YES;
    self.lbl_badgeCount.layer.cornerRadius=self.lbl_badgeCount.frame.size.width/2;
    
    
    
    //Offline Data
    //    OfflineDb *object=[[OfflineDb alloc]init];
    NSArray *dataArray=[object fetch_parentWiseCategory:fetchedId];
    NSArray *nArr = [dataArray valueForKey:@"category_name"];
    NSArray *iArr = [dataArray valueForKey:@"category_id"];
    
    
    if (iArr.count == 0) {
        [categoryIdArr addObject:fetchedId];
        [categoryNameArr addObject:fetchedName];
        
    }else{
        for (int i=0; i<[iArr count]; i++) {
            [categoryNameArr addObject:[nArr objectAtIndex:i]];
            [categoryIdArr addObject:[iArr objectAtIndex:i]];
        }
        
    }
    
    [self addHorzontalButtons:categoryIdArr withName:categoryNameArr];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    //    _progerssBackView.hidden=YES;
    
    UIView *border = [UIView new];
    border.backgroundColor = ButtonBlueColor;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(0, 0, self.progerssBackView.frame.size.width, 2.0);
    [self.progerssBackView addSubview:border];
    
    _pageTableView.backgroundColor = [UIColor whiteColor];
    
//    _titleBack.backgroundColor=TopBarColor;
//    _lbl_titleText.font=TopBarFont;
//    _lbl_titleText.textColor=TopBarTextColor;
//    _lbl_badgeCount.textColor=cartIconTextColor;
//    _lbl_badgeCount.backgroundColor=cartIconBackColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 480, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont boldSystemFontOfSize: 15.0f];
//    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = cartIconTextColor;
    label.text = fetchedName.uppercaseString;
    
    self.navigationItem.titleView = label;
    
    quntity=0;
    indexToShow=0;
    limit=99;
    offset=1;
    object=[[OfflineDb alloc]init];
    
    
    wishObj=[wishListHelper sharedInstance];
    mirchImagesArr=[[NSMutableArray alloc] initWithObjects:@"1.png",@"2.png",@"3.png", nil];
    mirchIndexArr=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3", nil];
    
    //    [self checkProgressBar];
    //    if ([comingFrom isEqualToString:@"MENU"]) {
    
    
    //SwipeGesture
    //    UISwipeGestureRecognizer *gestureRight;
    //    UISwipeGestureRecognizer *gestureLeft;
    gestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    gestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    [gestureLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    gestureRight.delegate=self;
    gestureLeft.delegate=self;
    
//    [[self tableBackView] addGestureRecognizer:gestureRight];
//    [[self tableBackView] addGestureRecognizer:gestureLeft];    //    self.imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexToShow]];
}


-(void)checkProgressBar{
    
    NSArray *getTotalAmountArray=[object fetch_Total_Amount];
    NSString *amt=[[getTotalAmountArray valueForKey:@"total_cnt"] objectAtIndex:0];
    if (amt.length<=0 || [amt isEqualToString:@"(null)"]) {
        NSLog(@"nill");
        _progressBackViewHeight.constant=0;
        _progressView.hidden=YES;
        _lbl_progressMax.hidden=YES;
        _lbl_progressMin.hidden=YES;
        _progerssBackView.hidden=YES;
        
    }else{
        _progressBackViewHeight.constant=60;
        _progressView.hidden=NO;
        _lbl_progressMax.hidden=NO;
        _lbl_progressMin.hidden=NO;
        _progerssBackView.hidden=NO;
        
        _lbl_progressMin.text=@"0";
        //total_cnt
        NSString *storedAmt=[[NSUserDefaults standardUserDefaults] valueForKey:MAXLIMIT];
        _lbl_progressMax.text=storedAmt;
        
        if([amt floatValue]<[_lbl_progressMax.text floatValue]) {
            _lbl_progressMessage.text=FREEFORSHIPPING;
            _progerssBackView.hidden=NO;
            
            float maxValue = 1.0;
            float minValue = 0.0;
            
            CGFloat diff = ([amt floatValue] - 0);
            CGFloat scope = ([_lbl_progressMax.text floatValue] - 0);
            CGFloat progress;
            
            if(diff != 0.0) {
                progress = diff / scope;
            } else {
                progress = 0.0f;
            }
            
            float myValueConstrained = MAX(minValue, MIN(maxValue, progress)); // 1.0
            NSLog(@"myValueConstrained = %f",myValueConstrained);
            _progressView.progress=myValueConstrained;
            
        }else{
            _lbl_progressMessage.text=ELIGIBLE;
            _progressBackViewHeight.constant=30;
            _minHeight.constant=0;
            _maxHeight.constant=0;
            _barHeight.constant=0;
            _progressView.hidden=YES;
            _lbl_progressMax.hidden=YES;
            _lbl_progressMin.hidden=YES;
            
        }
    }
}

#pragma mark -Gesture Recognizer Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([recogniseGesture isEqualToString:@"SwipeGesture"])
    {
        [_pageTableView setUserInteractionEnabled:YES];
        SWRevealViewController *revealViewController = self.revealViewController;
        revealViewController.panGestureRecognizer.enabled=NO;
        //        [gestureRecognizer requireGestureRecognizerToFail:revealViewController.panGestureRecognizer];
        
    }
    else if ([recogniseGesture isEqualToString:@"PanGesture"])
    {
        SWRevealViewController *revealViewController = self.revealViewController;
        revealViewController.panGestureRecognizer.enabled=YES;
        //[gestureRecognizer requireGestureRecognizerToFail:gestureRight];
        //        [gestureRecognizer requireGestureRecognizerToFail:gestureLeft];
        
    }
    return YES;
}

- (void)swipeRight:(UISwipeGestureRecognizer *)gesture
{
    //    indexToShow--;
    SWRevealViewController *revealViewController = self.revealViewController;
    [gesture requireGestureRecognizerToFail:revealViewController.panGestureRecognizer];
    //
    if (indexToShow > 0)
    {
        indexToShow--;
        [self.segmentedControl3 setSelectedSegmentIndex:indexToShow animated:YES];
        
        //        self.imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexToShow]];
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
        
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
        //        } completion:nil];
        
    }
    
    
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)gesture
{
    SWRevealViewController *revealViewController = self.revealViewController;
    [gesture requireGestureRecognizerToFail:revealViewController.panGestureRecognizer];
    
    if (indexToShow < categoryIdArr.count - 1)
    {
        indexToShow++;
        [self.segmentedControl3 setSelectedSegmentIndex:indexToShow animated:YES];
        
        //        self.imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexToShow]];
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
        
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
        
        
        //        } completion:nil];
    }
    
}

#pragma mark -Segment Control Method
-(void)addHorzontalButtons:(NSMutableArray *)IdArray withName:(NSMutableArray *)NameArray
{
    
    _segmentedControl3 = [[HMSegmentedControl alloc] initWithSectionTitles:NameArray];
    [_segmentedControl3 setFrame:CGRectMake(0, 0,self.view.frame.size.width, 40)];
    [_segmentedControl3 setIndexChangeBlock:^(NSInteger index) {
        NSLog(@"Selected index %ld (via block)", (long)index);
    }];
    _segmentedControl3.selectionIndicatorHeight = 2.0f;
    //_segmentedControl3.backgroundColor = TopBarColor;
    _segmentedControl3.backgroundColor = [UIColor whiteColor];
    _segmentedControl3.titleTextAttributes = @{NSForegroundColorAttributeName : unselectedSegmentTextColor};
    _segmentedControl3.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : TopBarColor};
    _segmentedControl3.selectionIndicatorColor = TopBarColor;
    _segmentedControl3.selectionIndicatorBoxColor = [UIColor whiteColor];
    //_segmentedControl3.selectionIndicatorBoxColor = YellowColor;
    _segmentedControl3.selectionIndicatorBoxOpacity = 1.0;
    _segmentedControl3.selectionStyle = HMSegmentedControlSelectionStyleBox;
    _segmentedControl3.selectedSegmentIndex = indexToShow;
    _segmentedControl3.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _segmentedControl3.shouldAnimateUserSelection = NO;
    _segmentedControl3.tag = 2;
    [_segmentedControl3 addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:_segmentedControl3];
    
    categoryId=[categoryIdArr objectAtIndex:indexToShow];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
    
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    
    
    long row = segmentedControl.selectedSegmentIndex;
    NSLog(@"row = %ld",row);
    
    if (indexToShow<(int)row) {
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    else if (indexToShow>(int)row){
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    indexToShow=(int)row;
    
    categoryId=[categoryIdArr objectAtIndex:indexToShow];
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
    
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld", (long)segmentedControl.selectedSegmentIndex);
}

- (IBAction)horizontalMenuButtonMethod:(id)sender{
    long row = [sender tag];
    NSLog(@"%ld",row);
    
    if (indexToShow<(int)row) {
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    else if (indexToShow>(int)row){
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    indexToShow=(int)row;
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
    
}


- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

#pragma  mark -UITableView Datasource Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableVw{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return idArray.count;
}

/*-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    recogniseGesture=@"SwipeGesture";
    if (cell == nil){
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    @try{
        
        cell.lbl_name.text=[nameArray objectAtIndex:indexPath.row];
        cell.lbl_shortDesc.text=[shortDescArray objectAtIndex:indexPath.row];
        NSString *image=[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]];
        [cell.img_product sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            [HUD hide:YES];
            
            
        }];
        
        CGSize constraint = CGSizeMake(cell.lbl_shortDesc.frame.size.width, CGFLOAT_MAX);
        CGSize size;
        
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [cell.lbl_shortDesc.text boundingRectWithSize:constraint
                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                attributes:@{NSFontAttributeName:cell.lbl_shortDesc.font}
                                                                   context:context].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        
        cell.shortDescHeight.constant=size.height;
        
        cell.lbl_qty.text = @"0";
        
        if([[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"0"] || [[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"(null)"] || [brandValueArr objectAtIndex:indexPath.row] == nil){
            cell.lbl_brandName.text=@"";
            cell.brandHeight.constant=0;
            cell.lbl_brandName.hidden=YES;
        }else{
            cell.lbl_brandName.hidden=NO;
            cell.brandHeight.constant=23;
            cell.lbl_brandName.text=[brandValueArr objectAtIndex:indexPath.row];
        }
        cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[[priceArray objectAtIndex:indexPath.row] floatValue]];
        
        NSString *weiarrr=[weightArray objectAtIndex:indexPath.row];
        if ((NSString *)[NSNull null] == weiarrr){
            [weightArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        
        cell.lbl_noAttrWeight.text=[NSString stringWithFormat:@"Weight: %.f gm",[[weightArray objectAtIndex:indexPath.row] floatValue]];
        
        if ([[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0.0000"] || [[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0"]){
            cell.weightLblHeight.constant=0;
            cell.lbl_noAttrWeightHeight.constant=0;
        }else{
            cell.weightLblHeight.constant=15;
            cell.lbl_noAttrWeightHeight.constant=15;
        }
        
        if ([[containInWishListArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
        }
        else{
            
            [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
        }
        
        cell.btn_add.tag=indexPath.row;
        cell.btn_sub.tag=indexPath.row;
        cell.btn_addCart.tag=indexPath.row;
        cell.btn_weight.tag=indexPath.row;
        cell.btn_cut.tag=indexPath.row;
        cell.btn_type.tag=indexPath.row;
        cell.btn_wish.tag=indexPath.row;
        
        cell.btn_weightHeight.constant=0;
        cell.lbl_weightHeight.constant=0;
        cell.btn_cutHeight.constant=0;
        cell.lbl_cutHeight.constant=0;
        cell.btn_typeHeight.constant=0;
        cell.lbl_typeHeight.constant=0;
        cell.btn_weight.hidden=YES;
        cell.lbl_weight.hidden=YES;
        cell.btn_cut.hidden=YES;
        cell.lbl_cut.hidden=YES;
        cell.btn_type.hidden=YES;
        cell.lbl_type.hidden=YES;
        cell.lblOutOfStock.hidden = YES;
        //cell.outOFStockHeightConstraints.constant = 0;
        
        [cell.contentBackView.layer setShadowOpacity:0.0];
        [cell.contentBackView.layer setShadowRadius:1.0];
        [cell.contentBackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        NSString *outOfStockTiming = [outOfStockTiminingArray objectAtIndex:indexPath.row];
        if(![outOfStockTiming isEqualToString:@""])
        {
            
            NSDate *currentTime = [NSDate date];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"HH"];
            
            NSString *hourCountString = [timeFormatter stringFromDate:currentTime];
            
            int hourCountInt = [hourCountString intValue];
            
            NSLog(@"hourCountInt=%d",hourCountInt);
            //time between 12PM - 8PM.
            //if(hourCountInt >= 12 && hourCountInt < 20)
            if(hourCountInt >= 14 && hourCountInt < 20)
            {
                if([[dropLabelArr objectAtIndex:indexPath.row] count ] > 0){
                    
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    int a=40;
                    
                    if ([[dropLabelArr objectAtIndex:indexPath.row] count]>0)
                    {
                        // cell2.qtyBackViewConstraint.constant=30;
                        
                        for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                        {
                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                            {
                                a=a+cell.btn_cutHeight.constant;
                                
                                cell.btn_cutHeight.constant=21;
                                cell.lbl_cutHeight.constant=21;
                                cell.btn_cut.hidden=NO;
                                cell.lbl_cut.hidden=NO;
                                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                                {
                                    cell.lbl_cut.text=@"Pieces:";
                                }
                                
                                if([[cutlabelArray objectAtIndex:indexPath.row] count]>0){
                                    // =[[[cutpriceArray objectAtIndex:i] objectAtIndex:0] intValue];
                                    [cell.btn_cut setTitle:[[cutlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                                }
                            }
                            
                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"]){
                                
                                a = a+cell.btn_weightHeight.constant;
                                
                                cell.btn_weightHeight.constant=21;
                                cell.lbl_weightHeight.constant=21;
                                cell.btn_weight.hidden=NO;
                                cell.lbl_weight.hidden=NO;
                                if([[weightlabelArray objectAtIndex:indexPath.row] count]>0){
                                    
                                    [cell.btn_weight setTitle:[[weightlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                                }
                                
                            }
                            
                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"]){
                                a=a+cell.btn_typeHeight.constant;
                                
                                cell.btn_typeHeight.constant=21;
                                cell.lbl_typeHeight.constant=21;
                                cell.btn_type.hidden=NO;
                                cell.lbl_type.hidden=NO;
                                if([[typelabelArray objectAtIndex:indexPath.row] count]>0){
                                    [cell.btn_type setTitle:[[typelabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                                }
                            }
                        }
                    }else{
                        
                        cell.btn_weightHeight.constant=0;
                        cell.lbl_weightHeight.constant=0;
                        cell.btn_cutHeight.constant=0;
                        cell.lbl_cutHeight.constant=0;
                        cell.btn_typeHeight.constant=0;
                        cell.lbl_typeHeight.constant=0;
                        cell.btn_weight.hidden=YES;
                        cell.lbl_weight.hidden=YES;
                        cell.btn_cut.hidden=YES;
                        cell.lbl_cut.hidden=YES;
                        cell.btn_type.hidden=YES;
                        cell.lbl_type.hidden=YES;
                        cell.lblOutOfStock.hidden = YES;
                        cell.outOFStockHeightConstraints.constant = 0;
                        
                    }
                    
                    cell.backViewConstraint.constant = cell.nameHeight.constant+cell.shortDescHeight.constant+cell.brandHeight.constant + a + 30;
                }
                
            }
        
            else
            {
                NSArray *components = [[outOfStockTiminingArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"-"];
                
                NSDateFormatter *firstTime = [[NSDateFormatter alloc]init];
                [firstTime setDateFormat:@"HH"];
                NSLog(@"current time------TTTTT%@",[firstTime stringFromDate:[NSDate date]]);
                if(([firstTime stringFromDate:[NSDate date]] > [components objectAtIndex:0]) || ([firstTime stringFromDate:[NSDate date]] < [components objectAtIndex:3]))
                {
                    
                    cell.btn_weightHeight.constant=0;
                    cell.lbl_weightHeight.constant=0;
                    cell.btn_cutHeight.constant=0;
                    cell.lbl_cutHeight.constant=0;
                    cell.btn_typeHeight.constant=0;
                    cell.lbl_typeHeight.constant=0;
                    cell.btn_weight.hidden=YES;
                    cell.lbl_weight.hidden=YES;
                    cell.btn_cut.hidden=YES;
                    cell.lbl_cut.hidden=YES;
                    cell.btn_type.hidden=YES;
                    cell.lbl_type.hidden=YES;
                    cell.btn_add.hidden = YES;
                    cell.lbl_qty.hidden = YES;
                    cell.btn_sub.hidden = YES;
                    cell.lbl_price.hidden = YES;
                    cell.btn_addCart.hidden = YES;
                    cell.qtyBackView.hidden = YES;
                    cell.lblOutOfStock.hidden = NO;
                    cell.lblOutOfStock.text = @"Currently out of Stock. Will be available for sale in 12PM to 8PM Hours.";
                    cell.lblOutOfStock.numberOfLines = 4;
                    CGSize constraint = CGSizeMake(cell.lblOutOfStock.frame.size.width, CGFLOAT_MAX);
                    CGSize size;
                    
                    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
                    CGSize boundingBox = [cell.lblOutOfStock.text boundingRectWithSize:constraint
                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                            attributes:@{NSFontAttributeName:cell.lblOutOfStock.font}
                                                                               context:context].size;
                    
                    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
                    
                    cell.outOFStockHeightConstraints.constant = size.height;
                    NSLog(@"Height---------787874377-----%f",size.height);
                    
                    cell.backViewConstraint.constant = cell.nameHeight.constant+cell.shortDescHeight.constant+cell.brandHeight.constant + cell.outOFStockHeightConstraints.constant;
                    NSLog(@"Height---------787874377-----%f",cell.backViewConstraint.constant);
 
            }

            }
            
            
    
        }else{
            if([[dropLabelArr objectAtIndex:indexPath.row] count ] > 0){
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                int a=40;
                
                if ([[dropLabelArr objectAtIndex:indexPath.row] count]>0)
                {
                    // cell2.qtyBackViewConstraint.constant=30;
                    
                    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                    {
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                        {
                            a=a+cell.btn_cutHeight.constant;
                            
                            cell.btn_cutHeight.constant=21;
                            cell.lbl_cutHeight.constant=21;
                            cell.btn_cut.hidden=NO;
                            cell.lbl_cut.hidden=NO;
                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                            {
                                cell.lbl_cut.text=@"Pieces:";
                            }
                            
                            if([[cutlabelArray objectAtIndex:indexPath.row] count]>0){
                                // =[[[cutpriceArray objectAtIndex:i] objectAtIndex:0] intValue];
                                [cell.btn_cut setTitle:[[cutlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                            }
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"]){
                            
                            a = a+cell.btn_weightHeight.constant;
                            
                            cell.btn_weightHeight.constant=21;
                            cell.lbl_weightHeight.constant=21;
                            cell.btn_weight.hidden=NO;
                            cell.lbl_weight.hidden=NO;
                            if([[weightlabelArray objectAtIndex:indexPath.row] count]>0){
                                
                                [cell.btn_weight setTitle:[[weightlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                            }
                            
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"]){
                            a=a+cell.btn_typeHeight.constant;
                            
                            cell.btn_typeHeight.constant=21;
                            cell.lbl_typeHeight.constant=21;
                            cell.btn_type.hidden=NO;
                            cell.lbl_type.hidden=NO;
                            if([[typelabelArray objectAtIndex:indexPath.row] count]>0){
                                [cell.btn_type setTitle:[[typelabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                            }
                        }
                    }
                }else{
                    
                    cell.btn_weightHeight.constant=0;
                    cell.lbl_weightHeight.constant=0;
                    cell.btn_cutHeight.constant=0;
                    cell.lbl_cutHeight.constant=0;
                    cell.btn_typeHeight.constant=0;
                    cell.lbl_typeHeight.constant=0;
                    cell.btn_weight.hidden=YES;
                    cell.lbl_weight.hidden=YES;
                    cell.btn_cut.hidden=YES;
                    cell.lbl_cut.hidden=YES;
                    cell.btn_type.hidden=YES;
                    cell.lbl_type.hidden=YES;
                    cell.lblOutOfStock.hidden = YES;
                    cell.outOFStockHeightConstraints.constant = 0;
                    
                }
                
                cell.backViewConstraint.constant = cell.nameHeight.constant+cell.shortDescHeight.constant+cell.brandHeight.constant + a + 30;
            }
        }
    }
    @catch (NSException *exception){
        [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell2 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    long a = 0;
    if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
    {
        NSString *outOfStockTiming = [outOfStockTiminingArray objectAtIndex:indexPath.row];
        if(![outOfStockTiming isEqualToString:@""])
        {
            NSArray *components = [[outOfStockTiminingArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"-"];
            
            NSDateFormatter *firstTime = [[NSDateFormatter alloc]init];
            [firstTime setDateFormat:@"HH"];
            NSLog(@"current time------TTTTT%@",[firstTime stringFromDate:[NSDate date]]);
            if(([firstTime stringFromDate:[NSDate date]] > [components objectAtIndex:0]) || ([firstTime stringFromDate:[NSDate date]] < [components objectAtIndex:3])){
                cell2.lblOutOfStock.text = @"Currently out of Stock. Will be available for sale in 12PM to 8PM Hours.";
                cell2.lblOutOfStock.numberOfLines = 4;
                CGSize constraint = CGSizeMake(cell2.lblOutOfStock.frame.size.width, CGFLOAT_MAX);
                CGSize size;
                
                NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
                CGSize boundingBox = [cell2.lblOutOfStock.text boundingRectWithSize:constraint
                                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                                        attributes:@{NSFontAttributeName:cell2.lblOutOfStock.font}
                                                                           context:context].size;
                
                size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
                cell2.cartWeightHeight.constant = 0;
                a = cell2.nameHeight.constant+cell2.shortDescHeight.constant+cell2.brandHeight.constant + cell2.outOFStockHeightConstraints.constant;
                
            }else{
                a = cell2.nameHeight.constant+cell2.brandHeight.constant+cell2.cartWeightHeight.constant+70;
                
                //cell2.qtyBackViewConstraint.constant;
                
                for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                {
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                    {
                        a=a+cell2.btn_cutHeight.constant;
                        
                    }
                    
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
                    {
                        a=a+cell2.btn_weightHeight.constant;
                        
                        
                    }
                    
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
                    {
                        a=a+cell2.btn_typeHeight.constant;
                        
                    }
                    
                }
            }
            
        }
        else
        {
            a = cell2.nameHeight.constant+cell2.brandHeight.constant+cell2.cartWeightHeight.constant+120;
            
            //cell2.qtyBackViewConstraint.constant;
            
            for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
            {
                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                {
                    a=a+cell2.btn_cutHeight.constant;
                    
                }
                
                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
                {
                    a=a+cell2.btn_weightHeight.constant;
                    
                    
                }
                
                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
                {
                    a=a+cell2.btn_typeHeight.constant;
                    
                }
                
            }
        }
        
        return a+40;
    }
    else
    {
        return 170.0f;
    }
}*/

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    recogniseGesture=@"SwipeGesture";
    if (cell == nil){
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    @try{
        
        [cell.viewPlusMinus.layer setCornerRadius:15.0f];
        cell.viewPlusMinus.layer.borderColor = TopBarTextColor.CGColor;
        cell.viewPlusMinus.layer.borderWidth = 0.5f;
        [cell.viewPlusMinus.layer setMasksToBounds:YES];
        
        cell.lbl_name.text=[nameArray objectAtIndex:indexPath.row];
        NSString *image=[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]];
        [cell.img_product sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            [HUD hide:YES];
            
            
        }];
        
        
        cell.shortDescHeight.constant=0;
        cell.lbl_qty.text = @"1";
        
        if([[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"0"] || [[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"(null)"] || [brandValueArr objectAtIndex:indexPath.row] == nil){
            cell.lbl_brandName.text=@"";
            cell.brandHeight.constant=0;
            cell.lbl_brandName.hidden=YES;
        }else{
            cell.lbl_brandName.hidden=NO;
            cell.brandHeight.constant=23;
            cell.lbl_brandName.text=[brandValueArr objectAtIndex:indexPath.row];
        }
        cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[[priceArray objectAtIndex:indexPath.row] floatValue]];
        
        
        cell.weightLblHeight.constant=0;
        cell.lbl_noAttrWeightHeight.constant=0;

        
        if ([[containInWishListArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
        }
        else{
            
            [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
        }
        
        cell.btn_add.tag=indexPath.row;
        cell.btn_sub.tag=indexPath.row;
        cell.btn_addCart.tag=indexPath.row;
        cell.btn_weight.tag=indexPath.row;
        cell.btn_cut.tag=indexPath.row;
        cell.btn_type.tag=indexPath.row;
        cell.btn_wish.tag=indexPath.row;
        
        cell.btn_weightHeight.constant=0;
        cell.lbl_weightHeight.constant=0;
        cell.btn_cutHeight.constant=0;
        cell.lbl_cutHeight.constant=0;
        cell.btn_typeHeight.constant=0;
        cell.lbl_typeHeight.constant=0;
        cell.btn_weight.hidden=YES;
        cell.lbl_weight.hidden=YES;
        cell.btn_cut.hidden=YES;
        cell.lbl_cut.hidden=YES;
        cell.btn_type.hidden=YES;
        cell.lbl_type.hidden=YES;
        cell.lbl_price.hidden = NO;
        cell.btn_addCart.hidden = YES;
        cell.qtyBackView.hidden = NO;
        cell.btn_add.hidden=YES;
        cell.btn_sub.hidden=NO;
        cell.lbl_qty.hidden=YES;
        cell.lblOutOfStock.hidden = YES;
        cell.outOFStockHeightConstraints.constant = 0;
        
        [cell.contentBackView.layer setShadowOpacity:0.0];
        [cell.contentBackView.layer setShadowRadius:1.0];
        [cell.contentBackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        NSString *outOfStockTiming = [outOfStockTiminingArray objectAtIndex:indexPath.row];
        if(![outOfStockTiming isEqualToString:@""])
        {
            
            NSDate *currentTime = [NSDate date];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"HH"];
            
            NSString *hourCountString = [timeFormatter stringFromDate:currentTime];
            
            int hourCountInt = [hourCountString intValue];
            
            NSLog(@"hourCountInt=%d",hourCountInt);
            //time between 12PM - 8PM.
            //if(hourCountInt >= 12 && hourCountInt < 20)
            if(hourCountInt >= 12 && hourCountInt < 20)
            {
                if([[dropLabelArr objectAtIndex:indexPath.row] count ] > 0){
                    
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    int a=40;
                    
                    cell.lbl_price.hidden = NO;
                    cell.btn_addCart.hidden = YES;
                    cell.qtyBackView.hidden = NO;
                    
                    if ([[dropLabelArr objectAtIndex:indexPath.row] count]>0)
                    {
                        cell.outOFStockHeightConstraints.constant=0;
                        for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                        {
                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                            {
                                a=a+cell.btn_cutHeight.constant;
                                
                                cell.btn_cutHeight.constant=21;
                                cell.lbl_cutHeight.constant=21;
                                cell.btn_cut.hidden=NO;
                                cell.lbl_cut.hidden=NO;
                                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                                {
                                    cell.lbl_cut.text=@"Pieces:";
                                }
                                
                                if([[cutlabelArray objectAtIndex:indexPath.row] count]>0){
                                    // =[[[cutpriceArray objectAtIndex:i] objectAtIndex:0] intValue];
                                    [cell.btn_cut setTitle:[[cutlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                                }
                            }
                            
                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"]){
                                
                                a = a+cell.btn_weightHeight.constant;
                                
                                cell.btn_weightHeight.constant=21;
                                cell.lbl_weightHeight.constant=21;
                                cell.btn_weight.hidden=NO;
                                cell.lbl_weight.hidden=NO;
                                if([[weightlabelArray objectAtIndex:indexPath.row] count]>0){
                                    
                                    [cell.btn_weight setTitle:[[weightlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                                }
                                
                            }
                            
                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"]){
                                a=a+cell.btn_typeHeight.constant;
                                
                                cell.btn_typeHeight.constant=21;
                                cell.lbl_typeHeight.constant=21;
                                cell.btn_type.hidden=NO;
                                cell.lbl_type.hidden=NO;
                                if([[typelabelArray objectAtIndex:indexPath.row] count]>0){
                                    [cell.btn_type setTitle:[[typelabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                                }
                            }
                        }
                    }
                    else
                    {
                        
                        cell.btn_weightHeight.constant=0;
                        cell.lbl_weightHeight.constant=0;
                        cell.btn_cutHeight.constant=0;
                        cell.lbl_cutHeight.constant=0;
                        cell.btn_typeHeight.constant=0;
                        cell.lbl_typeHeight.constant=0;
                        cell.btn_weight.hidden=YES;
                        cell.lbl_weight.hidden=YES;
                        cell.btn_cut.hidden=YES;
                        cell.lbl_cut.hidden=YES;
                        cell.btn_type.hidden=YES;
                        cell.lbl_type.hidden=YES;
                        cell.lbl_price.hidden = NO;
                        cell.btn_addCart.hidden = YES;
                        cell.qtyBackView.hidden = NO;
                        cell.lblOutOfStock.hidden = YES;
                        cell.outOFStockHeightConstraints.constant = 0;
                        
                    }
                    
//                    cell.backViewConstraint.constant = cell.nameHeight.constant+cell.shortDescHeight.constant+cell.brandHeight.constant + a + 30;
                }
                
            }
            
            else
            {
                NSArray *components = [[outOfStockTiminingArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"-"];
                
                NSDateFormatter *firstTime = [[NSDateFormatter alloc]init];
                [firstTime setDateFormat:@"HH"];
                NSLog(@"current time------TTTTT%@",[firstTime stringFromDate:[NSDate date]]);
                if(([firstTime stringFromDate:[NSDate date]] > [components objectAtIndex:0]) || ([firstTime stringFromDate:[NSDate date]] < [components objectAtIndex:3]))
                {
                    
                    cell.btn_weightHeight.constant=0;
                    cell.lbl_weightHeight.constant=0;
                    cell.btn_cutHeight.constant=0;
                    cell.lbl_cutHeight.constant=0;
                    cell.btn_typeHeight.constant=0;
                    cell.lbl_typeHeight.constant=0;
                    cell.btn_weight.hidden=YES;
                    cell.lbl_weight.hidden=YES;
                    cell.btn_cut.hidden=YES;
                    cell.lbl_cut.hidden=YES;
                    cell.btn_type.hidden=YES;
                    cell.lbl_type.hidden=YES;
                    cell.btn_add.hidden = YES;
                    cell.lbl_qty.hidden = YES;
                    cell.btn_sub.hidden = YES;
                    cell.lbl_price.hidden = YES;
                    cell.btn_addCart.hidden = YES;
                    cell.qtyBackView.hidden = YES;
                    cell.lblOutOfStock.hidden = NO;
                    cell.lblOutOfStock.text = @"Currently out of Stock. Will be available for sale in 12PM to 8PM Hours.";
                    cell.lblOutOfStock.numberOfLines = 4;
                    
                    
                    cell.outOFStockHeightConstraints.constant = 35;

                    
//                    cell.backViewConstraint.constant = cell.nameHeight.constant+cell.shortDescHeight.constant+cell.brandHeight.constant + cell.outOFStockHeightConstraints.constant;
                    NSLog(@"Height-----%f",cell.backViewConstraint.constant);
                    
                }
                
            }
            
            
            
        }else
        {
            if([[dropLabelArr objectAtIndex:indexPath.row] count ] > 0)
            {
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                int a=40;
                
                cell.lbl_price.hidden = NO;
                cell.btn_addCart.hidden = YES;
                cell.qtyBackView.hidden = NO;
                if ([[dropLabelArr objectAtIndex:indexPath.row] count]>0)
                {
                    
                    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                    {
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                        {
                            
                            cell.btn_cutHeight.constant=21;
                            cell.lbl_cutHeight.constant=21;
                            cell.btn_cut.hidden=NO;
                            cell.lbl_cut.hidden=NO;
                            a=a+cell.btn_cutHeight.constant;

                            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                            {
                                cell.lbl_cut.text=@"Pieces:";
                            }
                            
                            if([[cutlabelArray objectAtIndex:indexPath.row] count]>0){
                                // =[[[cutpriceArray objectAtIndex:i] objectAtIndex:0] intValue];
                                [cell.btn_cut setTitle:[[cutlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                            }
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"]){
                            
                            
                            cell.btn_weightHeight.constant=21;
                            cell.lbl_weightHeight.constant=21;
                            cell.btn_weight.hidden=NO;
                            cell.lbl_weight.hidden=NO;
                            a = a+cell.btn_weightHeight.constant;

                            if([[weightlabelArray objectAtIndex:indexPath.row] count]>0){
                                
                                [cell.btn_weight setTitle:[[weightlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                            }
                            
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"]){
                            
                            cell.btn_typeHeight.constant=21;
                            cell.lbl_typeHeight.constant=21;
                            cell.btn_type.hidden=NO;
                            cell.lbl_type.hidden=NO;
                            a=a+cell.btn_typeHeight.constant;

                            if([[typelabelArray objectAtIndex:indexPath.row] count]>0){
                                [cell.btn_type setTitle:[[typelabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                                
                            }
                        }
                    }
                }else{
                    
                    cell.btn_weightHeight.constant=0;
                    cell.lbl_weightHeight.constant=0;
                    cell.btn_cutHeight.constant=0;
                    cell.lbl_cutHeight.constant=0;
                    cell.btn_typeHeight.constant=0;
                    cell.lbl_typeHeight.constant=0;
                    cell.btn_weight.hidden=YES;
                    cell.lbl_weight.hidden=YES;
                    cell.btn_cut.hidden=YES;
                    cell.lbl_cut.hidden=YES;
                    cell.btn_type.hidden=YES;
                    cell.lbl_type.hidden=YES;
                    cell.lbl_price.hidden = NO;
                    cell.btn_addCart.hidden = YES;
                    cell.qtyBackView.hidden = NO;
                    cell.lblOutOfStock.hidden = YES;
                    cell.outOFStockHeightConstraints.constant = 0;
                    
                }
                
//                cell.backViewConstraint.constant = cell.nameHeight.constant+cell.shortDescHeight.constant+cell.brandHeight.constant + a + 30;
            }
        }
    }
    @catch (NSException *exception){
        [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell2 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    long a = 30;
    
    cell2.shortDescHeight.constant=0;
    cell2.cartWeightHeight.constant = 0;
    
    a = cell2.nameHeight.constant+cell2.brandHeight.constant;

    if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
    {
        NSString *outOfStockTiming = [outOfStockTiminingArray objectAtIndex:indexPath.row];
        if(![outOfStockTiming isEqualToString:@""])
        {
            
            NSDate *currentTime = [NSDate date];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"HH"];
            
            NSString *hourCountString = [timeFormatter stringFromDate:currentTime];
            
            int hourCountInt = [hourCountString intValue];
            
            NSLog(@"hourCountInt=%d",hourCountInt);
            //time between 12PM - 8PM.
            //if(hourCountInt >= 12 && hourCountInt < 20)
            if(hourCountInt >= 12 && hourCountInt < 20)
            {
                cell2.outOFStockHeightConstraints.constant=0;

                if([[dropLabelArr objectAtIndex:indexPath.row] count ] > 0)
                {

                    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                    {
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                        {
                            a=a+cell2.btn_cutHeight.constant;
                            
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
                        {
                            a=a+cell2.btn_weightHeight.constant;
                            
                            
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
                        {
                            a=a+cell2.btn_typeHeight.constant;
                            
                        }
                        
                    }
                }
            }
            else
            {
                NSArray *components = [[outOfStockTiminingArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"-"];
                
                NSDateFormatter *firstTime = [[NSDateFormatter alloc]init];
                [firstTime setDateFormat:@"HH"];
                NSLog(@"current time------TTTTT%@",[firstTime stringFromDate:[NSDate date]]);
                if(([firstTime stringFromDate:[NSDate date]] > [components objectAtIndex:0]) || ([firstTime stringFromDate:[NSDate date]] < [components objectAtIndex:3]))
                {
                    cell2.lblOutOfStock.numberOfLines = 4;
                    cell2.outOFStockHeightConstraints.constant=35;
                    cell2.cartWeightHeight.constant = 0;
                    a = a + cell2.outOFStockHeightConstraints.constant;
                }
                else
                {
                    cell2.outOFStockHeightConstraints.constant=0;
                    
                    
                    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                    {
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                        {
                            a=a+cell2.btn_cutHeight.constant;
                            
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
                        {
                            a=a+cell2.btn_weightHeight.constant;
                            
                            
                        }
                        
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
                        {
                            a=a+cell2.btn_typeHeight.constant;
                            
                        }
                        
                    }
                }
                
            }
            
            
            
            
            
        }
        else
        {
            cell2.outOFStockHeightConstraints.constant=0;

            
            for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
            {
                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                {
                    a=a+cell2.btn_cutHeight.constant;
                    
                }
                
                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
                {
                    a=a+cell2.btn_weightHeight.constant;
                    
                    
                }
                
                if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
                {
                    a=a+cell2.btn_typeHeight.constant;
                    
                }
                
            }
        }
        
    NSLog(@"a=%ld,nameHeight=%ld \n brandHeight=%ld,shortDescHeight=%ld,cartWeightHeight=%ld,btn_cutHeight=%ld,btn_weightHeight=%ld,btn_typeHeight=%ld,outOFStockHeightConstraints=%ld",(long)a,(long)cell2.nameHeight.constant,(long)cell2.brandHeight.constant,(long)cell2.shortDescHeight.constant,(long)cell2.cartWeightHeight.constant,(long)cell2.btn_cutHeight.constant,(long)cell2.btn_weightHeight.constant,(long)cell2.btn_typeHeight.constant,(long)cell2.outOFStockHeightConstraints.constant);
        
        return a+70;
    }
    else
    {
        return 130.0f;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell2 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSString *allcutLabels=[[cutlabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allcutValues=[[cutValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allcutPrices=[[cutpriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    NSString *allweightLabels=[[weightlabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allweightValues=[[weightValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allweightPrices=[[weightpriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    NSString *alltypeLabels=[[typelabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *alltypeValues=[[typeValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *alltypePrices=[[typepriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *outOfStockString = @"";
    
    NSString *outOfStockTiming = [outOfStockTiminingArray objectAtIndex:indexPath.row];
    if(![outOfStockTiming isEqualToString:@""])
    {
        NSArray *components = [[outOfStockTiminingArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"-"];
        
        NSDateFormatter *firstTime = [[NSDateFormatter alloc]init];
        [firstTime setDateFormat:@"HH"];
        if(([firstTime stringFromDate:[NSDate date]] > [components objectAtIndex:0]) || ([firstTime stringFromDate:[NSDate date]] < [components objectAtIndex:3])){
            outOfStockString = @"YES";
        }
        
    }
    
    ItemDetailsController *viewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"ItemDetailsController"];
    viewController.fetchedItemId=[idArray objectAtIndex:indexPath.row];
    viewController.fetchedItemName=[nameArray objectAtIndex:indexPath.row];
    viewController.fetchedItemDesc=[descriptionArray objectAtIndex:indexPath.row];
    viewController.fetchedItemImage=[imagesArray objectAtIndex:indexPath.row];
    viewController.fetchedFoodType=[foodTypeArr objectAtIndex:indexPath.row];
    viewController.fetchedWeight=[weightArray objectAtIndex:indexPath.row];
    viewController.fetchedName = fetchedName.uppercaseString;
    //        viewController.fetchedSelectedStatus=[idContainInLocal objectAtIndex:indexPath.row];
    viewController.fetchedItemPrice=[priceArray objectAtIndex:indexPath.row];
    viewController.fetchedCategoryId=categoryId;
    //        viewController.fetchedMsgId=[fieldIdArray objectAtIndex:indexPath.row];
    //        viewController.fetchedSpiceId=[dropDownIdArray objectAtIndex:indexPath.row];
    //        viewController.fetchedLevelIds=levelIds;
    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
    {
        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
        {
            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
            {
                viewController.fetchDropLabelName=@"Pieces:";
            }else{
                viewController.fetchDropLabelName=@"Cut:";
            }
        }
    }
    viewController.outOfStockString = outOfStockString; 
    viewController.fetchedBrandVal=[brandValueArr objectAtIndex:indexPath.row];
    viewController.fetchedBrandLabel=[brandIndexArr objectAtIndex:indexPath.row];
    viewController.allCutLabel=allcutLabels;
    viewController.allCutPrice=allcutPrices;
    viewController.allCutValues=allcutValues;
    viewController.allWeightLabel=allweightLabels;
    viewController.allWeightValues=allweightValues;
    viewController.allWeightPrice=allweightPrices;
    viewController.allTypeLabel=alltypeLabels;
    viewController.allTypeValues=alltypeValues;
    viewController.allTypePrice=alltypePrices;
    
    viewController.fetchedShortDesc=[shortDescArray objectAtIndex:indexPath.row];
    viewController.fetchedSmallImg=[imageSmallArray objectAtIndex:indexPath.row];
    viewController.fetchedMainImg=[imageMainArray objectAtIndex:indexPath.row];
    viewController.fetchedThumbImg=[imageThumbArray objectAtIndex:indexPath.row];
    viewController.fetchedWeight=[weightArray objectAtIndex:indexPath.row];
    viewController.fetchedCutOptionId=[cutOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedWeightOptionId=[weightOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedTypeOptionId=[typeOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedWishStat=[containInWishListArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:viewController animated:YES];
    
    
}


#pragma mark UIPickerDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// Total rows in our component.

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger i;
    if (pickerView.tag==1) {
        i=[pickerCutLabelArr count];
        
    }else if (pickerView.tag==2){
        i=[pickerWeightLabelArr count];
    }else {
        i=[pickerTypeLabelArr count];
        
    }
    return i;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 50;
}

// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (pickerView.tag==1) {
        
        selectedLabel=[pickerCutLabelArr objectAtIndex:row];
        selectedValue=[pickerCutValueArr objectAtIndex:row];
        selectedPrice=[pickerCutPriceArr objectAtIndex:row];
        
    }else if (pickerView.tag==2){
        selectedLabel=[pickerWeightLabelArr objectAtIndex:row];
        selectedValue=[pickerWeightValueArr objectAtIndex:row];
        selectedPrice=[pickerWeightPriceArr objectAtIndex:row];
        
    }else if (pickerView.tag==3){
        selectedLabel=[pickerTypeLabelArr objectAtIndex:row];
        selectedValue=[pickerTypeValueArr objectAtIndex:row];
        selectedPrice=[pickerTypePriceArr objectAtIndex:row];
        
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    // Get the text of the row.
    
    UILabel *lblRow = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,spicesPicker.bounds.size.width, 44.0f)];
    
    
    if (pickerView.tag==1) {
        NSString *rowItem = [pickerCutLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==2){
        NSString *rowItem = [pickerWeightLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }else if (pickerView.tag==3){
        NSString *rowItem = [pickerTypeLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    return lblRow;
}
#pragma mark UITextFieldDelegates
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//
//    globalvar=textField;
//    return YES;
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)textField{
//
//
//    long row = [globalvar tag];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
//    CustomTableViewCell2 *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
//
//    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
//    [idContainInLocal replaceObjectAtIndex:[globalvar tag] withObject:@"0"];
//    [localQtyArr replaceObjectAtIndex:[globalvar tag] withObject:cell.lbl_qty.text];
//
//}
//-(BOOL)textFieldShouldReturn:(UITextField *)textField{
//    [globalvar resignFirstResponder];
//    return [textField resignFirstResponder];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backMethod:(id)sender
{
    recogniseGesture=@"PanGesture";
    
    [self.revealViewController revealToggle:nil];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // self.revealViewController.panGestureRecognizer.delegate=self;
}

/*
 - (IBAction)spiceMethod:(id)sender {
 
 
 long row = [sender tag];
 NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
 CustomTableViewCell2 *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
 
 [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
 [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
 [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
 
 
 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
 
 
 
 
 alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
 spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
 
 spicesPicker.backgroundColor=[UIColor whiteColor];
 [spicesPicker setDataSource: self];
 [spicesPicker setDelegate: self];
 NSUInteger i=1;
 spicesPicker.tag=i;
 spicesPicker.showsSelectionIndicator = YES;
 [alertController.view addSubview:spicesPicker];
 
 //    [alertController.view addSubview:DateTimePicker];
 [alertController addAction:({
 UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
 {
 
 @try {
 NSLog(@"%@",selectedSpiceIndex);
 
 if (selectedSpiceIndex.length>0 || !([selectedSpiceIndex isEqualToString:@"0"])) {
 
 if ([selectedSpiceIndex isEqualToString:@"1"]) {
 cell.img_spices.image=[UIImage imageNamed:@"1.png"];
 
 }
 else if ([selectedSpiceIndex isEqualToString:@"2"]){
 cell.img_spices.image=[UIImage imageNamed:@"2.png"];
 
 }
 else if ([selectedSpiceIndex isEqualToString:@"3"]){
 cell.img_spices.image=[UIImage imageNamed:@"3.png"];
 
 }
 [spicelevelArray replaceObjectAtIndex:[sender tag] withObject:selectedSpiceIndex];
 
 //removing object
 selectedSpiceIndex=@"0";
 
 }
 else{
 cell.img_spices.image=[UIImage imageNamed:@"1.png"];
 
 }
 } @catch (NSException *exception) {
 [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
 
 }
 
 
 
 
 }];
 action;
 })];
 
 
 
 UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
 popoverController.sourceView = sender;
 popoverController.sourceRect = [sender bounds];
 [self presentViewController:alertController  animated:YES completion:nil];
 
 
 }
 */

- (IBAction)addCartMethod:(id)sender {
    
    
    //  if ([self validations:[sender tag]]) {
    NSString *name=[nameArray objectAtIndex:[sender tag]];
    //    NSString *sk=[skuArray objectAtIndex:[sender tag]];
    NSString *idStr = [idArray objectAtIndex:[sender tag]];
    NSString *catId = [NSString stringWithFormat:@"%@",categoryId];
    NSString *img=[imagesArray objectAtIndex:[sender tag]];
    NSString *imgSmall=[imageSmallArray objectAtIndex:[sender tag]];
    NSString *imgMain=[imageMainArray objectAtIndex:[sender tag]];
    NSString *imgThumb=[imageThumbArray objectAtIndex:[sender tag]];
    NSString *desc=[descriptionArray objectAtIndex:[sender tag]];
    NSString *shortDesc=[shortDescArray objectAtIndex:[sender tag]];
    NSString *weight=[weightArray objectAtIndex:[sender tag]];
    NSString *type=[foodTypeArr objectAtIndex:[sender tag]];
    
    NSString *price=[priceArray objectAtIndex:[sender tag]];
    NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    NSString *currIndQty;
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    currIndQty=cell.lbl_qty.text;
    
    
    NSString *opid=[optionIdArray objectAtIndex:[sender tag]];
    NSString *brand=[brandValueArr objectAtIndex:[sender tag]];
    NSString *cutopid=[cutOptionIdArray objectAtIndex:[sender tag]];
    NSString *cutopval=[selectedCutValueIndex objectAtIndex:row];
    NSString *cutoppri=[selectedCutPriceIndex objectAtIndex:row];
    NSString *weiopid=[weightOptionIdArray objectAtIndex:[sender tag]];
    NSString *weiopval=[selectedWeightValueIndex objectAtIndex:row];
    NSString *weioppri=[selectedWeightPriceIndex objectAtIndex:row];
    NSString *typopid=[typeOptionIdArray objectAtIndex:[sender tag]];
    NSString *typopval=[selectedTypeValueIndex objectAtIndex:row];
    NSString *typoppri=[selectedTypePriceIndex objectAtIndex:row];
    NSString *cutoplabel=[selectedCutLabelIndex objectAtIndex:row];
    NSString *weioplabel=[selectedWeightLabelIndex objectAtIndex:row];
    NSString *typoplabel=[selectedTypeLabelIndex objectAtIndex:row];
    
    //For storing All data in database
    NSString *allcutLabels=[[cutlabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allcutValues=[[cutValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allcutPrices=[[cutpriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    NSString *allweightLabels=[[weightlabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allweightValues=[[weightValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allweightPrices=[[weightpriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    NSString *alltypeLabels=[[typelabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *alltypeValues=[[typeValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *alltypePrices=[[typepriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    NSString *qty = currIndQty;
    
    if ([qty isEqualToString:@"0"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:@"Please select Quantity."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        float total=[qty floatValue]*[price floatValue];
        NSString *tot=[NSString stringWithFormat:@"%.2f",total];
        
        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
        
        ///Add in cart
        
        NSMutableArray *cartDbArray=[[NSMutableArray alloc]init];
        testDB_Obj.cart_id=idStr;
        testDB_Obj.cart_category_id=catId;
        testDB_Obj.cart_name=name;
        testDB_Obj.cart_image_url=img;
        testDB_Obj.cart_image_thumb=imgThumb;
        testDB_Obj.cart_image_small=imgSmall;
        testDB_Obj.cart_image_main=imgMain;
        testDB_Obj.cart_price=price;
        testDB_Obj.cart_qty=qty;
        testDB_Obj.cart_description=desc;
        testDB_Obj.cart_short_desc=shortDesc;
        testDB_Obj.cart_food_type=type;
        testDB_Obj.cart_total=tot;
        testDB_Obj.cart_origin=@"";
        
        testDB_Obj.cart_brand=brand;
        testDB_Obj.cart_weight=weight;
        testDB_Obj.cart_option_id=@"";
        testDB_Obj.cart_cut_option_id=cutopid;
        testDB_Obj.cart_cut_option_value=cutopval;
        testDB_Obj.cart_cut_option_price=cutoppri;
        testDB_Obj.cart_weight_option_id=weiopid;
        testDB_Obj.cart_weight_option_value=weiopval;
        testDB_Obj.cart_weight_option_price=weioppri;
        testDB_Obj.cart_type_option_id=typopid;
        testDB_Obj.cart_type_option_value=typopval;
        testDB_Obj.cart_type_option_price=typoppri;
        testDB_Obj.cart_cut_option_label=cutoplabel;
        testDB_Obj.cart_weight_option_label=weioplabel;
        testDB_Obj.cart_type_option_label=typoplabel;
        
        //For cart master
        testDB_Obj.cm_cart_id=idStr;
        testDB_Obj.cm_category_id=catId;
        
        if (allcutLabels.length > 0){
            testDB_Obj.cm_all_cut_values=allcutValues;
        }else{
            testDB_Obj.cm_all_cut_values=@"0";
        }
        
        if (allcutLabels.length > 0){
            testDB_Obj.cm_all_cut_labels=allcutValues;
        }else{
            testDB_Obj.cm_all_cut_labels=@"";
        }
        
        if (allcutPrices.length > 0){
            testDB_Obj.cm_all_cut_prices=allcutPrices;
        }else{
            testDB_Obj.cm_all_cut_prices=@"0";
        }
        
        if (allweightValues.length > 0){
            testDB_Obj.cm_all_weight_values=allweightValues;
        }else{
            testDB_Obj.cm_all_weight_values=@"0";
        }
        
        if (allweightLabels.length > 0){
            testDB_Obj.cm_all_weight_labels=allweightLabels;
        }else{
            testDB_Obj.cm_all_weight_labels=@"";
        }
        
        if (allweightPrices.length > 0){
            testDB_Obj.cm_all_weight_prices=allweightPrices;
        }else{
            testDB_Obj.cm_all_weight_prices=@"0";
        }
        
        if (alltypeValues.length > 0){
            testDB_Obj.cm_all_type_values=alltypeValues;
        }else{
            testDB_Obj.cm_all_type_values=@"0";
        }
        
        if (alltypeLabels.length > 0){
            testDB_Obj.cm_all_type_labels=alltypeLabels;
        }else{
            testDB_Obj.cm_all_type_labels=@"";
        }
        
        if (alltypePrices.length > 0){
            testDB_Obj.cm_all_type_prices=alltypePrices;
        }else{
            testDB_Obj.cm_all_type_prices=@"";
        }
        
        
        [cartDbArray addObject:testDB_Obj];
        [testDB_Obj checkCartEntry:cartDbArray];
        
        
        
        //                    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"1"];
        //                    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
        //                    [localCutOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_cut.titleLabel.text];
        //                    [localWeightOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_weight.titleLabel.text];
        //                    [localTypeOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_type.titleLabel.text];
        
        //                [messagesArray replaceObjectAtIndex:[sender tag] withObject:cell.txt_msg.text];
        
        //                    NSLog(@"%@",idContainInLocal);
        //                    NSLog(@"%@",localQtyArr);
        
        
        [self checkProgressBar];
        NSArray *str=[testDB_Obj fetch_CartDetails];
        
        NSString *cartCount=[NSString stringWithFormat:@"%lu",(unsigned long)[str count]];
        if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
            _lbl_badgeCount.hidden=YES;
            _btnRightCart.badge.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
            
        }else{
            _lbl_badgeCount.hidden=NO;
            _btnRightCart.badge.hidden = NO;
            [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
            
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
        self.lbl_badgeCount.text=cartCount;
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
            [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
        
        [ProgressHUD showSuccess:@"Item successfully added to cart" Interaction:NO];
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        
        [HUD hide:YES afterDelay:5.0];
        
        //        NSIndexPath *inde=[NSIndexPath indexPathForRow:[sender tag] inSection:0];
        //        [self.pageTableView beginUpdates];
        //        [self.pageTableView reloadRowsAtIndexPaths:@[inde] withRowAnimation:UITableViewRowAnimationNone];
        //        [self.pageTableView endUpdates];
        _pageTableView.delegate = self;
        _pageTableView.dataSource = self;
        
        [self.pageTableView reloadData];
        });
        
    }
}

- (IBAction)addQtyMethod:(id)sender
{
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    CustomTableViewCell *cell =(CustomTableViewCell *)[self.pageTableView cellForRowAtIndexPath:indexPath];
    NSString *currIndQty=cell.lbl_qty.text;
    quntity=[currIndQty intValue];
    if (quntity < 99)
    {
        quntity++;
        NSString *str=[NSString stringWithFormat:@"%d",quntity];
        cell.lbl_qty.text=str;
    }
    //    if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
    //    {
    //        CustomTableViewCell2 *cell2 = [self.pageTableView cellForRowAtIndexPath:indexPath];
    //        NSString *currIndQty=cell2.label_qty.text;
    //        quntity=[currIndQty intValue];
    //        if (quntity < 99)
    //        {
    //            quntity++;
    //            NSString *str=[NSString stringWithFormat:@"%d",quntity];
    //            cell2.label_qty.text=str;
    //        }
    //    }
    //    else
    //    {
    //        CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    //        NSString *currIndQty=cell.lbl_qty.text;
    //        quntity=[currIndQty intValue];
    //        if (quntity < 99)
    //        {
    //            quntity++;
    //            NSString *str=[NSString stringWithFormat:@"%d",quntity];
    //            cell.lbl_qty.text=str;
    //        }
    //    }
}

- (IBAction)subQtyMethod:(id)sender {
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    NSString *currIndQty=cell.lbl_qty.text;
    quntity=[currIndQty intValue];
    if (quntity > 0)
    {
        quntity--;
        cell.lbl_qty.text=[NSString stringWithFormat:@"%d",quntity];
    }
    
}

-(void)GetTopviewProduct:(NSString *)catId
{
    
    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    outOfStockTiminingArray =[[NSMutableArray alloc]init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    
    
    weightlabelArray=[[NSMutableArray alloc] init];
    weightValueArray=[[NSMutableArray alloc] init];
    weightpriceArray=[[NSMutableArray alloc] init];
    
    cutlabelArray=[[NSMutableArray alloc] init];
    cutValueArray=[[NSMutableArray alloc] init];
    cutpriceArray=[[NSMutableArray alloc] init];
    
    typelabelArray=[[NSMutableArray alloc] init];
    typeValueArray=[[NSMutableArray alloc] init];
    typepriceArray=[[NSMutableArray alloc] init];
    
    localCutOpIdArray=[[NSMutableArray alloc] init];
    localCutOpValArray=[[NSMutableArray alloc] init];
    localCutOpPriceArray=[[NSMutableArray alloc] init];
    
    localWeightOpIdArray=[[NSMutableArray alloc] init];
    localWeightOpValArray=[[NSMutableArray alloc] init];
    localWeightOpPriceArray=[[NSMutableArray alloc] init];
    
    localTypeOpIdArray=[[NSMutableArray alloc] init];
    localTypeOpValArray=[[NSMutableArray alloc] init];
    localTypeOpPriceArray=[[NSMutableArray alloc] init];
    brandIndexArr=[[NSMutableArray alloc] init];
    brandValueArr=[[NSMutableArray alloc] init];
    weightArray=[[NSMutableArray alloc] init];
    optionIdArray=[[NSMutableArray alloc] init];
    imageMainArray=[[NSMutableArray alloc] init];
    imageThumbArray=[[NSMutableArray alloc] init];
    imageSmallArray=[[NSMutableArray alloc] init];
    
    typeOptionIdArray=[[NSMutableArray alloc] init];
    weightOptionIdArray=[[NSMutableArray alloc] init];
    cutOptionIdArray=[[NSMutableArray alloc] init];
    localCutOpLabelArray=[[NSMutableArray alloc] init];
    localWeightOpLabelArray=[[NSMutableArray alloc] init];
    localTypeOpLabelArray=[[NSMutableArray alloc] init];
    selectedWeightLabelIndex=[[NSMutableArray alloc] init];
    selectedWeightValueIndex=[[NSMutableArray alloc] init];
    selectedWeightPriceIndex=[[NSMutableArray alloc] init];
    
    selectedCutLabelIndex=[[NSMutableArray alloc] init];
    selectedCutPriceIndex=[[NSMutableArray alloc] init];
    selectedCutValueIndex=[[NSMutableArray alloc] init];
    selectedTypeLabelIndex=[[NSMutableArray alloc] init];
    selectedTypeValueIndex=[[NSMutableArray alloc] init];
    selectedTypePriceIndex=[[NSMutableArray alloc] init];
    wishlistIdArray=[[NSMutableArray alloc] init];
    containInWishListArray=[[NSMutableArray alloc] init];
    // ,*selectedCutValueIndex,*selectedCutPriceIndex,selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    // NSLog(@"Idarray1=%@",idArray);
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        HelperClass *s = [[HelperClass alloc]init];
        
        BOOL connection = s.getInternetStatus;
        
        if (!connection){
            [HUD hide:YES];
            
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }else{
            @try {
                _pageTableView.delegate = self;
                _pageTableView.dataSource = self;
                [_pageTableView setHidden:NO];
                [_pageTableView reloadData];
                
                NSString *tempUrl;
                tempUrl=[NSString stringWithFormat:@"%@category_id=%@&limit=%d&page=%d",API_MENU_ITEMS,catId,limit,offset];
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
                
                if ([isLog isEqualToString:@"YES"]) {
                    NSMutableDictionary *wishDict=[wishObj getAllWishData];
                    BOOL success=[[wishDict valueForKey:@"success"] boolValue];
                    if (success) {
                        [wishlistIdArray addObjectsFromArray:[[wishDict valueForKey:@"message"] valueForKey:@"entity_id"]];
                    }
                }
                
                NSDictionary *jsonDict=[web GetTopviewProductWithUrl:tempUrl];
                
                if ([jsonDict count] > 0)
                {
                    NSArray *idArr=[jsonDict valueForKey:@"entity_id"];
                    NSArray *name=[jsonDict valueForKey:@"name"];
                    //                NSArray *skuArr=[jsonDict valueForKey:@"sku"];
                    NSArray *descriptionArr=[jsonDict valueForKey:@"description"];
                    NSArray *shortDescArr=[jsonDict valueForKey:@"short_description"];
                    NSArray *priceArr=[jsonDict valueForKey:@"final_price_with_tax"];
                    //                NSArray *isSaleableArr=[jsonDict valueForKey:@"is_saleable"];
                    
                    NSArray *outOfStockTiming = [jsonDict valueForKey:@"out_of_stock_timing"];
                    NSArray *foodType=[jsonDict valueForKey:@"food_type"];
                    NSArray *imagesArr=[jsonDict valueForKey:@"image_url"];
                    NSArray *imagesThumb=[jsonDict valueForKey:@"image_thumb"];
                    NSArray *imagesMainUrl=[jsonDict valueForKey:@"main_url"];
                    NSArray *imagesSmall=[jsonDict valueForKey:@"image_small"];
                    NSArray *brand=[jsonDict valueForKey:@"brand"];
                    NSArray *origin=[jsonDict valueForKey:@"origin"];
                    NSArray *weight=[jsonDict valueForKey:@"weight"];
                    
                    // dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray;
                    NSArray *attrDict=[jsonDict valueForKey:@"attributes"];
                    NSArray *InputTypeArray=[attrDict valueForKey:@"InputType"];
                    NSArray *opIdArr=[attrDict valueForKey:@"option_id"];
                    NSDictionary *attArr=[attrDict valueForKey:@"att"];
                    NSArray *labelArr=[attArr valueForKey:@"label"];
                    NSArray *valueArr=[attArr valueForKey:@"value"];
                    NSArray *priArr=[attArr valueForKey:@"price"];
                    NSArray *InputType=[attArr valueForKey:@"InputType"];
                    
                    
                    
                    [self getRangeValuesOfPrice:priceArr];
                    
                    NSString *isfilter=[[NSUserDefaults standardUserDefaults] valueForKey:ISFILTER];
                    int filterMin=[[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMINPRICE] intValue];
                    int filterMax=[[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMAXPRICE] intValue];
                    NSString *filterBrand=[[NSUserDefaults standardUserDefaults] valueForKey:BRANDVALUE];
                    
                    /****Set attributes*****/
                    NSMutableArray *newPriceArray=[[NSMutableArray alloc] init];
                    
                    if ([isfilter isEqualToString:@"YES"]) {
                        for (int i=0; i<[idArr count]; i++) {
                            int currPri=[[priceArr objectAtIndex:i] intValue];
                            if ((currPri >= filterMin) && (currPri <=filterMax)) {
                                NSArray *appArr = APP_DELEGATE.selectBarndArray;
                                if ([appArr containsObject:[brand objectAtIndex:i]]) {
                                    
                                    if ([[attrDict objectAtIndex:i] count]>0) {
                                        
                                        if ([[InputType objectAtIndex:i] count]>0) {
                                            NSMutableArray *wlArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *wvArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *wpArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *clArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *cvArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *cpArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *tlArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *tvArr=[[NSMutableArray alloc] init];
                                            NSMutableArray *tpArr=[[NSMutableArray alloc] init];
                                            
                                            for (int m=0; m<[[InputType objectAtIndex:i] count]; m++)
                                            {
                                                NSArray *arr=[[InputType objectAtIndex:i] objectAtIndex:m];
                                                for (int n=0; n<[arr count]; n++)
                                                {
                                                    if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"]) {
                                                        NSString *str_wlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                        NSString *str_wvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                        NSString *str_wpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                        [wlArr addObject:str_wlArr];
//                                                        [wvArr addObject:str_wvArr];
//                                                        [wpArr addObject:str_wpArr];
                                                        
//                                                        int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
//                                                        [wlArr addObject:str_wlArr];
//                                                        [wvArr addObject:str_wvArr];
//                                                        [wpArr addObject:@(str_wpArr)];
                                                        
                                                        int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
                                                        NSString *weightStr = [NSString stringWithFormat:@"%d",str_wpArr];
                                                        [wlArr addObject:str_wlArr];
                                                        [wvArr addObject:str_wvArr];
                                                        [wpArr addObject:weightStr];
                                                    }
                                                    
                                                    if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                                        NSString *str_clArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                        NSString *str_cvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                        NSString *str_cpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                        [clArr addObject:str_clArr];
                                                        [cvArr addObject:str_cvArr];
                                                        [cpArr addObject:str_cpArr];
                                                        
                                                    }
                                                    
                                                    if ([[arr objectAtIndex:n] isEqualToString:@"Type"]) {
                                                        NSString *str_tlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                        NSString *str_tvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                        NSString *str_tpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                        [tlArr addObject:str_tlArr];
                                                        [tvArr addObject:str_tvArr];
                                                        [tpArr addObject:str_tpArr];
                                                        
                                                    }
                                                }
                                            }
                                            [weightlabelArray addObject:wlArr];
                                            [weightValueArray addObject:wvArr];
                                            [weightpriceArray addObject:wpArr];
                                            [cutlabelArray addObject:clArr];
                                            [cutValueArray addObject:cvArr];
                                            [cutpriceArray addObject:cpArr];
                                            [typelabelArray addObject:tlArr];
                                            [typeValueArray addObject:tvArr];
                                            [typepriceArray addObject:tpArr];
                                            
                                        }
                                        
                                        NSArray *labArr=[opIdArr objectAtIndex:i];
                                        if ( (NSArray *)[NSNull null] == labArr )
                                        {
                                            NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                            [optionIdArray addObject:emptyArray];
                                            
                                        }else{
                                            //[dropLabelArr addObject:[labArr objectAtIndex:1]];
                                            [optionIdArray addObject:[opIdArr objectAtIndex:i]];
                                        }
                                        
                                        NSArray *valArr=[InputTypeArray objectAtIndex:i];
                                        if ( (NSArray *)[NSNull null] == valArr ){
                                            
                                        }else{
                                            [dropLabelArr addObject:valArr];
                                        }
                                    }else{
                                        
                                        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                        [dropLabelArr addObject:emptyArray];
                                        [optionIdArray addObject:emptyArray];
                                        [weightlabelArray addObject:emptyArray];
                                        [weightValueArray addObject:emptyArray];
                                        [weightpriceArray addObject:emptyArray];
                                        [cutlabelArray addObject:emptyArray];
                                        [cutValueArray addObject:emptyArray];
                                        [cutpriceArray addObject:emptyArray];
                                        [typelabelArray addObject:emptyArray];
                                        [typeValueArray addObject:emptyArray];
                                        [typepriceArray addObject:emptyArray];
                                        
                                    }
                                    
                                    [newPriceArray addObject:[priceArr objectAtIndex:i]];
                                    [idArray addObject:[idArr objectAtIndex:i]];
                                    [nameArray addObject:[name objectAtIndex:i]];
                                    [descriptionArray addObject:[descriptionArr objectAtIndex:i]];
                                    [imagesArray addObject:[imagesArr objectAtIndex:i]];
                                    [outOfStockTiminingArray addObject:[outOfStockTiming objectAtIndex:i]];
                                    
                                    if (!((NSArray *)[NSNull null] == [foodType objectAtIndex:i]))
                                    {
                                        [foodTypeArr addObject:[foodType objectAtIndex:i]];
                                        
                                    }else{
                                        [foodTypeArr addObject:@"0"];
                                    }
                                    
                                    //                        [skuArray addObject:[skuArr objectAtIndex:i]];
                                    [shortDescArray addObject:[shortDescArr objectAtIndex:i]];
                                    
                                    
                                    [brandIndexArr addObject:[brand objectAtIndex:i]];
                                    [imageMainArray addObject:[imagesMainUrl objectAtIndex:i]];
                                    [imageSmallArray addObject:[imagesSmall objectAtIndex:i]];
                                    [imageThumbArray addObject:[imagesThumb objectAtIndex:i]];
                                    NSString *weightArr;
                                    if (!((NSString *)[NSNull null] == [weight objectAtIndex:i])){
                                        weightArr=[weight objectAtIndex:i];
                                        //[foodTypeArr addObject:[foodType objectAtIndex:i]];
                                        
                                    }else{
                                        
                                        weightArr=@"0";
                                    }
                                    [weightArray addObject:weightArr];
                                    
                                    NSArray *getAttr=[object fetch_BrandValue:@"brand" AndValue:[brand objectAtIndex:i]];
                                    if ([getAttr count]>0) {
                                        [brandValueArr addObject:[[getAttr valueForKey:@"attribute_label"] objectAtIndex:0]];
                                        
                                    }
                                    else{
                                        [brandValueArr addObject:@"0"];
                                        
                                    }
                                }
                            }
                        }
                    }else{
                        for (int i=0; i<[idArr count]; i++) {
                            if ([[attrDict objectAtIndex:i] count]>0) {
                                
                                if ([[InputType objectAtIndex:i] count]>0) {
                                    NSMutableArray *wlArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *wvArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *wpArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *clArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *cvArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *cpArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *tlArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *tvArr=[[NSMutableArray alloc] init];
                                    NSMutableArray *tpArr=[[NSMutableArray alloc] init];
                                    
                                    for (int m=0; m<[[InputType objectAtIndex:i] count]; m++) {
                                        NSArray *arr=[[InputType objectAtIndex:i] objectAtIndex:m];
                                        for (int n=0; n<[arr count]; n++) {
                                            if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"]) {
                                                NSString *str_wlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                NSString *str_wvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                NSString *str_wpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                [wlArr addObject:str_wlArr];
//                                                [wvArr addObject:str_wvArr];
//                                                [wpArr addObject:str_wpArr];
                                                
//                                                int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
//                                                [wlArr addObject:str_wlArr];
//                                                [wvArr addObject:str_wvArr];
//                                                [wpArr addObject:@(str_wpArr)];

                                                int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
                                                NSString *weightStr = [NSString stringWithFormat:@"%d",str_wpArr];
                                                [wlArr addObject:str_wlArr];
                                                [wvArr addObject:str_wvArr];
                                                [wpArr addObject:weightStr];
                                            }
                                            
                                            if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                                NSString *str_clArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                NSString *str_cvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                NSString *str_cpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                [clArr addObject:str_clArr];
                                                [cvArr addObject:str_cvArr];
                                                [cpArr addObject:str_cpArr];
                                                
                                            }
                                            
                                            if ([[arr objectAtIndex:n] isEqualToString:@"Type"]) {
                                                NSString *str_tlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                NSString *str_tvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                NSString *str_tpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                [tlArr addObject:str_tlArr];
                                                [tvArr addObject:str_tvArr];
                                                [tpArr addObject:str_tpArr];
                                                
                                            }
                                        }
                                    }
                                    
                                    [weightlabelArray addObject:wlArr];
                                    [weightValueArray addObject:wvArr];
                                    [weightpriceArray addObject:wpArr];
                                    [cutlabelArray addObject:clArr];
                                    [cutValueArray addObject:cvArr];
                                    [cutpriceArray addObject:cpArr];
                                    [typelabelArray addObject:tlArr];
                                    [typeValueArray addObject:tvArr];
                                    [typepriceArray addObject:tpArr];
                                    
                                }
                                
                                NSArray *labArr=[opIdArr objectAtIndex:i];
                                if ( (NSArray *)[NSNull null] == labArr )
                                {
                                    NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                    [optionIdArray addObject:emptyArray];
                                    
                                }else{
                                    //[dropLabelArr addObject:[labArr objectAtIndex:1]];
                                    [optionIdArray addObject:[opIdArr objectAtIndex:i]];
                                }
                                
                                NSArray *valArr=[InputTypeArray objectAtIndex:i];
                                if ( (NSArray *)[NSNull null] == valArr ){
                                    
                                }else{
                                    [dropLabelArr addObject:valArr];
                                    
                                    //                            [dropValueArr addObject:[valArr objectAtIndex:1]];
                                }
                            }else{
                                
                                NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                [dropLabelArr addObject:emptyArray];
                                [optionIdArray addObject:emptyArray];
                                [weightlabelArray addObject:emptyArray];
                                [weightValueArray addObject:emptyArray];
                                [weightpriceArray addObject:emptyArray];
                                [cutlabelArray addObject:emptyArray];
                                [cutValueArray addObject:emptyArray];
                                [cutpriceArray addObject:emptyArray];
                                [typelabelArray addObject:emptyArray];
                                [typeValueArray addObject:emptyArray];
                                [typepriceArray addObject:emptyArray];
                                
                            }
                            
                            [newPriceArray addObject:[priceArr objectAtIndex:i]];
                            [idArray addObject:[idArr objectAtIndex:i]];
                            [nameArray addObject:[name objectAtIndex:i]];
                            [descriptionArray addObject:[descriptionArr objectAtIndex:i]];
                            [imagesArray addObject:[imagesArr objectAtIndex:i]];
                            [outOfStockTiminingArray addObject:[outOfStockTiming objectAtIndex:i]];
                            
                            if (!((NSArray *)[NSNull null] == [foodType objectAtIndex:i]))
                            {
                                [foodTypeArr addObject:[foodType objectAtIndex:i]];
                                
                            }else{
                                
                                [foodTypeArr addObject:@"0"];
                            }
                            
                            
                            [shortDescArray addObject:[shortDescArr objectAtIndex:i]];
                            [brandIndexArr addObject:[brand objectAtIndex:i]];
                            [imageMainArray addObject:[imagesMainUrl objectAtIndex:i]];
                            [imageSmallArray addObject:[imagesSmall objectAtIndex:i]];
                            [imageThumbArray addObject:[imagesThumb objectAtIndex:i]];
                            
                            NSString *weightArr;
                            
                            NSString * strWeight = [weight objectAtIndex:i];
                            
                            
                            if ( ( ![strWeight isEqual:[NSNull null]] ) && ( [strWeight length] != 0 ) )
                            {
                                weightArr=[weight objectAtIndex:i];
                                [weightArray addObject:[weight objectAtIndex:i]];
                                //[foodTypeArr addObject:[foodType objectAtIndex:i]];
                                
                            }else{
                                weightArr=@"0";
                                [weightArray addObject:@"0.00"];
                            }
                            
                            
                            
                            NSArray *getAttr=[object fetch_BrandValue:@"brand" AndValue:[brand objectAtIndex:i]];
                            if ([getAttr count]>0) {
                                [brandValueArr addObject:[[getAttr valueForKey:@"attribute_label"] objectAtIndex:0]];
                                
                            }else{
                                [brandValueArr addObject:@"0"];
                                
                            }
                        }
                    }
                    
                    /****END of attributes*********/
                    for (int m=0; m<[optionIdArray count]; m++) {
                        int a,b,c;
                        a=0;b=0;c=0;
                        if([[optionIdArray objectAtIndex:m] count]>0){
                            
                            for (int n=0; n<[[optionIdArray objectAtIndex:m] count]; n++) {
                                if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                    [cutOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                    a=1;
                                }
                                if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight"]) {
                                    [weightOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                    b=1;
                                }
                                if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Type"]) {
                                    [typeOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                    c=1;
                                }
                                
                                
                            }
                            if (a==0) {
                                [cutOptionIdArray addObject:@"0"];
                            }
                            if (b==0) {
                                [weightOptionIdArray addObject:@"0"];
                            }
                            if (c==0) {
                                [typeOptionIdArray addObject:@"0"];
                            }
                        }else{
                            
                            [cutOptionIdArray addObject:@"0"];
                            [typeOptionIdArray addObject:@"0"];
                            [weightOptionIdArray addObject:@"0"];
                            
                        }
                        
                        
                        NSString *concatedPrice=[NSString stringWithFormat:@"%@",[newPriceArray objectAtIndex:m]];
                        int conPri=[concatedPrice intValue];
                        
                        if (conPri == 0) {
                            int sum;
                            sum=0;
                            if([[weightpriceArray objectAtIndex:m] count]>0){
                                
                                sum=+[[[weightpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                            }
                            if([[cutpriceArray objectAtIndex:m] count]>0){
                                sum=sum+[[[cutpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                            }
                            if([[typepriceArray objectAtIndex:m] count]>0)
                            {
                                sum=sum+[[[typepriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                            }
                            NSString *calcSum=[NSString stringWithFormat:@"%d",sum];
                            [priceArray addObject:calcSum];
                        }
                        else{
                            [priceArray addObject:concatedPrice];
                        }
                        
                        if([[weightlabelArray objectAtIndex:m] count]>0){
                            [selectedWeightLabelIndex addObject:[[weightlabelArray objectAtIndex:m] objectAtIndex:0]];
                            [selectedWeightValueIndex addObject:[[weightValueArray objectAtIndex:m] objectAtIndex:0]];
                            [selectedWeightPriceIndex addObject:[[weightpriceArray objectAtIndex:m] objectAtIndex:0]];
                            
                        }else{
                            [selectedWeightLabelIndex addObject:@"0"];
                            [selectedWeightValueIndex addObject:@"0"];
                            [selectedWeightPriceIndex addObject:@"0"];
                            
                        }
                        if([[cutlabelArray objectAtIndex:m] count]>0){
                            [selectedCutLabelIndex addObject:[[cutlabelArray objectAtIndex:m] objectAtIndex:0]];
                            [selectedCutValueIndex addObject:[[cutValueArray objectAtIndex:m] objectAtIndex:0]];
                            [selectedCutPriceIndex addObject:[[cutpriceArray objectAtIndex:m] objectAtIndex:0]];
                            
                        }else{
                            [selectedCutLabelIndex addObject:@"0"];
                            [selectedCutValueIndex addObject:@"0"];
                            [selectedCutPriceIndex addObject:@"0"];
                            
                        }
                        if([[typelabelArray objectAtIndex:m] count]>0){
                            [selectedTypeLabelIndex addObject:[[typelabelArray objectAtIndex:m] objectAtIndex:0]];
                            [selectedTypeValueIndex addObject:[[typeValueArray objectAtIndex:m] objectAtIndex:0]];
                            [selectedTypePriceIndex addObject:[[typepriceArray objectAtIndex:m] objectAtIndex:0]];
                            
                        }else{
                            [selectedTypeLabelIndex addObject:@"0"];
                            [selectedTypeValueIndex addObject:@"0"];
                            [selectedTypePriceIndex addObject:@"0"];
                            
                        }
                    }
                    
                    [self fillContainwishArray:idArray AndWishIdArray:wishlistIdArray];
                    
                    [HUD hide:YES];
                    if ([idArray count] > 0){
                        _pageTableView.delegate = self;
                        _pageTableView.dataSource = self;
                        [self.pageTableView reloadData];
                        
                    }else{
                        _pageTableView.hidden=YES;
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:@"Alert"
                                                      message:@"No data found."
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       //Do Some action here
                                                                       [HUD hide:YES];
                                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                                       
                                                                   }];
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }else{
                    
                    [HUD hide:YES];
                    //                    UIAlertController * alert=   [UIAlertController
                    //                                                  alertControllerWithTitle:@"Alert"
                    //                                                  message:@"There might be a network issue.Please check your internet connection and reload again!"
                    //                                                  preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Alert"
                                                  message:@"No Data Found.Please try again later..!"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   //Do Some action here
                                                                   [HUD hide:YES];
                                                                   nameArray=[[NSMutableArray alloc] init];
                                                                   idArray=[[NSMutableArray alloc] init];
                                                                   imagesArray=[[NSMutableArray alloc] init];
                                                                   idArray=[[NSMutableArray alloc] init];
                                                                   nameArray=[[NSMutableArray alloc] init];
                                                                   descriptionArray=[[NSMutableArray alloc] init];
                                                                   imagesArray=[[NSMutableArray alloc] init];
                                                                   skuArray=[[NSMutableArray alloc] init];
                                                                   priceArray=[[NSMutableArray alloc] init];
                                                                   shortDescArray=[[NSMutableArray alloc] init];
                                                                   idContainInLocal=[[NSMutableArray alloc] init];
                                                                   localQtyArr=[[NSMutableArray alloc] init];
                                                                   foodTypeArr=[[NSMutableArray alloc] init];
                                                                   
                                                                   localCutOpIdArray=[[NSMutableArray alloc] init];
                                                                   localCutOpValArray=[[NSMutableArray alloc] init];
                                                                   localCutOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   localWeightOpIdArray=[[NSMutableArray alloc] init];
                                                                   localWeightOpValArray=[[NSMutableArray alloc] init];
                                                                   localWeightOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   localTypeOpIdArray=[[NSMutableArray alloc] init];
                                                                   localTypeOpValArray=[[NSMutableArray alloc] init];
                                                                   localTypeOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   NSLog(@"Idarray1=%@",idArray);
                                                                   [_pageTableView reloadData];
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                                   
                                                               }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                
            } @catch (NSException *exception){
                NSLog(@"Exception = %@",exception);
                [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                
            }
        }
    });
}

-(void)fillContainwishArray:(NSMutableArray *)prodIdArr AndWishIdArray:(NSMutableArray *)wIdArr{
    for (int i=0; i<[prodIdArr count]; i++) {
        if ([wIdArr containsObject:[prodIdArr objectAtIndex:i]]) {
            [containInWishListArray addObject:@"1"];
        }else{
            [containInWishListArray addObject:@"0"];
            
        }
    }
}

-(void)getRangeValuesOfPrice:(NSArray *)priArr{
    
    NSMutableArray *integerArrayFor=[[NSMutableArray alloc] init];
    [integerArrayFor addObjectsFromArray:priArr];
    for (int i=0; i<[integerArrayFor count]; i++) {
        NSString *a=0;
        
        for(int j=i+1 ;j<[integerArrayFor count]; j++)
        {
            if ([[integerArrayFor objectAtIndex:i] intValue]> [[integerArrayFor objectAtIndex:j] intValue]) {
                a=[integerArrayFor objectAtIndex:i] ;
                [integerArrayFor replaceObjectAtIndex:i withObject:[integerArrayFor objectAtIndex:j]];
                [integerArrayFor replaceObjectAtIndex:j withObject:a];
                
                
            }
        }
    }
    
    //    NSSortDescriptor *sortDescriptor;
    //    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil
    //                                                  ascending:YES] ;
    //    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //    NSArray *sortedArray;
    //    sortedArray = [priArr sortedArrayUsingDescriptors:sortDescriptors];
    NSString *min,*max;
    if ([integerArrayFor count]==1) {
        _btnFilter.hidden=YES;
        
        //        min=[NSString stringWithFormat:@"%d",[[sortedArray firstObject] intValue]];
        //        max=[NSString stringWithFormat:@"%d",[[sortedArray lastObject] intValue]];
    }else if([integerArrayFor count]==0){
        
        _btnFilter.hidden=YES;
    }else{
        _btnFilter.hidden=NO;
        
        min=[NSString stringWithFormat:@"%d",[[integerArrayFor firstObject] intValue]];
        max=[NSString stringWithFormat:@"%d",[[integerArrayFor lastObject] intValue]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] ==nil || [[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] length] <=0) {
        [[NSUserDefaults standardUserDefaults] setObject:min forKey:MINPRICE];
        
    }else{
        int compMin=[[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] intValue];
        if (compMin<[min intValue]) {
            
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:min forKey:MINPRICE];
            
        }
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] ==nil || [[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] length] <=0) {
        [[NSUserDefaults standardUserDefaults] setObject:max forKey:MAXPRICE];
        
    }else{
        int compMin=[[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] intValue];
        if (compMin>[max intValue]) {
            
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:max forKey:MAXPRICE];
        }
    }
}

- (IBAction)cartMethod:(id)sender
{
    
    //    OfflineDb *object=[[OfflineDb alloc]init];
    NSArray *dataArray=[object fetch_CartDetails];
    NSLog(@"%@",dataArray);
    if ([dataArray count]>0)
    {
        CartController *viewController =[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
        viewController.comingFrom=@"ItemsVC";
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    else
    {
        
        [ProgressHUD showError:@"Cart Empty" Interaction:NO];
    }
}

- (IBAction)wishMethod:(id)sender {
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
    
    if ([isLog isEqualToString:@"YES"]) {
        
        NSLog(@"%ld",[sender tag]);
        
        HelperClass *s=[[HelperClass alloc]init];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            BOOL connection=s.getInternetStatus;
            
            if (!connection)
            {
                
                
                NSLog(@"Hello Net connectiion is not pres;ent....");
                
                [HUD hide:YES];
                
                UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Connection Lost" message:@"Please check your internet connection!" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
                
                
                [alrt addAction:ok];
                [self presentViewController:alrt animated:YES completion:nil];
                
                
            }
            else
            {
                @try {
                    long row = [sender tag];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
                    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
                    
                    
                    
                    if ([[containInWishListArray objectAtIndex:row] isEqualToString:@"0"]) {
                        NSArray *getdata=[wishObj insertIntoProductList:[idArray objectAtIndex:row]];
                        NSLog(@"wishlist = %@",getdata);
                        if ([getdata count]>0) {
                            
                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
                            if (success) {
                                [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
                                [containInWishListArray replaceObjectAtIndex:row withObject:@"1"];
                                
                            }else{
                                NSString *msg=[getdata valueForKey:@"message"];
                                [ProgressHUD showError:msg Interaction:NO];
                                
                            }
                        }else{
                            
                            [ProgressHUD showError:@"Unable to add to wishlist" Interaction:NO];
                            
                        }
                        [HUD hide:YES];
                    }else if ([[containInWishListArray objectAtIndex:row] isEqualToString:@"1"]){
                        
                        NSArray *getdata=[wishObj deleteFromWishList:[idArray objectAtIndex:row]];
                        if ([getdata count]>0) {
                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
                            if (success) {
                                
                                [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
                                [containInWishListArray replaceObjectAtIndex:row withObject:@"0"];
                            }else{
                                NSString *msg=[getdata valueForKey:@"message"];
                                [ProgressHUD showError:msg Interaction:NO];
                                
                            }
                        }else{
                            
                            [ProgressHUD showError:@"Unable to delete from wishlist" Interaction:NO];
                            
                        }
                        [HUD hide:YES];
                    }
                } @catch (NSException *exception) {
                    [HUD hide:YES];
                    [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                }
            }
        });
    }else{
        [HUD hide:YES];
        
        UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Alert" message:@"You need to login first." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
        [alrt addAction:ok];
        [self presentViewController:alrt animated:YES completion:nil];
        
    }
}

- (IBAction)cutMethod:(id)sender {
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    
    pickerCutLabelArr=[[NSMutableArray alloc] init];
    pickerCutValueArr=[[NSMutableArray alloc] init];
    pickerCutPriceArr=[[NSMutableArray alloc] init];
    [pickerCutLabelArr addObjectsFromArray:[cutlabelArray objectAtIndex:row]];
    [pickerCutValueArr addObjectsFromArray:[cutValueArray objectAtIndex:row]];
    [pickerCutPriceArr addObjectsFromArray:[cutpriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=1;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     @try {
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             //                                     [spicelevelArray replaceObjectAtIndex:[sender tag] withObject:selectedSpiceIndex];
                                             [cell.btn_cut setTitle:[selectedCutLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                         }else{
                                             
                                             [selectedCutLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedCutValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedCutPriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             [cell.btn_cut setTitle:[selectedCutLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                         }
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                         cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                 }];
        action;
    })];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
}

- (IBAction)weightMethod:(id)sender {
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    pickerWeightLabelArr=[[NSMutableArray alloc] init];
    pickerWeightValueArr=[[NSMutableArray alloc] init];
    pickerWeightPriceArr=[[NSMutableArray alloc] init];
    [pickerWeightLabelArr addObjectsFromArray:[weightlabelArray objectAtIndex:row]];
    [pickerWeightValueArr addObjectsFromArray:[weightValueArray objectAtIndex:row]];
    [pickerWeightPriceArr addObjectsFromArray:[weightpriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=2;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     @try {
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             [cell.btn_weight setTitle:[selectedWeightLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                         }else{
                                             [selectedWeightLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedWeightValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedWeightPriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             
                                             [cell.btn_weight setTitle:[selectedWeightLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                         }
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         
                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                         cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         
                                     }@catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                 }];
        action;
    })];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
    
}

- (IBAction)typeMethod:(id)sender {
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    
    pickerTypeLabelArr=[[NSMutableArray alloc] init];
    pickerTypeValueArr=[[NSMutableArray alloc] init];
    pickerTypePriceArr=[[NSMutableArray alloc] init];
    [pickerTypeLabelArr addObjectsFromArray:[typelabelArray objectAtIndex:row]];
    [pickerTypeValueArr addObjectsFromArray:[typeValueArray objectAtIndex:row]];
    [pickerTypePriceArr addObjectsFromArray:[typepriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=3;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     @try {
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             [cell.btn_type setTitle:[selectedTypeLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                         }else{
                                             
                                             [selectedTypeLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedTypeValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedTypePriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             [cell.btn_type setTitle:[selectedTypeLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                         }
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         if(calcPrice==0)
                                         {
                                             
                                         }else{
                                             
                                             NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                             cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                             [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         }
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                 }];
        action;
    })];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
}

- (IBAction)searchMethod:(id)sender {
    
    NSArray *brandListArr=[[NSMutableArray alloc] init];
    NSArray *brandIdArr=[[NSMutableArray alloc] init];
    NSArray *brandData=[object fetch_AttributeValue:@"brand"];
    brandListArr=[brandData valueForKey:@"attribute_label"];
    brandIdArr=[brandData valueForKey:@"attribute_value"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMAXPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:BRANDVALUE];
    [APP_DELEGATE.selectBarndArray addObjectsFromArray:brandIdArr];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISFILTER];
    
    SearchController *lo=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchController"];
    [self.navigationController pushViewController:lo animated:YES];
    
}

- (IBAction)filterMethod:(id)sender
{
    FilterController *lo=[self.storyboard instantiateViewControllerWithIdentifier:@"FilterController"];
    lo.categoryId=fetchedId;
    lo.subCategoryId=[categoryIdArr objectAtIndex:indexToShow];
    [self.navigationController pushViewController:lo animated:YES];
    
    /*if([idArray count]>0)
     {
     FilterController *lo=[self.storyboard instantiateViewControllerWithIdentifier:@"FilterController"];
     lo.categoryId=fetchedId;
     lo.subCategoryId=[categoryIdArr objectAtIndex:indexToShow];
     [self.navigationController pushViewController:lo animated:YES];    }
     else
     {
     [ProgressHUD showError:@"No data to filter" Interaction:NO];
     }*/
    
}


-(BOOL)validations:(long)row{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.btn_cut.titleLabel.text isEqualToString:SELECTCUT] || [cell.btn_cut.titleLabel.text isEqualToString:@""] || cell.btn_cut.titleLabel.text==nil) {
        
        [ProgressHUD showError:@"Please select cut." Interaction:NO];
        return NO;
        
    }else if ([cell.btn_weight.titleLabel.text isEqualToString:SELECTWEIGHT] || [cell.btn_weight.titleLabel.text isEqualToString:@""] || cell.btn_weight.titleLabel.text==nil) {
        
        [ProgressHUD showError:@"Please select weight." Interaction:NO];
        return NO;
        
    }else if ([cell.btn_type.titleLabel.text isEqualToString:SELECTTYPE] || [cell.btn_type.titleLabel.text isEqualToString:@""] || cell.btn_type.titleLabel.text==nil) {
        
        [ProgressHUD showError:@"Please select type." Interaction:NO];
        return NO;
        
    }
    return YES;
}

- (IBAction)callUsBtnClicked:(id)sender
{
    NSString *phNo = @"+919699933330";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Alert" message:@"Call facility is not available!!!"];
    }
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Close App" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    
    [alrt addAction:ok];
    //[alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
