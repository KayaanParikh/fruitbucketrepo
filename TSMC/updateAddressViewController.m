//
//  updateAddressViewController.m
//  TSMC
//
//  Created by user on 03/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "updateAddressViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "CustomTableViewCell.h"
#import "ObjStateData.h"


@interface updateAddressViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UISearchResultsUpdating>
{
    
    NSMutableArray *searchedLocationArray;
    BOOL isSearching;
    NSString*selectedCountryKeyy;
    NSDictionary *dictCountries;
}
@end

@implementation updateAddressViewController
@synthesize locationPicker,chkAddressCount;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden=NO;
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"AddressView"] isEqualToString:@"AddAddress"])
    {
        //[defaultAddressBtn setUserInteractionEnabled:NO];
        
        [addBtn setTitle:@"ADD" forState:UIControlStateNormal];
        ChkBtnClick=@"ADD";
        cancelBtnclick=@"onAddView";
        editBtnHeight.constant=0;
        editBtn.hidden=YES;
        [self showUserDetailsForAddAddress];
        
    }else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"AddressView"] isEqualToString:@"EditAddress"]){
        
        [defaultAddressBtn setUserInteractionEnabled:NO];
        cancelBtnclick=@"onEditView";
        editBtnHeight.constant=33;
        editBtn.hidden=NO;
        [self showUserDetailsForAddAddress];
        [self showUserDetailsForEditAddress];
        [self OutletsInteractionEnabledNo];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isSearching=NO;
    _shieldView.hidden=YES;
    
    firstNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* First Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    lastNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Last Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    telephoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Telephone" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    StreetLine1TF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Street Address Line 1" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    streetLine2TF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Street Address Line 2" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    _locationTableView.backgroundColor = [UIColor whiteColor];
    txtPinCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Pincode" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    [self initObjects];
    [self InitAttributeFrame];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - InitObjects
-(void)OutletsInteractionEnabledNo{
    
    [firstNameTF setUserInteractionEnabled:NO];
    [lastNameTF setUserInteractionEnabled:NO];
    [telephoneTF setUserInteractionEnabled:NO];
    [StreetLine1TF setUserInteractionEnabled:NO];
    [streetLine2TF setUserInteractionEnabled:NO];
    [selectLocationBtn setUserInteractionEnabled:NO];
    [defaultAddressBtn setUserInteractionEnabled:NO];
    
}

-(void)OutletsInteractionEnabledYES{
    
    [firstNameTF setUserInteractionEnabled:YES];
    [lastNameTF setUserInteractionEnabled:YES];
    [telephoneTF setUserInteractionEnabled:YES];
    [StreetLine1TF setUserInteractionEnabled:YES];
    [streetLine2TF setUserInteractionEnabled:YES];
    [selectLocationBtn setUserInteractionEnabled:YES];
    
}

-(void)initObjects{
    
    defaults=[NSUserDefaults standardUserDefaults];
    firstNameTF.tag=104;
    lastNameTF.tag=105;
    telephoneTF.tag=106;
    chkBoxImgView.tag=0;
    selectLocationBtn.tag=0;
    locationArr=[[NSMutableArray alloc]init];
    selectedLocationArr=[[NSMutableArray alloc]init];
    locationStateArr=[[NSMutableArray alloc]init];
}

-(void)InitAttributeFrame
{
    [scrollView setContentSize:CGSizeMake(375, 800)];
    //    titleLbl.backgroundColor=TopBarColor;
    self.title = @"Profile".uppercaseString;
    //    titleText.font=TopBarFont;
    //    titleText.textColor=TopBarTextColor;
    
    selectLocationBtn.layer.borderWidth=0.7f;
    cityStateBtn.layer.borderWidth=0.7f;
    cityBtn.layer.borderWidth=0.7f;
}
#pragma mark -Show User Details

-(void)showUserDetailsForAddAddress
{
    if([defaults objectForKey:@"userEmail"]!=nil || [defaults objectForKey:@"userFName"]!= nil || [defaults objectForKey:@"userLName"]!=nil )
    {
        firstNameTF.text=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userFName"]];
        lastNameTF.text=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userLName"]];
    
    }else{
        firstNameTF.placeholder=@"First Name";
        lastNameTF.placeholder=@"last Name";
    }
}

-(void)showUserDetailsForEditAddress
{
    if([defaults objectForKey:@"userFName1"]!= nil || [defaults objectForKey:@"userLName1"]!=nil || [defaults objectForKey:@"telephone"]!=nil || [defaults objectForKey:@"street1"]!= nil || [defaults objectForKey:@"street2"]!=nil || [defaults objectForKey:@"postcode"]!=nil || [defaults objectForKey:@"city"]!=nil || [defaults objectForKey:@"countryName"]!=nil || [defaults objectForKey:@"stateName"]!=nil)
    {
        firstNameTF.text=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userFName1"]];
        lastNameTF.text=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userLName1"]];
        telephoneTF.text=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"telephone"]];
        StreetLine1TF.text=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"street1"]];
        streetLine2TF.text=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"street2"]];
        
        
        txtPinCode.text = [defaults objectForKey:@"postcode"];
        [selectLocationBtn setTitle:[defaults objectForKey:@"countryName"] forState:UIControlStateNormal];
        [cityStateBtn setTitle:[defaults objectForKey:@"stateName"] forState:UIControlStateNormal];
        [cityBtn setTitle:[defaults objectForKey:@"city"] forState:UIControlStateNormal];
        
        BOOL chkDA=[defaults boolForKey:@"defaultAddress"];
        if(chkDA)
        {
            chkBoxImgView.image=[UIImage imageNamed:@"checked.png"];
            [defaultAddressBtn setUserInteractionEnabled:NO];
        }else{
            chkBoxImgView.image=[UIImage imageNamed:@"square.png"];
        }
    }else{
        firstNameTF.placeholder=@"First Name";
        lastNameTF.placeholder=@"last Name";
        telephoneTF.placeholder=@"Telephone";
        StreetLine1TF.placeholder=@"Street 1";
        streetLine2TF.placeholder=@"Street 2";
        [selectLocationBtn setTitle:@"Select Country" forState:UIControlStateNormal];
        [cityStateBtn setTitle:@"Select State" forState:UIControlStateNormal];
        [cityBtn setTitle:@"Select City" forState:UIControlStateNormal];
    }
}

#pragma mark - Button Clicked Methods
- (IBAction)backBtn_Clicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selectLocationBtn_Clicked:(id)sender
{
    selectLocationBtn.tag=1;
    ChkAPICall=@"selectLocation";
    self.searchBar.text = @"";
    isSearching = NO;
    [cityBtn setTitle:@"Select City" forState:UIControlStateNormal];
    [cityStateBtn setTitle:@"Select State" forState:UIControlStateNormal];
    
    [self getUseraddressesAPI];
}

- (IBAction)selectStateBtn_Clicked:(id)sender{
    
    [cityBtn setTitle:@"Select City" forState:UIControlStateNormal];
    self.searchBar.text = @"";
    isSearching = NO;
    if ([selectLocationBtn.currentTitle isEqual: @"Select Country"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select Country"];
    }else{
        ChkAPICall=@"selectStateLocation";
        [self getUseraddressesAPI];
    }
    
}
- (IBAction)selectCityBtn_Clicked:(id)sender{
    
    self.searchBar.text = @"";
    isSearching = NO;
    if ([selectLocationBtn.currentTitle isEqual: @"Select Country"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select Country"];
    }else if ([cityStateBtn.currentTitle isEqual: @"Select State"]){
        [self UIAlertViewControllerMethodTitle:HomeScreenTitleText message:@"Please select State"];
    }else{
        ChkAPICall=@"selectCityLocation";
        [self getUseraddressesAPI];
    }
    
}



- (IBAction)defaultAddressBtn_Clicked:(id)sender
{
    if(chkBoxImgView.tag==0)
    {
        chkBoxImgView.image=[UIImage imageNamed:@"checked.png"];
        chkBoxImgView.tag=1;
        defaultAdd=@"1";
        ChkAPICall=@"SelectDefault";
        //[self getUseraddressesAPI];
    }else if(chkBoxImgView.tag==1){
        
        chkBoxImgView.image=[UIImage imageNamed:@"square.png"];
        chkBoxImgView.tag=0;
        defaultAdd=@"0";
    }
}

- (IBAction)addBtn_Clicked:(id)sender
{
    if([ChkBtnClick isEqualToString:@"ADD"])
    {
        /*if(firstNameTF.text.length>0 && lastNameTF.text.length>0 && telephoneTF.text.length>0 && StreetLine1TF.text.length>0 && streetLine2TF.text.length>0 && selectLocationBtn.tag==1)
         {
         ChkAPICall2=@"AddAddress";
         [self addSelectedAddressAPI];
         }
         else
         {
         [self UIAlertViewControllerMethodTitle:@"Error" message:@"All fields should be filled"];
         
         }*/
        if(firstNameTF.text.length>0)
        {
            if(lastNameTF.text.length>0)
            {
                if(telephoneTF.text.length>0)
                {
                    if(telephoneTF.text.length==10)
                    {
                        if(StreetLine1TF.text.length>0)
                        {
                            if(streetLine2TF.text.length>0)
                            {
                                if(txtPinCode.text.length>0)
                                {
                                    
                                if(![selectLocationBtn.currentTitle isEqual: @"Select Country"]) {
                                    
                                    if(![cityStateBtn.currentTitle isEqual: @"Select State"]){
//                                        [self UIAlertViewControllerMethodTitle:@"" message:@"Select State!"];
//                                    }else{
                                        if(![cityBtn.currentTitle isEqual: @"Select City"]){
//                                            [self UIAlertViewControllerMethodTitle:@"" message:@"Select City!"];
//                                        }else{
                                            ChkAPICall2=@"AddAddress";
                                            [self addSelectedAddressAPI];
//                                        }
                                        }else{
                                            
                                        [self UIAlertViewControllerMethodTitle:@"" message:@"Select City!"];
                                        }
                                    }else{
                                        [self UIAlertViewControllerMethodTitle:@"" message:@"Select State!"];
                                    }
                                }else{
                                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Country!"];
                                }
                                
                                  }else{
                                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Pincode!"];
                                }
                                }else{
                                [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Street Line 2!"];
                                //enter lname
                            }
                        }else{
                            [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Street Line 1!"];
                            //enter fname
                        }
                    }else{
                        [self UIAlertViewControllerMethodTitle:@"" message:@"Enter valid 10 digit number!"];
                    }
                }else{
                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter valid 10 digit number!"];
                    //cp and pwrd must same
                }
            }else{
                [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Last Name!"];
                //enter password
            }
        }else{
            [self UIAlertViewControllerMethodTitle:@"" message:@"Enter First Name!"];
            //enter email
        }
    }
    else if([ChkBtnClick isEqualToString:@"Edit"])
    {
        /* if(firstNameTF.text.length>0 && lastNameTF.text.length>0 && telephoneTF.text.length>0 && StreetLine1TF.text.length>0 && streetLine2TF.text.length>0)
         {
         ChkAPICall2=@"EditAddress";
         [self addSelectedAddressAPI];
         }
         else
         {
         [self UIAlertViewControllerMethodTitle:@"Error" message:@"All fields should be filled"];
         
         }*/
        
        if(firstNameTF.text.length>0)
        {
            if(lastNameTF.text.length>0)
            {
                if(telephoneTF.text.length>0)
                {
                    if(telephoneTF.text.length==10)
                    {
                        if(StreetLine1TF.text.length>0)
                        {
                            if(streetLine2TF.text.length>0)
                            {
                                if(txtPinCode.text.length>0)
                                {
                                    
                                if(selectLocationBtn.titleLabel.text.length>0)
                                {
                                    ChkAPICall2=@"EditAddress";
                                    [self addSelectedAddressAPI];
                                }
                                else
                                {
                                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Location!"];
                                }
                                }else{
                                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Pincode!"];
                                }
                            }
                            else
                            {
                                [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Street Line 2!"];
                                //enter lname
                            }
                        }
                        else
                        {
                            [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Street Line 1!"];
                            //enter fname
                        }
                    }else
                    {
                        [self UIAlertViewControllerMethodTitle:@"" message:@"Enter valid 10 digit number!"];
                    }
                }
                else{
                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter valid 10 digit number!"];
                    //cp and pwrd must same
                }
            }else{
                [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Last Name!"];
                //enter password
            }
        }else{
            [self UIAlertViewControllerMethodTitle:@"" message:@"Enter First Name!"];
            //enter email
        }
    }
}


- (IBAction)EditBtnClicked:(id)sender
{
    ChkBtnClick=@"Edit";
    [self OutletsInteractionEnabledYES];
    editBtnHeight.constant=0;
    editBtn.hidden=YES;
    [addBtn setTitle:@"UPDATE" forState:UIControlStateNormal];
    
    
    BOOL chkDA = [defaults boolForKey:@"defaultAddress"];
    if(chkDA)
    {
        chkBoxImgView.image=[UIImage imageNamed:@"checked.png"];
        [defaultAddressBtn setUserInteractionEnabled:NO];
    }else{
        chkBoxImgView.image=[UIImage imageNamed:@"square.png"];
        [defaultAddressBtn setUserInteractionEnabled:NO];
    }
    
    /*if(firstNameTF.text.length>0 && lastNameTF.text.length>0 && telephoneTF.text.length>0 && StreetLine1TF.text.length>0 && streetLine2TF.text.length>0)
     {
     ChkAPICall2=@"EditAddress";
     [self addSelectedAddressAPI];
     }
     else
     {
     [self UIAlertViewControllerMethodTitle:@"Error" message:@"All fields should be filled"];
     
     }*/
}

- (IBAction)cancelBtn_Clicked:(id)sender
{
    if([cancelBtnclick isEqualToString:@"onAddView"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if([cancelBtnclick isEqualToString:@"onEditView"])
    {
        editBtnHeight.constant=33;
        editBtn.hidden=NO;
        [self OutletsInteractionEnabledNo];
    }
}

#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - API Call Methods

-(void)getUseraddressesAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }else{
            @try
            {
                NSDictionary *getJsonData;
                
                if([ChkAPICall isEqualToString:@"selectLocation"])
                {
//                    NSString *url=API_SELECT_LOCATIONS;
                    NSString *url = API_GET_COUNTRYLIST;
                    // NSString *userId=[defaults objectForKey:@"userID"];
                    
                    NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    if(getJsonData == nil || getJsonData.count<=0 || [getJsonData isEqual:@"(null)"])
                    {
                        locationPicker.hidden=YES;
                        [ProgressHUD showError:@"No Addresses Found" Interaction:NO];
                    }
                    else if(getJsonData.count > 0)
                    {
//                        NSDictionary * tempArray = [getJsonData valueForKey:@"list"];
                        
                        dictCountries = [[NSDictionary alloc]initWithDictionary:[getJsonData objectForKey:@"list"]];
                        locationKeyArr = [[NSMutableArray alloc] initWithArray:[dictCountries allKeys]];
                        locationArr = [[NSMutableArray alloc] initWithArray:[dictCountries allValues]];
                        _lbl_availability.text = @"Please select country";
                        _shieldView.hidden=NO;
                        //                        locationPicker.hidden=NO;
                        [_locationTableView reloadData];
                    }
                    
                }else if([ChkAPICall isEqualToString:@"selectStateLocation"]){
    //                    NSString *url=API_GET_STATELIST;
                        NSString *strCountryId = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"countryId"];
                        NSString *url =  [API_GET_STATELIST stringByAppendingString:strCountryId];
                        // NSString *userId=[defaults objectForKey:@"userID"];
                        
                        NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
                        
                        WebservicesClass *web=[[WebservicesClass alloc] init];
                        
                        getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                        
                        if(getJsonData == nil || getJsonData.count<=0 || [getJsonData isEqual:@"(null)"])
                        {
                            locationPicker.hidden=YES;
                            [ProgressHUD showError:@"No Addresses Found" Interaction:NO];
                        }
                        else if(getJsonData.count > 0)
                        {
                            
                            NSArray *arraytsik = [getJsonData allValues];
                            
                            [arraytsik[1] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                
                                ObjStateData *event = [ObjStateData modelObjectWithDictionary:obj];
                                [locationStateArr addObject:event];
                            }];
                            
                            
                            _lbl_availability.text = @"Please select state";
                            _shieldView.hidden=NO;
                            //                        locationPicker.hidden=NO;
                            [_locationTableView reloadData];
                        }
                        
                    }else if([ChkAPICall isEqualToString:@"selectCityLocation"]){
                        
                        NSString *stateId = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"stateId"];
        //                    NSString *url=API_SELECT_LOCATIONS;
                        NSString *url =  [API_GET_CITY stringByAppendingString:stateId];
                            // NSString *userId=[defaults objectForKey:@"userID"];
                            
                            NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
                            
                            WebservicesClass *web=[[WebservicesClass alloc] init];
                            
                            getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                            
                            if(getJsonData == nil || getJsonData.count<=0 || [getJsonData isEqual:@"(null)"])
                            {
                                locationPicker.hidden=YES;
                                [ProgressHUD showError:@"No Addresses Found" Interaction:NO];
                            }
                            else if(getJsonData.count > 0)
                            {
        //                        NSDictionary * tempArray = [getJsonData valueForKey:@"list"];
                                
                                NSDictionary *dict = [[NSDictionary alloc]initWithDictionary:[getJsonData objectForKey:@"list"]];
                                locationKeyArr = [[NSMutableArray alloc] initWithArray:[dict allKeys]];
                                locationArr = [[NSMutableArray alloc] initWithArray:[dict allValues]];
                                _lbl_availability.text = @"Please select city";
                                _shieldView.hidden=NO;
                                //                        locationPicker.hidden=NO;
                                [_locationTableView reloadData];
                            }
                                            
                    }else if([ChkAPICall isEqualToString:@"SelectDefault"]){
                    NSString *userId=[defaults objectForKey:USERID];
                    NSString *addressId=[[NSUserDefaults standardUserDefaults] objectForKey:@"Address_ID"];
                    
                    NSString *url=API_SET_DEFAULT_ADDRESSES;
                    
                    NSMutableDictionary *parameterDict;
                    
                    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"AddressView"] isEqualToString:@"AddAddress"])
                    {
                        parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id",@"",@"address_id", nil];
                    }
                    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"AddressView"] isEqualToString:@"EditAddress"])
                    {
                        
                        parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id",addressId,@"address_id", nil];
                    }
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                    
                    if ([getJsonData count] >0)
                    {
                        if (success){
                            [ProgressHUD showSuccess:@"Customer address default set successfully" Interaction:NO];
                            // [self UIAlertViewControllerMethodTitle:@"Done" message:@"Customer address default set successfully"];
                        }else{
                            [ProgressHUD showError:@"Customer address not set as default" Interaction:NO];
                            // [self UIAlertViewControllerMethodTitle:@"Error" message:@"Customer address not set as default"];
                        }
                    }
                }
                HUD.hidden=YES;
            }
            @catch (NSException *exception){
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Error" message:exc];
            }
        }
    });
}

-(void)addSelectedAddressAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not present....");
            
            //[HUD hide:YES];
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }else{
            @try
            {
                NSDictionary *getJsonData;
                NSString *userId=[defaults objectForKey:USERID];
                NSString *addressId=[[NSUserDefaults standardUserDefaults] objectForKey:@"Address_ID"];
                NSString *fname=firstNameTF.text;
                NSString *lname=lastNameTF.text;
                NSString *telephone=telephoneTF.text;
                NSString *street=StreetLine1TF.text;
                NSString *area=streetLine2TF.text;
                NSString *city = [NSString stringWithFormat:@"%@",cityBtn.titleLabel.text];
                
                NSString *country=[NSString stringWithFormat:@"%@",selectLocationBtn.titleLabel.text];
                // NSString *encodedUrlStr = [self urlencode:location];
                NSString *postcode= txtPinCode.text;
                
                NSString *countryId = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"countryId"];
                NSString *regionId = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"stateValue"];
                NSString *regionName = cityStateBtn.titleLabel.text;
                
                if([[[NSUserDefaults standardUserDefaults] objectForKey:@"chkAddressCount"] isEqualToString:@"ZeroAddress"])
                {
                    // chkBoxImgView.image=[UIImage imageNamed:@"checked.png"];
                    // [defaultAddressBtn setUserInteractionEnabled:NO];
                    defaultAdd=@"1";
                }
                else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"chkAddressCount"] isEqualToString:@"MoreThanOneAddress"])
                {
                    //chkBoxImgView.image=[UIImage imageNamed:@"square.png"];
                    // [defaultAddressBtn setUserInteractionEnabled:YES];
                    
                    if(chkBoxImgView.tag==1)
                    {
                        defaultAdd=@"1";
                    }else{
                        defaultAdd=@"0";
                    }
                }
                
                if([ChkAPICall2 isEqualToString:@"AddAddress"])
                {
                    NSString *url=API_ADD_ADDRESS;
                    
                    NSMutableDictionary * parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                         userId,@"id",
                                                         fname,@"firstname",
                                                         lname,@"lastname",
                                                         telephone,@"telephone",
                                                         street,@"street",
                                                         area,@"area",
                                                         postcode,@"postcode",
                                                         city,@"city",
                                                         defaultAdd,@"default",
                                                         countryId,@"country_id",
                                                         regionName,@"region",
                                                         regionId,@"region_id", nil];
                    
//                    NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"id",fname,@"firstname",lname,@"lastname",telephone,@"telephone",street,@"street",area,@"area",postcode,@"postcode",city,@"city",defaultAdd,@"default", nil];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:postcode forKey:@"postcode"];
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    NSLog(@"%@",getJsonData);
                    
                    BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                    
                    if ([getJsonData count] >0)
                    {
                        if(success)
                        {
                            NSString *msg=[getJsonData valueForKey:@"message"];
                            [ProgressHUD showSuccess:msg Interaction:NO];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                        else
                        {
                            [ProgressHUD showError:@"address not added" Interaction:NO];
                        }
                    }
                    
                }else if ([ChkAPICall2 isEqualToString:@"EditAddress"]){
                    NSString *url=API_EDIT_ADDRESSES;
                    
//                    NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"id",fname,@"firstname",lname,@"lastname",telephone,@"telephone",street,@"street",area,@"area",postcode,@"postcode",city,@"city",defaultAdd,@"default",addressId,@"addressId", nil];
                    
                    NSMutableDictionary * parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"id",
                                                         fname,@"firstname",
                                                         lname,@"lastname",
                                                         telephone,@"telephone",
                                                         street,@"street",
                                                         area,@"area",
                                                         postcode,@"postcode",
                                                         city,@"city",
                                                         defaultAdd,@"default",
                                                         countryId,@"country_id",
                                                         regionName,@"region",
                                                         regionId,@"region_id",
                                                         addressId,@"addressId"
                                                         , nil];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:postcode forKey:@"postcode"];
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    NSLog(@"%@",getJsonData);
                    
                    BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                    
                    if ([getJsonData count] >0)
                    {
                        if(success)
                        {
                            NSString *msg=[getJsonData valueForKey:@"message"];
                            [ProgressHUD showSuccess:msg Interaction:NO];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                        else
                        {
                            NSString *msg=[getJsonData valueForKey:@"message"];
                            [ProgressHUD showError:msg Interaction:NO];
                        }
                    }
                }
                HUD.hidden=YES;
            }@catch (NSException *exception){
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Error" message:exc];
            }
        }
        
    });
}

#pragma mark - Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    /*Restrict user to enter numbers in First Name and Last Name*/
    if (textField.tag==104) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    if (textField.tag==105) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    
    /*Restrict user to enter Charaters in Mobile textfield*/
    if (textField.tag==106) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return YES;
            }
            else{
                return NO;
            }
        }
    }
    return YES;
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

#pragma mark Picker View Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(locationArr.count>0){
        return locationArr.count;
    }else{
        return 0;
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,locationPicker.bounds.size.width, 44.0f)];
    
    NSString *rowItem = [locationArr objectAtIndex:row];
    [locationLbl setTextAlignment:NSTextAlignmentCenter];
    [locationLbl setTextColor: [UIColor blackColor]];
    [locationLbl setText:rowItem];
    [locationLbl setBackgroundColor:[UIColor clearColor]];
    
    return locationLbl;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedLocationIndex=[locationArr objectAtIndex:row];
}

#pragma mark - URLEncodedString
- (NSString *)urlencode:(NSString *)str
{
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[str UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i)
    {
        const unsigned char thisChar = source[i];
        if (thisChar == ' ')
        {
            [output appendString:@"%20"];
        }
        else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                 (thisChar >= 'a' && thisChar <= 'z') ||
                 (thisChar >= 'A' && thisChar <= 'Z') ||
                 (thisChar >= '0' && thisChar <= '9'))
        {
            [output appendFormat:@"%c", thisChar];
        }
        else
        {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

#pragma mark - Search delegate methods

- (void)filterContentForSearchText:(NSString*)searchText
{
    

    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF CONTAINS %@",
                                    searchText];
    
    
    if ([ChkAPICall isEqualToString:@"selectStateLocation"]){
        
        NSArray *sortedArray= [[locationStateArr valueForKey:@"label"] filteredArrayUsingPredicate:resultPredicate];
        searchedLocationArray=[[NSMutableArray alloc] init];
        [searchedLocationArray addObjectsFromArray:sortedArray];

    }else if ([ChkAPICall isEqualToString:@"selectCityLocation"]){
//        selectedLoc=[locationArr objectAtIndex:indexPath.row];
//        selectedCountryKey = [locationKeyArr objectAtIndex:indexPath.row];
////            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"cityName"];
//        [cityStateBtn setTitle:selectedLoc forState:UIControlStateNormal];
//        [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKey forKey:@"cityId"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        NSArray *sortedArray= [locationArr filteredArrayUsingPredicate:resultPredicate];
        NSArray *sortedKeyArray= [locationKeyArr filteredArrayUsingPredicate:resultPredicate];
        searchedLocationArray=[[NSMutableArray alloc] init];
        searchedLocationArray=[[NSMutableArray alloc] init];
        [searchedLocationArray addObjectsFromArray:sortedArray];
        [searchedLocationArray addObjectsFromArray:sortedKeyArray];
    }
    
//    NSArray *sortedArray= [locationArr filteredArrayUsingPredicate:resultPredicate];
//    searchedLocationArray=[[NSMutableArray alloc] init];
//    [searchedLocationArray addObjectsFromArray:sortedArray];
    
    [_locationTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchBar.text.length == 0) {
        isSearching = NO;
        [self.locationTableView reloadData];
    }
    else {
        isSearching = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    isSearching = YES;
    [self filterContentForSearchText:searchBar.text];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    isSearching = NO;
    _shieldView.hidden = true;
    [_locationTableView reloadData];
    
}

#pragma mark TableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableVw{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isSearching) {
        return searchedLocationArray.count;
    }
    else{
        if([ChkAPICall isEqualToString:@"selectStateLocation"]){
            if(locationStateArr.count>0){
                return locationStateArr.count;
            }else{
                return 0;
            }
        }else{
            if(locationArr.count>0){
                return locationArr.count;
            }else{
                return 0;
            }
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    if (isSearching) {
        cell.textLabel.text=[searchedLocationArray objectAtIndex:indexPath.row];
        
    }else{
        if ([ChkAPICall isEqualToString:@"selectStateLocation"]){
            ObjStateData *event = [locationStateArr objectAtIndex:indexPath.row];
            cell.textLabel.text= [NSString stringWithFormat:@"%@",event.label];
            
        }else if ([ChkAPICall isEqualToString:@"selectLocation"] || [ChkAPICall isEqualToString:@"selectCityLocation"]){
            cell.textLabel.text= [locationArr objectAtIndex:indexPath.row];
            
        }else{
            cell.textLabel.text=[searchedLocationArray objectAtIndex:indexPath.row];
        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int height=50;
    return height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //fetchedItemId,*fetchedItemName,*fetchedItemImage,*fetchedFoodType,*fetchedItemDesc,*fetchedSelectedStatus,*fetchedItemPrice,*fetchedCategoryId,*fetchedMsgId,*fetchedSpiceId,*fetchedLevelIds
    
    
    NSString *selectedLoc;
    if (isSearching) {
        selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
        [_searchBar resignFirstResponder];
        
        if ([ChkAPICall isEqualToString:@"selectStateLocation"]){
            selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
            
            for (ObjStateData *event in locationStateArr) {
              if ([event.title isEqualToString:selectedLoc]) {
                  [[NSUserDefaults standardUserDefaults] setObject:event.label forKey:@"stateId"];
                  [[NSUserDefaults standardUserDefaults] setObject:event.value forKey:@"stateValue"];
                  [[NSUserDefaults standardUserDefaults] synchronize];
                  [cityStateBtn setTitle:event.label forState:UIControlStateNormal];
              }
            }
            }else{
                selectedLoc=[searchedLocationArray objectAtIndex:indexPath.row];
                NSArray *temp = [dictCountries allKeysForObject:selectedLoc];
                NSString *key = [temp lastObject];
    //            selectedCountryKey = [searchedLocationKeyArray objectAtIndex:indexPath.row];
    //            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"countryName"];
                [[NSUserDefaults standardUserDefaults] setObject:key forKey:@"countryId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [selectLocationBtn setTitle:selectedLoc forState:UIControlStateNormal];
            }
         
    }else{
//        selectedLoc=[locationArr objectAtIndex:indexPath.row];
        
        if ([ChkAPICall isEqualToString:@"selectLocation"]){
            selectedLoc=[locationArr objectAtIndex:indexPath.row];
            selectedCountryKeyy = [locationKeyArr objectAtIndex:indexPath.row];
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"countryName"];
            [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKeyy forKey:@"countryId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [selectLocationBtn setTitle:selectedLoc forState:UIControlStateNormal];
            
        }else if ([ChkAPICall isEqualToString:@"selectStateLocation"]){
            
            ObjStateData *event = [locationStateArr objectAtIndex:indexPath.row];
            
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"stateName"];
            [[NSUserDefaults standardUserDefaults] setObject:event.label forKey:@"stateId"];
            [[NSUserDefaults standardUserDefaults] setObject:event.value forKey:@"stateValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [cityStateBtn setTitle:event.label  forState:UIControlStateNormal];
            
        }else if ([ChkAPICall isEqualToString:@"selectCityLocation"]){
            selectedLoc=[locationArr objectAtIndex:indexPath.row];
            selectedCountryKeyy = [locationKeyArr objectAtIndex:indexPath.row];
//            [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:@"cityName"];
            
            [[NSUserDefaults standardUserDefaults] setObject:selectedCountryKeyy forKey:@"cityId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [cityBtn setTitle:selectedLoc forState:UIControlStateNormal];
        }
    }
//    [selectLocationBtn setTitle:selectedLoc forState:UIControlStateNormal];
    _shieldView.hidden=YES;
    
    //        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ShowLocationViewFirstTime"];
    
    //        [[NSUserDefaults standardUserDefaults] setObject:selectedLoc forKey:SELECTEDLOCATION];
    //        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //        HUD.dimBackground = YES;
    //        HUD.labelText = @"Please wait...";
    //        [self getLocationWisePrice:selectedLoc];
    
}


@end
