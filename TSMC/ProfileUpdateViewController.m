//
//  ProfileUpdateViewController.m
//  TSMC
//
//  Created by user on 03/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "ProfileUpdateViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "CartController.h"

#import "updateAddressViewController.h"

@interface ProfileUpdateViewController ()
{
    CustomTableViewCell *cell;

}
@end

@implementation ProfileUpdateViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    [self getAddressesAPI];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
       FirstNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* First Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
       LastNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Last Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        EmailTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Emaid Id" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        PasswordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Password " attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        ConfirmPasswordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        addressesTblView.backgroundColor = [UIColor whiteColor];
    
    [self initObjects];
    [self InitAttributeFrame];
    [self showUserDetails];
    
    //[userInfoView bringSubviewToFront:addAddressesBtn];
   // [userInfoView addSubview:addAddressesBtn];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - InitObjects

-(void)initObjects
{
    defaults=[NSUserDefaults standardUserDefaults];
    [self InitialViewSizeAndHiddenControls];
    FirstNameTF.tag=104;
    LastNameTF.tag=105;
}
-(void)InitAttributeFrame
{
    [scrollView setContentSize:CGSizeMake(375, 603)];
    //[addressesTblView setUserInteractionEnabled:NO];
//    [scrollView setUserInteractionEnabled:YES];
   scrollView.delegate=self;
    
    //_titleLbl.backgroundColor=TopBarColor;
    self.title = @"Profile".uppercaseString;
    //_titleText.font=TopBarFont;
    //_titleText.textColor=TopBarTextColor;
    NSString *cartCount=[[NSUserDefaults standardUserDefaults] valueForKey:@"BADGECOUNT"];
    if ([cartCount isEqualToString:@"0"] || [cartCount isEqualToString:@"(null)"]) {
        lbl_badgeCount.hidden=YES;
    }
    else
    {
        lbl_badgeCount.hidden=YES;
        lbl_badgeCount.text=cartCount;
        
    }
    
    lbl_badgeCount.layer.masksToBounds=YES;
    lbl_badgeCount.layer.cornerRadius=lbl_badgeCount.frame.size.width/2;
    

    editBtn.tag=0;
    // textView.textContainerInset = UIEdgeInsetsMake(10, 0, 10, 0);
    
    
}
#pragma mark - FlagArrForHeader

-(void)InitialViewSizeAndHiddenControls
{
    confirmPasswordHeight.constant=0;
    confirmPasswordLblHeight.constant=0;
    cancelBtnHeight.constant=0;
    updateBtnHeight.constant=0;
    userInfoViewHeight.constant=0;
    
}
#pragma mark -Show User Details

-(void)showUserDetails
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"]!=nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"userFName"]!= nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"userLName"]!=nil )
    {
        EmailTF.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"]];
        FirstNameTF.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userFName"]];
        LastNameTF.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userLName"]];
        PasswordTF.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"entity_id"]];
    }
    else
    {
        EmailTF.placeholder=@"Email Id";
        FirstNameTF.placeholder=@"First Name";
        LastNameTF.placeholder=@"last Name";
        PasswordTF.placeholder=@"Password";
    }
    [EmailTF setUserInteractionEnabled:NO];
    [FirstNameTF setUserInteractionEnabled:NO];
    [LastNameTF setUserInteractionEnabled:NO];
    [PasswordTF setUserInteractionEnabled:NO];
    
}

#pragma mark - Button Clicked Methods
- (IBAction)backBtn_Clicked:(id)sender
{
    [self loadStoryBoardWithIdentifier:@"ViewController"];
}

- (IBAction)editBtn_Clicked:(id)sender
{
    editBtn.tag=0;
    if(editBtn.tag==0)
    {
        editBtn.hidden=YES;
        confirmPasswordHeight.constant=30;
        confirmPasswordLblHeight.constant=1;
        cancelBtnHeight.constant=30;
        updateBtnHeight.constant=30;
        myAddresesTop.constant=myAddresesTop.constant+80;
        userInfoViewHeight.constant=370;
        
        PasswordTF.placeholder=@"New Password";
        PasswordTF.text=@"";
        PasswordTF.secureTextEntry=YES;
        
        editBtn.tag=1;
        
       // [EmailTF setUserInteractionEnabled:YES];
        [FirstNameTF setUserInteractionEnabled:YES];
        [LastNameTF setUserInteractionEnabled:YES];
        [PasswordTF setUserInteractionEnabled:YES];
    }
    
    
    
}

- (IBAction)cancelBtn_Clicked:(id)sender
{
    editBtn.hidden=NO;
    confirmPasswordHeight.constant=0;
    confirmPasswordLblHeight.constant=0;
    cancelBtnHeight.constant=0;
    updateBtnHeight.constant=0;
    myAddresesTop.constant=myAddresesTop.constant-80;
    
    PasswordTF.placeholder=@"New Password";
    PasswordTF.text=@"Password";
    PasswordTF.secureTextEntry=YES;
    
    [EmailTF setUserInteractionEnabled:NO];
    [FirstNameTF setUserInteractionEnabled:NO];
    [LastNameTF setUserInteractionEnabled:NO];
    [PasswordTF setUserInteractionEnabled:NO];
}
-(BOOL)validation{
    if (![self validemail]){
        
        [ProgressHUD showError:@"Please Enter Valid Email." Interaction:NO];
        
        return NO;
        
    }
    
    else if ([FirstNameTF.text isEqualToString:@""]) {
        
        [ProgressHUD showError:@"Please Enter First Name." Interaction:NO];
        return NO;
    }
    else if ([LastNameTF.text isEqualToString:@""]){
        
        [ProgressHUD showError:@"Please Enter Last Name." Interaction:NO];
        
        return NO;
        
    }
    
    return YES;
}

-(BOOL) validemail

{
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    //Valid email address
    if ([emailTest evaluateWithObject:EmailTF.text] == YES)
    {
        NSLog(@"Valid email Id");
        
        return true;
        
    }
    else
        
    {
        
        //        [ProgressHUD showError:PROGRESS_ENTER_VALID_EMAIL Interaction:NO];
        NSLog(@"email not in proper format");
        return false;
        
    }
    
}

- (IBAction)updateBtn_Clicked:(id)sender
{
    [LastNameTF resignFirstResponder];
    if ([self validation]) {
        [self updateAPI];

    }
    
//    if(EmailTF.text.length>0)
//    {
//        if([self validEmail:EmailTF.text])
//        {
//            if(FirstNameTF.text.length>0)
//            {
//                if(LastNameTF.text.length>0)
//                {
//                    if(PasswordTF.text.length>0)
//                    {
//                        if(!(PasswordTF.text.length < 6) && !(PasswordTF.text.length > 15))
//                        {
//                            if(ConfirmPasswordTF.text.length>0)
//                            {
//                                if([ConfirmPasswordTF.text isEqualToString:PasswordTF.text])
//                                {
//                                    [self updateAPI];
//                                }
//                                else
//                                {
//                                    [self UIAlertViewControllerMethodTitle:@"" message:@"Password and confirm password must be same!"];
//                                    //enter lname
//                                }
//                            }
//                            else
//                            {
//                                [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Confirm Password!"];
//                                //enter fname
//                            }
//                        }
//                        else
//                        {
//                            [self UIAlertViewControllerMethodTitle:@"" message:@"Password must have 6 Characters!"];
//                            //cp and pwrd must same
//                        }
//                        
//                    }
//                    else
//                    {
//                        [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Password!"];
//                        //enter cp
//                    }
//                }
//                else
//                {
//                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Last Name!"];
//                    //need 6 character long password
//                }
//            }
//            else
//            {
//                [self UIAlertViewControllerMethodTitle:@"" message:@"Enter First Name!"];
//                //enter password
//            }
//        }
//        else
//        {
//            [self UIAlertViewControllerMethodTitle:@"" message:@"Enter valid Email Address!"];
//            //invalid email
//        }
//    }
//    else
//    {
//        [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Email!"];
//        //enter email
//    }


}

- (IBAction)addAddressBtn_Clicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:@"AddAddress" forKey:@"AddressView"];
    [self loadStoryBoardWithIdentifier:@"UpdateAddressVC"];
}

- (IBAction)cartMethod:(id)sender {
    
    CartController *crt=[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
    [self.navigationController pushViewController:crt animated:NO];
}

- (IBAction)addAddressesBtn2_Clicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:@"AddAddress" forKey:@"AddressView"];
    [self loadStoryBoardWithIdentifier:@"UpdateAddressVC"];
}


#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (finalAddressArr.count >0)
    {
       // return [[[NSUserDefaults standardUserDefaults]objectForKey:@"SELECTED_ADDRESS"] count];
        return [finalAddressArr count];
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"CustomTableViewCell";
    cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    //UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CELLID"];
    if (cell==nil)
    {
        cell=[[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    cell.addressLbl.text=[NSString stringWithFormat:@"%@",[finalAddressArr objectAtIndex:indexPath.row]];

    cell.selectedAddresChkBox.tag=indexPath.row;
    [cell.selectedAddresChkBox addTarget:self action:@selector(chkBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.selectedAddresChkBox setFrame:CGRectMake(10, 10, 30, 30)];


    if(![ChkAPICall isEqualToString:@"getAddress"])
    {
        if ([finalAddressArr objectAtIndex:indexPath.row] == checkedData)
        {
            [cell.selectedAddresChkBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            //cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            [cell.selectedAddresChkBox setImage:[UIImage imageNamed:@"square.png"] forState:UIControlStateNormal];
            //cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else
    {
        BOOL chkDefault=[[ChkDefaultArr objectAtIndex:indexPath.row] boolValue];
        if(chkDefault)
        {
            [cell.selectedAddresChkBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.selectedAddresChkBox setImage:[UIImage imageNamed:@"square.png"] forState:UIControlStateNormal];
        }

    }
    
//   [cell layoutSubviews];
//   [cell layoutIfNeeded];
    
   return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *FirstStr,*secStr;
    NSRange whiteSpaceRange = [[decodeStreetArr objectAtIndex:indexPath.row] rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound)
    {
        FirstStr=[[decodeStreetArr objectAtIndex:indexPath.row] substringToIndex:whiteSpaceRange.location-1];
        [[NSUserDefaults standardUserDefaults]setObject:FirstStr forKey:@"street1"];
        
        NSInteger secIndex=FirstStr.length+whiteSpaceRange.length;
        secStr=[[decodeStreetArr objectAtIndex:indexPath.row] substringFromIndex:secIndex];
        [[NSUserDefaults standardUserDefaults]setObject:secStr forKey:@"street2"];
    
    }
    [[NSUserDefaults standardUserDefaults] setObject:[fNameArr objectAtIndex:indexPath.row] forKey:@"userFName1"];
    [[NSUserDefaults standardUserDefaults] setObject:[lNameArr objectAtIndex:indexPath.row] forKey:@"userLName1"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"EditAddress" forKey:@"AddressView"];
    [[NSUserDefaults standardUserDefaults]setObject:[telephoneArr objectAtIndex:indexPath.row] forKey:@"telephone"];
    [[NSUserDefaults standardUserDefaults]setObject:[postCodeArray objectAtIndex:indexPath.row] forKey:@"postcode"];
    [[NSUserDefaults standardUserDefaults]setObject:[cityArr objectAtIndex:indexPath.row] forKey:@"city"];
    [[NSUserDefaults standardUserDefaults] setObject:[addressesIDArr objectAtIndex:indexPath.row] forKey:@"Address_ID"];
    [[NSUserDefaults standardUserDefaults] setObject:[stateListArray objectAtIndex:indexPath.row] forKey:@"stateName"];
    [[NSUserDefaults standardUserDefaults] setObject:[countryListArray objectAtIndex:indexPath.row] forKey:@"countryName"];

    NSLog(@"%d",(int)[[ChkDefaultArr objectAtIndex:indexPath.row] boolValue]);
    [[NSUserDefaults standardUserDefaults] setBool:[[ChkDefaultArr objectAtIndex:indexPath.row] boolValue] forKey:@"defaultAddress"];
    
    [self loadStoryBoardWithIdentifier:@"UpdateAddressVC"];
   /*if ([finalAddressArr objectAtIndex:indexPath.row] != checkedData)
    {
        checkedData = [finalAddressArr objectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults] setObject:[addressesIDArr objectAtIndex:indexPath.row] forKey:@"Address_ID"];
         [self setDefaultAddressAPI];
    }
    
    [tableView reloadData];*/
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
/*- (void)scrollViewDidScroll:(UIScrollView *)scrollView1
{
    if(scrollView1==scrollView)
    {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           CGRect frame = addressesTblView.frame;
                           frame.size.height = addressesTblView.contentSize.height;
                           addressesTblView.frame = frame;
                       });
        
        [addressesTblView setNeedsUpdateConstraints];
        [scrollView setNeedsUpdateConstraints];
        [addressesTblView updateConstraintsIfNeeded];
        [scrollView updateConstraintsIfNeeded];
        
        
    }
}
*/-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView1
{
    
        
        [addressesTblView setNeedsUpdateConstraints];
        [scrollView setNeedsUpdateConstraints];
        [addressesTblView updateConstraintsIfNeeded];
        [scrollView updateConstraintsIfNeeded];
        
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView1 willDecelerate:(BOOL)decelerate
{
    if(scrollView1==scrollView)
    {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           CGRect frame = addressesTblView.frame;
                           frame.size.height = addressesTblView.contentSize.height;
                           addressesTblView.frame = frame;
                       });
    }
    [addressesTblView setNeedsUpdateConstraints];
    [scrollView setNeedsUpdateConstraints];
    [addressesTblView updateConstraintsIfNeeded];
    [scrollView updateConstraintsIfNeeded];
    decelerate=YES;

}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView1
{
    if(scrollView1==scrollView)
    {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           CGRect frame = addressesTblView.frame;
                           frame.size.height = addressesTblView.contentSize.height;
                           addressesTblView.frame = frame;
                       });
    }
    [addressesTblView setNeedsUpdateConstraints];
    [scrollView setNeedsUpdateConstraints];
    [addressesTblView updateConstraintsIfNeeded];
    [scrollView updateConstraintsIfNeeded];
}

-(void)chkBoxButtonClicked:(UIButton*)sender
{
    
//    if (!(sender.tag == 0))
//    {
    ChkAPICall = @"notGetAddress";
    checkedData = [finalAddressArr objectAtIndex:sender.tag];
    [[NSUserDefaults standardUserDefaults] setObject:[addressesIDArr objectAtIndex:sender.tag] forKey:@"Address_ID"];
    [self setDefaultAddressAPI];
    [self getAddressesAPI];
//    }
    [addressesTblView reloadData];
}

#pragma mark - OtherMethods

-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}


#pragma mark - API Call Methods

-(void)updateAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }
        else
        {
            @try
            {
                NSString *email=EmailTF.text;
                NSString *pass=PasswordTF.text;
                NSString *fname=FirstNameTF.text;
                NSString *lname=LastNameTF.text;
                NSString *userId=[defaults objectForKey:USERID];
                
                NSString *url=API_EDIT_PROFILE;
                
                NSMutableDictionary *parameterDict;
                if(pass.length>0)
                {
                   parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"id",email,@"email",fname,@"firstname",lname,@"lastname",pass,@"password", nil];
                }
                else
                {
                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"id",email,@"email",fname,@"firstname",lname,@"lastname", nil];
                }
                
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                 HUD.hidden=YES;
                
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    if (success)
                    {
                        HUD.hidden=YES;
                        
                        NSDictionary *userDict=[getJsonData valueForKey:@"data"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[userDict valueForKey:@"email"] forKey:USEREMAIL];;
                        [[NSUserDefaults standardUserDefaults] setObject:[userDict valueForKey:@"firstname"] forKey:USERFIRSTNAME];
                        [[NSUserDefaults standardUserDefaults] setObject:[userDict valueForKey:@"lastname"] forKey:USERLASTNAME];
                        [[NSUserDefaults standardUserDefaults] setObject:[userDict valueForKey:@"password_hash"] forKey:@"userPassword"];
                        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:ISLOGIN];
                        
                        [ProgressHUD showSuccess:@"Update Successful" Interaction:YES];
                        [self loadStoryBoardWithIdentifier:@"ProfileUpdateVC"];
                        //[self UIAlertViewControllerMethodTitle:@"Success" message:@"You have updated successfully!"];
                    }
                    
                    else
                    {
                        NSString *msg=[NSString stringWithFormat:@"%@",[getJsonData valueForKey:@"message"]];
                        [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:msg];
                        
                    }
                }
                
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
        
    });
}
-(void)getAddressesAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    ChkAPICall = @"getAddress";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not present....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }
        else
        {
            @try
            {
                
                NSString *userId=[defaults objectForKey:USERID];
                
                NSString *url=API_GET_ADDRESSES;
                
                NSMutableDictionary *parameterDict;
                
                parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id", nil];
                    
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
               NSLog(@"%@",getJsonData);
                 HUD.hidden=YES;
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    addressesArr=[[NSMutableArray alloc]init];
                    addressesArr2=[[NSMutableArray alloc]init];
                    addressesIDArr=[[NSMutableArray alloc]init];
                    decodedAddressesArr=[[NSMutableArray alloc]init];
                    streerAddressArr=[[NSMutableArray alloc]init];
                    decodeStreetArr=[[NSMutableArray alloc]init];
                    finalAddressArr=[[NSMutableArray alloc]init];
                    telephoneArr=[[NSMutableArray alloc]init];
                    cityArr=[[NSMutableArray alloc]init];
                    ChkDefaultArr=[[NSMutableArray alloc]init];
                    fNameArr=[[NSMutableArray alloc]init];
                    lNameArr=[[NSMutableArray alloc]init];
                    postCodeArray = [[NSMutableArray alloc]init];
                    countryListArray = [[NSMutableArray alloc]init];
                    stateListArray = [[NSMutableArray alloc]init];
                    
                    if (success)
                    {
                       [addressesArr addObject:[getJsonData valueForKey:@"addresses"]];
                    
                        for(int i=0;i<addressesArr.count;i++)
                        {
                            for (int j=0; j<[[addressesArr objectAtIndex:i] count]; j++)
                            {
                                
//                                if([[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] objectForKey:@"postcode"] != nil){
                                
//                                if ([[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] containsObject:@"postcode"]) {
//                                  NSLog(@"'key' exists.");
                                    [postCodeArray addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"postcode"]];
//                                }else{
//                                    [postCodeArray addObject:@"123456"];
//                                }
                                   
//                                }
                               
                                //[addressesArr2 addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"postcode"]];
                                
//                                if([[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"street"] != nil){
                                    [streerAddressArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"street"]];
//                                }
                                [addressesIDArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"entity_id"]];
                                [telephoneArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"telephone"]];
                                [cityArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"city"]];
                                [fNameArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"firstname"]];
                                [lNameArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"lastname"]];
                                [countryListArray addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"country"]];
                                [stateListArray addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"region"]];
                                
                                [ChkDefaultArr addObject:[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"default"]];
                                
                                
                            }
                            
                        }
                        // NSLog(@"Default Address:%@", ChkDefaultArr);
                        
                    }
                    //Decode postcode address array
                    for(int i=0;i<postCodeArray.count;i++)
                    {
                        NSString *encoded = [postCodeArray objectAtIndex:i];
                        NSString *decoded = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)encoded, CFSTR(""), kCFStringEncodingUTF8);
                        [decodedAddressesArr addObject:decoded];
                        
                    }
                    //decode street array
                    for(int i=0;i<streerAddressArr.count;i++)
                    {
                        NSString *decode=[[streerAddressArr objectAtIndex:i] stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
                        [decodeStreetArr addObject:decode];
                        
                    }
                    //Concating addressesArr and Street Array
                    for(int i=0;i<decodeStreetArr.count && i<decodedAddressesArr.count && cityArr.count;i++)
                    {
                        NSString *finalAddressStr=[NSString stringWithFormat:@"%@, %@, %@",[decodeStreetArr objectAtIndex:i],[decodedAddressesArr objectAtIndex:i],[cityArr objectAtIndex:i]];
                        
                        [finalAddressArr addObject:finalAddressStr];
                        
                    }
                   // NSLog(@"%@",finalAddressArr);
                    HUD.hidden=YES;
                    
                    
                  
                    if (finalAddressArr.count > 0)
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"MoreThanOneAddress" forKey:@"chkAddressCount"];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"ZeroAddress" forKey:@"chkAddressCount"];
                    }
                    
                    [addressesTblView reloadData];
                    
                    
                    long AddressTblHeight = postCodeArray.count * 50;
                    //addressesTblView.frame = CGRectMake(addressesTblView.frame.origin.x,addressesTblView.frame.origin.y, addressesTblView.frame.size.width, addressesTblView.contentSize.height);
                   // addressesTblViewHeight.constant=myAddressesBtn.frame.origin.y+50*(addressesArr2.count)+64;
                    addressesTblViewHeight.constant=AddressTblHeight;
                    
                    float ftbl = addressesTblView.frame.origin.y + addressesTblView.contentSize.height + 15;
                    scrollView.contentSize=CGSizeMake(320, ftbl);
                    
                    [self.view needsUpdateConstraints];

                }
                
            
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
        
    });
 
}
-(void)setDefaultAddressAPI
{
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }
        else
        {
            @try
            {
                NSString *userId=[defaults objectForKey:USERID];
                NSString *addressId=[[NSUserDefaults standardUserDefaults] objectForKey:@"Address_ID"];
                
                NSString *url=API_SET_DEFAULT_ADDRESSES;
                
                NSMutableDictionary *parameterDict;
                
                parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id",addressId,@"address_id", nil];
                
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                 HUD.hidden=YES;
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    
                    if (success)
                    {
                        [ProgressHUD showSuccess:@"Customer address default set successfully" Interaction:NO];
                      //  [self UIAlertViewControllerMethodTitle:@"Done" message:@"Customer address default set successfully"];
                    }
                    else
                    {
                        [ProgressHUD showError:@"Customer address not set as default" Interaction:NO];
                       //[self UIAlertViewControllerMethodTitle:@"Error" message:@"Customer address not set as default"];
                    }
                
                }
                
                
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
        
    });
    
}

#pragma mark - Text field Validation
- (IBAction)EmailTFDidEndEditing:(id)sender
{
   /* BOOL email=[self validEmail:EmailTF.text];
    if(email)
    {
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Signu Up Error" message:@"Invalid Email!"];
    }*/
}

- (IBAction)FirstNameTFDidEndEditing:(id)sender
{
}

- (IBAction)LastNameTFDidEndEditing:(id)sender
{
   // [LastNameTF resignFirstResponder];
}

- (IBAction)passwordTFDidEndEditing:(id)sender
{
   /* if(!(PasswordTF.text.length < 6))
    {
        if(!(PasswordTF.text.length > 15))
        {
            PasswordTF.tag=1;
        }
        else
        {
            [self UIAlertViewControllerMethodTitle:@"Signu Up Error" message:@"Large Password!"];
           
        }
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Signu Up Error" message:@"Short Password!"];
    }*/
}

- (IBAction)confirmPasswordTFDidEndEditing:(id)sender
{
   /* if([ConfirmPasswordTF.text isEqualToString:PasswordTF.text])
    {
        
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Signu Up Error" message:@"Password Mismatch!"];
       
    }*/
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    /*Restrict user to enter numbers in First Name and Last Name*/
    if (textField.tag==104) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    if (textField.tag==105) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    
    /*Restrict user to enter Charaters in Mobile textfield*/
    if (textField.tag==106) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return YES;
            }
            else{
                return NO;
            }
        }
    }
    
    
    return YES;
}


#pragma mark - ValidationMethods

- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           HUD.hidden=YES;
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               HUD.hidden=YES;
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
