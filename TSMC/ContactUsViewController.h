//
//  ContactUsViewController.h
//  TSMC
//
//  Created by user on 14/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface ContactUsViewController : UIViewController
{
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *titleText;
    IBOutlet UIButton *backBtn;
    
    IBOutlet UIScrollView *scrollView1;
    IBOutlet UIView *subView;
    IBOutlet UITextField *NameTF;
    IBOutlet UITextField *EmailTF;
    IBOutlet UITextField *TelephoneTF;
    IBOutlet UITextField *CommentTF;
    IBOutlet UITextView *CommentTV;
    IBOutlet UIButton *submitBtn;
    
    MBProgressHUD *HUD;
}
- (IBAction)backBtn_Clicked:(id)sender;
- (IBAction)submitBtn_Clicked:(id)sender;
@end
