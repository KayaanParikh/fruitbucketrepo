//
//  SidebarViewController.h
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrayForBool;
    NSMutableArray *sectionTitleArray;
}


@property (nonatomic,strong)NSMutableArray *sectionImagesArray,*subTitleImgsArray;

@end

