//
//  HistoryViewController.m
//  TSMC
//
//  Created by user on 05/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "HistoryViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "CustomTableViewCell.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    historyTblView.backgroundColor = [UIColor whiteColor];
    [self InitAttributeFrame];
    [self getHistoryAPI];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;
}
    

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark Init methods
-(void)InitAttributeFrame
{
    
    
//    titleLbl.backgroundColor=TopBarColor;
    self.title = @"History".uppercaseString;
//    titleText.font=TopBarFont;
//    titleText.textColor=TopBarTextColor;
    

}
#pragma mark - API Methods
-(void)getHistoryAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not present....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }
        else
        {
            @try
            {
                
                NSString *userId=[[NSUserDefaults standardUserDefaults] objectForKey:USERID];
                
                NSString *url=API_GET_HISTORY;
                
                NSMutableDictionary *parameterDict;
                
                parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"user_id", nil];
                
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    historyArr=[[NSMutableArray alloc]init];
                    customerArr=[[NSMutableArray alloc]init];
                    orderIdArr=[[NSMutableArray alloc]init];
                    orderStatusArr=[[NSMutableArray alloc]init];
                    orderDateTimeArr=[[NSMutableArray alloc]init];
                    orderAmount=[[NSMutableArray alloc]init];
                    orderAmount2=[[NSMutableArray alloc]init];
                    shippingCharges=[[NSMutableArray alloc]init];
                    subTotal=[[NSMutableArray alloc]init];
                    comment=[[NSMutableArray alloc]init];
                    orderDateArr2=[[NSMutableArray alloc]init];
                    orderTimeArr2=[[NSMutableArray alloc]init];
                    customDeliveryDateArr=[[NSMutableArray alloc]init];
                    customDeliveryTimeArr=[[NSMutableArray alloc]init];
                    couponCodeNameArr=[[NSMutableArray alloc]init];
                    couponDiscountArr=[[NSMutableArray alloc]init];
                    
                    if (success)
                    {
                        HUD.hidden=YES;
                        [customerArr addObject:[getJsonData valueForKey:@"customer"]];
                        for(int i=0;i<customerArr.count;i++)
                        {
                            for (int j=0; j<[[customerArr objectAtIndex:i] count]; j++)
                            {
                                [orderIdArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"increment_id"] ];
                                [orderStatusArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"status"]];
                                [orderDateTimeArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"created_at"]];
                                [orderAmount addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"grand_total"]];
                                [shippingCharges addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"shipping_amount"]];
                                [subTotal addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"subtotal"]];
                                [comment addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"comment"]];
                                [orderDateArr2 addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"delivery_date"]];
                                [orderTimeArr2 addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"delivery_time"]];
                                [customDeliveryDateArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"custom_product_delivery_date"]];
                                [customDeliveryTimeArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"custom_product_delivery_time"]];
                                [couponCodeNameArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"coupon_code"]];
                                [couponDiscountArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"discount_amount"]];
                            }
                            
                            for(int i=0;i<orderAmount.count;i++)
                            {
                                CGFloat value2=[[orderAmount objectAtIndex:i] floatValue];
                                NSString *str2=[NSString stringWithFormat:@"%.2f",value2];
                                [orderAmount2 addObject:str2];
                            }
                        }
                        [historyTblView reloadData];
                    }
                    else
                    {
                         [ProgressHUD showError:[getJsonData valueForKey:@"message"] Interaction:NO];
                    }
                    
                }
                
                HUD.hidden=YES;
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
        
    });
    
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(orderIdArr.count>0)
    {
        return orderIdArr.count;
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"CustomTableViewCell";
    
    CustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil)
    {
        cell=[[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    [cell.orderBackView.layer setShadowOpacity:0.3];
    [cell.orderBackView.layer setShadowRadius:2.0];
    [cell.orderBackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    
    
    cell.lbl_orderId.text=[NSString stringWithFormat:@"#%@",[orderIdArr objectAtIndex:indexPath.row]];
    cell.lbl_orderStatus.text=[NSString stringWithFormat:@"%@",[orderStatusArr objectAtIndex:indexPath.row]];
    cell.lbl_order_Time.text=[NSString stringWithFormat:@"%@",[orderDateTimeArr objectAtIndex:indexPath.row]];
    cell.lbl_orderAmount.text=[NSString stringWithFormat:@"₹ %@",[orderAmount2 objectAtIndex:indexPath.row]];
    cell.forwarderImgView.image=[UIImage imageNamed:@"forwarder.png"];
   // cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults]setObject:[orderIdArr objectAtIndex:indexPath.row] forKey:@"orderId"];
    [[NSUserDefaults standardUserDefaults]setObject:[orderDateTimeArr objectAtIndex:indexPath.row] forKey:@"DateTime"];
    [[NSUserDefaults standardUserDefaults]setObject:[orderStatusArr objectAtIndex:indexPath.row] forKey:@"Status"];
    [[NSUserDefaults standardUserDefaults]setObject:@"Delivery Service" forKey:@"OrderType"];
    [[NSUserDefaults standardUserDefaults]setObject:[orderAmount2 objectAtIndex:indexPath.row] forKey:@"OrderAmount"];
    [[NSUserDefaults standardUserDefaults]setObject:[shippingCharges objectAtIndex:indexPath.row] forKey:@"ShippingCharge"];
    [[NSUserDefaults standardUserDefaults]setObject:[subTotal objectAtIndex:indexPath.row] forKey:@"SubTotal"];
    //[[NSUserDefaults standardUserDefaults]setObject:[comment objectAtIndex:indexPath.row] forKey:@"OrderComment"];
//    [[NSUserDefaults standardUserDefaults]setObject:[orderDateArr2 objectAtIndex:indexPath.row] forKey:@"OrderDate2"];
//    [[NSUserDefaults standardUserDefaults]setObject:[orderTimeArr2 objectAtIndex:indexPath.row] forKey:@"OrderTime2"];
    
    //NSLog(@"%@",[customDeliveryDateArr objectAtIndex:indexPath.row]);
    if(![[couponCodeNameArr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
        [[NSUserDefaults standardUserDefaults]setObject:[couponCodeNameArr objectAtIndex:indexPath.row] forKey:@"CouponName"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"null" forKey:@"CouponName"];
    }
    [[NSUserDefaults standardUserDefaults]setObject:[couponDiscountArr objectAtIndex:indexPath.row] forKey:@"CouponDiscount"];
    
    if(![[customDeliveryDateArr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
        [[NSUserDefaults standardUserDefaults]setObject:[customDeliveryDateArr objectAtIndex:indexPath.row] forKey:@"customDeliveryDate"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"customDeliveryDate"];
    }
    if(![[customDeliveryTimeArr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
        [[NSUserDefaults standardUserDefaults]setObject:[customDeliveryTimeArr objectAtIndex:indexPath.row] forKey:@"customDeliveryTime"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"customDeliveryTime"];
    }
//    [[NSUserDefaults standardUserDefaults]setObject:[customDeliveryDateArr objectAtIndex:indexPath.row] forKey:@"customDeliveryDate"];
//    [[NSUserDefaults standardUserDefaults]setObject:[customDeliveryTimeArr objectAtIndex:indexPath.row] forKey:@"customDeliveryTime"];
    
    [self loadStoryBoardWithIdentifier:@"HistoryDetailsVC"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 126.0f;
}


#pragma mark - Button Clicked Methods

- (IBAction)backBtnClicked:(id)sender
{
    [self loadStoryBoardWithIdentifier:@"ViewController"];
}

#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           HUD.hidden=YES;
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               HUD.hidden=YES;
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
