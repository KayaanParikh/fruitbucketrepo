//
//  ItemsViewController2.m
//  TSMC
//
//  Created by user on 13/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "WishListViewController.h"
#import "OfflineDb.h"
#import "HelperClass.h"
#import "WebservicesClass.h"
#import "HMSegmentedControl.h"
#import "ItemDetailsController.h"
#import "AppConstant.h"
#import "CartController.h"
#import "SWRevealViewController.h"
#import "FilterController.h"
#import "SearchController.h"
#import "CartController.h"
#import "wishListHelper.h"
#import "UIBarButtonItem+Badge.h"

@interface WishListViewController ()
{
    
    UISwipeGestureRecognizer *gestureRight,*gestureLeft;
    wishListHelper *wishObj;
    
    
    NSMutableArray *nameArray,*idArray,*imagesArray,*categoryIdArr,*categoryNameArr,*imageMainArray,*imageThumbArray,*imageSmallArray,*weightArray,*brandIndexArr,*brandValueArr;
    int offset,limit;
    NSMutableArray *descriptionArray,*skuArray,*shortDescArray,*priceArray,*idContainInLocal,*localQtyArr,*foodTypeArr,*mirchImagesArr,*mirchIndexArr;
    NSMutableArray *localCutOpIdArray,*localCutOpValArray,*localCutOpPriceArray,*localWeightOpIdArray,*localWeightOpValArray,*localWeightOpPriceArray,*localTypeOpIdArray,*localTypeOpValArray,*localTypeOpPriceArray,*optionIdArray,*localTypeOpLabelArray,*localWeightOpLabelArray,*localCutOpLabelArray;
    int quntity;
    WishListViewController *items;
    NSString *yesrr;
    NSMutableArray *dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray,*spicelevelArray,*messagesArray;
    NSMutableArray *wishlistIdArray,*containInWishListArray;
    NSString *categoryId;
    int indexToShow;
    UILabel *lbl_line;
    OfflineDb *object;
    
    
    //Wight required array's
    NSMutableArray *weightlabelArray,*weightOptionIdArray;
    NSMutableArray *weightValueArray;
    NSMutableArray *weightpriceArray;
    NSMutableArray *pickerWeightLabelArr,*pickerWeightValueArr,*pickerWeightPriceArr;
    NSMutableArray *selectedWeightLabelIndex,*selectedWeightValueIndex,*selectedWeightPriceIndex;
    
    
    //Cut required array's
    NSMutableArray *cutlabelArray,*cutOptionIdArray;
    NSMutableArray *cutValueArray;
    NSMutableArray *cutpriceArray;
    NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
    NSMutableArray *selectedCutLabelIndex,*selectedCutValueIndex,*selectedCutPriceIndex;
    
    //Type required array's
    NSMutableArray *typelabelArray,*typeOptionIdArray;
    NSMutableArray *typeValueArray;
    NSMutableArray *typepriceArray;
    NSMutableArray *pickerTypeLabelArr,*pickerTypeValueArr,*pickerTypePriceArr;
    NSMutableArray *selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    NSString *selectedLabel,*selectedValue,*selectedPrice;
    
    
}
@property (nonatomic, strong) HMSegmentedControl *segmentedControl3;

@end

@implementation WishListViewController
@synthesize fetchedId,fetchedName,spicesPicker;

-(void)viewWillAppear:(BOOL)animated{
    
    [_pageTableView setHidden:YES];
    self.navigationController.navigationBarHidden = NO;
   // [_pageTableView setSeparatorColor:[UIColor clearColor]];
    _pageTableView.backgroundColor = [UIColor whiteColor];
    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    categoryIdArr=[[NSMutableArray alloc] init];
    categoryNameArr=[[NSMutableArray alloc] init];
    wishlistIdArray=[[NSMutableArray alloc] init];
    containInWishListArray=[[NSMutableArray alloc] init];
    //    weightlabelArray=[[NSMutableArray alloc] init];
    //    weightValueArray=[[NSMutableArray alloc] init];
    //    weightpriceArray=[[NSMutableArray alloc] init];
    
    
    
    NSArray *str=[object fetch_CartDetails];
    NSLog(@"%@",str);
    NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
    if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
        self.navigationItem.rightBarButtonItem.badge.hidden = true;
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
        
    }
    else{
        self.navigationItem.rightBarButtonItem.badge.hidden = false;
        [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
//        _lbl_badgeCount.text=cartCount;
        
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
            [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;

        
    }

    self.lbl_badgeCount.layer.masksToBounds=YES;
    self.lbl_badgeCount.layer.cornerRadius=self.lbl_badgeCount.frame.size.width/2;
    
    
    
    //Offline Data
    //    OfflineDb *object=[[OfflineDb alloc]init];
//    NSArray *dataArray=[object fetch_parentWiseCategory:fetchedId];
//    NSArray *nArr=[dataArray valueForKey:@"category_name"];
//    NSArray *iArr=[dataArray valueForKey:@"category_id"];
    
    
//    if (iArr.count == 0) {
//        
//        
//        [categoryIdArr addObject:fetchedId];
//        [categoryNameArr addObject:fetchedName];
//        
//    }else{
//        for (int i=0; i<[iArr count]; i++) {
//            [categoryNameArr addObject:[nArr objectAtIndex:i]];
//            [categoryIdArr addObject:[iArr objectAtIndex:i]];
//        }
//        
//    }
//    [_pageTableView reloadData];
//    [_pageTableView setNeedsLayout];
//    [_pageTableView setNeedsDisplay];

//    [self addHorzontalButtons:categoryIdArr withName:categoryNameArr];
    
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:nil animated:YES];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
//    _titleBack.backgroundColor=TopBarColor;
//    _lbl_titleText.font=TopBarFont;
//    _lbl_titleText.textColor=TopBarTextColor;
//    _lbl_badgeCount.textColor=cartIconTextColor;
//    _lbl_badgeCount.backgroundColor=cartIconBackColor;
    self.title = @"Wishlist".uppercaseString;
    
    quntity=0;
    indexToShow=0;
    limit=99;
    offset=1;
    object=[[OfflineDb alloc]init];
    
    
    wishObj=[wishListHelper sharedInstance];
    mirchImagesArr=[[NSMutableArray alloc] initWithObjects:@"1.png",@"2.png",@"3.png", nil];
    mirchIndexArr=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3", nil];
    
    
    
    //    if ([comingFrom isEqualToString:@"MENU"]) {
    
    
    //SwipeGesture
    //    UISwipeGestureRecognizer *gestureRight;
    //    UISwipeGestureRecognizer *gestureLeft;
    gestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self      action:@selector(swipeRight:)];
    gestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    [gestureLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    gestureRight.delegate=self;
    gestureLeft.delegate=self;
    
   // [[self tableBackView] addGestureRecognizer:gestureRight];
   // [[self tableBackView] addGestureRecognizer:gestureLeft];    //    self.imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexToShow]];
}

#pragma mark -Gesture Recognizer Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([recogniseGesture isEqualToString:@"SwipeGesture"])
    {
        [_pageTableView setUserInteractionEnabled:YES];
        SWRevealViewController *revealViewController = self.revealViewController;
        revealViewController.panGestureRecognizer.enabled=NO;
        //        [gestureRecognizer requireGestureRecognizerToFail:revealViewController.panGestureRecognizer];
        
    }
    else if ([recogniseGesture isEqualToString:@"PanGesture"])
    {
        SWRevealViewController *revealViewController = self.revealViewController;
        revealViewController.panGestureRecognizer.enabled=YES;
        //[gestureRecognizer requireGestureRecognizerToFail:gestureRight];
        //        [gestureRecognizer requireGestureRecognizerToFail:gestureLeft];
        
    }
    return YES;
    
   /* if (touch.view == self.pageTableView)
    {
        SWRevealViewController *revealViewController = self.revealViewController;
        revealViewController.panGestureRecognizer.enabled=NO;
//        [gestureRecognizer requireGestureRecognizerToFail:revealViewController.panGestureRecognizer];
        
    }
    else
    {
        SWRevealViewController *revealViewController = self.revealViewController;
        revealViewController.panGestureRecognizer.enabled=YES;
        //[gestureRecognizer requireGestureRecognizerToFail:gestureRight];
//        [gestureRecognizer requireGestureRecognizerToFail:gestureLeft];
        
    }
    return YES;
    */
}
- (void)swipeRight:(UISwipeGestureRecognizer *)gesture
{
    //    indexToShow--;
   SWRevealViewController *revealViewController = self.revealViewController;
    [gesture requireGestureRecognizerToFail:revealViewController.panGestureRecognizer];
//
    if (indexToShow > 0)
    {
        indexToShow--;
        [self.segmentedControl3 setSelectedSegmentIndex:indexToShow animated:YES];
        
        //        self.imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexToShow]];
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
        
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
        //        } completion:nil];
        
    }
    
    
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)gesture
{
    SWRevealViewController *revealViewController = self.revealViewController;
   [gesture requireGestureRecognizerToFail:revealViewController.panGestureRecognizer];
    
    if (indexToShow < categoryIdArr.count - 1)
    {
        indexToShow++;
        [self.segmentedControl3 setSelectedSegmentIndex:indexToShow animated:YES];
        
        //        self.imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexToShow]];
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
        
        categoryId=[categoryIdArr objectAtIndex:indexToShow];
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
        
        
        //        } completion:nil];
    }
    
}

#pragma mark -Segment Control Method
-(void)addHorzontalButtons:(NSMutableArray *)IdArray withName:(NSMutableArray *)NameArray
{
    
    _segmentedControl3 = [[HMSegmentedControl alloc] initWithSectionTitles:NameArray];
    [_segmentedControl3 setFrame:CGRectMake(0, 64,self.view.frame.size.width, 40)];
    [_segmentedControl3 setIndexChangeBlock:^(NSInteger index) {
        NSLog(@"Selected index %ld (via block)", (long)index);
    }];
    _segmentedControl3.selectionIndicatorHeight = 2.0f;
    //_segmentedControl3.backgroundColor = TopBarColor;
    _segmentedControl3.backgroundColor = [UIColor whiteColor];
    _segmentedControl3.titleTextAttributes = @{NSForegroundColorAttributeName : TopBarColor};
    _segmentedControl3.selectionIndicatorColor = TopBarColor;
    _segmentedControl3.selectionIndicatorBoxColor = [UIColor whiteColor];
    //_segmentedControl3.selectionIndicatorBoxColor = YellowColor;
    _segmentedControl3.selectionIndicatorBoxOpacity = 1.0;
    _segmentedControl3.selectionStyle = HMSegmentedControlSelectionStyleBox;
    _segmentedControl3.selectedSegmentIndex = indexToShow;
    _segmentedControl3.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _segmentedControl3.shouldAnimateUserSelection = NO;
    _segmentedControl3.tag = 2;
    [_segmentedControl3 addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:_segmentedControl3];
    
    categoryId=[categoryIdArr objectAtIndex:indexToShow];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
    
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    
    
    long row = segmentedControl.selectedSegmentIndex;
    NSLog(@"%ld",row);
    
    if (indexToShow<(int)row) {
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    else if (indexToShow>(int)row){
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    indexToShow=(int)row;
    
    categoryId=[categoryIdArr objectAtIndex:indexToShow];
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
    
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld", (long)segmentedControl.selectedSegmentIndex);
}



- (IBAction)horizontalMenuButtonMethod:(id)sender
{
    long row = [sender tag];
    NSLog(@"%ld",row);
    
    if (indexToShow<(int)row) {
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    else if (indexToShow>(int)row){
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_tableBackView.layer addAnimation:transition forKey:@"transition" ];
    }
    indexToShow=(int)row;
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:[categoryIdArr objectAtIndex:indexToShow] animated:YES];
    
    //    [self GetTopviewProduct:[categoryIdArr objectAtIndex:indexToShow]];
    
    
}


- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
#pragma  mark -UITableView Datasource Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableVw
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return idArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *simpleTableIdentifier1 = @"CustomTableViewCell";
//    
//    CustomTableViewCell *cell1 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier1];
    
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell2 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    recogniseGesture=@"SwipeGesture";
    if (cell2 == nil)
    {
        cell2 = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell2.qtyBackView.layer setCornerRadius:15.0f];
    cell2.qtyBackView.layer.borderColor = [UIColor colorWithRed:189.0/255.0f green:189.0/255.0f blue:189.0/255.0f alpha:1.0].CGColor;
    cell2.qtyBackView.layer.borderWidth = 1.0f;
    [cell2.qtyBackView.layer setMasksToBounds:YES];
    
    
    
    //[_pageTableView setSeparatorColor:[UIColor grayColor]];
    //cell.img_product.backgroundColor=[UIColor greenColor];
    //cell.img_product.image=[UIImage imageNamed:[_detailArr objectAtIndex:indexPath.row]];
    @try
    {
        
        cell2.lbl_name.text=[nameArray objectAtIndex:indexPath.row];
//        [cell2.lbl_name sizeToFit];

        cell2.lbl_shortDesc.text=[shortDescArray objectAtIndex:indexPath.row];
        NSString *image=[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]];
        [cell2.img_product sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            [HUD hide:YES];
            
            
        }];

       // NSLog(@"image=%@",image);
//        [cell2.lbl_shortDesc sizeToFit];
        
        CGSize constraint = CGSizeMake(cell2.lbl_shortDesc.frame.size.width, CGFLOAT_MAX);
        CGSize size;
        
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [cell2.lbl_shortDesc.text boundingRectWithSize:constraint
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{NSFontAttributeName:cell2.lbl_shortDesc.font}
                                                      context:context].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        
        cell2.shortDescHeight.constant=size.height;
        
        NSLog(@"shortDescHeight=%ld\n size.height=%ld",(long)cell2.shortDescHeight.constant,(long)size.height);
        cell2.lbl_qty.text=@"1";

        if([[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"0"] || [[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"(null)"] || [brandValueArr objectAtIndex:indexPath.row] == nil)
        {
            cell2.lbl_brandName.text=@"";
            cell2.brandHeight.constant=0;
            cell2.lbl_brandName.hidden=YES;
        }
        else
        {
            cell2.lbl_brandName.hidden=NO;
            cell2.brandHeight.constant=23;
            cell2.lbl_brandName.text=[brandValueArr objectAtIndex:indexPath.row];
        }
        cell2.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[[priceArray objectAtIndex:indexPath.row] floatValue]];
        
        
        NSString *weiarrr=[weightArray objectAtIndex:indexPath.row];
        if ((NSString *)[NSNull null] == weiarrr)
        {
            [weightArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        

        cell2.lbl_noAttrWeight.text=[NSString stringWithFormat:@"Weight: %.f gm",[[weightArray objectAtIndex:indexPath.row] floatValue]];

        if ([[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0.0000"] || [[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
        {
            cell2.weightLblHeight.constant=0;
            cell2.lbl_noAttrWeightHeight.constant=0;
        }
        else
        {
            cell2.weightLblHeight.constant=15;
            cell2.lbl_noAttrWeightHeight.constant=15;
            
        }
        
        if ([[containInWishListArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            [cell2.btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
        }
        else{
        
            [cell2.btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
        }
        
        cell2.btn_add.tag=indexPath.row;
        cell2.btn_sub.tag=indexPath.row;
        cell2.btn_addCart.tag=indexPath.row;
        cell2.btn_weight.tag=indexPath.row;
        cell2.btn_cut.tag=indexPath.row;
        cell2.btn_type.tag=indexPath.row;
        cell2.btn_wish.tag=indexPath.row;

        cell2.btn_weightHeight.constant=0;
        cell2.lbl_weightHeight.constant=0;
        cell2.btn_cutHeight.constant=0;
        cell2.lbl_cutHeight.constant=0;
        cell2.btn_typeHeight.constant=0;
        cell2.lbl_typeHeight.constant=0;
        cell2.btn_weight.hidden=YES;
        cell2.lbl_weight.hidden=YES;
        cell2.btn_cut.hidden=YES;
        cell2.lbl_cut.hidden=YES;
        cell2.btn_type.hidden=YES;
        cell2.lbl_type.hidden=YES;
        
        [cell2.contentBackView.layer setShadowOpacity:0.0];
        [cell2.contentBackView.layer setShadowRadius:1.0];
        [cell2.contentBackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        
        if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
        {
            
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            
            
            
            int a=40;
            
            if ([[dropLabelArr objectAtIndex:indexPath.row] count]>0)
            {
               // cell2.qtyBackViewConstraint.constant=30;
                
                for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                {
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                    {
                        a=a+cell2.btn_cutHeight.constant;
                        
                        cell2.btn_cutHeight.constant=21;
                        cell2.lbl_cutHeight.constant=21;
                        cell2.btn_cut.hidden=NO;
                        cell2.lbl_cut.hidden=NO;
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                        {
                            cell2.lbl_cut.text=@"Pieces:";
                        }
                        
                        if([[cutlabelArray objectAtIndex:indexPath.row] count]>0){
                     // =[[[cutpriceArray objectAtIndex:i] objectAtIndex:0] intValue];
                        [cell2.btn_cut setTitle:[[cutlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                        }
                        
                        
                    }
                    else
                    {
                    }
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
                    {
                        a=a+cell2.btn_weightHeight.constant;
                        
                        cell2.btn_weightHeight.constant=21;
                        cell2.lbl_weightHeight.constant=21;
                        cell2.btn_weight.hidden=NO;
                        cell2.lbl_weight.hidden=NO;
                        if([[weightlabelArray objectAtIndex:indexPath.row] count]>0){
                            
                        [cell2.btn_weight setTitle:[[weightlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                        }
                        
                    }
                    else
                    {
                    }
                    
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
                    {
                        a=a+cell2.btn_typeHeight.constant;
                        
                        cell2.btn_typeHeight.constant=21;
                        cell2.lbl_typeHeight.constant=21;
                        cell2.btn_type.hidden=NO;
                        cell2.lbl_type.hidden=NO;
                        if([[typelabelArray objectAtIndex:indexPath.row] count]>0){
                        [cell2.btn_type setTitle:[[typelabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                        }
                        
                        
                    }
                    else
                    {
                    }
                    
                }
            }
            else
            {
                
                cell2.btn_weightHeight.constant=0;
                cell2.lbl_weightHeight.constant=0;
                cell2.btn_cutHeight.constant=0;
                cell2.lbl_cutHeight.constant=0;
                cell2.btn_typeHeight.constant=0;
                cell2.lbl_typeHeight.constant=0;
                cell2.btn_weight.hidden=YES;
                cell2.lbl_weight.hidden=YES;
                cell2.btn_cut.hidden=YES;
                cell2.lbl_cut.hidden=YES;
                cell2.btn_type.hidden=YES;
                cell2.lbl_type.hidden=YES;
                
            }

            cell2.backViewConstraint.constant=cell2.nameHeight.constant+cell2.shortDescHeight.constant+cell2.brandHeight.constant+a+30;
            
            
        }
        
    }
    @catch (NSException *exception)
    {
        [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
    }
    
    
    return cell2;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell2 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell2.lbl_shortDesc.text=[shortDescArray objectAtIndex:indexPath.row];
    
    CGSize constraint = CGSizeMake(cell2.lbl_shortDesc.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [cell2.lbl_shortDesc.text boundingRectWithSize:constraint
                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{NSFontAttributeName:cell2.lbl_shortDesc.font}
                                                                context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    cell2.shortDescHeight.constant=size.height;
    
    if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
    {
        
        long a=cell2.nameHeight.constant+cell2.brandHeight.constant+cell2.cartWeightHeight.constant+cell2.shortDescHeight.constant+70;
        
        //cell2.qtyBackViewConstraint.constant;
        
        for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
        {
            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
            {
                a=a+cell2.btn_cutHeight.constant;
                
            }
            
            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
            {
                a=a+cell2.btn_weightHeight.constant;
                
                
            }
            
            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
            {
                a=a+cell2.btn_typeHeight.constant;
                
            }
            
        }

        NSLog(@"%ld",a);
        return a+40;
    }
    else
    {
        long a=cell2.nameHeight.constant+cell2.brandHeight.constant+cell2.cartWeightHeight.constant+cell2.shortDescHeight.constant+70;
        return a+40;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell2 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    //fetchedItemId,*fetchedItemName,*fetchedItemImage,*fetchedFoodType,*fetchedItemDesc,*fetchedSelectedStatus,*fetchedItemPrice,*fetchedCategoryId,*fetchedMsgId,*fetchedSpiceId,*fetchedLevelIds
    
    //    NSString *levelIds;
    //    NSString *selectecdLevelId;
    //
    //    NSMutableArray *getSpiceVals=[[NSMutableArray alloc] init];
    //    [getSpiceVals addObject:[dropValueArr objectAtIndex:indexPath.row]];
    //    if ([[getSpiceVals objectAtIndex:0] count]<=0) {
    //        levelIds=@"0";
    ////        selectecdLevelId=@"0";
    //
    //    }
    //    else{
    //            levelIds=[[getSpiceVals objectAtIndex:0] componentsJoinedByString:@","];
    ////            selectecdLevelId=[spicelevelArray objectAtIndex:indexPath.row];
    //
    //
    //    }
    
    NSString *allcutLabels=[[cutlabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allcutValues=[[cutValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allcutPrices=[[cutpriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    NSString *allweightLabels=[[weightlabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allweightValues=[[weightValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allweightPrices=[[weightpriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    NSString *alltypeLabels=[[typelabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *alltypeValues=[[typeValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *alltypePrices=[[typepriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    ItemDetailsController *viewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"ItemDetailsController"];
    viewController.fetchedItemId=[idArray objectAtIndex:indexPath.row];
    viewController.fetchedItemName=[nameArray objectAtIndex:indexPath.row];
    viewController.fetchedItemDesc=[descriptionArray objectAtIndex:indexPath.row];
    viewController.fetchedItemImage=[imagesArray objectAtIndex:indexPath.row];
    viewController.fetchedFoodType=[foodTypeArr objectAtIndex:indexPath.row];
    viewController.fetchedWeight=[weightArray objectAtIndex:indexPath.row];
    viewController.fetchedName = self.title;
    //        viewController.fetchedSelectedStatus=[idContainInLocal objectAtIndex:indexPath.row];
    viewController.fetchedItemPrice=[priceArray objectAtIndex:indexPath.row];
    viewController.fetchedCategoryId=categoryId;
    //        viewController.fetchedMsgId=[fieldIdArray objectAtIndex:indexPath.row];
    //        viewController.fetchedSpiceId=[dropDownIdArray objectAtIndex:indexPath.row];
    //        viewController.fetchedLevelIds=levelIds;
    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
    {
        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
        {
            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
            {
                 viewController.fetchDropLabelName=@"Pieces:";
            }
            else
            {
                viewController.fetchDropLabelName=@"Cut:";
            }
            
        }
        else
        {
        }
    }
    
   
    viewController.fetchedBrandVal=[brandValueArr objectAtIndex:indexPath.row];
    viewController.fetchedBrandLabel=[brandIndexArr objectAtIndex:indexPath.row];
    viewController.allCutLabel=allcutLabels;
    viewController.allCutPrice=allcutPrices;
    viewController.allCutValues=allcutValues;
    viewController.allWeightLabel=allweightLabels;
    viewController.allWeightValues=allweightValues;
    viewController.allWeightPrice=allweightPrices;
    viewController.allTypeLabel=alltypeLabels;
    viewController.allTypeValues=alltypeValues;
    viewController.allTypePrice=alltypePrices;
    
    viewController.fetchedShortDesc=[shortDescArray objectAtIndex:indexPath.row];
    viewController.fetchedSmallImg=[imageSmallArray objectAtIndex:indexPath.row];
    viewController.fetchedMainImg=[imageMainArray objectAtIndex:indexPath.row];
    viewController.fetchedThumbImg=[imageThumbArray objectAtIndex:indexPath.row];
    viewController.fetchedWeight=[weightArray objectAtIndex:indexPath.row];
    viewController.fetchedCutOptionId=[cutOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedWeightOptionId=[weightOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedTypeOptionId=[typeOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedWishStat=[containInWishListArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:viewController animated:YES];
    
    
    
    
    
}


#pragma mark UIPickerDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// Total rows in our component.

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger i;
    if (pickerView.tag==1) {
        i=[pickerCutLabelArr count];
        
    }
    else if (pickerView.tag==2){
        i=[pickerWeightLabelArr count];
    }
    else if (pickerView.tag==3){
        i=[pickerTypeLabelArr count];
        
    }
    
    //    i=[mirchImagesArr count];
    return i;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 50;
}
// Display each row's data.

//-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    id i;
//    i=[mirchImagesArr objectAtIndex: row];
//    return i;
//}

// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (pickerView.tag==1) {
        
        selectedLabel=[pickerCutLabelArr objectAtIndex:row];
        selectedValue=[pickerCutValueArr objectAtIndex:row];
        selectedPrice=[pickerCutPriceArr objectAtIndex:row];
        
    }
    else if (pickerView.tag==2){
        selectedLabel=[pickerWeightLabelArr objectAtIndex:row];
        selectedValue=[pickerWeightValueArr objectAtIndex:row];
        selectedPrice=[pickerWeightPriceArr objectAtIndex:row];
        
    }
    else if (pickerView.tag==3){
        selectedLabel=[pickerTypeLabelArr objectAtIndex:row];
        selectedValue=[pickerTypeValueArr objectAtIndex:row];
        selectedPrice=[pickerTypePriceArr objectAtIndex:row];
        
    }
    
    //    selectedCustId=[custIdArray objectAtIndex:row];
    
    
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    // Get the text of the row.
    
    UILabel *lblRow = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,spicesPicker.bounds.size.width, 44.0f)];
    
    
    if (pickerView.tag==1)
    {
        NSString *rowItem = [pickerCutLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==2)
    {
        NSString *rowItem = [pickerWeightLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==3)
    {
        NSString *rowItem = [pickerTypeLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
    }
    
    
    
    
    
    return lblRow;
    
    
}
#pragma mark UITextFieldDelegates
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//
//    globalvar=textField;
//    return YES;
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)textField{
//
//
//    long row = [globalvar tag];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
//    CustomTableViewCell2 *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
//
//    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
//    [idContainInLocal replaceObjectAtIndex:[globalvar tag] withObject:@"0"];
//    [localQtyArr replaceObjectAtIndex:[globalvar tag] withObject:cell.lbl_qty.text];
//
//}
//-(BOOL)textFieldShouldReturn:(UITextField *)textField{
//    [globalvar resignFirstResponder];
//    return [textField resignFirstResponder];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backMethod:(id)sender
{
    recogniseGesture=@"PanGesture";
    
    [self.revealViewController revealToggle:nil];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
   // self.revealViewController.panGestureRecognizer.delegate=self;
}

/*
 - (IBAction)spiceMethod:(id)sender {
 
 
 long row = [sender tag];
 NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
 CustomTableViewCell2 *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
 
 [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
 [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
 [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
 
 
 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
 
 
 
 
 alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
 spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
 
 spicesPicker.backgroundColor=[UIColor whiteColor];
 [spicesPicker setDataSource: self];
 [spicesPicker setDelegate: self];
 NSUInteger i=1;
 spicesPicker.tag=i;
 spicesPicker.showsSelectionIndicator = YES;
 [alertController.view addSubview:spicesPicker];
 
 //    [alertController.view addSubview:DateTimePicker];
 [alertController addAction:({
 UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
 {
 
 @try {
 NSLog(@"%@",selectedSpiceIndex);
 
 if (selectedSpiceIndex.length>0 || !([selectedSpiceIndex isEqualToString:@"0"])) {
 
 if ([selectedSpiceIndex isEqualToString:@"1"]) {
 cell.img_spices.image=[UIImage imageNamed:@"1.png"];
 
 }
 else if ([selectedSpiceIndex isEqualToString:@"2"]){
 cell.img_spices.image=[UIImage imageNamed:@"2.png"];
 
 }
 else if ([selectedSpiceIndex isEqualToString:@"3"]){
 cell.img_spices.image=[UIImage imageNamed:@"3.png"];
 
 }
 [spicelevelArray replaceObjectAtIndex:[sender tag] withObject:selectedSpiceIndex];
 
 //removing object
 selectedSpiceIndex=@"0";
 
 }
 else{
 cell.img_spices.image=[UIImage imageNamed:@"1.png"];
 
 }
 } @catch (NSException *exception) {
 [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
 
 }
 
 
 
 
 }];
 action;
 })];
 
 
 
 UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
 popoverController.sourceView = sender;
 popoverController.sourceRect = [sender bounds];
 [self presentViewController:alertController  animated:YES completion:nil];
 
 
 }
 */
- (IBAction)addCartMethod:(id)sender {
    
    NSLog(@"%ld",[sender tag]);
    
    //  if ([self validations:[sender tag]]) {
    NSString *name=[nameArray objectAtIndex:[sender tag]];
    //    NSString *sk=[skuArray objectAtIndex:[sender tag]];
    NSString *idStr=[idArray objectAtIndex:[sender tag]];
    NSString *catId=categoryId;
    NSString *img=[imagesArray objectAtIndex:[sender tag]];
    NSString *imgSmall=[imageSmallArray objectAtIndex:[sender tag]];
    NSString *imgMain=[imageMainArray objectAtIndex:[sender tag]];
    NSString *imgThumb=[imageThumbArray objectAtIndex:[sender tag]];
    NSString *desc=[descriptionArray objectAtIndex:[sender tag]];
    NSString *shortDesc=[shortDescArray objectAtIndex:[sender tag]];
    NSString *weight=[weightArray objectAtIndex:[sender tag]];
    NSString *type=[foodTypeArr objectAtIndex:[sender tag]];
    
    NSString *price=[priceArray objectAtIndex:[sender tag]];
    NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    NSString *currIndQty;
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    currIndQty = cell.lbl_qty.text;

//    if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
//    {
//        CustomTableViewCell2 *cell2 = [self.pageTableView cellForRowAtIndexPath:indexPath];
//        currIndQty=cell2.label_qty.text;
//    }
//    else
//    {
//        CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
//        currIndQty=cell.lbl_qty.text;
//    }
    

    //    NSString *msg=cell.txt_msg.text;
    //        NSString *msgId=[fieldIdArray objectAtIndex:[sender tag]];
    //                        [cutOptionIdArray addObject:@"0"];
    // [typeOptionIdArray addObject:@"0"];
    //[weightOptionIdArray addObject:@"0"];
    
    NSString *opid=[optionIdArray objectAtIndex:[sender tag]];
    NSString *brand=[brandValueArr objectAtIndex:[sender tag]];
    NSString *cutopid=[cutOptionIdArray objectAtIndex:[sender tag]];
    NSString *cutopval=[selectedCutValueIndex objectAtIndex:row];
    NSString *cutoppri=[selectedCutPriceIndex objectAtIndex:row];
    NSString *weiopid=[weightOptionIdArray objectAtIndex:[sender tag]];
    NSString *weiopval=[selectedWeightValueIndex objectAtIndex:row];
    NSString *weioppri=[selectedWeightPriceIndex objectAtIndex:row];
    NSString *typopid=[typeOptionIdArray objectAtIndex:[sender tag]];
    NSString *typopval=[selectedTypeValueIndex objectAtIndex:row];
    NSString *typoppri=[selectedTypePriceIndex objectAtIndex:row];
    NSString *cutoplabel=[selectedCutLabelIndex objectAtIndex:row];
    NSString *weioplabel=[selectedWeightLabelIndex objectAtIndex:row];
    NSString *typoplabel=[selectedTypeLabelIndex objectAtIndex:row];
    
    //For storing All data in database
    NSString *allcutLabels=[[cutlabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allcutValues=[[cutValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allcutPrices=[[cutpriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    NSString *allweightLabels=[[weightlabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allweightValues=[[weightValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allweightPrices=[[weightpriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    NSString *alltypeLabels=[[typelabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *alltypeValues=[[typeValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *alltypePrices=[[typepriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    
    //
    //        NSString *spiceLevel,*spiceId,*levelIds;
    //        NSString *selectecdLevelId;
    //        NSMutableArray *getSpiceVals=[[NSMutableArray alloc] init];
    //        int level=[[spicelevelArray objectAtIndex:[sender tag]] intValue];
    //        [getSpiceVals addObject:[dropValueArr objectAtIndex:[sender tag]]];
    //        if ([[getSpiceVals objectAtIndex:0] count]<=0) {
    //            spiceLevel=@"0";
    //            levelIds=@"0";
    //            selectecdLevelId=@"0";
    //
    //        }
    //        else{
    //
    //            if (level==0) {
    //                spiceLevel=[[getSpiceVals objectAtIndex:0] objectAtIndex:0];
    //                levelIds=[[getSpiceVals objectAtIndex:0] objectAtIndex:0];
    //                selectecdLevelId=@"1";
    //
    //
    //            }
    //            else{
    //                spiceLevel=[[getSpiceVals objectAtIndex:0] objectAtIndex:level-1];
    //                levelIds=[[getSpiceVals objectAtIndex:0] componentsJoinedByString:@","];
    //                selectecdLevelId=[spicelevelArray objectAtIndex:[sender tag]];
    //
    //            }
    //        }
    //        spiceId=[dropDownIdArray objectAtIndex:[sender tag]];
    NSString *qty=currIndQty;
    
    if ([qty isEqualToString:@"0"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:@"Please select Quantity."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        
        float total=[qty floatValue]*[price floatValue];
        NSString *tot=[NSString stringWithFormat:@"%.2f",total];
        
        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
        
        ///Add in cart
        
        NSMutableArray *cartDbArray=[[NSMutableArray alloc]init];
        testDB_Obj.cart_id=idStr;
        testDB_Obj.cart_category_id=catId;
        testDB_Obj.cart_name=name;
        testDB_Obj.cart_image_url=img;
        testDB_Obj.cart_image_thumb=imgThumb;
        testDB_Obj.cart_image_small=imgSmall;
        testDB_Obj.cart_image_main=imgMain;
        testDB_Obj.cart_price=price;
        testDB_Obj.cart_qty=qty;
        testDB_Obj.cart_description=desc;
        testDB_Obj.cart_short_desc=shortDesc;
        testDB_Obj.cart_food_type=type;
        testDB_Obj.cart_total=tot;
        testDB_Obj.cart_origin=@"";
        
        testDB_Obj.cart_brand=brand;
        testDB_Obj.cart_weight=weight;
        testDB_Obj.cart_option_id=@"";
        testDB_Obj.cart_cut_option_id=cutopid;
        testDB_Obj.cart_cut_option_value=cutopval;
        testDB_Obj.cart_cut_option_price=cutoppri;
        testDB_Obj.cart_weight_option_id=weiopid;
        testDB_Obj.cart_weight_option_value=weiopval;
        testDB_Obj.cart_weight_option_price=weioppri;
        testDB_Obj.cart_type_option_id=typopid;
        testDB_Obj.cart_type_option_value=typopval;
        testDB_Obj.cart_type_option_price=typoppri;
        testDB_Obj.cart_cut_option_label=cutoplabel;
        testDB_Obj.cart_weight_option_label=weioplabel;
        testDB_Obj.cart_type_option_label=typoplabel;
        
        //For cart master
        testDB_Obj.cm_cart_id=idStr;
        testDB_Obj.cm_category_id=catId;
        testDB_Obj.cm_all_cut_values=allcutValues;
        testDB_Obj.cm_all_cut_labels=allcutLabels;
        testDB_Obj.cm_all_cut_prices=allcutPrices;
        testDB_Obj.cm_all_weight_values=allweightValues;
        testDB_Obj.cm_all_weight_labels=allweightLabels;
        testDB_Obj.cm_all_weight_prices=allweightPrices;
        testDB_Obj.cm_all_type_values=alltypeValues;
        testDB_Obj.cm_all_type_labels=alltypeLabels;
        testDB_Obj.cm_all_type_prices=alltypePrices;

        
        [cartDbArray addObject:testDB_Obj];
        [testDB_Obj checkCartEntry:cartDbArray];
        
        
        
        //                    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"1"];
        //                    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
        //                    [localCutOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_cut.titleLabel.text];
        //                    [localWeightOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_weight.titleLabel.text];
        //                    [localTypeOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_type.titleLabel.text];
        
        //                [messagesArray replaceObjectAtIndex:[sender tag] withObject:cell.txt_msg.text];
        
        //                    NSLog(@"%@",idContainInLocal);
        //                    NSLog(@"%@",localQtyArr);
        
        
        NSArray *str=[testDB_Obj fetch_CartDetails];
        NSLog(@"%@",str);
        NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
        if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
            self.navigationItem.rightBarButtonItem.badge.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
            
        }
        else{
            self.navigationItem.rightBarButtonItem.badge.hidden = NO;
            [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
            [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
        
//        self.lbl_badgeCount.text=cartCount;
        
        [ProgressHUD showSuccess:@"Item successfully added to cart" Interaction:NO];
        
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        
        [HUD hide:YES afterDelay:5.0];
        
//        NSIndexPath *inde=[NSIndexPath indexPathForRow:[sender tag] inSection:0];
//        [self.pageTableView beginUpdates];
//        [self.pageTableView reloadRowsAtIndexPaths:@[inde] withRowAnimation:UITableViewRowAnimationNone];
//        [self.pageTableView endUpdates];
            _pageTableView.delegate = self;
            _pageTableView.dataSource = self;

            [self.pageTableView reloadData];
        });
    }
    //    }
}

- (IBAction)addQtyMethod:(id)sender
{
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    CustomTableViewCell *cell =(CustomTableViewCell *)[self.pageTableView cellForRowAtIndexPath:indexPath];
    NSString *currIndQty=cell.lbl_qty.text;
    quntity=[currIndQty intValue];
    if (quntity < 99)
    {
        quntity++;
        NSString *str=[NSString stringWithFormat:@"%d",quntity];
        cell.lbl_qty.text=str;
    }
//    if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
//    {
//        CustomTableViewCell2 *cell2 = [self.pageTableView cellForRowAtIndexPath:indexPath];
//        NSString *currIndQty=cell2.label_qty.text;
//        quntity=[currIndQty intValue];
//        if (quntity < 99)
//        {
//            quntity++;
//            NSString *str=[NSString stringWithFormat:@"%d",quntity];
//            cell2.label_qty.text=str;
//        }
//    }
//    else
//    {
//        CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
//        NSString *currIndQty=cell.lbl_qty.text;
//        quntity=[currIndQty intValue];
//        if (quntity < 99)
//        {
//            quntity++;
//            NSString *str=[NSString stringWithFormat:@"%d",quntity];
//            cell.lbl_qty.text=str;
//        }
//    }
    
    
}

- (IBAction)subQtyMethod:(id)sender {
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    NSString *currIndQty=cell.lbl_qty.text;
    quntity=[currIndQty intValue];
    if (quntity > 0)
    {
        quntity--;
        cell.lbl_qty.text=[NSString stringWithFormat:@"%d",quntity];
    }

    
//    if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
//    {
//        CustomTableViewCell2 *cell2 = [self.pageTableView cellForRowAtIndexPath:indexPath];
//        NSString *currIndQty=cell2.label_qty.text;
//        quntity=[currIndQty intValue];
//        if (quntity > 0)
//        {
//            quntity--;
//            cell2.label_qty.text=[NSString stringWithFormat:@"%d",quntity];
//        }
//    }
//    else
//    {
//        CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
//        NSString *currIndQty=cell.lbl_qty.text;
//        quntity=[currIndQty intValue];
//        if (quntity > 0)
//        {
//            quntity--;
//            cell.lbl_qty.text=[NSString stringWithFormat:@"%d",quntity];
//        }
//    }
    
}
/*
 -(void)removeItem:(long) sender{
 
 long row =sender;
 NSString *idStr=[idArray objectAtIndex:sender];
 NSString *price=[priceArray objectAtIndex:sender];
 NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
 NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
 CustomTableViewCell2 *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
 NSString *currIndQty=cell.lbl_qty.text;
 NSString *qty=currIndQty;
 //    [idContainInLocal replaceObjectAtIndex:sender withObject:@"0"];
 //    [localQtyArr replaceObjectAtIndex:sender withObject:cell.lbl_qty.text];
 
 float total=[qty floatValue]*[withotDollarPrice floatValue];
 NSString *tot=[NSString stringWithFormat:@"%f",total];
 
 OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
 
 //    if ([[idContainInLocal objectAtIndex:[sender tag]] isEqualToString:@"1"]) {
 
 UIAlertController * alert=   [UIAlertController
 alertControllerWithTitle:@""
 message:@"Are you sure you want to remove?"
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action) {
 //Do Some action here
 [alert dismissViewControllerAnimated:YES completion:nil];
 
 if ([testDB_Obj Delete_Cart_Data:idStr])
 {
 [idContainInLocal replaceObjectAtIndex:sender withObject:@"0"];
 
 [localQtyArr replaceObjectAtIndex:sender withObject:@"0"];
 
 NSLog(@"%@",idContainInLocal);
 
 NSArray *str=[testDB_Obj fetch_Quantity_Count];
 NSLog(@"%@",str);
 NSArray *cartCountArr=[str valueForKey:@"qnty_cnt"];
 NSString *cartCount=[cartCountArr objectAtIndex:0];
 
 
 
 //                                                               int cc=[[[NSUserDefaults standardUserDefaults] valueForKey:@"BADGECOUNT"] intValue];
 //
 //                                                               cc=cc-1;
 //                                                               NSString *cartCount=[NSString stringWithFormat:@"%d",cc];                               [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
 //
 if ([cartCount isEqualToString:@"0"] || [cartCount isEqualToString:@"(null)"]) {
 _lbl_badgeCount.hidden=YES;
 [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
 
 }
 else{
 _lbl_badgeCount.hidden=NO;
 [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
 
 
 }
 
 
 //                                                               cell.btn_add.userInteractionEnabled=YES;
 //                                                               cell.btn_sub.userInteractionEnabled=YES;
 
 ///cartBtn.badgeValue=cartCount;
 //[delegate updateLabelWithString:cartCount];
 self.lbl_badgeCount.text=cartCount;
 //                                                           [self.pageTableView reloadData];
 NSIndexPath *inde=[NSIndexPath indexPathForRow:sender inSection:0];
 [self.pageTableView beginUpdates];
 [self.pageTableView reloadRowsAtIndexPaths:@[inde] withRowAnimation:UITableViewRowAnimationNone];
 [self.pageTableView endUpdates];
 
 
 
 UIAlertController * alert=   [UIAlertController
 alertControllerWithTitle:@"Success"
 message:@"Item Successfully Removed."
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action) {
 //Do Some action here
 
 
 
 [alert dismissViewControllerAnimated:YES completion:nil];
 
 }];
 [alert addAction:ok];
 
 [self presentViewController:alert animated:YES completion:nil];
 
 }
 
 
 
 
 
 }];
 UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action) {
 //Do Some action here
 quntity=1;
 cell.lbl_qty.text=@"1";
 NSIndexPath *inde=[NSIndexPath indexPathForRow:sender inSection:0];
 //                                                           [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
 
 [self.pageTableView beginUpdates];
 [self.pageTableView reloadRowsAtIndexPaths:@[inde] withRowAnimation:UITableViewRowAnimationNone];
 [self.pageTableView endUpdates];
 
 [alert dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alert addAction:ok];
 [alert addAction:cancel];
 
 [self presentViewController:alert animated:YES completion:nil];
 }
 */



-(void)GetTopviewProduct:(NSString *)catId
{
    
    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    
    
    weightlabelArray=[[NSMutableArray alloc] init];
    weightValueArray=[[NSMutableArray alloc] init];
    weightpriceArray=[[NSMutableArray alloc] init];
    
    cutlabelArray=[[NSMutableArray alloc] init];
    cutValueArray=[[NSMutableArray alloc] init];
    cutpriceArray=[[NSMutableArray alloc] init];
    
    typelabelArray=[[NSMutableArray alloc] init];
    typeValueArray=[[NSMutableArray alloc] init];
    typepriceArray=[[NSMutableArray alloc] init];
    
    localCutOpIdArray=[[NSMutableArray alloc] init];
    localCutOpValArray=[[NSMutableArray alloc] init];
    localCutOpPriceArray=[[NSMutableArray alloc] init];
    
    localWeightOpIdArray=[[NSMutableArray alloc] init];
    localWeightOpValArray=[[NSMutableArray alloc] init];
    localWeightOpPriceArray=[[NSMutableArray alloc] init];
    
    localTypeOpIdArray=[[NSMutableArray alloc] init];
    localTypeOpValArray=[[NSMutableArray alloc] init];
    localTypeOpPriceArray=[[NSMutableArray alloc] init];
    brandIndexArr=[[NSMutableArray alloc] init];
    brandValueArr=[[NSMutableArray alloc] init];
    weightArray=[[NSMutableArray alloc] init];
    optionIdArray=[[NSMutableArray alloc] init];
    imageMainArray=[[NSMutableArray alloc] init];
    imageThumbArray=[[NSMutableArray alloc] init];
    imageSmallArray=[[NSMutableArray alloc] init];
    
    typeOptionIdArray=[[NSMutableArray alloc] init];
    weightOptionIdArray=[[NSMutableArray alloc] init];
    cutOptionIdArray=[[NSMutableArray alloc] init];
    localCutOpLabelArray=[[NSMutableArray alloc] init];
    localWeightOpLabelArray=[[NSMutableArray alloc] init];
    localTypeOpLabelArray=[[NSMutableArray alloc] init];
    selectedWeightLabelIndex=[[NSMutableArray alloc] init];
    selectedWeightValueIndex=[[NSMutableArray alloc] init];
    selectedWeightPriceIndex=[[NSMutableArray alloc] init];
    
    selectedCutLabelIndex=[[NSMutableArray alloc] init];
    selectedCutPriceIndex=[[NSMutableArray alloc] init];
    selectedCutValueIndex=[[NSMutableArray alloc] init];
    selectedTypeLabelIndex=[[NSMutableArray alloc] init];
    selectedTypeValueIndex=[[NSMutableArray alloc] init];
    selectedTypePriceIndex=[[NSMutableArray alloc] init];
    wishlistIdArray=[[NSMutableArray alloc] init];
    containInWishListArray=[[NSMutableArray alloc] init];
    // ,*selectedCutValueIndex,*selectedCutPriceIndex,selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    // NSLog(@"Idarray1=%@",idArray);
    
   
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        
        HelperClass *s=[[HelperClass alloc]init];
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            
            
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            [HUD hide:YES];
            
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }
        else
        {
            @try {
                _pageTableView.delegate = self;
                _pageTableView.dataSource = self;
                [_pageTableView setHidden:NO];
                [_pageTableView reloadData];

                
//                HelperClass *s1=[[HelperClass alloc]init];
//                NSString *urlabc=s1.Get_url;
                //http://order.earlofhindh.com/api/productlist.php?category_id=57&limit=50page=1
//                NSString *tempUrl;
//                tempUrl=[NSString stringWithFormat:@"%@category_id=%@&limit=%d&page=%d",API_MENU_ITEMS,catId,limit,offset];
//                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
                NSMutableDictionary *wishDict=[NSMutableDictionary dictionary];
                if ([isLog isEqualToString:@"YES"]) {
                    wishDict=[wishObj getAllWishData];
                    BOOL success=[[wishDict valueForKey:@"success"] boolValue];
                    if (success) {
                        [wishlistIdArray addObjectsFromArray:[[wishDict valueForKey:@"message"] valueForKey:@"entity_id"]];
                        NSLog(@"%@",wishDict);
                        if ([wishDict count] > 0)
                        {
                            
                            
                            
                            
                            
                            
                            NSArray *idArr=[[wishDict valueForKey:@"message"] valueForKey:@"entity_id"];
                            NSArray *name=[[wishDict valueForKey:@"message"] valueForKey:@"name"];
                            //                NSArray *skuArr=[jsonDict valueForKey:@"sku"];
                            NSArray *descriptionArr=[[wishDict valueForKey:@"message"] valueForKey:@"description"];
                            NSArray *shortDescArr=[[wishDict valueForKey:@"message"] valueForKey:@"short_description"];
                            NSArray *priceArr=[[wishDict valueForKey:@"message"] valueForKey:@"final_price_with_tax"];
                            //                NSArray *isSaleableArr=[jsonDict valueForKey:@"is_saleable"];
                            
                            NSArray *foodType=[[wishDict valueForKey:@"message"] valueForKey:@"food_type"];
                            NSArray *imagesArr=[[wishDict valueForKey:@"message"] valueForKey:@"image_url"];
                            NSArray *imagesThumb=[[wishDict valueForKey:@"message"] valueForKey:@"image_thumb"];
                            NSArray *imagesMainUrl=[[wishDict valueForKey:@"message"] valueForKey:@"main_url"];
                            NSArray *imagesSmall=[[wishDict valueForKey:@"message"] valueForKey:@"image_small"];
                            NSArray *brand=[[wishDict valueForKey:@"message"] valueForKey:@"brand"];
                            NSArray *origin=[[wishDict valueForKey:@"message"] valueForKey:@"origin"];
                            NSArray *weight=[[wishDict valueForKey:@"message"] valueForKey:@"weight"];
                            
                            
                            
                            // dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray;
                            NSArray *attrDict=[[wishDict valueForKey:@"message"] valueForKey:@"attributes"];
                            NSArray *InputTypeArray=[attrDict valueForKey:@"InputType"];
                            NSArray *opIdArr=[attrDict valueForKey:@"option_id"];
                            NSDictionary *attArr=[attrDict valueForKey:@"att"];
                            NSArray *labelArr=[attArr valueForKey:@"label"];
                            NSArray *valueArr=[attArr valueForKey:@"value"];
                            NSArray *priArr=[attArr valueForKey:@"price"];
                            NSArray *InputType=[attArr valueForKey:@"InputType"];
                            
                            
                            //                    for (int j=0; j<[attrDict count]; j++) {
                            //                                          }
                            //
                            
                            //                    [self getRangeValuesOfPrice:priceArr];
                            
                            
                            //                    NSString *isfilter=[[NSUserDefaults standardUserDefaults] valueForKey:ISFILTER];
                            //                    int filterMin=[[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMINPRICE] intValue];
                            //                    int filterMax=[[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMAXPRICE] intValue];
                            //                    NSString *filterBrand=[[NSUserDefaults standardUserDefaults] valueForKey:BRANDVALUE];
                            
                            
                            
                            
                            
                            /****Set attributes*****/
                            NSMutableArray *newPriceArray=[[NSMutableArray alloc] init];
                            
                            //                    NSLog(@"%@",APP_DELEGATE.selectBarndArray);
                            for (int i=0; i<[idArr count]; i++) {
                                if ([[attrDict objectAtIndex:i] count]>0) {
                                    
                                    if ([[InputType objectAtIndex:i] count]>0) {
                                        NSMutableArray *wlArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *wvArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *wpArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *clArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *cvArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *cpArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *tlArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *tvArr=[[NSMutableArray alloc] init];
                                        NSMutableArray *tpArr=[[NSMutableArray alloc] init];
                                        
                                        for (int m=0; m<[[InputType objectAtIndex:i] count]; m++) {
                                            NSArray *arr=[[InputType objectAtIndex:i] objectAtIndex:m];
                                            for (int n=0; n<[arr count]; n++) {
                                                if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"]) {
                                                    NSString *str_wlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_wvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_wpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    [wlArr addObject:str_wlArr];
//                                                    [wvArr addObject:str_wvArr];
//                                                    [wpArr addObject:str_wpArr];
                                                    
//                                                    int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
//                                                    [wlArr addObject:str_wlArr];
//                                                    [wvArr addObject:str_wvArr];
//                                                    [wpArr addObject:@(str_wpArr)];

                                                    int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
                                                    NSString *weightStr = [NSString stringWithFormat:@"%d",str_wpArr];
                                                    [wlArr addObject:str_wlArr];
                                                    [wvArr addObject:str_wvArr];
                                                    [wpArr addObject:weightStr];
                                                }
                                                
                                                if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                                    NSString *str_clArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_cvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_cpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    [clArr addObject:str_clArr];
                                                    [cvArr addObject:str_cvArr];
                                                    [cpArr addObject:str_cpArr];
                                                    
                                                }
                                                
                                                if ([[arr objectAtIndex:n] isEqualToString:@"Type"]) {
                                                    NSString *str_tlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_tvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_tpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    [tlArr addObject:str_tlArr];
                                                    [tvArr addObject:str_tvArr];
                                                    [tpArr addObject:str_tpArr];
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            
                                        }
                                        [weightlabelArray addObject:wlArr];
                                        [weightValueArray addObject:wvArr];
                                        [weightpriceArray addObject:wpArr];
                                        [cutlabelArray addObject:clArr];
                                        [cutValueArray addObject:cvArr];
                                        [cutpriceArray addObject:cpArr];
                                        [typelabelArray addObject:tlArr];
                                        [typeValueArray addObject:tvArr];
                                        [typepriceArray addObject:tpArr];
                                        
                                        
                                    }
                                    
                                    
                                    NSArray *labArr=[opIdArr objectAtIndex:i];
                                    if ( (NSArray *)[NSNull null] == labArr )
                                    {
                                        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                        [optionIdArray addObject:emptyArray];
                                        
                                    }
                                    else
                                    {
                                        //[dropLabelArr addObject:[labArr objectAtIndex:1]];
                                        [optionIdArray addObject:[opIdArr objectAtIndex:i]];
                                        
                                    }
                                    
                                    NSArray *valArr=[InputTypeArray objectAtIndex:i];
                                    if ( (NSArray *)[NSNull null] == valArr )
                                    {
                                        
                                    }
                                    else
                                    {
                                        [dropLabelArr addObject:valArr];
                                        
                                        //                            [dropValueArr addObject:[valArr objectAtIndex:1]];
                                    }
                                    NSLog(@"dropLabelArr=%@",dropLabelArr);
                                    
                                }
                                else{
                                    
                                    NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                    [dropLabelArr addObject:emptyArray];
                                    [optionIdArray addObject:emptyArray];
                                    [weightlabelArray addObject:emptyArray];
                                    [weightValueArray addObject:emptyArray];
                                    [weightpriceArray addObject:emptyArray];
                                    [cutlabelArray addObject:emptyArray];
                                    [cutValueArray addObject:emptyArray];
                                    [cutpriceArray addObject:emptyArray];
                                    [typelabelArray addObject:emptyArray];
                                    [typeValueArray addObject:emptyArray];
                                    [typepriceArray addObject:emptyArray];
                                    
                                    
                                    
                                    
                                }
                                
                                [newPriceArray addObject:[priceArr objectAtIndex:i]];
                                
                                [idArray addObject:[idArr objectAtIndex:i]];
                                [nameArray addObject:[name objectAtIndex:i]];
                                [descriptionArray addObject:[descriptionArr objectAtIndex:i]];
                                [imagesArray addObject:[imagesArr objectAtIndex:i]];
                                
                                
                                if (!((NSArray *)[NSNull null] == [foodType objectAtIndex:i]))
                                {
                                    [foodTypeArr addObject:[foodType objectAtIndex:i]];
                                    
                                }
                                else{
                                    
                                    [foodTypeArr addObject:@"0"];
                                    
                                    
                                }
                                
                                
                                /*if(!([foodType objectAtIndex:i] ==nil) || [NSNull null]!=[foodType objectAtIndex:i])
                                 {
                                 [foodTypeArr addObject:[foodType objectAtIndex:i]];
                                 
                                 }
                                 else
                                 {
                                 [foodTypeArr addObject:@"0"];
                                 }*/
                                
                                
                                //                        [skuArray addObject:[skuArr objectAtIndex:i]];
                                [shortDescArray addObject:[shortDescArr objectAtIndex:i]];
                                
                                
                                [brandIndexArr addObject:[brand objectAtIndex:i]];
                                [imageMainArray addObject:[imagesMainUrl objectAtIndex:i]];
                                [imageSmallArray addObject:[imagesSmall objectAtIndex:i]];
                                [imageThumbArray addObject:[imagesThumb objectAtIndex:i]];
                                
                                NSString *weightArr;
                                if (!((NSString *)[NSNull null] == [weight objectAtIndex:i]))
                                {
                                    weightArr=[weight objectAtIndex:i];
                                    //[foodTypeArr addObject:[foodType objectAtIndex:i]];
                                    
                                }
                                else
                                {
                                    
                                    weightArr=@"0";
                                }
                                
                                [weightArray addObject:[weight objectAtIndex:i]];
                                
                                NSArray *getAttr=[object fetch_BrandValue:@"brand" AndValue:[brand objectAtIndex:i]];
                                if ([getAttr count]>0) {
                                    [brandValueArr addObject:[[getAttr valueForKey:@"attribute_label"] objectAtIndex:0]];
                                    
                                }
                                else{
                                    [brandValueArr addObject:@"0"];
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            /****END of attributes*********/
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            //                    if ([[isSaleableArr objectAtIndex:i] boolValue] == YES) {
                            
                            
                            
                            
                            NSMutableDictionary *dict=[NSMutableDictionary dictionary];
                            for (int m=0; m<[optionIdArray count]; m++) {
                                int a,b,c;
                                a=0;b=0;c=0;
                                if([[optionIdArray objectAtIndex:m] count]>0){
                                    
                                    for (int n=0; n<[[optionIdArray objectAtIndex:m] count]; n++) {
                                        if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                            [cutOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                            a=1;
                                        }
                                        if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight"]) {
                                            [weightOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                            b=1;
                                        }
                                        if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Type"]) {
                                            [typeOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                            c=1;
                                        }
                                        
                                        
                                    }
                                    if (a==0) {
                                        [cutOptionIdArray addObject:@"0"];
                                    }
                                    if (b==0) {
                                        [weightOptionIdArray addObject:@"0"];
                                    }
                                    if (c==0) {
                                        [typeOptionIdArray addObject:@"0"];
                                    }
                                    
                                    
                                    
                                }
                                else{
                                    
                                    [cutOptionIdArray addObject:@"0"];
                                    [typeOptionIdArray addObject:@"0"];
                                    [weightOptionIdArray addObject:@"0"];
                                    
                                }
                                
                                
                                NSString *concatedPrice=[NSString stringWithFormat:@"%@",[newPriceArray objectAtIndex:m]];
                                int conPri=[concatedPrice intValue];
                                
                                if (conPri == 0) {
                                    int sum;
                                    sum=0;
                                    if([[weightpriceArray objectAtIndex:m] count]>0){
                                        
                                        sum=+[[[weightpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                                    }
                                    if([[cutpriceArray objectAtIndex:m] count]>0){
                                        sum=sum+[[[cutpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                                    }
                                    if([[typepriceArray objectAtIndex:m] count]>0)
                                    {
                                        sum=sum+[[[typepriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                                    }
                                    NSString *calcSum=[NSString stringWithFormat:@"%d",sum];
                                    [priceArray addObject:calcSum];
                                }
                                else{
                                    [priceArray addObject:concatedPrice];
                                }
                                
                                if([[weightlabelArray objectAtIndex:m] count]>0){
                                    [selectedWeightLabelIndex addObject:[[weightlabelArray objectAtIndex:m] objectAtIndex:0]];
                                    [selectedWeightValueIndex addObject:[[weightValueArray objectAtIndex:m] objectAtIndex:0]];
                                    [selectedWeightPriceIndex addObject:[[weightpriceArray objectAtIndex:m] objectAtIndex:0]];
                                    
                                }
                                else{
                                    [selectedWeightLabelIndex addObject:@"0"];
                                    [selectedWeightValueIndex addObject:@"0"];
                                    [selectedWeightPriceIndex addObject:@"0"];
                                    
                                }
                                if([[cutlabelArray objectAtIndex:m] count]>0){
                                    [selectedCutLabelIndex addObject:[[cutlabelArray objectAtIndex:m] objectAtIndex:0]];
                                    [selectedCutValueIndex addObject:[[cutValueArray objectAtIndex:m] objectAtIndex:0]];
                                    [selectedCutPriceIndex addObject:[[cutpriceArray objectAtIndex:m] objectAtIndex:0]];
                                    
                                }
                                else{
                                    [selectedCutLabelIndex addObject:@"0"];
                                    [selectedCutValueIndex addObject:@"0"];
                                    [selectedCutPriceIndex addObject:@"0"];
                                    
                                }
                                if([[typelabelArray objectAtIndex:m] count]>0){
                                    [selectedTypeLabelIndex addObject:[[typelabelArray objectAtIndex:m] objectAtIndex:0]];
                                    [selectedTypeValueIndex addObject:[[typeValueArray objectAtIndex:m] objectAtIndex:0]];
                                    [selectedTypePriceIndex addObject:[[typepriceArray objectAtIndex:m] objectAtIndex:0]];
                                    
                                }
                                else{
                                    [selectedTypeLabelIndex addObject:@"0"];
                                    [selectedTypeValueIndex addObject:@"0"];
                                    [selectedTypePriceIndex addObject:@"0"];
                                    
                                }
                                
                            }
                            
                            [self fillContainwishArray:idArray AndWishIdArray:wishlistIdArray];
                            
                            
                            
                            
                            //                OfflineDb *object=[[OfflineDb alloc]init];
                            
                            //                    [self getRangeValuesOfPrice:priceArray];
                            
                            [HUD hide:YES];
                            
                            NSLog(@"Idarray1=%@",idArray);
                            
                            if ([idArray count] > 0)
                            {
                                _pageTableView.delegate = self;
                                _pageTableView.dataSource = self;
                                [self.pageTableView reloadData];
                                
                            }
                            else
                            {
                                _pageTableView.hidden=YES;
                                UIAlertController * alert=   [UIAlertController
                                                              alertControllerWithTitle:@"Alert"
                                                              message:@"No data found."
                                                              preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                           handler:^(UIAlertAction * action) {
                                                                               //Do Some action here
                                                                               [HUD hide:YES];
                                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                                               
                                                                           }];
                                [alert addAction:ok];
                                
                                [self presentViewController:alert animated:YES completion:nil];
                                
                                
                            }
                            
                            
                        }
                        else
                        {
                            
                            [HUD hide:YES];
                            //                    UIAlertController * alert=   [UIAlertController
                            //                                                  alertControllerWithTitle:@"Alert"
                            //                                                  message:@"There might be a network issue.Please check your internet connection and reload again!"
                            //                                                  preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@"Alert"
                                                          message:@"No Data Found or Network Issue!"
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action) {
                                                                           //Do Some action here
                                                                           [HUD hide:YES];
                                                                           nameArray=[[NSMutableArray alloc] init];
                                                                           idArray=[[NSMutableArray alloc] init];
                                                                           imagesArray=[[NSMutableArray alloc] init];
                                                                           idArray=[[NSMutableArray alloc] init];
                                                                           nameArray=[[NSMutableArray alloc] init];
                                                                           descriptionArray=[[NSMutableArray alloc] init];
                                                                           imagesArray=[[NSMutableArray alloc] init];
                                                                           skuArray=[[NSMutableArray alloc] init];
                                                                           priceArray=[[NSMutableArray alloc] init];
                                                                           shortDescArray=[[NSMutableArray alloc] init];
                                                                           idContainInLocal=[[NSMutableArray alloc] init];
                                                                           localQtyArr=[[NSMutableArray alloc] init];
                                                                           foodTypeArr=[[NSMutableArray alloc] init];
                                                                           
                                                                           localCutOpIdArray=[[NSMutableArray alloc] init];
                                                                           localCutOpValArray=[[NSMutableArray alloc] init];
                                                                           localCutOpPriceArray=[[NSMutableArray alloc] init];
                                                                           
                                                                           localWeightOpIdArray=[[NSMutableArray alloc] init];
                                                                           localWeightOpValArray=[[NSMutableArray alloc] init];
                                                                           localWeightOpPriceArray=[[NSMutableArray alloc] init];
                                                                           
                                                                           localTypeOpIdArray=[[NSMutableArray alloc] init];
                                                                           localTypeOpValArray=[[NSMutableArray alloc] init];
                                                                           localTypeOpPriceArray=[[NSMutableArray alloc] init];
                                                                           
                                                                           NSLog(@"Idarray1=%@",idArray);
                                                                           [_pageTableView reloadData];
                                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                                           
                                                                       }];
                            [alert addAction:ok];
                            
                            [self presentViewController:alert animated:YES completion:nil];
                            
                        }
                        

                    }
                    else{
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:@"Alert"
                                                      message:@"No data found."
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       //Do Some action here
                                                                       [HUD hide:YES];
                                                                       nameArray=[[NSMutableArray alloc] init];
                                                                       idArray=[[NSMutableArray alloc] init];
                                                                       imagesArray=[[NSMutableArray alloc] init];
                                                                       idArray=[[NSMutableArray alloc] init];
                                                                       nameArray=[[NSMutableArray alloc] init];
                                                                       descriptionArray=[[NSMutableArray alloc] init];
                                                                       imagesArray=[[NSMutableArray alloc] init];
                                                                       skuArray=[[NSMutableArray alloc] init];
                                                                       priceArray=[[NSMutableArray alloc] init];
                                                                       shortDescArray=[[NSMutableArray alloc] init];
                                                                       idContainInLocal=[[NSMutableArray alloc] init];
                                                                       localQtyArr=[[NSMutableArray alloc] init];
                                                                       foodTypeArr=[[NSMutableArray alloc] init];
                                                                       
                                                                       localCutOpIdArray=[[NSMutableArray alloc] init];
                                                                       localCutOpValArray=[[NSMutableArray alloc] init];
                                                                       localCutOpPriceArray=[[NSMutableArray alloc] init];
                                                                       
                                                                       localWeightOpIdArray=[[NSMutableArray alloc] init];
                                                                       localWeightOpValArray=[[NSMutableArray alloc] init];
                                                                       localWeightOpPriceArray=[[NSMutableArray alloc] init];
                                                                       
                                                                       localTypeOpIdArray=[[NSMutableArray alloc] init];
                                                                       localTypeOpValArray=[[NSMutableArray alloc] init];
                                                                       localTypeOpPriceArray=[[NSMutableArray alloc] init];
                                                                       
                                                                       NSLog(@"Idarray1=%@",idArray);
                                                                       [_pageTableView reloadData];
                                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                                       
                                                                   }];
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];

                    
                    }
//                    [wishlistIdArray addObjectsFromArray:[[wishDict valueForKey:@"message"] valueForKey:@"entity_id"]];
                    
                    
                }
                else{
                
                    [HUD hide:YES];
                    //                    UIAlertController * alert=   [UIAlertController
                    //                                                  alertControllerWithTitle:@"Alert"
                    //                                                  message:@"There might be a network issue.Please check your internet connection and reload again!"
                    //                                                  preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Alert"
                                                  message:@"You need to login first."
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   //Do Some action here
                                                                   [HUD hide:YES];
                                                                   nameArray=[[NSMutableArray alloc] init];
                                                                   idArray=[[NSMutableArray alloc] init];
                                                                   imagesArray=[[NSMutableArray alloc] init];
                                                                   idArray=[[NSMutableArray alloc] init];
                                                                   nameArray=[[NSMutableArray alloc] init];
                                                                   descriptionArray=[[NSMutableArray alloc] init];
                                                                   imagesArray=[[NSMutableArray alloc] init];
                                                                   skuArray=[[NSMutableArray alloc] init];
                                                                   priceArray=[[NSMutableArray alloc] init];
                                                                   shortDescArray=[[NSMutableArray alloc] init];
                                                                   idContainInLocal=[[NSMutableArray alloc] init];
                                                                   localQtyArr=[[NSMutableArray alloc] init];
                                                                   foodTypeArr=[[NSMutableArray alloc] init];
                                                                   
                                                                   localCutOpIdArray=[[NSMutableArray alloc] init];
                                                                   localCutOpValArray=[[NSMutableArray alloc] init];
                                                                   localCutOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   localWeightOpIdArray=[[NSMutableArray alloc] init];
                                                                   localWeightOpValArray=[[NSMutableArray alloc] init];
                                                                   localWeightOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   localTypeOpIdArray=[[NSMutableArray alloc] init];
                                                                   localTypeOpValArray=[[NSMutableArray alloc] init];
                                                                   localTypeOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   NSLog(@"Idarray1=%@",idArray);
                                                                   [_pageTableView reloadData];
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                                   
                                                               }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];

                
                }

                
                
                //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
//                NSDictionary *jsonDict=[web GetTopviewProductWithUrl:tempUrl];
               
                
            } @catch (NSException *exception)
            {
                NSLog(@"Exception = %@",exception);
                [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                
            }
            
        }
        
    });
    
    
}

-(void)fillContainwishArray:(NSMutableArray *)prodIdArr AndWishIdArray:(NSMutableArray *)wIdArr{
    for (int i=0; i<[prodIdArr count]; i++) {
        if ([wIdArr containsObject:[prodIdArr objectAtIndex:i]]) {
            [containInWishListArray addObject:@"1"];
        }
        else
        {
            [containInWishListArray addObject:@"0"];

        }
    }

}
-(void)getRangeValuesOfPrice:(NSArray *)priArr{
    
    NSMutableArray *integerArrayFor=[[NSMutableArray alloc] init];
    [integerArrayFor addObjectsFromArray:priArr];
    for (int i=0; i<[integerArrayFor count]; i++) {
        NSString *a=0;
        
        for(int j=i+1 ;j<[integerArrayFor count]; j++)
        {
            if ([[integerArrayFor objectAtIndex:i] intValue]> [[integerArrayFor objectAtIndex:j] intValue]) {
                a=[integerArrayFor objectAtIndex:i] ;
                [integerArrayFor replaceObjectAtIndex:i withObject:[integerArrayFor objectAtIndex:j]];
                [integerArrayFor replaceObjectAtIndex:j withObject:a];
                
                
            }
            
        }
    }

//    NSSortDescriptor *sortDescriptor;
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil
//                                                  ascending:YES] ;
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    NSArray *sortedArray;
//    sortedArray = [priArr sortedArrayUsingDescriptors:sortDescriptors];
    NSString *min,*max;
    if ([integerArrayFor count]==1) {
        _btnFilter.hidden=YES;

//        min=[NSString stringWithFormat:@"%d",[[sortedArray firstObject] intValue]];
//        max=[NSString stringWithFormat:@"%d",[[sortedArray lastObject] intValue]];
    }
    else if([integerArrayFor count]==0){
    
        _btnFilter.hidden=YES;
    }
    else{
        _btnFilter.hidden=NO;

        min=[NSString stringWithFormat:@"%d",[[integerArrayFor firstObject] intValue]];
        max=[NSString stringWithFormat:@"%d",[[integerArrayFor lastObject] intValue]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] ==nil || [[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] length] <=0) {
        [[NSUserDefaults standardUserDefaults] setObject:min forKey:MINPRICE];

    }
    else{
        int compMin=[[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] intValue];
        if (compMin<[min intValue]) {

        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:min forKey:MINPRICE];

        }
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] ==nil || [[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] length] <=0) {
        [[NSUserDefaults standardUserDefaults] setObject:max forKey:MAXPRICE];
        
    }
    else{
        int compMin=[[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] intValue];
        if (compMin>[max intValue]) {
            
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:max forKey:MAXPRICE];
            
        }
    }


//    for (int i=0; i<[priArr count]; i++) {
//        if ([[priArr objectAtIndex:i] intValue] < min) {
//            min=[[priArr objectAtIndex:i] intValue];
//        }
//        if ([[priArr objectAtIndex:i] intValue] > max) {
//            max=[[priArr objectAtIndex:i] intValue];
//        }
//
//    }

}




/*
 -(void)GetTopviewProduct:(NSString *)catId
 {
 //[ProgressHUD show:PROGRESS_PLEASE_WAIT_EN Interaction:NO];
 
 
 nameArray=[[NSMutableArray alloc] init];
 idArray=[[NSMutableArray alloc] init];
 imagesArray=[[NSMutableArray alloc] init];
 idArray=[[NSMutableArray alloc] init];
 nameArray=[[NSMutableArray alloc] init];
 descriptionArray=[[NSMutableArray alloc] init];
 imagesArray=[[NSMutableArray alloc] init];
 skuArray=[[NSMutableArray alloc] init];
 priceArray=[[NSMutableArray alloc] init];
 shortDescArray=[[NSMutableArray alloc] init];
 idContainInLocal=[[NSMutableArray alloc] init];
 localQtyArr=[[NSMutableArray alloc] init];
 foodTypeArr=[[NSMutableArray alloc] init];
 
 dropLabelArr=[[NSMutableArray alloc] init];
 dropValueArr=[[NSMutableArray alloc] init];
 fieldIdArray=[[NSMutableArray alloc] init];
 dropDownIdArray=[[NSMutableArray alloc] init];
 spicelevelArray=[[NSMutableArray alloc] init];
 messagesArray=[[NSMutableArray alloc] init];
 
 HelperClass *s1=[[HelperClass alloc]init];
 NSString *urlabc=s1.Get_url;
 //http://order.earlofhindh.com/api/productlist.php?category_id=57&limit=50page=1
 NSString *tempUrl;
 tempUrl=[NSString stringWithFormat:@"%@productlist.php?category_id=%@&limit=%d&page=%d",urlabc,catId,limit,offset];
 
 AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
 //    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"video/mpeg",@"application/x-www-form-urlencoded", nil];
 [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
 
 manager.requestSerializer=[AFJSONRequestSerializer serializer];
 manager.responseSerializer=[AFJSONResponseSerializer serializer];
 //    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
 AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
 responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
 
 @try
 {
 [manager GET:tempUrl
 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
 {
 NSLog(@"%@",responseObject);
 //             BOOL Success=[[responseObject valueForKey:@"success"] boolValue];
 if ([responseObject count] > 0) {
 NSMutableArray *getkeys=[[NSMutableArray alloc] init];
 NSMutableArray *getValues=[[NSMutableArray alloc] init];
 
 
 
 
 
 
 NSArray *idArr=[responseObject valueForKey:@"entity_id"];
 NSArray *name=[responseObject valueForKey:@"name"];
 //                NSArray *skuArr=[jsonDict valueForKey:@"sku"];
 NSArray *descriptionArr=[responseObject valueForKey:@"description"];
 NSArray *shortDescArr=[responseObject valueForKey:@"short_description"];
 NSArray *priceArr=[responseObject valueForKey:@"final_price_with_tax"];
 //                NSArray *isSaleableArr=[jsonDict valueForKey:@"is_saleable"];
 NSArray *foodType=[responseObject valueForKey:@"food_type"];
 
 NSArray *imagesArr=[responseObject valueForKey:@"image_url"];
 NSArray *imagesThumb=[responseObject valueForKey:@"image_thumb"];
 NSArray *imagesMainUrl=[responseObject valueForKey:@"main_url"];
 NSArray *imagesSmall=[responseObject valueForKey:@"image_small"];
 
 // dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray;
 NSArray *attrDict=[responseObject valueForKey:@"attributes"];
 for (int j=0; j<[attrDict count]; j++) {
 if ([[attrDict objectAtIndex:j] count]>0) {
 
 NSArray *InputTypeArray=[attrDict valueForKey:@"Type"];
 NSArray *optionIdArray=[attrDict valueForKey:@"option_id"];
 NSDictionary *attArr=[attrDict valueForKey:@"att"];
 NSArray *labelArr=[attArr valueForKey:@"label"];
 NSArray *valueArr=[attArr valueForKey:@"value"];
 //                        NSArray *tempLabel = [labelArr valueForKeyPath:@"@unionOfArrays.self"];
 //                        NSArray *tempValue = [valueArr valueForKeyPath:@"@unionOfArrays.self"];
 
 NSArray *labArr=[labelArr objectAtIndex:j];
 if ( (NSArray *)[NSNull null] == labArr )
 {
 
 }
 else
 {
 [dropLabelArr addObject:[labArr objectAtIndex:1]];
 }
 
 NSArray *valArr=[valueArr objectAtIndex:j];
 if ( (NSArray *)[NSNull null] == valArr )
 {
 
 }
 else
 {
 [dropValueArr addObject:[valArr objectAtIndex:1]];
 }
 
 
 
 
 
 NSArray *optsArr=[optionIdArray objectAtIndex:j];
 if ( (NSArray *)[NSNull null] == optsArr )
 {
 [fieldIdArray addObject:@"0"];
 [dropDownIdArray addObject:@"0"];
 }
 else
 {
 [fieldIdArray addObject:[optsArr objectAtIndex:0]];
 [dropDownIdArray addObject:[optsArr objectAtIndex:1]];
 
 }
 
 
 }
 else{
 NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
 [dropLabelArr addObject:@"0"];
 [dropValueArr addObject:emptyArray];
 [fieldIdArray addObject:@"0"];
 [dropDownIdArray addObject:@"0"];
 
 
 }
 }
 
 
 for (int i=0; i<[idArr count]; i++) {
 
 
 
 //                    if ([[isSaleableArr objectAtIndex:i] boolValue] == YES) {
 NSString *concatedPrice=[NSString stringWithFormat:@"S$ %@",[priceArr objectAtIndex:i]];
 
 [idArray addObject:[idArr objectAtIndex:i]];
 [nameArray addObject:[name objectAtIndex:i]];
 [descriptionArray addObject:[descriptionArr objectAtIndex:i]];
 [imagesArray addObject:[imagesArr objectAtIndex:i]];
 //                        [skuArray addObject:[skuArr objectAtIndex:i]];
 [shortDescArray addObject:[shortDescArr objectAtIndex:i]];
 [priceArray addObject:concatedPrice];
 
 
 NSString *aString=[foodType objectAtIndex:i];
 
 if ( (NSString *)[NSNull null] == aString )
 {
 aString=@"4";
 [foodTypeArr addObject:aString];
 }
 else
 {
 [foodTypeArr addObject:aString];
 //[nameArray insertObject:aString atIndex:k];
 }
 
 //                    }
 }
 
 NSLog(@"%@",shortDescArray);
 
 
 
 
 OfflineDb *object=[[OfflineDb alloc]init];
 NSArray *dataArray=[object fetch_CartDetails];
 NSLog(@"%@",dataArray);
 
 NSArray *localIds=[dataArray valueForKey:@"cart_id"];
 NSArray *localSpiceId=[dataArray valueForKey:@"cart_spiceId"];
 
 for (int i=0; i<[idArray count]; i++)
 {
 if ([localIds containsObject:[idArray objectAtIndex:i]])
 {
 [idContainInLocal addObject:@"1"];
 NSArray *getQtyFromDb=[object fetch_Cart_Quantity_With_Msg_Details:catId :[idArray objectAtIndex:i]];
 NSArray *locQty=[getQtyFromDb valueForKey:@"cart_qty"];
 NSArray *locMsg=[getQtyFromDb valueForKey:@"cart_msg"];
 NSArray *locmsgId=[getQtyFromDb valueForKey:@"cart_msgId"];
 NSArray *locspiceLevel=[getQtyFromDb valueForKey:@"cart_spiceLevel"];
 NSArray *locspiceId=[getQtyFromDb valueForKey:@"cart_spiceId"];
 
 NSLog(@"%@",getQtyFromDb);
 [localQtyArr addObject:[locQty objectAtIndex:0]];
 [spicelevelArray addObject:[locspiceLevel objectAtIndex:0]];
 [messagesArray addObject:[locMsg objectAtIndex:0]];
 
 
 }
 else
 {
 [idContainInLocal addObject:@"0"];
 [localQtyArr addObject:@"0"];
 [spicelevelArray addObject:@"0"];
 [messagesArray addObject:@"0"];
 }
 }
 NSLog(@"%@",localQtyArr);
 //                NSArray *getQtyFromDb=[object fetch_CartDetails:_categoryId];
 //                NSLog(@"%@",getQtyFromDb);
 //                NSArray *locQty=[getQtyFromDb valueForKey:@"cart_qty"];
 
 [HUD hide:YES];
 
 
 [self.pageTableView reloadData];
 
 }
 else{
 
 [HUD hide:YES];
 UIAlertController * alert=   [UIAlertController
 alertControllerWithTitle:@"Empty"
 message:@"No data found."
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Back" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action) {
 //Do Some action here
 [HUD hide:YES];
 nameArray=[[NSMutableArray alloc] init];
 idArray=[[NSMutableArray alloc] init];
 imagesArray=[[NSMutableArray alloc] init];
 idArray=[[NSMutableArray alloc] init];
 nameArray=[[NSMutableArray alloc] init];
 descriptionArray=[[NSMutableArray alloc] init];
 imagesArray=[[NSMutableArray alloc] init];
 skuArray=[[NSMutableArray alloc] init];
 priceArray=[[NSMutableArray alloc] init];
 shortDescArray=[[NSMutableArray alloc] init];
 idContainInLocal=[[NSMutableArray alloc] init];
 localQtyArr=[[NSMutableArray alloc] init];
 foodTypeArr=[[NSMutableArray alloc] init];
 
 dropLabelArr=[[NSMutableArray alloc] init];
 dropValueArr=[[NSMutableArray alloc] init];
 fieldIdArray=[[NSMutableArray alloc] init];
 dropDownIdArray=[[NSMutableArray alloc] init];
 spicelevelArray=[[NSMutableArray alloc] init];
 messagesArray=[[NSMutableArray alloc] init];
 
 [_pageTableView reloadData];
 [alert dismissViewControllerAnimated:YES completion:nil];
 
 }];
 [alert addAction:ok];
 
 [self presentViewController:alert animated:YES completion:nil];
 
 }
 
 
 
 }
 failure:^(AFHTTPRequestOperation *operation, NSError *error)
 {
 // handle ur failure response
 if (error.code==-1009 ||error.code==-1004) {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:@"Please check your internet connection!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 }
 else
 {
 
 NSLog(@"%@",error.description);
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
 message:@"Please try again later!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 }
 }];
 }
 @catch (NSException *exception)
 {
 // handle ur exception
 NSLog(@"%@",exception);
 }
 
 }
 */






//- (IBAction)profileMethod:(id)sender {
//            FilterController *lo=[self.storyboard instantiateViewControllerWithIdentifier:@"FilterController"];
//            [self.navigationController pushViewController:lo animated:YES];
//
//
//
//
//    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
//    if ([isLog isEqualToString:@"YES"]) {
////        ProfileController *lo=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileController"];
////        [self.navigationController pushViewController:lo animated:YES];
//
//    }
//    else{
//
////        LoginController *lo=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
////        lo.comingFrom=@"PROFILE";
////        lo.goBackTo=@"ItemsViewController";
////        [self.navigationController pushViewController:lo animated:YES];
//    }
//
//}

- (IBAction)cartMethod:(id)sender
{
    
    //    OfflineDb *object=[[OfflineDb alloc]init];
    NSArray *dataArray=[object fetch_CartDetails];
    NSLog(@"%@",dataArray);
    if ([dataArray count]>0)
    {
        CartController *viewController =[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
        viewController.comingFrom=@"ItemsVC";
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    else
    {
        
        [ProgressHUD showError:@"Cart Empty" Interaction:NO];
    }
}

- (IBAction)wishMethod:(id)sender {
    
    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];

    if ([isLog isEqualToString:@"YES"]) {
        
        NSLog(@"%ld",[sender tag]);
        
        HelperClass *s=[[HelperClass alloc]init];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            BOOL connection=s.getInternetStatus;
            
            if (!connection)
            {
                
                
                NSLog(@"Hello Net connectiion is not pres;ent....");
                
                [HUD hide:YES];
                
                UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Connection Lost" message:@"Please check your internet connection!" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
                
                
                [alrt addAction:ok];
                [self presentViewController:alrt animated:YES completion:nil];
                
                
            }
            else
            {
                @try {
                    long row = [sender tag];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
                    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
                    
                    
                    
//                    if ([[containInWishListArray objectAtIndex:row] isEqualToString:@"0"]) {
//                        NSArray *getdata=[wishObj insertIntoProductList:[idArray objectAtIndex:row]];
//                        NSLog(@"%@",getdata);
//                        if ([getdata count]>0) {
//                            
//                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
//                            if (success) {
//                                [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
//                                [containInWishListArray replaceObjectAtIndex:row withObject:@"1"];
//
//                            }
//                            else{
//                                NSString *msg=[getdata valueForKey:@"message"];
//                                [ProgressHUD showError:msg Interaction:NO];
//
//                            }
//                        }
//                        else{
//                            
//                            [ProgressHUD showError:@"Unable to add to wishlist" Interaction:NO];
//                            
//                        }
//                        
//                    }
//                    else if ([[containInWishListArray objectAtIndex:row] isEqualToString:@"1"]){
                    
                        NSArray *getdata=[wishObj deleteFromWishList:[idArray objectAtIndex:row]];
                        NSLog(@"%@",getdata);
                        if ([getdata count]>0) {
                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
                            if (success) {
                                HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                HUD.dimBackground = YES;
                                HUD.labelText = @"Please wait...";
                                [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:nil animated:YES];

                                
                            }
                            else{
                                NSString *msg=[getdata valueForKey:@"message"];
                                [ProgressHUD showError:msg Interaction:NO];

                            }
                        }
                        else{
                            
                            [ProgressHUD showError:@"Unable to delete from wishlist" Interaction:NO];
                            
                        }
                        
                        
//                    }
                    
                    
                } @catch (NSException *exception) {
                    [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                }
                
            }
        });

        
    }
    else{
        UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Alert" message:@"You need to login first." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
        [alrt addAction:ok];
        [self presentViewController:alrt animated:YES completion:nil];

        
    
    }
    
    

}

- (IBAction)cutMethod:(id)sender {
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    
    pickerCutLabelArr=[[NSMutableArray alloc] init];
    pickerCutValueArr=[[NSMutableArray alloc] init];
    pickerCutPriceArr=[[NSMutableArray alloc] init];
    [pickerCutLabelArr addObjectsFromArray:[cutlabelArray objectAtIndex:row]];
    [pickerCutValueArr addObjectsFromArray:[cutValueArray objectAtIndex:row]];
    [pickerCutPriceArr addObjectsFromArray:[cutpriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=1;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedCutLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             //                                     [spicelevelArray replaceObjectAtIndex:[sender tag] withObject:selectedSpiceIndex];
                                             [cell.btn_cut setTitle:[selectedCutLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                             

                                         }
                                         else{
                                             
                                             [selectedCutLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedCutValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedCutPriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             [cell.btn_cut setTitle:[selectedCutLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             

                                             
                                         }
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                         cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
    
}
- (IBAction)weightMethod:(id)sender {
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    pickerWeightLabelArr=[[NSMutableArray alloc] init];
    pickerWeightValueArr=[[NSMutableArray alloc] init];
    pickerWeightPriceArr=[[NSMutableArray alloc] init];
    [pickerWeightLabelArr addObjectsFromArray:[weightlabelArray objectAtIndex:row]];
    [pickerWeightValueArr addObjectsFromArray:[weightValueArray objectAtIndex:row]];
    [pickerWeightPriceArr addObjectsFromArray:[weightpriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=2;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedWeightLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             [cell.btn_weight setTitle:[selectedWeightLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                         }
                                         else{
                                             
                                             
                                             [selectedWeightLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedWeightValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedWeightPriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             
                                             [cell.btn_weight setTitle:[selectedWeightLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                             
                                         }
                                         
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         
                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                         cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         
                                     }
                                     @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
    
}

- (IBAction)typeMethod:(id)sender {
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    
    pickerTypeLabelArr=[[NSMutableArray alloc] init];
    pickerTypeValueArr=[[NSMutableArray alloc] init];
    pickerTypePriceArr=[[NSMutableArray alloc] init];
    [pickerTypeLabelArr addObjectsFromArray:[typelabelArray objectAtIndex:row]];
    [pickerTypeValueArr addObjectsFromArray:[typeValueArray objectAtIndex:row]];
    [pickerTypePriceArr addObjectsFromArray:[typepriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=3;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedTypeLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             [cell.btn_type setTitle:[selectedTypeLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                         }
                                         else{
                                             
                                             [selectedTypeLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedTypeValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedTypePriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             [cell.btn_type setTitle:[selectedTypeLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                             
                                             
                                         }
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         if(calcPrice==0)
                                         {
                                         
                                         }
                                         else
                                         {
                                             NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                             cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                             [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         }
                                         
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
    
}




-(BOOL)validations:(long)row{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.btn_cut.titleLabel.text isEqualToString:SELECTCUT] || [cell.btn_cut.titleLabel.text isEqualToString:@""] || cell.btn_cut.titleLabel.text==nil) {
        
        [ProgressHUD showError:@"Please select cut." Interaction:NO];
        return NO;
        
    }
    else if ([cell.btn_weight.titleLabel.text isEqualToString:SELECTWEIGHT] || [cell.btn_weight.titleLabel.text isEqualToString:@""] || cell.btn_weight.titleLabel.text==nil) {
        
        [ProgressHUD showError:@"Please select weight." Interaction:NO];
        return NO;
        
    }
    else if ([cell.btn_type.titleLabel.text isEqualToString:SELECTTYPE] || [cell.btn_type.titleLabel.text isEqualToString:@""] || cell.btn_type.titleLabel.text==nil) {
        
        [ProgressHUD showError:@"Please select type." Interaction:NO];
        return NO;
        
    }
    
    
    return YES;
}

- (IBAction)callUsBtnClicked:(id)sender
{
    NSString *phNo = @"+919699933330";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Alert" message:@"Call facility is not available!!!"];
    }
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Close App" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    //[alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}






@end
