//
//  SidebarViewController.m
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "ViewController.h"
#import "CustomTableViewCell.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "ItemsViewController.h"
#import "WishListViewController.h"
#import "ContactUsViewController.h"
#import "HelpController.h"
@interface SidebarViewController ()
{
    NSUserDefaults *defaults;
    NSArray *subTitleArray;
    NSMutableArray *idArray;
    NSMutableArray *nameArray,*imagesArray,*staticStoreIdArr,*brandListArr,*brandIdArr;
    OfflineDb *object;
    
}

@property (strong, nonatomic) IBOutlet UITableView *sideBarTableView;

@end

@implementation SidebarViewController

@synthesize sectionImagesArray,subTitleImgsArray;



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    
    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
    NSString *selectedLocation=[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDLOCATION];
    
    _sideBarTableView.backgroundColor = [UIColor whiteColor];
    
    brandListArr=[[NSMutableArray alloc] init];
    brandIdArr=[[NSMutableArray alloc] init];
    NSArray *brandData=[object fetch_AttributeValue:@"brand"];
    brandListArr=[brandData valueForKey:@"attribute_label"];
    brandIdArr=[brandData valueForKey:@"attribute_value"];
    
    if ([isLog isEqualToString:@"YES"]) {
        
        
        
        arrayForBool=[[NSMutableArray alloc]init];
        sectionTitleArray=[[NSMutableArray alloc]initWithObjects:
                           @"",
                           @"Home",
                           @"Menu",
                           @"Order History",
//                           @"Location",
                           @"Wishlist",
                           @"Profile",
                           @"Visit Website",
                           @"Contact Us",
                           //@"Blog",
                           //@"Help",
                           @"Sign Out",
                           @"",
                           nil];
        
    }
    else{
        arrayForBool=[[NSMutableArray alloc]init];
        sectionTitleArray=[[NSMutableArray alloc]initWithObjects:
                           @"",
                           @"Home",
                           @"Menu",
                           @"Order History",
//                           @"Location",
                           @"Wishlist",
                           @"Profile",
                           @"Visit Website",
                           @"Contact Us",
                           //@"Blog",
                           //@"Help",
                           @"Sign In",
                           @"",
                           nil];
        
    }
    
    if (!(selectedLocation.length>0) || [selectedLocation isEqualToString:@""] || selectedLocation==nil){
        NSLog(@"not to add");
    }else{
        [sectionTitleArray replaceObjectAtIndex:4 withObject:selectedLocation];
    }
    
    for (int i=0; i<[sectionTitleArray count]; i++)
    {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    [_sideBarTableView reloadData];
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    
    OfflineDb *object=[[OfflineDb alloc]init];
    NSString *parentId=PARENTID;
    NSArray *dataArray=[object fetch_parentWiseCategory:parentId];
    //NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
    //[dictObj setObject:dataArray forKey:@"Data"];
    
    //idArray=[dataArray valueForKey:@"category_id"];
    
    NSArray *nArr=[dataArray valueForKey:@"category_name"];
    NSArray *iArr=[dataArray valueForKey:@"category_id"];
    NSArray *imArr=[[NSMutableArray alloc] initWithObjects:@"bg_bond.png",@"bg_mica.png",@"bg_ceramic.png",@"bg_exterior.png",@"bg_veneer.png",@"bg_plywood.png", nil];
    
    /////code for sequence
    for (int j=0; j<[iArr count]; j++) {
        NSString *formattedStr=[[nArr objectAtIndex:j] stringByReplacingOccurrencesOfString:@" ‚Äì " withString:@"-"];
        [nameArray addObject:formattedStr];
        [idArray addObject:[iArr objectAtIndex:j]];
        [imagesArray addObject:@"burger.png"];
        
    }
    
    sectionImagesArray = [[NSMutableArray alloc] initWithObjects:@"home_icon_bk.png",
                          @"restaurant_bk.png",
                          @"restore_icon.png",
//                          @"location_bk.png",
                          @"gray-01.png",
                          @"person_bk.png",
                          @"people_bk.png",
                          @"account_icon_bk.png",
                          //@"blog_bk.png",
                          //@"icon.png",
                          @"logout_bk.png",
                          @"", nil];
    
    /*Ashish code*/
    //    OfflineDb *object=[[OfflineDb alloc]init];
    //    NSString *parentId=@"1";
    //    NSArray *dataArray=[object fetch_parentWiseCategory:parentId];
    //NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
    //[dictObj setObject:dataArray forKey:@"Data"];
    
    //    nameArray=[dataArray valueForKey:@"category_name"];
    //    idArray=[dataArray valueForKey:@"category_id"];
    
    
    
    
    //    subTitleImgsArray=[[NSMutableArray alloc]initWithObjects:@"drawer_invoice.png",@"drawer_packages.png", nil];
    //
    //
    //    subTitleArray=[[NSArray alloc]initWithObjects:
    //                   @"Invoice",
    //                   @"Packages",nil];
    
    
    
    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
    if ([isLog isEqualToString:@"YES"]) {
        arrayForBool=[[NSMutableArray alloc]init];
        sectionTitleArray=[[NSMutableArray alloc]initWithObjects:
                           @"",
                           @"Home",
                           @"Menu",
                           @"Order History",
//                           @"Location",
                           @"Wishlist",
                           @"Profile",
                           @"Visit Website",
                           @"Contact Us",
                           //@"Blog",
                           //@"Help",
                           @"Sign Out",
                           @"",
                           nil];
        
    }else{
        arrayForBool=[[NSMutableArray alloc]init];
        sectionTitleArray=[[NSMutableArray alloc] initWithObjects:
                           @"",
                           @"Home",
                           @"Menu",
                           @"Order History",
//                           @"Location",
                           @"Wishlist",
                           @"Profile",
                           @"Visit Website",
                           @"Contact Us",
                           //@"Blog",
                           //@"Help",
                           @"Sign In",
                           @"",
                           nil];
        
    }
    
    NSString *selectedLocation=[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDLOCATION];
    
    if (!(selectedLocation.length>0) || [selectedLocation isEqualToString:@""] || selectedLocation==nil){
        NSLog(@"not to add");
    }else{
        [sectionTitleArray replaceObjectAtIndex:4 withObject:selectedLocation];
    }
    
    for (int i=0; i<[sectionTitleArray count]; i++)
    {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    
    _sideBarTableView.dataSource=self;
    _sideBarTableView.delegate=self;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int row;
    
    if (section ==2){
    
        NSString *noOfSub=[NSString stringWithFormat:@"%ld",[idArray count]];
        return row=[noOfSub intValue];
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"hello";
    CustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    
    /********** If the section supposed to be closed *******************/
    
    if(!manyCells)
    {
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.text=@"";
        
    }
    /********** If the section supposed to be Opened *******************/
    else{
        
        // cell.lbl_SubTitle.text=[NSString stringWithFormat:@"%@",[subTitleArray objectAtIndex:indexPath.row]];
        
        cell.lbl_SubTitle.text=[NSString stringWithFormat:@"%@",[nameArray objectAtIndex:indexPath.row]];
        cell.backgroundColor=[UIColor clearColor];
        
        //cell.subTitleImg.image=[UIImage imageNamed:[subTitleImgsArray objectAtIndex:indexPath.row]];
        cell.selectionStyle=UITableViewCellSelectionStyleNone ;
    }
    cell.textLabel.textColor=[UIColor blackColor];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [sectionTitleArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==2)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMINPRICE];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMAXPRICE];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MINPRICE];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MAXPRICE];
        	
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:BRANDVALUE];
        APP_DELEGATE.selectBarndArray=[[NSMutableArray alloc] init];
        [APP_DELEGATE.selectBarndArray addObjectsFromArray:brandIdArr];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISFILTER];
        
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
        NSString *str=[idArray objectAtIndex:indexPath.row];
        NSString *indStr=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        int ind=[indStr intValue];
        if (indexPath.section==2 && indexPath.row==ind)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[idArray objectAtIndex:indexPath.row] forKey:@"FETCHID"];
            [[NSUserDefaults standardUserDefaults] setObject:[nameArray objectAtIndex:indexPath.row] forKey:@"FETCHNAME"];
            
            ItemsViewController *it = [self.storyboard instantiateViewControllerWithIdentifier:@"ItemsViewController"];
            
            it.fetchedId=[idArray objectAtIndex:indexPath.row];
            it.fetchedName=[nameArray objectAtIndex:indexPath.row];
            //            it.comingFrom=@"MENU";
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:it];
            [navController setViewControllers: @[it] animated: YES];
            
             navController.navigationBar.barTintColor = TopBarColor;
            [navController.navigationBar setTitleTextAttributes:
             @{NSForegroundColorAttributeName:cartIconTextColor}];
            navController.navigationBar.translucent = NO;
            
            [self.revealViewController setFrontViewController:navController];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
            
            
            
            
            //            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //            CategoriesController * qlvc = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesController"];
            //            qlvc.fetchedParentId
            //            [self.navigationController pushViewController:qlvc animated:YES];
            
            if (ind ==1) {
                //                        SubCategoriesController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SubCategoriesController"];
                //                        rootViewController.fetchedParentId=[idArray objectAtIndex:indexPath.row];
                //                        rootViewController.fetchedParentName=[nameArray objectAtIndex:indexPath.row];
                //                        rootViewController.comingFrom=@"HOME";
                //                        rootViewController.hideFilter=@"NO";
                //                        rootViewController.fetchedStoreId=[staticStoreIdArr objectAtIndex:indexPath.row];
                //                        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
                //                        [navController setViewControllers: @[rootViewController] animated: YES];
                //                        [self.revealViewController setFrontViewController:navController];
                //                        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
                //
                
                
                
            }
            else{
                //                        CategoriesController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriesController"];
                //                        rootViewController.fetchedParentId=[idArray objectAtIndex:indexPath.row];
                //                        rootViewController.fetchedParentName=[nameArray objectAtIndex:indexPath.row];
                //                        rootViewController.comingFrom=@"HOME";
                //                        rootViewController.isMica=@"NO";
                //                        rootViewController.fetchedStoreId=[staticStoreIdArr objectAtIndex:indexPath.row];
                //                        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
                //                        [navController setViewControllers: @[rootViewController] animated: YES];
                //                        [self.revealViewController setFrontViewController:navController];
                //                        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
                
                
            }
        }
        /*
         else if (indexPath.section==2 && indexPath.row==1)
         {
         CategoriesController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesController"];
         rootViewController.fetchedParentId=[idArray objectAtIndex:indexPath.row];
         rootViewController.comingFrom=@"SIDEBAR";
         UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
         [navController setViewControllers: @[rootViewController] animated: YES];
         [self.revealViewController setFrontViewController:navController];
         [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
         
         }
         else if (indexPath.section==2 && indexPath.row==2)
         {
         CategoriesController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesController"];
         rootViewController.fetchedParentId=[idArray objectAtIndex:indexPath.row];
         rootViewController.comingFrom=@"SIDEBAR";
         UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
         [navController setViewControllers: @[rootViewController] animated: YES];
         [self.revealViewController setFrontViewController:navController];
         [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
         }
         else if (indexPath.section==2 && indexPath.row==3)
         {
         //            CategoriesController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesController"];
         //            rootViewController.fetchedParentId=[idArray objectAtIndex:indexPath.row];
         //            rootViewController.comingFrom=@"SIDEBAR";
         //            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
         //            [navController setViewControllers: @[rootViewController] animated: YES];
         //            [self.revealViewController setFrontViewController:navController];
         //            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
         }
         else if (indexPath.section==2 && indexPath.row==4)
         {
         //            CategoriesController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesController"];
         //            rootViewController.fetchedParentId=[idArray objectAtIndex:indexPath.row];
         //            rootViewController.comingFrom=@"SIDEBAR";
         //            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
         //            [navController setViewControllers: @[rootViewController] animated: YES];
         //            [self.revealViewController setFrontViewController:navController];
         //            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
         }
         else if (indexPath.section==2 && indexPath.row==5)
         {
         //            CategoriesController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesController"];
         //            rootViewController.fetchedParentId=[idArray objectAtIndex:indexPath.row];
         //            rootViewController.comingFrom=@"SIDEBAR";
         //            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
         //            [navController setViewControllers: @[rootViewController] animated: YES];
         //            [self.revealViewController setFrontViewController:navController];
         //            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
         }
         */
        
    }
    //    else if (indexPath.section==5)
    //    {
    //        ShippingRequestViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ShippingRequestViewController"];
    //        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    //        [navController setViewControllers: @[rootViewController] animated: YES];
    //        [self.revealViewController setFrontViewController:navController];
    //        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    //    }
    
    //    [_sideBarTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        return 40;
    }
    return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //    defaults= [NSUserDefaults standardUserDefaults];
    //    NSString *firstname =[defaults objectForKey:@"firstname"];
    //    NSString *lastname =[defaults objectForKey:@"lastname"];
    //
    //    firstname = [NSString stringWithFormat:@"%@%@",[[firstname substringToIndex:1] uppercaseString],[firstname substringFromIndex:1]];
    //    lastname = [NSString stringWithFormat:@"%@%@",[[lastname substringToIndex:1] uppercaseString],[lastname substringFromIndex:1]];
    //    NSLog(@"abc = %@",firstname);
    //    NSLog(@"abc = %@",lastname);
    //
    //    NSString *custname=@"Hello ";
    //    custname=[custname stringByAppendingString:@" "];
    //    custname=[custname stringByAppendingString:firstname];
    //    custname=[custname stringByAppendingString:@" "];
    //    custname=[custname stringByAppendingString:lastname];
    
    NSString *custname=@"Fruit Bucket";
    
    
    
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,40)];
    sectionView.tag=section;
    
    if (section==0)
    {
        sectionView.backgroundColor=TopBarColor;
        
        UILabel *lbl_custname=[[UILabel alloc]initWithFrame:CGRectMake(35, 0,_sideBarTableView.frame.size.width-35, 39)];
        lbl_custname.backgroundColor=[UIColor clearColor];
        lbl_custname.textColor=TopBarTextColor;
        lbl_custname.font=[UIFont systemFontOfSize:15];
        lbl_custname.text=[NSString stringWithFormat:@"%@", custname];
        [sectionView addSubview:lbl_custname];
    
    }else{
        sectionView.backgroundColor=[UIColor whiteColor];
        
        /*Ashish commented code*/
        UIImageView *logo =[[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+7,7.5,25,25)];
        logo.image=[UIImage imageNamed:[sectionImagesArray objectAtIndex:section-1]];
        [sectionView addSubview:logo];
        
        UILabel *viewLabel=[[UILabel alloc]initWithFrame:CGRectMake(35, 0,_sideBarTableView.frame.size.width-35, 39)];
        viewLabel.backgroundColor=[UIColor clearColor];
        viewLabel.textColor=[UIColor blackColor];
        viewLabel.font=[UIFont systemFontOfSize:15];
        viewLabel.text=[NSString stringWithFormat:@"   %@",[sectionTitleArray objectAtIndex:section]];
        [sectionView addSubview:viewLabel];
        
        
        
        UILabel *add_miunus_lbl;
        if (section==2)
        {
            add_miunus_lbl=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-80,0,10,50)];
            add_miunus_lbl.backgroundColor=[UIColor clearColor];
            add_miunus_lbl.textColor=[UIColor blackColor];
            add_miunus_lbl.font=[UIFont systemFontOfSize:20];
            add_miunus_lbl.text=[NSString stringWithFormat:@"+"];
            [sectionView addSubview:add_miunus_lbl];
            sectionView.backgroundColor=[UIColor whiteColor];
        }
        
        /********** Add a custom Separator with Section view *******************/
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 39, _sideBarTableView.frame.size.width, 1)];
        separatorLineView.backgroundColor = [UIColor clearColor];
        [sectionView addSubview:separatorLineView];
        
        /********** Add UITapGestureRecognizer to SectionView   **************/
        
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [sectionView addGestureRecognizer:headerTapped];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:headerTapped.view.tag];
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        
        if (collapsed == YES)
        {
            if (section==2)
            {
                add_miunus_lbl.hidden=YES;
                UILabel *miunus_lbl=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-80,0,10,50)];
                miunus_lbl.backgroundColor=[UIColor clearColor];
                miunus_lbl.textColor=[UIColor blackColor];
                miunus_lbl.font=[UIFont systemFontOfSize:20];
                miunus_lbl.text=[NSString stringWithFormat:@"-"];
                [sectionView addSubview:miunus_lbl];
                
                //                sectionView.backgroundColor=[UIColor colorWithRed:240.0/255.0f green:59.0/255.0f blue:66.0/255.0f alpha:1.0];
            }
        }
    }
    return  sectionView;
}

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMAXPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:MAXPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:BRANDVALUE];
    APP_DELEGATE.selectBarndArray=[[NSMutableArray alloc] init];
    [APP_DELEGATE.selectBarndArray addObjectsFromArray:brandIdArr];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISFILTER];
    
    NSLog(@"%ld",indexPath.section);
    switch (indexPath.section)
    {
        case 1:
        {
            ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
            [navController setViewControllers: @[rootViewController] animated: YES];
            navController.navigationBar.barTintColor = TopBarColor;
//            navController.navigationBar.tintColor = [UIColor whiteColor];
            [navController.navigationBar setTitleTextAttributes:
            @{NSForegroundColorAttributeName:cartIconTextColor}];
            navController.navigationBar.translucent = NO;


            [self.revealViewController setFrontViewController:navController];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
            
            break;
        }
        case 2:
        {
            //            CategoriesViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
            //            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
            //            [navController setViewControllers: @[rootViewController] animated: YES];
            //            [self.revealViewController setFrontViewController:navController];
            //            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
            
            break;
        }
        case 3:
        {
            if([[[NSUserDefaults standardUserDefaults] objectForKey:ISLOGIN] isEqualToString:@"YES"]){
                NSLog(@"History");
                [self loadStoryBoardWithIdentifier:@"HistoryVC"];
                
            }else{
                [self UIAlertViewControllerMethodTitle:@"" message:@"You need to Login First."];
            }
            
            /*
             NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
             if (!(isLog.length>0) || isLog==nil) {
             
             NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
             ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
             rootViewController.signinpopup=@"YES";
             UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
             [navController setViewControllers: @[rootViewController] animated: YES];
             [self.revealViewController setFrontViewController:navController];
             [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
             
             }
             else{
             
             NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
             HistoryController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"HistoryController"];
             //            rootViewController.getPosition=[indStr intValue];
             UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
             [navController setViewControllers: @[rootViewController] animated: YES];
             [self.revealViewController setFrontViewController:navController];
             [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
             }
             
             */
            break;
        }
//        case 4:
//        {
//            NSLog(@"Location");
//            NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
//            //            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShowLocationViewFirstTime"];
//
//            ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            rootViewController.comingFrom=LOCATION;
//            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
//            [navController setViewControllers: @[rootViewController] animated: YES];
//            navController.navigationBar.barTintColor = TopBarColor;
//            [navController.navigationBar setTitleTextAttributes:
//             @{NSForegroundColorAttributeName:cartIconTextColor}];
//            navController.navigationBar.translucent = NO;
//            [self.revealViewController setFrontViewController:navController];
//            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//            break;
//
//        }
        case 4:
        {
            
            NSLog(@"Location");
            NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
            //            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShowLocationViewFirstTime"];
            
            WishListViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"WishListViewController"];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
            [navController setViewControllers: @[rootViewController] animated: YES];
            navController.navigationBar.barTintColor = TopBarColor;
            [navController.navigationBar setTitleTextAttributes:
             @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
            navController.navigationBar.translucent = NO;
            [self.revealViewController setFrontViewController:navController];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
            break;
            
        }
        case 5:
        {
            
            NSString *isLogged=[[NSUserDefaults standardUserDefaults] objectForKey:ISLOGIN];
            
            if([isLogged isEqualToString:@"YES"]){
                [self loadStoryBoardWithIdentifier:@"ProfileUpdateVC"/*"UpdateAddressVC"*/];
                
            }else{
                [self loadStoryBoardWithIdentifier:@"LoginVC"];
                
            }
            
            break;
            /*
             NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
             if (isLog.length>0) {
             
             NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
             ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
             rootViewController.logOutWindow=@"YES";
             UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
             [navController setViewControllers: @[rootViewController] animated: YES];
             [self.revealViewController setFrontViewController:navController];
             [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
             
             }
             else{
             NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
             LoginController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
             UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
             [navController setViewControllers: @[rootViewController] animated: YES];
             [self.revealViewController setFrontViewController:navController];
             [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
             }
             [self viewWillAppear:YES];
             break;
             */
            
            
        }
        case 6:
        {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://demo.myavenue.in/"]];
//            [[NSUserDefaults standardUserDefaults]setObject:@"aboutUs" forKey:@"CMSURLToLoad"];
//            [self loadStoryBoardWithIdentifier:@"CMSVC"];
            break;
            /*
             NSLog(@"About Us");
             NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
             AboutusController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"AboutusController"];
             //            rootViewController.getPosition=[indStr intValue];
             UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
             [navController setViewControllers: @[rootViewController] animated: YES];
             [self.revealViewController setFrontViewController:navController];
             [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
             */
            
        }
//        case 8:
//        {
//            //            [self loadStoryBoardWithIdentifier:@"OrderInformationVC"];
//            [[NSUserDefaults standardUserDefaults]setObject:@"recipes" forKey:@"CMSURLToLoad"]; // TO Remove
//            [self loadStoryBoardWithIdentifier:@"CMSVC"];
//
//            break;
//            /*
//             NSLog(@"About Us");
//             NSString *indStr=[NSString stringWithFormat:@"%ld",indexPath.section];
//             AboutusController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"AboutusController"];
//             //            rootViewController.getPosition=[indStr intValue];
//             UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
//             [navController setViewControllers: @[rootViewController] animated: YES];
//             [self.revealViewController setFrontViewController:navController];
//             [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//             */
//
//        }
        case 7:
        {
            ContactUsViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
            //            rootViewController.getPosition=[indStr intValue];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
            [navController setViewControllers: @[rootViewController] animated: YES];
            navController.navigationBar.barTintColor = TopBarColor;
            [navController.navigationBar setTitleTextAttributes:
             @{NSForegroundColorAttributeName:cartIconTextColor}];
            navController.navigationBar.translucent = NO;
            [self.revealViewController setFrontViewController:navController];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];                   break;
            
            break;
        }
//        case 10:
//        {
//            [[NSUserDefaults standardUserDefaults]setObject:@"qualityControl" forKey:@"CMSURLToLoad"];
//            [self loadStoryBoardWithIdentifier:@"CMSVC"];
//
//            break;
//        }
            
//        case 9:
//        {
////            http://blog.fresh-pick.in/
////            http://blog.seafoodandmeatco.in/
//
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://blog.fresh-pick.in/"]];
//
//            //            NSLog(@"Logout");
//            //            UIAlertController * alert=   [UIAlertController
//            //                                          alertControllerWithTitle:@"Alert"
//            //                                          message:@"Do you want logout?"
//            //                                          preferredStyle:UIAlertControllerStyleAlert];
//            //
//            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
//            //                                                       handler:^(UIAlertAction * action)
//            //                                 {
//            //                                     //Do Some action here
//            //
//            //                                     NSString *loginStatus=@"No";
//            //                                     [defaults setObject:loginStatus forKey:@"loginStatus"];
//            //
//            //                                     ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            //                                     UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
//            //                                     [navController setViewControllers: @[rootViewController] animated: YES];
//            //                                     [self.revealViewController setFrontViewController:navController];
//            //                                     [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//            //
//            //                                 }];
//            //
//            //            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
//            //                                                           handler:^(UIAlertAction * action) {
//            //                                                               [alert dismissViewControllerAnimated:YES completion:nil];
//            //                                                           }];
//            //
//            //            [alert addAction:ok];
//            //            [alert addAction:cancel];
//            //
//            //            [self presentViewController:alert animated:YES completion:nil];
//
//            break;
//        }
//        case 10:
//        {
//            HelpController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"HelpController"];
//            rootViewController.comingFrom=@"Sidebar";
//            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
//            [navController setViewControllers: @[rootViewController] animated: YES];
//            navController.navigationBar.barTintColor = TopBarColor;
//            [navController.navigationBar setTitleTextAttributes:
//             @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
//            navController.navigationBar.translucent = NO;
//            [self.revealViewController setFrontViewController:navController];
//            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//
//
//            break;
//        }
        case 8:
        {
            NSLog(@"Sign In/Out");
            
            if([[[NSUserDefaults standardUserDefaults] objectForKey:ISLOGIN] isEqualToString:@"YES"])
            {
                
                ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                rootViewController.logOutWindow=@"YES";
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
                [navController setViewControllers: @[rootViewController] animated: YES];
                navController.navigationBar.barTintColor = TopBarColor;
                [navController.navigationBar setTitleTextAttributes:
                 @{NSForegroundColorAttributeName:cartIconTextColor}];
                navController.navigationBar.translucent = NO;
                [self.revealViewController setFrontViewController:navController];
                [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
                
                //                [self UIAlertViewControllerMethodForSignOutWithTitle:@"Alert" message:@"Do you want logout?"];
            }else{
                [self loadStoryBoardWithIdentifier:@"LoginVC"];
            }
            break;
        }
        default:
            
            break;
    }
    
    if (indexPath.section == 2)
    {
        
        subTitleImgsArray=[[NSMutableArray alloc]initWithObjects:@"img_feature.png",@"img_feature.png",@"img_feature.png",@"img_feature.png",@"img_feature.png",@"img_feature.png", nil];
        
    }
    
    if (indexPath.row == 0){
        
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[sectionTitleArray count]; i++)
        {
            if (indexPath.section==i)
            {
                if (indexPath.section==2)
                {
                    [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
                }
            }
        }
        [_sideBarTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:identifier];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    [navController setViewControllers: @[rootViewController] animated: YES];
    navController.navigationBar.barTintColor = TopBarColor;
    [navController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:cartIconTextColor}];
    navController.navigationBar.translucent = NO;
    [self.revealViewController setFrontViewController:navController];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    // [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

-(void)UIAlertViewControllerMethodForSignOutWithTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userEmail"];;
                           [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userPassword"];
                           [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISLOGIN];
                           
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
