//
//  thankYouViewController.h
//  TSMC
//
//  Created by user on 16/10/18.
//  Copyright © 2018 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface thankYouViewController : UIViewController
@property (nonatomic, strong) NSString *orderIDStr;

@property (strong, nonatomic) IBOutlet UILabel *titleBack;
@property (strong, nonatomic) IBOutlet UIButton *btn_back;
- (IBAction)backMethod:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_titleText;

@end
