//
//  SearchController.h
//  TSMC
//
//  Created by Rahul Lekurwale on 10/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
//#import "ItemsViewController.h"
#import "ProgressHUD.h"
#import "CustomTableViewCell.h"


@interface SearchController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    
    MBProgressHUD *HUD;
    UITextField *globalvar;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *img_BackIcon;

@property (weak, nonatomic) IBOutlet UIView *tableBackView;
@property (weak, nonatomic) IBOutlet UITextField *txt_search;
@property (weak, nonatomic) IBOutlet UIImageView *img_search;
@property (weak, nonatomic) IBOutlet UITableView *pageTableView;

@property (weak, nonatomic) IBOutlet UIButton *btn_cart;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (strong,nonatomic)UIPickerView *spicesPicker;

@property (weak, nonatomic) IBOutlet UILabel *lbl_badgeCount;
@property (weak, nonatomic) IBOutlet UILabel *titleBack;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleText;
@property (strong, nonatomic) IBOutlet UIButton *btn_filter;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *loadMoreHeight;
@property (strong, nonatomic) IBOutlet UIButton *btn_loadMore;
- (IBAction)loadMoreMethod:(id)sender;


- (IBAction)subQtyMethod:(id)sender;
- (IBAction)addQtyMethod:(id)sender;
- (IBAction)addCartMethod:(id)sender;
- (IBAction)filterMethod:(id)sender;

- (IBAction)cartMethod:(id)sender;
- (IBAction)backMethod:(id)sender;

- (IBAction)cutMethod:(id)sender;
- (IBAction)weightMethod:(id)sender;
- (IBAction)typeMethod:(id)sender;
- (IBAction)wishMethod:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *progressBackView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMessage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMax;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMin;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *progressBackViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *minHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *maxHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *barHeight;


@end
