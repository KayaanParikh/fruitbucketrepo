//
//  HelpController.m
//  TSMC
//
//  Created by Rahul Lekurwale on 15/09/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "HelpController.h"
#import "UIImageView+WebCache.h"
#import "TestimonialController.h"
#import "ViewController.h"
#import "AppConstant.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define iPadPro12 (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height == 2732)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)



@interface HelpController ()<UIGestureRecognizerDelegate>
{
    UISwipeGestureRecognizer *gestureRight,*gestureLeft;
    //int indexToShow;
    NSInteger currentIndex;
    NSArray *imgArr;
    NSUInteger indexToShow;
    
    NSMutableArray *screenshotArray;
}

@end

@implementation HelpController
@synthesize nameArray,imageArray,messageArray,comingFrom;

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    indexToShow=0;
    screenshotArray=[[NSMutableArray alloc] init];
//    [screenshotArray addObjectsFromArray:imageArray];
//    [self getBackgroundImage];
    _btn_skip.layer.cornerRadius=10.0f;
//    [_pageControl setTag:12];
//    _pageControl.numberOfPages=[screenshotArray count];
    
    
    
//    _img_Back.image=[UIImage imageNamed:[screenshotArray objectAtIndex:indexToShow]];

    
//    NSString *image=[NSString stringWithFormat:@"%@",[screenshotArray objectAtIndex:indexToShow]];
//    [_img_Back sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//    }];
    
    
    
//    gestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
//    [gestureRight setNumberOfTouchesRequired:1];
//
//    [gestureRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
//    gestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
//    [gestureLeft setNumberOfTouchesRequired:1];
//
//    [gestureLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
//
//    gestureRight.delegate=self;
//    gestureLeft.delegate=self;
//
//    _img_Back.userInteractionEnabled=YES;
//
//    [_img_Back addGestureRecognizer:gestureRight];
//    [_img_Back addGestureRecognizer:gestureLeft];
    
    
    [self skipMethod:nil];

}
-(void)getBackgroundImage{
    if (IS_IPHONE_5) {
//        _img_background.image=[UIImage imageNamed:@"splash_1136.png"];
        screenshotArray=[[NSMutableArray alloc] init];
        [screenshotArray addObject:@"01-home_640.png"];
        [screenshotArray addObject:@"02-product list_640.png"];
        [screenshotArray addObject:@"03- free shipping_640.png"];
        [screenshotArray addObject:@"04-cart_640.png"];
        [screenshotArray addObject:@"05-shipping address_640.png"];
        [screenshotArray addObject:@"06-delivery and payment_640.png"];



    }
    else if(IS_IPHONE_6){
        screenshotArray=[[NSMutableArray alloc] init];
        [screenshotArray addObject:@"01-home_750.png"];
        [screenshotArray addObject:@"02-product list_750.png"];
        [screenshotArray addObject:@"03- free shipping_750.png"];
        [screenshotArray addObject:@"04-cart_750.png"];
        [screenshotArray addObject:@"05-shipping address_750.png"];
        [screenshotArray addObject:@"06-delivery and payment_750.png"];
        
    }
    else if(IS_IPHONE_6P){
        screenshotArray=[[NSMutableArray alloc] init];
        [screenshotArray addObject:@"01-home_1080.png"];
        [screenshotArray addObject:@"02-product list_1080.png"];
        [screenshotArray addObject:@"03- free shipping_1080.png"];
        [screenshotArray addObject:@"04-cart_1080.png"];
        [screenshotArray addObject:@"05-shipping address_1080.png"];
        [screenshotArray addObject:@"06-delivery and payment_1080.png"];
        
    }
    else{
        screenshotArray=[[NSMutableArray alloc] init];
        [screenshotArray addObject:@"01-home_750.png"];
        [screenshotArray addObject:@"02-product list_750.png"];
        [screenshotArray addObject:@"03- free shipping_750.png"];
        [screenshotArray addObject:@"04-cart_750.png"];
        [screenshotArray addObject:@"05-shipping address_750.png"];
        [screenshotArray addObject:@"06-delivery and payment_750.png"];

    
    }
    
    
}

#pragma Mark SwipeGesturesMethods

- (void)swipeRight:(UISwipeGestureRecognizer *)gesture
{
    if (indexToShow > 0)
    {
        
        indexToShow--;
        // [self.segmentedControl3 setSelectedSegmentIndex:indexToShow animated:YES];
        
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:
        //        ^{
        
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_img_Back.layer addAnimation:transition forKey:@"transition" ];
        
        _pageControl.currentPage=indexToShow;
        
        if (indexToShow ==[screenshotArray count]-1) {
            [_btn_skip setTitle:@"DONE" forState:UIControlStateNormal];
        }else{
            
            [_btn_skip setTitle:@"SKIP" forState:UIControlStateNormal];
            
        }
        
        
        _img_Back.image=[UIImage imageNamed:[screenshotArray objectAtIndex:indexToShow]];
        
//        NSString *image=[NSString stringWithFormat:@"%@",[screenshotArray objectAtIndex:indexToShow]];
//        [_img_Back sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//            //            [HUD hide:YES];
//            
//            
//        }];
        //        }
        //        completion:nil];
        
    }
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)gesture
{
    if (indexToShow < screenshotArray.count - 1)
    {
        indexToShow++;
        
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:
        //         ^{
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_img_Back.layer addAnimation:transition forKey:@"transition" ];
        _pageControl.currentPage=indexToShow;
        
        
        if (indexToShow ==[screenshotArray count]-1) {
            [_btn_skip setTitle:@"DONE" forState:UIControlStateNormal];
        }else{
            
            [_btn_skip setTitle:@"SKIP" forState:UIControlStateNormal];
            
        }
        
        _img_Back.image=[UIImage imageNamed:[screenshotArray objectAtIndex:indexToShow]];

        
//        NSString *image=[NSString stringWithFormat:@"%@",[screenshotArray objectAtIndex:indexToShow]];
//        [_img_Back sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//            //            [HUD hide:YES];
//            
//            
//        }];
        //            } completion:nil];
    }
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)skipMethod:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:HELPSESSION];

    if ([comingFrom isEqualToString:@"Landing"]) {
        TestimonialController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TestimonialController"];
        vc.nameArray=nameArray;
        vc.imageArray=imageArray;
        vc.messageArray=messageArray;
        [self.navigationController pushViewController:vc animated:NO];

    }
    else{
        ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.navigationController pushViewController:vc animated:NO];

    
    }
    

}
@end
