//
//  CartController.m
//  EarlOfHindh
//
//  Created by Rahul Lekurwale on 18/02/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "CartController.h"
#import "SWRevealViewController.h"
#import "OfflineDb.h"
#import "CustomTableViewCell.h"
#import "AppDelegate.h"
#import "HelperClass.h"
#import "ViewController.h"
#import "AppConstant.h"
#import "ItemsViewController.h"
#import "OrderInformationViewController.h"
#import "WebservicesClass.h"
#import "LoginViewController.h"
#import "ItemDetailsController.h"
#import "SearchController.h"
#import "FilterController.h"
#import "ShippingAddressController.h"


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define iPadPro12 (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height == 2732)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


@interface CartController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate,UIActionSheetDelegate>
{
    NSString *grandT;
    
    //,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*cart_type_option_label
    NSMutableArray *cartIdArray,*cartCategoryIdArray,*cartNameArray,*cartImageUrlArray,*cartImageThumbArray,*cartImageSmallArray,*cartImageMainArray,*cartPriceArray,*cartQtyArray,*cartDescriptionArray,*cartShortDescArray,*cartFoodTypeArray,*cartTotalArray,*cartBrandArray,*cartWeightArray,*cartOriginArray,*cartOptionIdArray,*cartCutOptionIdArray,*cartCutOptionValueArray,*cartCutOptionPriceArray,*cartWeightOptionIdArray,*cartWeightOptionValueArray,*cartWeightOptionPriceArray,*cartTypeOptionIdArray,*cartTypeOptionValueArray,*cartTypeOptionPriceArray,*cartCutOptionLabelArray,*cartWeightOptionLabelArray,*cartTypeOptionLabelArray;
    
    NSMutableArray *cartAllCutLabelArray,*cartAllCutPriceArray,*cartAllCutValueArray,*cartAllWeightLabelArray,*cartAllWeightValueArray,*cartAllWeightPriceArray,*cartAllTypeLabelArray,*cartAllTypeValueArray,*cartAllTypePriceArray,*arrayTotal,*arrayRedeemPoints;
    NSMutableArray *cartQtyPriceArray;
//    NSMutableArray *cartIdArray,*cartCategoryIdArray,*cartSkuArray,*cartNameArray,*cartImageUrlArray,*cartPriceArray,*cartQtyArray,*cartTypeArray,*cartTotalArray,*cartQtyPriceArray,*cartMsg,*cartMsgId,*cartSpicesId,*cartSpiceLevel,*cartLevelIds,*cartSelectedLevelId;
    float sum;
    UIView *popupView,*quntityView,*popupTitleBackView;
    UILabel *lbl_popupTitle,*lbl_size,*lbl_sizeValue,*lbl_qty,*lbl_popupTitleBack,*lbl_spices,*lbl_msg;
    UIButton *btn_Minus,*btn_Plus,*btn_Cancel,*btn_Remove,*btn_Add,*btn_spices,*btnRedeem;
    UITextField *txt_Qty,*txt_msg;
    UIImageView *spicesBackImage;
    int quntity;
    NSString *selectedSpiceIndex;
    NSMutableArray *mirchImagesArr,*mirchIndexArr;
    NSString *popId,*popName,*popQty,*popPrice;
    long currIndex;
    
    
    NSMutableArray *pickerWeightLabelArr,*pickerWeightValueArr,*pickerWeightPriceArr;
    NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
    NSMutableArray *pickerTypeLabelArr,*pickerTypeValueArr,*pickerTypePriceArr;
    NSString *selectedLabel,*selectedPrice,*selectedValue;
    NSString *selectedCutLabelIndex,*selectedCutValueIndex,*selectedCutPriceIndex;
    NSString *selectedWeightLabelIndex,*selectedWeightValueIndex,*selectedWeightPriceIndex;
    NSString *selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    NSString *strRedeemValue;
    NSMutableArray *arrayItem;
    NSString *prevCartId,*prevCartName,*prevCartQty,*prevCartCutLabel,*prevCartWeightLabel,*prevCartTypeLabel;

    
    NSMutableArray *prodIdKeyArr,*prodIdValArr,*prodQtyKeyArr,*prodQtyValArr,*prod1Idkey,*prod1IdVal,*prod1ValKey,*Prod1Value,*prod2Idkey,*prod2IdVal,*prod2ValKey,*Prod2Value,*prod3Idkey,*prod3IdVal,*prod3ValKey,*Prod3Value;
    NSString *selectedCouponCode;
}

@end

@implementation CartController
@synthesize spicesPicker;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *border = [UIView new];
    border.backgroundColor = ButtonBlueColor;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(0, 0, self.cartBottomView.frame.size.width, 2.0);
    [self.cartBottomView addSubview:border];
    
    _cartTableView.backgroundColor = [UIColor whiteColor];
    _txt_couponCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Coupon Code" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    sum=0;
    quntity=0;
    
    //,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*
    cartIdArray=[[NSMutableArray alloc] init];
    cartCategoryIdArray=[[NSMutableArray alloc] init];
    cartNameArray=[[NSMutableArray alloc] init];
    cartImageUrlArray=[[NSMutableArray alloc] init];
    cartImageThumbArray=[[NSMutableArray alloc] init];
    cartImageSmallArray=[[NSMutableArray alloc] init];
    cartImageMainArray=[[NSMutableArray alloc] init];
    cartPriceArray=[[NSMutableArray alloc] init];
    cartTotalArray=[[NSMutableArray alloc] init];
    cartQtyArray=[[NSMutableArray alloc] init];
    
    cartDescriptionArray=[[NSMutableArray alloc] init];
    cartShortDescArray=[[NSMutableArray alloc] init];
    cartFoodTypeArray=[[NSMutableArray alloc] init];
    cartTotalArray=[[NSMutableArray alloc] init];
    cartBrandArray=[[NSMutableArray alloc] init];
    cartWeightArray=[[NSMutableArray alloc] init];
    
    cartOriginArray=[[NSMutableArray alloc] init];
    cartCutOptionIdArray=[[NSMutableArray alloc] init];
    cartOptionIdArray=[[NSMutableArray alloc] init];
    cartCutOptionValueArray=[[NSMutableArray alloc] init];
    cartCutOptionPriceArray=[[NSMutableArray alloc] init];
    cartWeightOptionIdArray=[[NSMutableArray alloc] init];

    
    cartWeightOptionValueArray=[[NSMutableArray alloc] init];
    cartWeightOptionPriceArray=[[NSMutableArray alloc] init];
    cartTypeOptionIdArray=[[NSMutableArray alloc] init];
    cartTypeOptionValueArray=[[NSMutableArray alloc] init];
    cartTypeOptionPriceArray=[[NSMutableArray alloc] init];
    cartCutOptionLabelArray=[[NSMutableArray alloc] init];

    cartWeightOptionLabelArray=[[NSMutableArray alloc] init];
    cartTypeOptionLabelArray=[[NSMutableArray alloc] init];

    
    
    cartAllCutLabelArray=[[NSMutableArray alloc] init];
    cartAllCutPriceArray=[[NSMutableArray alloc] init];
    cartAllCutValueArray=[[NSMutableArray alloc] init];
    cartAllWeightLabelArray=[[NSMutableArray alloc] init];
    cartAllWeightValueArray=[[NSMutableArray alloc] init];
    cartAllWeightPriceArray=[[NSMutableArray alloc] init];
    
    cartAllTypeLabelArray=[[NSMutableArray alloc] init];
    cartAllTypeValueArray=[[NSMutableArray alloc] init];
    cartAllTypePriceArray=[[NSMutableArray alloc] init];
    
    pickerCutLabelArr=[[NSMutableArray alloc] init];
    pickerCutPriceArr=[[NSMutableArray alloc] init];
    pickerCutValueArr=[[NSMutableArray alloc] init];
    pickerTypeLabelArr=[[NSMutableArray alloc] init];
    pickerTypePriceArr=[[NSMutableArray alloc] init];
    pickerTypeValueArr=[[NSMutableArray alloc] init];
    pickerWeightLabelArr=[[NSMutableArray alloc] init];
    pickerWeightPriceArr=[[NSMutableArray alloc] init];
    pickerWeightValueArr=[[NSMutableArray alloc] init];
    
    prodIdKeyArr=[[NSMutableArray alloc] init];
    prodIdValArr=[[NSMutableArray alloc] init];
    prodQtyKeyArr=[[NSMutableArray alloc] init];
    prodQtyValArr=[[NSMutableArray alloc] init];
    prod1Idkey=[[NSMutableArray alloc] init];
    prod1IdVal=[[NSMutableArray alloc] init];
    prod1ValKey=[[NSMutableArray alloc] init];
    Prod1Value=[[NSMutableArray alloc] init];
    prod2Idkey=[[NSMutableArray alloc] init];
    prod2IdVal=[[NSMutableArray alloc] init];
    prod2ValKey=[[NSMutableArray alloc] init];
    Prod2Value=[[NSMutableArray alloc] init];
    prod3Idkey=[[NSMutableArray alloc] init];
    prod3IdVal=[[NSMutableArray alloc] init];
    prod3ValKey=[[NSMutableArray alloc] init];
    Prod3Value=[[NSMutableArray alloc] init];
    arrayItem = [[NSMutableArray alloc]init];
    
//    _titleBack.backgroundColor=TopBarColor;
//    _lbl_titleText.textColor=TopBarTextColor;
//    _lbl_titleText.font=TopBarFont;
    self.title = CartTitleText;
    _shieldView.hidden=YES;
    
    
    _lbl_subTotHeight.constant=0;
    _lbl_subTotal.hidden=YES;
    _lbl_cupNameheight.constant=0;
    _lbl_couponName.hidden=YES;
    _lbl_subTotValHeight.constant=0;
    _lbl_subTotalValue.hidden=YES;
    _lbl_copValHeight.constant=0;
    _lbl_couponVal.hidden=YES;

//    mirchImagesArr=[[NSMutableArray alloc] initWithObjects:@"1.png",@"2.png",@"3.png", nil];
//    mirchIndexArr=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3", nil];

    
    [self getOfflineCartData];
    
    
//    if (IS_IPAD) {
//        [self editCartLayoutForIpad];
//
//    }
//    else{
//        [self editCartLayout];
//
//    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getOfflineCartData
{

    OfflineDb *object=[[OfflineDb alloc]init];
    NSArray *dataArray=[object fetch_CartDetails];
    NSLog(@"%@",dataArray);
    cartPriceArray=[[NSMutableArray alloc] init];

    //,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*qnty_cnt,*,*,*
    cartIdArray=[dataArray valueForKey:@"cart_id"];
    cartCategoryIdArray=[dataArray valueForKey:@"cart_category_id"];
    cartNameArray=[dataArray valueForKey:@"cart_name"];
    cartImageUrlArray=[dataArray valueForKey:@"cart_image_url"];
    cartImageThumbArray=[dataArray valueForKey:@"cart_image_thumb"];
    cartImageSmallArray=[dataArray valueForKey:@"cart_image_small"];
    cartImageMainArray=[dataArray valueForKey:@"cart_image_main"];
    NSArray *crPriArr=[dataArray valueForKey:@"cart_price"];
//    NSArray *totalArr=[dataArray valueForKey:@"cart_id"];
    cartQtyArray=[dataArray valueForKey:@"cart_qty"];
    
    cartDescriptionArray=[dataArray valueForKey:@"cart_description"];
    cartShortDescArray=[dataArray valueForKey:@"cart_short_desc"];
    cartFoodTypeArray=[dataArray valueForKey:@"cart_food_type"];
    NSArray *totalArr=[dataArray valueForKey:@"cart_total"];
    cartBrandArray=[dataArray valueForKey:@"cart_brand"];
    cartWeightArray=[dataArray valueForKey:@"cart_weight"];
    
    cartOriginArray=[dataArray valueForKey:@"cart_origin"];
    cartCutOptionIdArray=[dataArray valueForKey:@"cart_cut_option_id"];
    cartOptionIdArray=[dataArray valueForKey:@"cart_option_id"];
    cartCutOptionValueArray=[dataArray valueForKey:@"cart_cut_option_value"];
    cartCutOptionPriceArray=[dataArray valueForKey:@"cart_cut_option_price"];
    cartWeightOptionIdArray=[dataArray valueForKey:@"cart_weight_option_id"];
    
    
    cartWeightOptionValueArray=[dataArray valueForKey:@"cart_weight_option_value"];
    cartWeightOptionPriceArray=[dataArray valueForKey:@"cart_weight_option_price"];
    cartTypeOptionIdArray=[dataArray valueForKey:@"cart_type_option_id"];
    cartTypeOptionValueArray=[dataArray valueForKey:@"cart_type_option_value"];
    cartTypeOptionPriceArray=[dataArray valueForKey:@"cart_type_option_price"];
    cartCutOptionLabelArray=[dataArray valueForKey:@"cart_cut_option_label"];
    
    cartWeightOptionLabelArray=[dataArray valueForKey:@"cart_weight_option_label"];
    cartTypeOptionLabelArray=[dataArray valueForKey:@"cart_type_option_label"];
    cartAllCutValueArray=[dataArray valueForKey:@"cm_all_cut_values"];
    cartAllCutLabelArray=[dataArray valueForKey:@"cm_all_cut_labels"];
    cartAllCutPriceArray=[dataArray valueForKey:@"cm_all_cut_prices"];
    cartAllWeightValueArray=[dataArray valueForKey:@"cm_all_weight_values"];
    cartAllWeightLabelArray=[dataArray valueForKey:@"cm_all_weight_labels"];
    cartAllWeightPriceArray=[dataArray valueForKey:@"cm_all_weight_prices"];
    cartAllTypeValueArray=[dataArray valueForKey:@"cm_all_type_values"];
    cartAllTypeLabelArray=[dataArray valueForKey:@"cm_all_type_labels"];
    cartAllTypePriceArray=[dataArray valueForKey:@"cm_all_type_prices"];



    [cartPriceArray addObjectsFromArray:crPriArr];
    
    if ([dataArray count]==0)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:@"Please order something first."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                       ViewController * qlvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                       [self.navigationController pushViewController:qlvc animated:YES];
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    cartQtyPriceArray=[[NSMutableArray alloc] init];
    cartTotalArray=[[NSMutableArray alloc] init];
    sum=0;
    for (int i=0; i<[cartQtyArray count]; i++) {
        sum=sum+[[totalArr objectAtIndex:i] floatValue];
        
        NSString *removedDollarStr=[[cartPriceArray objectAtIndex:i] stringByReplacingOccurrencesOfString:@"₹ " withString:@""];
        NSString *decimalStr=[NSString stringWithFormat:@"₹ %.02f",[removedDollarStr floatValue]];
        
        NSString *str=[NSString stringWithFormat:@"%@ x %@",decimalStr,[cartQtyArray objectAtIndex:i]];
        [cartQtyPriceArray addObject:str];
        
        ///Converting into two decimal
        NSString *totWithTwoDecimal=[NSString stringWithFormat:@"%.02f",[[totalArr objectAtIndex:i] floatValue]];
        [cartTotalArray addObject:totWithTwoDecimal];

        
    }
    self.lbl_total.text=[NSString stringWithFormat:@"%.02f",sum];
    
    //Check minimum order value for selected area
    
    minimumValueOfSelectedLocation=[[[NSUserDefaults standardUserDefaults]objectForKey:SELECTEDLOCATIONPRICE] intValue];
    totalValue=[[NSString stringWithFormat:@"%.02f",sum] intValue];
    
    if(totalValue>=minimumValueOfSelectedLocation)
    {
        _min_Order_Suggestion_Lbl.hidden=YES;
    }
    else
    {
        _min_Order_Suggestion_Lbl.text=[NSString stringWithFormat:@" Minimum order for selected area is ₹ %.2d",minimumValueOfSelectedLocation];
        _min_Order_Suggestion_Lbl.hidden=NO;
    }

    
  //  sum=0;
    
    _lbl_noOfItems.text=[NSString stringWithFormat:@"%ld items",[dataArray count]];
    
    
    prodIdKeyArr=[[NSMutableArray alloc] init];
    prodIdValArr=[[NSMutableArray alloc] init];
    prodQtyKeyArr=[[NSMutableArray alloc] init];
    prodQtyValArr=[[NSMutableArray alloc] init];
    prod1Idkey=[[NSMutableArray alloc] init];
    prod1IdVal=[[NSMutableArray alloc] init];
    prod1ValKey=[[NSMutableArray alloc] init];
    Prod1Value=[[NSMutableArray alloc] init];
    
    prod2Idkey=[[NSMutableArray alloc] init];
    prod2IdVal=[[NSMutableArray alloc] init];
    prod2ValKey=[[NSMutableArray alloc] init];
    Prod2Value=[[NSMutableArray alloc] init];
    
    prod3Idkey=[[NSMutableArray alloc] init];
    prod3IdVal=[[NSMutableArray alloc] init];
    prod3ValKey=[[NSMutableArray alloc] init];
    Prod3Value=[[NSMutableArray alloc] init];

    
    for (int i=0; i<[cartIdArray count]; i++) {
        

        
        
        [prodIdKeyArr addObject:[NSString stringWithFormat:@"products[%d][id]",i]];
        [prodIdValArr addObject:[cartIdArray objectAtIndex:i]];
        
        [prodQtyKeyArr addObject:[NSString stringWithFormat:@"products[%d][qty]",i]];
        [prodQtyValArr addObject:[cartQtyArray objectAtIndex:i]];
        
        
        [prod1Idkey addObject:[NSString stringWithFormat:@"products[%d][option_id]",i]];
        [prod1IdVal addObject:[cartCutOptionIdArray objectAtIndex:i]];
        
        [prod1ValKey addObject:[NSString stringWithFormat:@"products[%d][option_value]",i]];
        [Prod1Value addObject:[cartCutOptionValueArray objectAtIndex:i]];
        
        [prod2Idkey addObject:[NSString stringWithFormat:@"products[%d][option_id1]",i]];
        [prod2IdVal addObject:[cartWeightOptionIdArray objectAtIndex:i]];
        
        [prod2ValKey addObject:[NSString stringWithFormat:@"products[%d][option_value1]",i]];
        [Prod2Value addObject:[cartWeightOptionValueArray objectAtIndex:i]];
        
        [prod3Idkey addObject:[NSString stringWithFormat:@"products[%d][option_id2]",i]];
        [prod3IdVal addObject:[cartTypeOptionIdArray objectAtIndex:i]];
        
        [prod3ValKey addObject:[NSString stringWithFormat:@"products[%d][option_value2]",i]];
        [Prod3Value addObject:[cartTypeOptionValueArray objectAtIndex:i]];
        
    }

    
    //Customise product Note
    
    /*for(int i=0;i<cartFoodTypeArray.count;i++)
    {
        if([[cartFoodTypeArray objectAtIndex:i] isEqualToString:@"42"] && ![[[NSUserDefaults standardUserDefaults] objectForKey:@"ShowCustomiseNote"] isEqualToString:@"NO"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"ShowCustomiseNote"];
            [self UIAlertViewControllerMethodTitle:@"" message:@"Note: Kindly note that you have chosen one or more items in your cart that may not be available in your slot selected. Our customer care team shall get back to you in 60mins would there be any availability concerns with respect to the items ordered by you."];
        }
    }*/
    
    [self.cartTableView reloadData];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (IBAction)menuMethod:(id)sender {
//    
//    [self.revealViewController revealToggle:nil];
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//
//}


-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           HUD.hidden=YES;
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               HUD.hidden=YES;
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    // [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}



//- (IBAction)minusMethod:(id)sender {
//    if (quntity > 1) {
//        quntity--;
//        txt_Qty.text=[NSString stringWithFormat:@"%d",quntity];
//        
//    }
//}
//- (IBAction)plusMethod:(id)sender {
//    if (quntity < 99) {
//        quntity++;
//        txt_Qty.text=[NSString stringWithFormat:@"%d",quntity];
//        
//    }
//} 
-(void)updateMethod:(id)sender{
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    CustomTableViewCell *cell =(CustomTableViewCell *)[self.cartTableView cellForRowAtIndexPath:indexPath];
    NSString *currIndQty=cell.lbl_popQty.text;
    
    NSString *name=[cartNameArray objectAtIndex:currIndex];
    //    NSString *sk=[skuArray objectAtIndex:[sender tag]];
    NSString *idStr=[cartIdArray objectAtIndex:currIndex];
    NSString *catId=[cartCategoryIdArray objectAtIndex:currIndex];;
    NSString *img=[cartImageUrlArray objectAtIndex:currIndex];;
    NSString *imgSmall=[cartImageSmallArray objectAtIndex:currIndex];;
    NSString *imgMain=[cartImageMainArray objectAtIndex:currIndex];;
    NSString *imgThumb=[cartImageThumbArray objectAtIndex:currIndex];;
    NSString *desc=[cartDescriptionArray objectAtIndex:currIndex];;
    NSString *shortDesc=[cartShortDescArray objectAtIndex:currIndex];;
    NSString *weight=[cartWeightArray objectAtIndex:currIndex];;
    NSString *price=[cartPriceArray objectAtIndex:currIndex];;
    NSString *type=[cartFoodTypeArray objectAtIndex:currIndex];;

    //    NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
    //    long row = [sender tag];
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    //    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
//    NSString *currIndQty;
    if ([cell.lbl_popQty.text isEqualToString:@"0"]) {
        currIndQty=cell.lbl_popQty.text;
    }
    else{
        currIndQty=cell.lbl_popQty.text;

//_lbl_Qty
    }
    
    NSString *opid=@"0";
    NSString *brand=[cartBrandArray objectAtIndex:currIndex];;
    NSString *cutopid=[cartCutOptionIdArray objectAtIndex:currIndex];
    NSString *cutopval=selectedCutValueIndex;
    NSString *cutoppri=selectedCutPriceIndex;
    NSString *weiopid=[cartWeightOptionIdArray objectAtIndex:currIndex];;
    NSString *weiopval=selectedWeightValueIndex;
    NSString *weioppri=selectedWeightPriceIndex;
    NSString *typopid=[cartTypeOptionIdArray objectAtIndex:currIndex];;
    NSString *typopval=selectedTypeValueIndex;
    NSString *typoppri=selectedTypePriceIndex;
    
    NSString *cutoplabel=selectedCutLabelIndex;
    NSString *weioplabel=selectedWeightLabelIndex;
    NSString *typoplabel=selectedTypeLabelIndex;
    
    //For storing All data in database
    NSString *allcutLabels=[cartAllCutLabelArray objectAtIndex:currIndex];
    NSString *allcutValues=[cartAllCutValueArray objectAtIndex:currIndex];
    NSString *allcutPrices=[cartAllCutPriceArray objectAtIndex:currIndex];
    
    NSString *allweightLabels=[cartAllWeightLabelArray objectAtIndex:currIndex];
    NSString *allweightValues=[cartAllWeightValueArray objectAtIndex:currIndex];
    NSString *allweightPrices=[cartAllWeightPriceArray objectAtIndex:currIndex];
    
    NSString *alltypeLabels=[cartAllTypeLabelArray objectAtIndex:currIndex];
    NSString *alltypeValues=[cartAllTypeValueArray objectAtIndex:currIndex];
    NSString *alltypePrices=[cartAllTypePriceArray objectAtIndex:currIndex];
    
    
    NSString *qty=currIndQty;
    
    if ([qty isEqualToString:@"0"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:@"Please select Quantity."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        
        float total=[qty floatValue]*[price floatValue];
        NSString *tot=[NSString stringWithFormat:@"%.2f",total];
        
        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];

        ///First delete previous entry
        
        [testDB_Obj Delete_Cart_Data:prevCartId AndCutLab:prevCartCutLabel AndWeightlab:prevCartWeightLabel AndTypeLab:prevCartTypeLabel];
        
        
        ///Add in cart
        
        NSMutableArray *cartDbArray=[[NSMutableArray alloc]init];
        testDB_Obj.cart_id=idStr;
        testDB_Obj.cart_category_id=catId;
        testDB_Obj.cart_name=name;
        testDB_Obj.cart_image_url=img;
        testDB_Obj.cart_image_thumb=imgThumb;
        testDB_Obj.cart_image_small=imgSmall;
        testDB_Obj.cart_image_main=imgMain;
        testDB_Obj.cart_price=price;
        testDB_Obj.cart_qty=qty;
        testDB_Obj.cart_description=desc;
        testDB_Obj.cart_short_desc=shortDesc;
        testDB_Obj.cart_food_type=type;
        testDB_Obj.cart_total=tot;
        testDB_Obj.cart_origin=@"";
        
        testDB_Obj.cart_brand=brand;
        testDB_Obj.cart_weight=weight;
        testDB_Obj.cart_option_id=@"";
        testDB_Obj.cart_cut_option_id=cutopid;
        testDB_Obj.cart_cut_option_value=cutopval;
        testDB_Obj.cart_cut_option_price=cutoppri;
        testDB_Obj.cart_weight_option_id=weiopid;
        testDB_Obj.cart_weight_option_value=weiopval;
        testDB_Obj.cart_weight_option_price=weioppri;
        testDB_Obj.cart_type_option_id=typopid;
        testDB_Obj.cart_type_option_value=typopval;
        testDB_Obj.cart_type_option_price=typoppri;
        testDB_Obj.cart_cut_option_label=cutoplabel;
        testDB_Obj.cart_weight_option_label=weioplabel;
        testDB_Obj.cart_type_option_label=typoplabel;
        
        //For cart master
        testDB_Obj.cm_cart_id=idStr;
        testDB_Obj.cm_category_id=catId;
        testDB_Obj.cm_all_cut_values=allcutValues;
        testDB_Obj.cm_all_cut_labels=allcutLabels;
        testDB_Obj.cm_all_cut_prices=allcutPrices;
        testDB_Obj.cm_all_weight_values=allweightValues;
        testDB_Obj.cm_all_weight_labels=allweightLabels;
        testDB_Obj.cm_all_weight_prices=allweightPrices;
        testDB_Obj.cm_all_type_values=alltypeValues;
        testDB_Obj.cm_all_type_labels=alltypeLabels;
        testDB_Obj.cm_all_type_prices=alltypePrices;
        
        [cartDbArray addObject:testDB_Obj];
        [testDB_Obj checkCartEntry:cartDbArray];
        
        [ProgressHUD showSuccess:@"Updated successfully" Interaction:NO];
        
        _shieldView.hidden=YES;
        [self getOfflineCartData];
     }
    
    
    
}
/*
-(void)removeMethod:(id)sender{
    NSLog(@"%ld",currIndex);
    OfflineDb *object=[[OfflineDb alloc]init];
    
    NSString *message=[NSString stringWithFormat:@"Are you sure you want to remove this item from order?\n\nItem :     %@",popName];
    NSLog(@"%@",message);
    
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Remove Item"
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       if ([object Delete_Cart_Data:popId]) {

                                                       [self getOfflineCartData];
                                                       shieldView.hidden=YES;
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }
                                                   }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
        [self presentViewController:alert animated:YES completion:nil];
        
        
    


}

- (IBAction)updateMethod:(id)sender {
}
-(void)cancelMethod:(id)sender{
    shieldView.hidden=YES;

    
}
*/

#pragma Mark UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [cartIdArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
//    currIndex=indexPath.row;
//
//    selectedSpiceIndex=@"0";
    
//    [cell.btn_popPlus addTarget:self action:@selector(addMethod:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.btn_popMinus addTarget:self action:@selector(subMethod:) forControlEvents:UIControlEventTouchUpInside];
    
        popQty=[cartQtyArray objectAtIndex:indexPath.row];
        
    
//    cell.lbl_cartItemName.textColor=CartLabelColor;
//    cell.lbl_cartItemName.font=CartLabelFont;
//    
//    cell.lbl_cartQtyPrice.textColor=CartLabelColor;
//    cell.lbl_cartQtyPrice.font=CartLabelFont;
//
//    cell.lbl_cartSubtotal.textColor=CartLabelColor;
//    cell.lbl_cartSubtotal.font=CartLabelFont;

//    cell.lbl_cartItemName.text=[cartNameArray objectAtIndex:indexPath.row];
//    cell.lbl_cartQtyPrice.text=[cartQtyPriceArray objectAtIndex:indexPath.row];
//    cell.lbl_cartSubtotal.text=[cartTotalArray objectAtIndex:indexPath.row];

     [cell.viewPlusMinus.layer setCornerRadius:15.0f];
     cell.viewPlusMinus.layer.borderColor = [UIColor colorWithRed:189.0/255.0f green:189.0/255.0f blue:189.0/255.0f alpha:1.0].CGColor;
     cell.viewPlusMinus.layer.borderWidth = 1.0f;
     [cell.viewPlusMinus.layer setMasksToBounds:YES];
     
     cell.lbl_popQty.tag = indexPath.row;
     cell.btn_popPlus.tag = indexPath.row;
     cell.btn_popMinus.tag = indexPath.row;
    
    cell.lbl_popQty.text = popQty;
    
    
    @try {
        cell.cart_lbl_name.text=[cartNameArray objectAtIndex:indexPath.row];
        if([[cartBrandArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
        {
            cell.brandHeight.constant=0;
            cell.cart_lbl_brand.text=@"";
        }
        else
        {
            cell.cart_lbl_brand.text=[cartBrandArray objectAtIndex:indexPath.row];
        }
        
        cell.cart_lbl_weight.text=[NSString stringWithFormat:@"Weight %.2f gm",[[cartWeightArray objectAtIndex:indexPath.row] floatValue]];
        cell.btn_add.tag=indexPath.row;
        cell.btn_sub.tag=indexPath.row;
        cell.btn_addCart.tag=indexPath.row;
        cell.btn_weight.tag=indexPath.row;
        cell.btn_cut.tag=indexPath.row;
        cell.btn_type.tag=indexPath.row;
        
//        cell.btn_cutHeight.constant=0;
//        cell.lbl_cutHeight.constant=0;
//        cell.btn_weightHeight.constant=0;
//        cell.lbl_weightHeight.constant=0;
//        cell.btn_typeHeight.constant=0;
//        cell.lbl_typeHeight.constant=0;
//        cell.btn_weight.hidden=YES;
//        cell.lbl_weight.hidden=YES;
//        cell.btn_cut.hidden=YES;
//        cell.lbl_cut.hidden=YES;
//        cell.btn_type.hidden=YES;
//        cell.lbl_type.hidden=YES;
        
        
        
        if ([[cartWeightArray objectAtIndex:indexPath.row] isEqualToString:@"0.0000"] || [[cartWeightArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            cell.cartWeightHeight.constant=0;
        }
        else{
            cell.cartWeightHeight.constant=15;
            
        }

//        if (dropLabelArr.count>0) {
//            if ([[dropLabelArr objectAtIndex:indexPath.row] count]>0) {
//                int space=10;
//                for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++) {
                    if ([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"] || ([cartCutOptionIdArray objectAtIndex:indexPath.row]==nil)) {
                        
                        cell.btn_cutHeight.active=YES;
                        cell.lbl_cutHeight.active=YES;
                        cell.btn_cutHeight.constant=0;
                        cell.lbl_cutHeight.constant=0;
                        cell.cart_btn_cut.hidden=YES;
                        cell.cart_lbl_cut.hidden=YES;


                    }
                    else
                    {
                        cell.btn_cutHeight.constant=21;
                        cell.lbl_cutHeight.constant=21;
                        cell.cart_btn_cut.hidden=NO;
                        cell.cart_lbl_cut.hidden=NO;

                        if([[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"Pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"PCS"])
                        {
                            cell.cart_lbl_cut.text=@"Pieces:";
                        }
                        else
                        {
                            cell.cart_lbl_cut.text=@"Cut:";
                        }
                        
                        
                        [cell.cart_btn_cut setTitle:[cartCutOptionLabelArray objectAtIndex:indexPath.row]  forState:UIControlStateNormal];
                        //[cell.cart_btn_cut sizeToFit];

                        
                    }
                    if ([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"] || ([cartWeightOptionIdArray objectAtIndex:indexPath.row]==nil)) {
                
                        cell.btn_weightHeight.constant=0;
                        cell.lbl_weightHeight.constant=0;
                        cell.cart_btn_weightRange.hidden=YES;
                        cell.cart_lbl_weightRange.hidden=YES;

                        
                        //                            cell.btn_weight.hidden=YES;
                        //                            cell.lbl_weight.hidden=YES;

                    }
                    else{
                        cell.btn_weightHeight.constant=21;
                        cell.lbl_weightHeight.constant=21;
                        cell.cart_btn_weightRange.hidden=NO;
                        cell.cart_lbl_weightRange.hidden=NO;

                        //                        cell.btn_weight.hidden=NO;
                        //                        cell.lbl_weight.hidden=NO;
                        
                        [cell.cart_btn_weightRange setTitle:[cartWeightOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];

                        
                    }
                    
                    if ([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"] || ([cartTypeOptionIdArray objectAtIndex:indexPath.row]==nil)) {
                        cell.btn_typeHeight.constant=0;
                        cell.lbl_typeHeight.constant=0;
                        cell.cart_btn_type.hidden=YES;
                        cell.cart_lbl_type.hidden=YES;

                        //                            cell.btn_type.hidden=YES;
                        //                            cell.lbl_type.hidden=YES;
                        
                        
                    }
                    else{
                       
                        
                        cell.btn_typeHeight.constant=21;
                        cell.lbl_typeHeight.constant=21;
                        cell.cart_btn_type.hidden=NO;
                        cell.cart_lbl_type.hidden=NO;

                        //                        cell.btn_type.hidden=NO;
                        //                        cell.lbl_type.hidden=NO;
                        [cell.cart_btn_type setTitle:[cartTypeOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];

                    }
        
        

        cell.backViewConstraint.constant=cell.nameHeight.constant+cell.brandHeight.constant+cell.cartWeightHeight.constant+cell.lbl_cutHeight.constant+cell.lbl_weightHeight.constant+cell.lbl_typeHeight.constant+60;
        [cell.contentView layoutIfNeeded];

        //
        NSString *cartimage=[NSString stringWithFormat:@"%@",[cartImageThumbArray objectAtIndex:indexPath.row]];
        [cell.cart_img_product sd_setImageWithURL:[NSURL URLWithString:cartimage]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            [HUD hide:YES];
            
            
        }];
//        cell.lbl_brandName.text=[cartBrandArray objectAtIndex:indexPath.row];
        cell.cart_lbl_qtyPrice.text=[cartQtyPriceArray objectAtIndex:indexPath.row];
        cell.cart_lbl_price.text=[NSString stringWithFormat:@"₹ %@",[cartTotalArray objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    } @catch (NSException *exception) {
        [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
    }

    
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    selectedSpiceIndex=@"0";
//
////    popId=[cartIdArray objectAtIndex:indexPath.row];
//  //  popName=[cartNameArray objectAtIndex:indexPath.row];
//    popQty=[cartQtyArray objectAtIndex:indexPath.row];
////    popPrice=[cartPriceArray objectAtIndex:indexPath.row];
//    currIndex=indexPath.row;
//
//
//    _lbl_titleName.text=[NSString stringWithFormat:@"%@",[cartNameArray objectAtIndex:indexPath.row]];
//    //[_lbl_titleName sizeToFit];
//    //prevCartId,*prevCartName,*prevCartQty,*prevCartCutLabel,*prevCartWeightLabel,*prevCartTypeLabel
//
//    prevCartId= [cartIdArray objectAtIndex:indexPath.row];
//    prevCartName= [cartNameArray objectAtIndex:indexPath.row];
//    prevCartQty=[cartQtyArray objectAtIndex:indexPath.row];
//    if (!([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) || !([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) || !([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]))
//    {
//
//        _shieldView.hidden=NO;
//        _attrPopView.hidden=NO;
//        _popUpView.hidden=YES;
//        _lbl_Qty.text=popQty;
//        quntity=[_lbl_Qty.text intValue];
//
//
//
//
//
//
//       int a=123;
//        if (!([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]))
//        {
//
//            _lbl_cut_height.constant=24;
//            _btn_cut_height.constant=24;
//
//            _lbl_cut.hidden=NO;
//            _btn_cut.hidden=NO;
//
//            a=a+_btn_cut_height.constant;
//
//            [_btn_cut setTitle:[cartCutOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//            NSArray *allCutLabArr=[[cartAllCutLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//            NSArray *allCutPriArr=[[cartAllCutPriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//            NSArray *allCutValArr=[[cartAllCutValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//
//            pickerCutLabelArr=[[NSMutableArray alloc] init];
//            pickerCutPriceArr=[[NSMutableArray alloc] init];
//            pickerCutValueArr=[[NSMutableArray alloc] init];
//
//            [pickerCutLabelArr addObjectsFromArray:allCutLabArr];
//            [pickerCutPriceArr addObjectsFromArray:allCutPriArr];
//            [pickerCutValueArr addObjectsFromArray:allCutValArr];
//
//            selectedCutLabelIndex=[cartCutOptionLabelArray objectAtIndex:indexPath.row];
//            selectedCutValueIndex=[cartCutOptionValueArray objectAtIndex:indexPath.row];
//            selectedCutPriceIndex=[cartCutOptionPriceArray objectAtIndex:indexPath.row];
//            prevCartCutLabel=selectedCutLabelIndex;
//
//            if([[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"Pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"PCS"])
//            {
//                _lbl_cut.text=@"Pieces:";
//            }
//            else
//            {
//                _lbl_cut.text=@"Cut:";
//            }
//
////
//
//        }
//        else{
//            _lbl_cut_height.constant=0;
//            _btn_cut_height.constant=0;
//            _lbl_cut.hidden=YES;
//            _btn_cut.hidden=YES;
//            selectedCutLabelIndex=@"0";
//            selectedCutValueIndex=@"0";
//            selectedCutPriceIndex=@"0";
//            prevCartCutLabel=@"0";
//
//
//        }
//        if (!([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
//            _lbl_WeightRange_height.constant=24;
//            _btn_weightRange_height.constant=24;
//
//            _lbl_weightRange.hidden=NO;
//            _btn_weightRange.hidden=NO;
//
//            a=a+_btn_weightRange_height.constant;
//
//            [_btn_weightRange setTitle:[cartWeightOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//
//            NSArray *allWeiLabArr=[[cartAllWeightLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//            NSArray *allWeiPriArr=[[cartAllWeightPriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//            NSArray *allWeiValArr=[[cartAllWeightValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//            pickerWeightLabelArr=[[NSMutableArray alloc] init];
//            pickerWeightPriceArr=[[NSMutableArray alloc] init];
//            pickerWeightValueArr=[[NSMutableArray alloc] init];
//
//            [pickerWeightLabelArr addObjectsFromArray:allWeiLabArr];
//            [pickerWeightPriceArr addObjectsFromArray:allWeiPriArr];
//            [pickerWeightValueArr addObjectsFromArray:allWeiValArr];
//
//            selectedWeightLabelIndex=[cartWeightOptionLabelArray objectAtIndex:indexPath.row];
//            selectedWeightValueIndex=[cartWeightOptionValueArray objectAtIndex:indexPath.row];
//            selectedWeightPriceIndex=[cartWeightOptionPriceArray objectAtIndex:indexPath.row];
//            prevCartWeightLabel=selectedWeightLabelIndex;
//
//        }
//        else{
//            _lbl_WeightRange_height.constant=0;
//            _btn_weightRange_height.constant=0;
//            _lbl_weightRange.hidden=YES;
//            _btn_weightRange.hidden=YES;
//
//            selectedWeightLabelIndex=@"0";
//            selectedWeightValueIndex=@"0";
//            selectedWeightPriceIndex=@"0";
//            prevCartWeightLabel=@"0";
//
//        }
//
//        if (!([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
//            _lbl_type_height.constant=24;
//            _btn_type_height.constant=24;
//
//            _lbl_type.hidden=NO;
//            _btn_type.hidden=NO;
//
//            a=a+_btn_type_height.constant;
//
//            [_btn_type setTitle:[cartTypeOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//
//            NSArray *allTypLabArr=[[cartAllTypeLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//            NSArray *allTypPriArr=[[cartAllTypePriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//            NSArray *allTypValArr=[[cartAllTypeValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
//
//            pickerTypeLabelArr=[[NSMutableArray alloc] init];
//            pickerTypePriceArr=[[NSMutableArray alloc] init];
//            pickerTypeValueArr=[[NSMutableArray alloc] init];
//
//            [pickerTypeLabelArr addObjectsFromArray:allTypLabArr];
//            [pickerTypePriceArr addObjectsFromArray:allTypPriArr];
//            [pickerTypeValueArr addObjectsFromArray:allTypValArr];
//
//            selectedTypeLabelIndex=[cartTypeOptionLabelArray objectAtIndex:indexPath.row];
//            selectedTypeValueIndex=[cartTypeOptionValueArray objectAtIndex:indexPath.row];
//            selectedTypePriceIndex=[cartTypeOptionPriceArray objectAtIndex:indexPath.row];
//
//            prevCartTypeLabel=selectedTypeLabelIndex;
//
//
//        }
//        else{
//            _lbl_type_height.constant=0;
//            _btn_type_height.constant=0;
//            _lbl_type.hidden=YES;
//            _btn_type.hidden=YES;
//
//            selectedTypeLabelIndex=@"0";
//            selectedTypeValueIndex=@"0";
//            selectedTypePriceIndex=@"0";
//            prevCartTypeLabel=@"0";
//
//        }
//
//        _pop_view_height.constant=a+50;
//
//
////        if (IS_IPAD) {
////            [self editCartLayoutForIpad];
////
////        }
////        else{
////            [self editCartLayoutForSpices];
////
////        }
//
//    }
//    else{
//
//        _lbl_popTitleName.text=[NSString stringWithFormat:@"%@",[cartNameArray objectAtIndex:indexPath.row]];
//        //[_lbl_popTitleName sizeToFit];
//
//        _shieldView.hidden=NO;
//        _attrPopView.hidden=YES;
//        _popUpView.hidden=NO;
//        _lbl_popQty.text=popQty;
//        quntity=[_lbl_popQty.text intValue];
//
//        selectedCutLabelIndex=@"0";
//        selectedCutValueIndex=@"0";
//        selectedCutPriceIndex=@"0";
//
//        selectedWeightLabelIndex=@"0";
//        selectedWeightValueIndex=@"0";
//        selectedWeightPriceIndex=@"0";
//
//        selectedTypeLabelIndex=@"0";
//        selectedTypeValueIndex=@"0";
//        selectedTypePriceIndex=@"0";
//
//        prevCartWeightLabel=@"0";
//        prevCartTypeLabel=@"0";
//        prevCartCutLabel=@"0";
//
////        if (IS_IPAD) {
////            [self editCartLayoutForIpad];
////
////        }
////        else{
////            [self editCartLayout];
////
////        }
//
//    }


//    lbl_sizeValue.text=popName;
//    txt_Qty.text=popQty;
//    quntity=[txt_Qty.text intValue];

//    shieldView.hidden=NO;

}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//    long siz =cell.contentView.frame.size.height;
//    return siz;
    
    
    // edit: Need to call [cell layoutIfNeeded]; otherwise the layout changes wont be applied to the   cell
    
    [cell layoutIfNeeded];
    long a=cell.nameHeight.constant+cell.brandHeight.constant+cell.cartWeightHeight.constant+60;
    if (!([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
    a=a+cell.btn_cutHeight.constant;

    }
    if (!([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
        a=a+cell.btn_weightHeight.constant;
        
    }
    if (!([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
        a=a+cell.btn_typeHeight.constant;
        
    }
    
    return a;

}
- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}


#pragma mark Coupon API

-(void)submitData
{
    
    HelperClass *s=[[HelperClass alloc]init];
    BOOL connection=s.getInternetStatus;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (!connection)
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Connectivity Lost"
                                          message:@"Please check your internet connectivity."
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           //Do Some action here
                                                       }];
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            
            @try {
                
                
                
                
                
                
                NSString *url=API_APPLYCOUPON;
                NSString *CustId=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
                NSString *couponTxt=_txt_couponCode.text;
                NSString *replaceSpace=[couponTxt stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSMutableDictionary *dict=[NSMutableDictionary dictionary];
//                    dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:CustId,@"customer_id",couponTxt,@"coupon_code", nil];
                [dict setObject:CustId forKey:@"customer_id"];
                [dict setObject:replaceSpace forKey:@"coupon_code"];

                
                for (int j=0; j<[prodIdKeyArr count]; j++)
                {
                    [dict setObject:[prodIdValArr objectAtIndex:j] forKey:[prodIdKeyArr objectAtIndex:j]];
                    [dict setObject:[prodQtyValArr objectAtIndex:j] forKey:[prodQtyKeyArr objectAtIndex:j]];

                    if (!([[prod1IdVal objectAtIndex:j] isEqualToString:@"0"])) {
                        [dict setObject:[prod1IdVal objectAtIndex:j] forKey:[prod1Idkey objectAtIndex:j]];
                        [dict setObject:[Prod1Value objectAtIndex:j] forKey:[prod1ValKey objectAtIndex:j]];
 
                    }
                    if (!([[prod2IdVal objectAtIndex:j] isEqualToString:@"0"])) {
                        [dict setObject:[prod2IdVal objectAtIndex:j] forKey:[prod2Idkey objectAtIndex:j]];
                        [dict setObject:[Prod2Value objectAtIndex:j] forKey:[prod2ValKey objectAtIndex:j]];
                        
                    }
                    if (!([[prod3IdVal objectAtIndex:j] isEqualToString:@"0"])) {
                        [dict setObject:[prod3IdVal objectAtIndex:j] forKey:[prod3Idkey objectAtIndex:j]];
                        [dict setObject:[Prod3Value objectAtIndex:j] forKey:[prod3ValKey objectAtIndex:j]];
                        
                    }
                }

                
                NSLog(@"%@",dict);
                WebservicesClass *web=[[WebservicesClass alloc] init];
                //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:dict];
                NSLog(@"%@",getJsonData);
                
                
                if (getJsonData.count>0)
                {
                    
                    BOOL value=[[getJsonData objectForKey:@"success"] boolValue];
                    NSString *message=[getJsonData objectForKey:@"message"];
                    NSString *desc=[getJsonData objectForKey:@"description"];
                    grandT=[getJsonData objectForKey:@"grand_total"];
                    NSString *subT=[getJsonData objectForKey:@"sub_total"];
                    NSString *name=[getJsonData objectForKey:@"name"];
                    selectedCouponCode=name;
                    //NSLog(@"%@",value);
                    
                    if (value) {
                        
                        
                        float Sumval=[subT floatValue]-[grandT floatValue];
                        NSString *discountTot=[NSString stringWithFormat:@"%.2f",Sumval];
                        _lbl_couponName.text=[NSString stringWithFormat:@"%@",name];
                        _lbl_couponVal.text=discountTot;
                        _lbl_subTotalValue.text=[NSString stringWithFormat:@"%@",subT];
                        
                        _lbl_subTotalValue.hidden=NO;
                        _lbl_subTotal.hidden=NO;
                        _lbl_subTotHeight.constant=15;
                        _lbl_subTotValHeight.constant=15;

                        _lbl_couponVal.hidden=NO;
                        _lbl_couponName.hidden=NO;
                        _lbl_cupNameheight.constant=15;
                        _lbl_copValHeight.constant=15;


                        _lbl_total.text=[NSString stringWithFormat:@"%@",grandT];
                        
                        [_btn_apply setTitle:@"Clear" forState:UIControlStateNormal];
                        [_txt_couponCode setUserInteractionEnabled:NO];
                        _btn_apply.tag=2;
//                        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
//                        NSString *cartCount=[[NSUserDefaults standardUserDefaults] valueForKey:@"BADGECOUNT"];
                        //cartBtn.badgeValue=cartCount;
                        
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:@"Success"
                                                      message:message
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       //Do Some action here
                                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                                       
                                                                       
                                                                   }];
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    else
                    {
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:@""
                                                      message:@"This is not a valid coupon."
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       //Do Some action here
                                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                                       
                                                                   }];
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                    
                }
                
            } @catch (NSException *exception) {
                [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];

            }
            
            
            
        }
        
        [HUD hide:YES];
        
    });
}

/*

- (IBAction)spicesMethod:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=1;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedSpiceIndex);
                                         long row = [sender tag];
                                         NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
                                         CustomTableViewCell *cell = [self.cartTableView cellForRowAtIndexPath:indexPath];
                                         
                                         if (selectedSpiceIndex.length>0 || !([selectedSpiceIndex isEqualToString:@"0"])) {
                                             
                                             if ([selectedSpiceIndex isEqualToString:@"1"]) {
                                                 spicesBackImage.image=[UIImage imageNamed:@"1.png"];
                                                 
                                             }
                                             else if ([selectedSpiceIndex isEqualToString:@"2"]){
                                                 spicesBackImage.image=[UIImage imageNamed:@"2.png"];
                                                 
                                             }
                                             else if ([selectedSpiceIndex isEqualToString:@"3"]){
                                                 spicesBackImage.image=[UIImage imageNamed:@"3.png"];
                                                 
                                             }
                                             //                                         [spicelevelArray replaceObjectAtIndex:[sender tag] withObject:selectedSpiceIndex];
                                             
                                             //removing object
                                             
                                         }
                                         else{
                                             spicesBackImage.image=[UIImage imageNamed:@"1.png"];
                                             
                                         }

                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];

                                     } 
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
    
    
}
*/

#pragma mark UIPickerDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// Total rows in our component.

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger i;
    if (pickerView.tag==1) {
        i=[pickerCutLabelArr count];
        
    }
    else if (pickerView.tag==2){
        i=[pickerWeightLabelArr count];
    }
    else if (pickerView.tag==3){
        i=[pickerTypeLabelArr count];
        
    }
    
    //    i=[mirchImagesArr count];
    return i;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 50;
}
// Display each row's data.



// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==1) {
        
        selectedLabel=[pickerCutLabelArr objectAtIndex:row];
        selectedValue=[pickerCutValueArr objectAtIndex:row];
        selectedPrice=[pickerCutPriceArr objectAtIndex:row];
        
    }
    else if (pickerView.tag==2){
        selectedLabel=[pickerWeightLabelArr objectAtIndex:row];
        selectedValue=[pickerWeightValueArr objectAtIndex:row];
        selectedPrice=[pickerWeightPriceArr objectAtIndex:row];
        
    }
    else if (pickerView.tag==3)
    {
        selectedLabel=[pickerTypeLabelArr objectAtIndex:row];
        selectedValue=[pickerTypeValueArr objectAtIndex:row];
        selectedPrice=[pickerTypePriceArr objectAtIndex:row];
        
    }
    
    //    selectedCustId=[custIdArray objectAtIndex:row];
    
    
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    // Get the text of the row.
    
    
    UILabel *lblRow = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,spicesPicker.bounds.size.width, 44.0f)];
    
    
    if (pickerView.tag==1) {
        NSString *rowItem = [pickerCutLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==2){
        NSString *rowItem = [pickerWeightLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==3){
        NSString *rowItem = [pickerTypeLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    
    
    
    
    
    return lblRow;
    
    
    
    
}

#pragma mark IBActions
- (IBAction)backMethod:(id)sender {
    
    
    //    if ([comingFromItems isEqualToString:@"YES"]) {
    //        UIStoryboard *storyBoard;
    //        storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //        ItemsViewController *viewController =
    //        [storyBoard instantiateViewControllerWithIdentifier:@"ItemsViewController"];
    //        [self presentViewController:viewController animated:YES completion:nil];
    //    }
    //    else{
    //        APP_DEL.cartToItem=@"YES";
    //        [self.navigationController popViewControllerAnimated:YES];
    //
    //    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)continueMethod:(id)sender {
    
    
    //    UIStoryboard *storyBoard;
    //    storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    OrderInfoViewController *viewController =
    //    [storyBoard instantiateViewControllerWithIdentifier:@"OrderInfoViewController"];
    //    [self presentViewController:viewController animated:YES completion:nil];
    //
    
    
    //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //        OrderInfoViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrderInfoViewController"];
    //
    //        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    //        [navController setViewControllers: @[rootViewController] animated: YES];
    //
    //        [self.revealViewController setFrontViewController:navController];
    //        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
    
    
    if(totalValue>=minimumValueOfSelectedLocation)
    {
        //        OrderInformationViewController *ord=[self.storyboard instantiateViewControllerWithIdentifier:@"OrderInformationVC"];
        //
        //        ord.fetchedCartTotal=_lbl_total.text;
        //        [[NSUserDefaults standardUserDefaults]setObject:_lbl_total.text forKey:@"grandTotal"];
        NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
        if ([isLog isEqualToString:@"YES"])
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:_lbl_total.text forKey:@"grandTotal"];
            [[NSUserDefaults standardUserDefaults] setObject:selectedCouponCode forKey:@"COUPONCODE"];
            
            ShippingAddressController *ord=[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingAddressController"];
            ord.comingFrom=@"CART";
            ord.arrayRedeeemShip = arrayRedeemPoints;
            [self.navigationController pushViewController:ord animated:YES];
            
            
            //ord.fetchedCartTotal=[NSString stringWithFormat:@"%@",grandT];
            //            ord.fetchedCoupon=selectedCouponCode;
            //            ord.comingFrom=@"CARTVC";
            //            [self.navigationController pushViewController:ord animated:YES];
        }
        else
        {
            //            ord.comingFrom=@"anotherVC";
            LoginViewController *ord1=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            ord1.comingFrom=@"CART";
            [self.navigationController pushViewController:ord1 animated:YES];
        }
    }
    else
    {
        NSString *msg=[NSString stringWithFormat:@"Minimum order for selected area is ₹ %.2d",minimumValueOfSelectedLocation];
        [self UIAlertViewControllerMethodTitle:@"" message:msg];
    }
    
    
    
    
    
    //    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
    //    if ([isLog isEqualToString:@"YES"]) {
    //        OrderInformationViewController *ord=[self.storyboard instantiateViewControllerWithIdentifier:@"OrderInformationVC"];
    //       ord.fetchedCoupon=selectedCouponCode;
    //        [self.navigationController pushViewController:ord animated:YES];
    //    }
    //    else{
    //        LoginViewController *ord=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    //        ord.comingFrom=@"CART";
    //        [self.navigationController pushViewController:ord animated:YES];
    //    }
    
    
    
    //
    
    //    if ([cartIdArray count] > 0) {
    //        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //        HUD.dimBackground = YES;
    //        HUD.labelText = @"Please wait...";
    //        [HUD showWhileExecuting:@selector(submitData) onTarget:self withObject:Nil animated:YES];
    //    }
}

- (IBAction)applyMethod:(id)sender
{
    if(_btn_apply.tag==2)
    {
        [_btn_apply setTitle:@"Apply" forState:UIControlStateNormal];
        [_txt_couponCode setUserInteractionEnabled:YES];
        _btn_apply.tag=1;
        _lbl_subTotalValue.hidden=YES;
        _lbl_subTotal.hidden=YES;
        _lbl_subTotHeight.constant=0;
        _lbl_subTotValHeight.constant=0;
        selectedCouponCode=nil;
        _lbl_couponVal.hidden=YES;
        _lbl_couponName.hidden=YES;
        _lbl_cupNameheight.constant=0;
        _lbl_copValHeight.constant=0;
        
        
        _lbl_total.text=[NSString stringWithFormat:@"%.02f",sum];
    }
    else
    {
        NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
        if ([isLog isEqualToString:@"YES"])
        {
            if ([_txt_couponCode.text isEqualToString:@""])
            {
                [ProgressHUD showError:@"Please Enter coupon code" Interaction:NO];
            }
            else
            {
                HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                HUD.dimBackground = YES;
                HUD.labelText = @"Please wait...";
                [HUD showWhileExecuting:@selector(submitData) onTarget:self withObject:Nil animated:YES];
                
            }
            
            
        }
        else{
            [ProgressHUD showError:@"Please login first." Interaction:NO];
        }
    }
}
- (IBAction)closeMethod:(id)sender {
    
    _shieldView.hidden=YES;

}
- (IBAction)subMethod:(id)sender {
    
    long row = [sender tag];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
        
        CustomTableViewCell *cell =(CustomTableViewCell *)[self.cartTableView cellForRowAtIndexPath:indexPath];
        NSString *currIndQty=cell.lbl_popQty.text;
        quntity=[currIndQty intValue];
        
        prevCartId= [cartIdArray objectAtIndex:indexPath.row];
              prevCartName= [cartNameArray objectAtIndex:indexPath.row];
              prevCartQty=[cartQtyArray objectAtIndex:indexPath.row];
              if (!([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) || !([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) || !([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]))
              {
                  
                  _shieldView.hidden=YES;
                  _attrPopView.hidden=YES;
                  _popUpView.hidden=YES;
                  cell.lbl_popQty.text=popQty;
                  quntity=[cell.lbl_popQty.text intValue];
        
                 int a=123;
                  if (!([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]))
                  {
                      
                      _lbl_cut_height.constant=24;
                      _btn_cut_height.constant=24;
                      
                      _lbl_cut.hidden=NO;
                      _btn_cut.hidden=NO;

                      a=a+_btn_cut_height.constant;

                      [_btn_cut setTitle:[cartCutOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                      NSArray *allCutLabArr=[[cartAllCutLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      NSArray *allCutPriArr=[[cartAllCutPriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      NSArray *allCutValArr=[[cartAllCutValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      
                      pickerCutLabelArr=[[NSMutableArray alloc] init];
                      pickerCutPriceArr=[[NSMutableArray alloc] init];
                      pickerCutValueArr=[[NSMutableArray alloc] init];

                      [pickerCutLabelArr addObjectsFromArray:allCutLabArr];
                      [pickerCutPriceArr addObjectsFromArray:allCutPriArr];
                      [pickerCutValueArr addObjectsFromArray:allCutValArr];
                      
                      selectedCutLabelIndex=[cartCutOptionLabelArray objectAtIndex:indexPath.row];
                      selectedCutValueIndex=[cartCutOptionValueArray objectAtIndex:indexPath.row];
                      selectedCutPriceIndex=[cartCutOptionPriceArray objectAtIndex:indexPath.row];
                      prevCartCutLabel=selectedCutLabelIndex;

                      if([[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"Pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"PCS"])
                      {
                          _lbl_cut.text=@"Pieces:";
                      }
                      else
                      {
                          _lbl_cut.text=@"Cut:";
                      }

          //

                  }
                  else{
                      _lbl_cut_height.constant=0;
                      _btn_cut_height.constant=0;
                      _lbl_cut.hidden=YES;
                      _btn_cut.hidden=YES;
                      selectedCutLabelIndex=@"0";
                      selectedCutValueIndex=@"0";
                      selectedCutPriceIndex=@"0";
                      prevCartCutLabel=@"0";


                  }
                  if (!([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
                      _lbl_WeightRange_height.constant=24;
                      _btn_weightRange_height.constant=24;
                      
                      _lbl_weightRange.hidden=NO;
                      _btn_weightRange.hidden=NO;

                      a=a+_btn_weightRange_height.constant;

                      [_btn_weightRange setTitle:[cartWeightOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];

                      NSArray *allWeiLabArr=[[cartAllWeightLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      NSArray *allWeiPriArr=[[cartAllWeightPriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      NSArray *allWeiValArr=[[cartAllWeightValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      pickerWeightLabelArr=[[NSMutableArray alloc] init];
                      pickerWeightPriceArr=[[NSMutableArray alloc] init];
                      pickerWeightValueArr=[[NSMutableArray alloc] init];

                      [pickerWeightLabelArr addObjectsFromArray:allWeiLabArr];
                      [pickerWeightPriceArr addObjectsFromArray:allWeiPriArr];
                      [pickerWeightValueArr addObjectsFromArray:allWeiValArr];
                      
                      selectedWeightLabelIndex=[cartWeightOptionLabelArray objectAtIndex:indexPath.row];
                      selectedWeightValueIndex=[cartWeightOptionValueArray objectAtIndex:indexPath.row];
                      selectedWeightPriceIndex=[cartWeightOptionPriceArray objectAtIndex:indexPath.row];
                      prevCartWeightLabel=selectedWeightLabelIndex;

                  }
                  else{
                      _lbl_WeightRange_height.constant=0;
                      _btn_weightRange_height.constant=0;
                      _lbl_weightRange.hidden=YES;
                      _btn_weightRange.hidden=YES;
                      
                      selectedWeightLabelIndex=@"0";
                      selectedWeightValueIndex=@"0";
                      selectedWeightPriceIndex=@"0";
                      prevCartWeightLabel=@"0";

                  }

                  if (!([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
                      _lbl_type_height.constant=24;
                      _btn_type_height.constant=24;
                      
                      _lbl_type.hidden=NO;
                      _btn_type.hidden=NO;

                      a=a+_btn_type_height.constant;

                      [_btn_type setTitle:[cartTypeOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];

                      NSArray *allTypLabArr=[[cartAllTypeLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      NSArray *allTypPriArr=[[cartAllTypePriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                      NSArray *allTypValArr=[[cartAllTypeValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                     
                      pickerTypeLabelArr=[[NSMutableArray alloc] init];
                      pickerTypePriceArr=[[NSMutableArray alloc] init];
                      pickerTypeValueArr=[[NSMutableArray alloc] init];

                      [pickerTypeLabelArr addObjectsFromArray:allTypLabArr];
                      [pickerTypePriceArr addObjectsFromArray:allTypPriArr];
                      [pickerTypeValueArr addObjectsFromArray:allTypValArr];
                      
                      selectedTypeLabelIndex=[cartTypeOptionLabelArray objectAtIndex:indexPath.row];
                      selectedTypeValueIndex=[cartTypeOptionValueArray objectAtIndex:indexPath.row];
                      selectedTypePriceIndex=[cartTypeOptionPriceArray objectAtIndex:indexPath.row];

                      prevCartTypeLabel=selectedTypeLabelIndex;


                  }
                  else{
                      _lbl_type_height.constant=0;
                      _btn_type_height.constant=0;
                      _lbl_type.hidden=YES;
                      _btn_type.hidden=YES;
                      
                      selectedTypeLabelIndex=@"0";
                      selectedTypeValueIndex=@"0";
                      selectedTypePriceIndex=@"0";
                      prevCartTypeLabel=@"0";

                  }

                  _pop_view_height.constant=a+50;

          
                  
              }
              else{
                  
                 // _lbl_popTitleName.text=[NSString stringWithFormat:@"%@",[cartNameArray objectAtIndex:indexPath.row]];
                  //[_lbl_popTitleName sizeToFit];
                  
                  _shieldView.hidden=YES;
                  _attrPopView.hidden=YES;
                  _popUpView.hidden=YES;
    //             cell.lbl_popQty.text=popQty;
    //              quntity=[cell.lbl_popQty.text intValue];

                  selectedCutLabelIndex=@"0";
                  selectedCutValueIndex=@"0";
                  selectedCutPriceIndex=@"0";

                  selectedWeightLabelIndex=@"0";
                  selectedWeightValueIndex=@"0";
                  selectedWeightPriceIndex=@"0";
                  
                  selectedTypeLabelIndex=@"0";
                  selectedTypeValueIndex=@"0";
                  selectedTypePriceIndex=@"0";
                 
                  prevCartWeightLabel=@"0";
                  prevCartTypeLabel=@"0";
                  prevCartCutLabel=@"0";
              }
        
        
        
        
        currIndex = indexPath.row;
    
    if (quntity > 1) {
        quntity--;
//        txt_Qty.text=[NSString stringWithFormat:@"%d",quntity];
        _lbl_Qty.text=[NSString stringWithFormat:@"%d",quntity];
        cell.lbl_popQty.text=[NSString stringWithFormat:@"%d",quntity];
        [self updateMethod:sender];
    }else{
        [self removeMethod:sender];
    }

}

- (IBAction)addMethod:(id)sender {
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    CustomTableViewCell *cell =(CustomTableViewCell *)[self.cartTableView cellForRowAtIndexPath:indexPath];
    NSString *currIndQty=cell.lbl_popQty.text;
    quntity=[currIndQty intValue];
    
    prevCartId= [cartIdArray objectAtIndex:indexPath.row];
          prevCartName= [cartNameArray objectAtIndex:indexPath.row];
          prevCartQty=[cartQtyArray objectAtIndex:indexPath.row];
          if (!([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) || !([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) || !([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]))
          {
              
              _shieldView.hidden=YES;
              _attrPopView.hidden=YES;
              _popUpView.hidden=YES;
              cell.lbl_popQty.text=popQty;
              quntity=[cell.lbl_popQty.text intValue];
    
             int a=123;
              if (!([[cartCutOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"]))
              {
                  
                  _lbl_cut_height.constant=24;
                  _btn_cut_height.constant=24;
                  
                  _lbl_cut.hidden=NO;
                  _btn_cut.hidden=NO;

                  a=a+_btn_cut_height.constant;

                  [_btn_cut setTitle:[cartCutOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                  NSArray *allCutLabArr=[[cartAllCutLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  NSArray *allCutPriArr=[[cartAllCutPriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  NSArray *allCutValArr=[[cartAllCutValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  
                  pickerCutLabelArr=[[NSMutableArray alloc] init];
                  pickerCutPriceArr=[[NSMutableArray alloc] init];
                  pickerCutValueArr=[[NSMutableArray alloc] init];

                  [pickerCutLabelArr addObjectsFromArray:allCutLabArr];
                  [pickerCutPriceArr addObjectsFromArray:allCutPriArr];
                  [pickerCutValueArr addObjectsFromArray:allCutValArr];
                  
                  selectedCutLabelIndex=[cartCutOptionLabelArray objectAtIndex:indexPath.row];
                  selectedCutValueIndex=[cartCutOptionValueArray objectAtIndex:indexPath.row];
                  selectedCutPriceIndex=[cartCutOptionPriceArray objectAtIndex:indexPath.row];
                  prevCartCutLabel=selectedCutLabelIndex;

                  if([[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"Pcs"] || [[cartCutOptionLabelArray objectAtIndex:indexPath.row] containsString:@"PCS"])
                  {
                      _lbl_cut.text=@"Pieces:";
                  }
                  else
                  {
                      _lbl_cut.text=@"Cut:";
                  }

      //

              }
              else{
                  _lbl_cut_height.constant=0;
                  _btn_cut_height.constant=0;
                  _lbl_cut.hidden=YES;
                  _btn_cut.hidden=YES;
                  selectedCutLabelIndex=@"0";
                  selectedCutValueIndex=@"0";
                  selectedCutPriceIndex=@"0";
                  prevCartCutLabel=@"0";


              }
              if (!([[cartWeightOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
                  _lbl_WeightRange_height.constant=24;
                  _btn_weightRange_height.constant=24;
                  
                  _lbl_weightRange.hidden=NO;
                  _btn_weightRange.hidden=NO;

                  a=a+_btn_weightRange_height.constant;

                  [_btn_weightRange setTitle:[cartWeightOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];

                  NSArray *allWeiLabArr=[[cartAllWeightLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  NSArray *allWeiPriArr=[[cartAllWeightPriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  NSArray *allWeiValArr=[[cartAllWeightValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  pickerWeightLabelArr=[[NSMutableArray alloc] init];
                  pickerWeightPriceArr=[[NSMutableArray alloc] init];
                  pickerWeightValueArr=[[NSMutableArray alloc] init];

                  [pickerWeightLabelArr addObjectsFromArray:allWeiLabArr];
                  [pickerWeightPriceArr addObjectsFromArray:allWeiPriArr];
                  [pickerWeightValueArr addObjectsFromArray:allWeiValArr];
                  
                  selectedWeightLabelIndex=[cartWeightOptionLabelArray objectAtIndex:indexPath.row];
                  selectedWeightValueIndex=[cartWeightOptionValueArray objectAtIndex:indexPath.row];
                  selectedWeightPriceIndex=[cartWeightOptionPriceArray objectAtIndex:indexPath.row];
                  prevCartWeightLabel=selectedWeightLabelIndex;

              }
              else{
                  _lbl_WeightRange_height.constant=0;
                  _btn_weightRange_height.constant=0;
                  _lbl_weightRange.hidden=YES;
                  _btn_weightRange.hidden=YES;
                  
                  selectedWeightLabelIndex=@"0";
                  selectedWeightValueIndex=@"0";
                  selectedWeightPriceIndex=@"0";
                  prevCartWeightLabel=@"0";

              }

              if (!([[cartTypeOptionIdArray objectAtIndex:indexPath.row] isEqualToString:@"0"])) {
                  _lbl_type_height.constant=24;
                  _btn_type_height.constant=24;
                  
                  _lbl_type.hidden=NO;
                  _btn_type.hidden=NO;

                  a=a+_btn_type_height.constant;

                  [_btn_type setTitle:[cartTypeOptionLabelArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];

                  NSArray *allTypLabArr=[[cartAllTypeLabelArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  NSArray *allTypPriArr=[[cartAllTypePriceArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                  NSArray *allTypValArr=[[cartAllTypeValueArray objectAtIndex:currIndex] componentsSeparatedByString:@","];
                 
                  pickerTypeLabelArr=[[NSMutableArray alloc] init];
                  pickerTypePriceArr=[[NSMutableArray alloc] init];
                  pickerTypeValueArr=[[NSMutableArray alloc] init];

                  [pickerTypeLabelArr addObjectsFromArray:allTypLabArr];
                  [pickerTypePriceArr addObjectsFromArray:allTypPriArr];
                  [pickerTypeValueArr addObjectsFromArray:allTypValArr];
                  
                  selectedTypeLabelIndex=[cartTypeOptionLabelArray objectAtIndex:indexPath.row];
                  selectedTypeValueIndex=[cartTypeOptionValueArray objectAtIndex:indexPath.row];
                  selectedTypePriceIndex=[cartTypeOptionPriceArray objectAtIndex:indexPath.row];

                  prevCartTypeLabel=selectedTypeLabelIndex;


              }
              else{
                  _lbl_type_height.constant=0;
                  _btn_type_height.constant=0;
                  _lbl_type.hidden=YES;
                  _btn_type.hidden=YES;
                  
                  selectedTypeLabelIndex=@"0";
                  selectedTypeValueIndex=@"0";
                  selectedTypePriceIndex=@"0";
                  prevCartTypeLabel=@"0";

              }

              _pop_view_height.constant=a+50;

      
              
          }
          else{
              
             // _lbl_popTitleName.text=[NSString stringWithFormat:@"%@",[cartNameArray objectAtIndex:indexPath.row]];
              //[_lbl_popTitleName sizeToFit];
              
              _shieldView.hidden=YES;
              _attrPopView.hidden=YES;
              _popUpView.hidden=YES;
//             cell.lbl_popQty.text=popQty;
//              quntity=[cell.lbl_popQty.text intValue];

              selectedCutLabelIndex=@"0";
              selectedCutValueIndex=@"0";
              selectedCutPriceIndex=@"0";

              selectedWeightLabelIndex=@"0";
              selectedWeightValueIndex=@"0";
              selectedWeightPriceIndex=@"0";
              
              selectedTypeLabelIndex=@"0";
              selectedTypeValueIndex=@"0";
              selectedTypePriceIndex=@"0";
             
              prevCartWeightLabel=@"0";
              prevCartTypeLabel=@"0";
              prevCartCutLabel=@"0";
          }
    
    
    
    
    currIndex = indexPath.row;
    
    if (quntity < 99) {
        quntity++;
//        txt_Qty.text=[NSString stringWithFormat:@"%d",quntity];
        _lbl_Qty.text=[NSString stringWithFormat:@"%d",quntity];
        
        cell.lbl_popQty.text=[NSString stringWithFormat:@"%d",quntity];
        [self updateMethod:sender];
    }


}
- (IBAction)removeMethod:(id)sender {
    
    NSString *idStr=[cartIdArray objectAtIndex:currIndex];
    NSString *catId=[cartCategoryIdArray objectAtIndex:currIndex];;
    NSString *cutoplabel=selectedCutLabelIndex;
    NSString *weioplabel=selectedWeightLabelIndex;
    NSString *typoplabel=selectedTypeLabelIndex;
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert"
                                  message:@"Are you sure you want to remove this item?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   
                                                   OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
                                                   [testDB_Obj Delete_Cart_Data:idStr AndCutLab:cutoplabel AndWeightlab:weioplabel AndTypeLab:typoplabel];
                                                   _shieldView.hidden=YES;
                                                   
                                                   [self getOfflineCartData];
 
                                                   NSArray *str=[testDB_Obj fetch_CartDetails];
                                                   NSLog(@"%@",str);
                                                   NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
                                                   if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
                                                       [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
                                                       
                                                   }
                                                   else{
                                                       [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
                                                       
                                                       
                                                   }

                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   
                                                
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   
                                               }];

    [alert addAction:ok];
    [alert addAction:cancel];

    [self presentViewController:alert animated:YES completion:nil];



}
- (IBAction)cutMethod:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=1;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedLabel);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             
                                             [_btn_cut setTitle:selectedCutLabelIndex forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                             
                                         }
                                         else{
                                             
                                             
                                             selectedCutLabelIndex=selectedLabel;
                                             selectedCutValueIndex =selectedValue;
                                             selectedCutPriceIndex=selectedPrice;
                                             
                                             [_btn_cut setTitle:selectedCutLabelIndex forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                         }
                                         
                                         
                                         int calcPrice=([selectedCutPriceIndex  intValue]+[selectedWeightPriceIndex intValue]+[selectedTypePriceIndex intValue]);
                                         if(calcPrice==0)
                                         {
                                             
                                         }
                                         else
                                         {
                                             NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                             //_lbl_itemPrice.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                             [cartPriceArray replaceObjectAtIndex:currIndex withObject:strPrice];
                                             
                                         }
                                         
                                     } @catch (NSException *exception) {
                                         NSLog(@"%@",exception);
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    

}

- (IBAction)weightMethod:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=2;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         //                                         NSLog(@"%@",selectedWeightLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             
                                             [_btn_weightRange setTitle:selectedWeightLabelIndex forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                         }
                                         else{
                                             
                                             
                                             selectedWeightLabelIndex=selectedLabel;
                                             selectedWeightValueIndex=selectedValue;
                                             selectedWeightPriceIndex=selectedPrice;
                                             
                                             
                                             [_btn_weightRange setTitle:selectedWeightLabelIndex forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                         }
                                         
                                         
                                         int calcPrice=([selectedCutPriceIndex intValue]+[selectedWeightPriceIndex intValue]+[selectedTypePriceIndex intValue]);
                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
//                                         _lbl_itemPrice.text=strPrice;
                                        [cartPriceArray replaceObjectAtIndex:currIndex withObject:strPrice];
                                         
                                     }
                                     @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];

    
}

- (IBAction)btnRedeemPressed:(id)sender{
    
    if (_btnRedeem.selected){
        _btnRedeem.selected = false;
    }else{
        _btnRedeem.selected = true;
        NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
        if ([isLog isEqualToString:@"YES"]){
            HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.dimBackground = YES;
            HUD.labelText = @"Please wait...";
            [HUD showWhileExecuting:@selector(getRedeemPoints) onTarget:self withObject:Nil animated:YES];
            
        }else{
            [ProgressHUD showError:@"Please login first." Interaction:NO];
        }
    }
    
}

-(void)getRedeemPoints{
    
    HelperClass *s=[[HelperClass alloc]init];
        BOOL connection=s.getInternetStatus;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (!connection)
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Connectivity Lost"
                                              message:@"Please check your internet connectivity."
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               //Do Some action here
                                                           }];
                UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                               }];
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                
                @try {
                    
                    
                    NSString *url=API_REDEEMPOINT;
                    NSString *CustId=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
                    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    //                    dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:CustId,@"customer_id",couponTxt,@"coupon_code", nil];
                    [dict setObject:CustId forKey:@"customer_id"];
                    
                    NSLog(@"%@",dict);
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
                    NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:dict];
                    NSLog(@"Redeem Data = %@", getJsonData);
                    
                    
                    if (getJsonData.count>0)
                    {
                        
                        strRedeemValue = [[getJsonData objectForKey:@"user_points"] stringValue];
                         arrayTotal = [[NSMutableArray alloc]init];
                        
                         arrayTotal = [[[getJsonData objectForKey:@"full"] objectForKey:@"user_information"]objectForKey:@"normal_rewards"];
                        NSMutableArray * arrayName = [arrayTotal valueForKey:@"name"];
                        NSMutableArray * arrPointsOfRedeem = [arrayTotal valueForKey:@"points"];
                        arrayItem = [[NSMutableArray alloc]init];
                    
                        for( int i = 0; i < [arrPointsOfRedeem count]; ++i ){
                            NSString * strName = [arrayName objectAtIndex:i];
                            NSString * strValue = [arrPointsOfRedeem objectAtIndex:i];
                            [arrayItem addObject:[NSString stringWithFormat:@"Redeem %@ Points for %@",[NSString stringWithFormat:@"%@",strValue],strName]];
                        }

                        NSLog(@"arrNameOfRedeem = %@",arrayItem);
                        
                        
                        if (![strRedeemValue isEqualToString:@"User doesn't exists!"]) {
                            
                            
                            DYAlertPickView *picker = [[DYAlertPickView alloc] initWithHeaderTitle:[NSString stringWithFormat:@"Your current balance of reward points: %@",strRedeemValue] cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Confirm" switchButtonTitle:nil];
                            picker.delegate = self;
                            picker.dataSource = self;
                            [picker showAndSelectedIndex:3];
 
                        }
                        else
                        {
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@""
                                                          message:@"No redeem points are available."
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            _btnRedeem.selected = false;
                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action) {
                                                                           //Do Some action here
                                                                            
                                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                                           
                                                                       }];
                            [alert addAction:ok];
                            
                            [self presentViewController:alert animated:YES completion:nil];
                            
                        }
                        
                    }
                    
                } @catch (NSException *exception) {
                    [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];

                }
                
                
                
            }
            
            [HUD hide:YES];
            
        });
}

- (NSAttributedString *)pickerview:(DYAlertPickView *)pickerView
                           titleForRow:(NSInteger)row{
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:arrayItem[row]];
    return str;
}
- (NSInteger)numberOfRowsInPickerview:(DYAlertPickView *)pickerView {
    return arrayTotal.count;
}
- (void)pickerview:(DYAlertPickView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    NSLog(@"%@ didConfirm", arrayTotal[row]);
    arrayRedeemPoints = [[NSMutableArray alloc]init];
    arrayRedeemPoints = arrayTotal[row];
}

- (void)pickerviewDidClickCancelButton:(DYAlertPickView *)pickerView {
    NSLog(@"Canceled");
    _btnRedeem.selected = false;
}

- (void)pickerviewDidClickSwitchButton:(DYAlertPickView *)pickerView switchButton:(UISwitch *)switchButton {
    NSLog(@"switch:%@",(switchButton.isOn?@"On":@"Off"));
}



- (IBAction)typeMethod:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=3;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedTypeLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             [_btn_type setTitle:selectedTypeLabelIndex forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                         }
                                         else{
                                             
                                             selectedTypeLabelIndex =selectedLabel;
                                             selectedTypeValueIndex=selectedValue;
                                             selectedTypePriceIndex=selectedPrice;
                                             
                                             [_btn_type setTitle:selectedTypeLabelIndex forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                             
                                             
                                         }
                                         
                                        int calcPrice=([selectedCutPriceIndex intValue]+[selectedWeightPriceIndex intValue]+[selectedTypePriceIndex intValue]);
                                         if(calcPrice==0)
                                         {
                                             
                                         }
                                         else
                                         {
                                             NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                             //_lbl_itemPrice.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                             [cartPriceArray replaceObjectAtIndex:currIndex withObject:strPrice];
                                             
                                         }
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
    

    
}
@end
