////
//  OrderInformationViewController.m
//  TSMC
//
//  Created by user on 07/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "OrderInformationViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "PaymentController.h"
#import "ViewController.h"
#import "CartController.h"
#import "thankYouViewController.h"
#import "MOSLPickerView.h"


@interface OrderInformationViewController ()
{
    NSMutableArray *cartIdArray,*cartCategoryIdArray,*cartNameArray,*cartImageUrlArray,*cartImageThumbArray,*cartImageSmallArray,*cartImageMainArray,*cartPriceArray,*cartQtyArray,*cartDescriptionArray,*cartShortDescArray,*cartFoodTypeArray,*cartTotalArray,*cartBrandArray,*cartWeightArray,*cartOriginArray,*cartOptionIdArray,*cartCutOptionIdArray,*cartCutOptionValueArray,*cartCutOptionPriceArray,*cartWeightOptionIdArray,*cartWeightOptionValueArray,*cartWeightOptionPriceArray,*cartTypeOptionIdArray,*cartTypeOptionValueArray,*cartTypeOptionPriceArray,*cartCutOptionLabelArray,*cartWeightOptionLabelArray,*cartTypeOptionLabelArray;
    
    NSMutableArray *cartAllCutLabelArray,*cartAllCutPriceArray,*cartAllCutValueArray,*cartAllWeightLabelArray,*cartAllWeightValueArray,*cartAllWeightPriceArray,*cartAllTypeLabelArray,*cartAllTypeValueArray,*cartAllTypePriceArray;
    
    
    NSString *isPayNowClicked;

    
    OfflineDb *object;
    NSMutableArray *nameArr,*foodValArr;
    NSMutableArray *prodIdKeyArr,*prodIdValArr,*prodQtyKeyArr,*prodQtyValArr,*prod1Idkey,*prod1IdVal,*prod1ValKey,*Prod1Value,*prod2Idkey,*prod2IdVal,*prod2ValKey,*Prod2Value,*prod3Idkey,*prod3IdVal,*prod3ValKey,*Prod3Value;
    
}
@end

@implementation OrderInformationViewController
@synthesize fetchedCoupon,fetchedCartTotal,isBillAddressSame,arrayRedeemOrder;

-(void)viewWillAppear:(BOOL)animated
{
    payTypeBtn.userInteractionEnabled = YES;
    self.navigationController.navigationBarHidden=NO;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self InitAttributeFrame];
    
    
    instructionTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Special Instructions" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    

    
    object=[[OfflineDb alloc] init];
    nameArr=[[NSMutableArray alloc] init];
    foodValArr=[[NSMutableArray alloc] init];

    fetchedCoupon=[[NSUserDefaults standardUserDefaults] valueForKey:@"COUPONCODE"];
    replaceSpaceFetchedCoupon=[fetchedCoupon stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"%@",replaceSpaceFetchedCoupon);
    
    
    
    [self getCartDetails];
    NSArray *data=[object fetch_customized_Data:@"42"];
    nameArr=[data valueForKey:@"cart_name"];
    foodValArr=[data valueForKey:@"cart_food_type"];
    if ([data count]>0) {
        foodtype=1;
        
    }
    else{
        foodtype=0;
        
    }
}

-(void)getCartDetails{
    NSArray *dataArray=[object fetch_CartDetails];
    NSLog(@"%@",dataArray);
    cartPriceArray=[[NSMutableArray alloc] init];
    
    
    cartIdArray=[dataArray valueForKey:@"cart_id"];
    cartCategoryIdArray=[dataArray valueForKey:@"cart_category_id"];
    cartNameArray=[dataArray valueForKey:@"cart_name"];
    cartImageUrlArray=[dataArray valueForKey:@"cart_image_url"];
    cartImageThumbArray=[dataArray valueForKey:@"cart_image_thumb"];
    cartImageSmallArray=[dataArray valueForKey:@"cart_image_small"];
    cartImageMainArray=[dataArray valueForKey:@"cart_image_main"];
    NSArray *crPriArr=[dataArray valueForKey:@"cart_price"];
    //    NSArray *totalArr=[dataArray valueForKey:@"cart_id"];
    cartQtyArray=[dataArray valueForKey:@"cart_qty"];
    
    cartDescriptionArray=[dataArray valueForKey:@"cart_description"];
    cartShortDescArray=[dataArray valueForKey:@"cart_short_desc"];
    cartFoodTypeArray=[dataArray valueForKey:@"cart_food_type"];
    NSArray *totalArr=[dataArray valueForKey:@"cart_total"];
    cartBrandArray=[dataArray valueForKey:@"cart_brand"];
    cartWeightArray=[dataArray valueForKey:@"cart_weight"];
    
    cartOriginArray=[dataArray valueForKey:@"cart_origin"];
    cartCutOptionIdArray=[dataArray valueForKey:@"cart_cut_option_id"];
    cartOptionIdArray=[dataArray valueForKey:@"cart_option_id"];
    cartCutOptionValueArray=[dataArray valueForKey:@"cart_cut_option_value"];
    cartCutOptionPriceArray=[dataArray valueForKey:@"cart_cut_option_price"];
    cartWeightOptionIdArray=[dataArray valueForKey:@"cart_weight_option_id"];
    
    
    cartWeightOptionValueArray=[dataArray valueForKey:@"cart_weight_option_value"];
    cartWeightOptionPriceArray=[dataArray valueForKey:@"cart_weight_option_price"];
    cartTypeOptionIdArray=[dataArray valueForKey:@"cart_type_option_id"];
    cartTypeOptionValueArray=[dataArray valueForKey:@"cart_type_option_value"];
    cartTypeOptionPriceArray=[dataArray valueForKey:@"cart_type_option_price"];
    cartCutOptionLabelArray=[dataArray valueForKey:@"cart_cut_option_label"];
    
    cartWeightOptionLabelArray=[dataArray valueForKey:@"cart_weight_option_label"];
    cartTypeOptionLabelArray=[dataArray valueForKey:@"cart_type_option_label"];
    cartAllCutValueArray=[dataArray valueForKey:@"cm_all_cut_values"];
    cartAllCutLabelArray=[dataArray valueForKey:@"cm_all_cut_labels"];
    cartAllCutPriceArray=[dataArray valueForKey:@"cm_all_cut_prices"];
    cartAllWeightValueArray=[dataArray valueForKey:@"cm_all_weight_values"];
    cartAllWeightLabelArray=[dataArray valueForKey:@"cm_all_weight_labels"];
    cartAllWeightPriceArray=[dataArray valueForKey:@"cm_all_weight_prices"];
    cartAllTypeValueArray=[dataArray valueForKey:@"cm_all_type_values"];
    cartAllTypeLabelArray=[dataArray valueForKey:@"cm_all_type_labels"];
    cartAllTypePriceArray=[dataArray valueForKey:@"cm_all_type_prices"];
    
    prodIdKeyArr=[[NSMutableArray alloc] init];
    prodIdValArr=[[NSMutableArray alloc] init];
    prodQtyKeyArr=[[NSMutableArray alloc] init];
    prodQtyValArr=[[NSMutableArray alloc] init];
    prod1Idkey=[[NSMutableArray alloc] init];
    prod1IdVal=[[NSMutableArray alloc] init];
    prod1ValKey=[[NSMutableArray alloc] init];
    Prod1Value=[[NSMutableArray alloc] init];
    
    prod2Idkey=[[NSMutableArray alloc] init];
    prod2IdVal=[[NSMutableArray alloc] init];
    prod2ValKey=[[NSMutableArray alloc] init];
    Prod2Value=[[NSMutableArray alloc] init];
    
    prod3Idkey=[[NSMutableArray alloc] init];
    prod3IdVal=[[NSMutableArray alloc] init];
    prod3ValKey=[[NSMutableArray alloc] init];
    Prod3Value=[[NSMutableArray alloc] init];
    
    
    for (int i=0; i<[cartIdArray count]; i++) {
        
        [prodIdKeyArr addObject:[NSString stringWithFormat:@"products[%d][id]",i]];
        [prodIdValArr addObject:[cartIdArray objectAtIndex:i]];
        
        [prodQtyKeyArr addObject:[NSString stringWithFormat:@"products[%d][qty]",i]];
        [prodQtyValArr addObject:[cartQtyArray objectAtIndex:i]];
        
        
        [prod1Idkey addObject:[NSString stringWithFormat:@"products[%d][option_id]",i]];
        [prod1IdVal addObject:[cartCutOptionIdArray objectAtIndex:i]];
        
        [prod1ValKey addObject:[NSString stringWithFormat:@"products[%d][option_value]",i]];
        [Prod1Value addObject:[cartCutOptionValueArray objectAtIndex:i]];
        
        [prod2Idkey addObject:[NSString stringWithFormat:@"products[%d][option_id1]",i]];
        [prod2IdVal addObject:[cartWeightOptionIdArray objectAtIndex:i]];
        
        [prod2ValKey addObject:[NSString stringWithFormat:@"products[%d][option_value1]",i]];
        [Prod2Value addObject:[cartWeightOptionValueArray objectAtIndex:i]];
        
        [prod3Idkey addObject:[NSString stringWithFormat:@"products[%d][option_id2]",i]];
        [prod3IdVal addObject:[cartTypeOptionIdArray objectAtIndex:i]];
        
        [prod3ValKey addObject:[NSString stringWithFormat:@"products[%d][option_value2]",i]];
        [Prod3Value addObject:[cartTypeOptionValueArray objectAtIndex:i]];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -Init Methods

-(void)InitAttributeFrame{
    
    [scrollView setContentSize:CGSizeMake(375, 800)];
    
    
    
//    titleLbl.backgroundColor=TopBarColor;
    self.title = @"Order".uppercaseString;
//    titleText.font=TopBarFont;
//    titleText.textColor=TopBarTextColor;
    
    [self initialViewConstraints];
    [self initialBtnTags];
}

-(void)initialViewConstraints{
    // shippingAddressViewHeight.constant=0;
    //    [self showShippingAddressViewMethod];
    
    ChkAPICall = @"getShippingCharge";
    
    [self getShippingInformationAPI];
    
    
    //    billingAddressViewHeight.constant=0;
    shippingInformationViewHeight.constant=100;
    deliveryInformationViewHeight.constant=0;
    paymentInformationViewHeight.constant=0;
    
    specialProductInfoLblHeight.constant=0;
    specialDateBtnHeight.constant=0;
    specialTimeBtnHeight.constant=0;
    dateLblHeight.constant=0;
    timeLblHeight.constant=0;
    
    deliveryInfoContinueBtn.hidden=YES;
    timeBtn.hidden=YES;
    [self viewHiddenMethod];
    
}
-(void)initialBtnTags
{
    //    shippingAddressBtn.tag=0;
    //    billingAddressBtn.tag=0;
    shippingInformationBtn.tag=0;
    deliveryInformationBtn.tag=0;
    paymentInformationBtn.tag=0;
    
    //    billToThisAddressBtn.tag=0;
    //    billToDifferentAddressBtn.tag=0;
    
    //    selectShippingAddressBtn.tag=0;
    //    selectBillingAddressBtn.tag=0;
}
-(void)viewHiddenMethod
{
    // shippingAddressView.hidden=YES;
    //    billingAddressView.hidden=YES;
    shippingInformationView.hidden=NO;
    deliveryInformationView.hidden=YES;
    paymentInformationView.hidden=YES;
    
    payTypeBtn.hidden=YES;
    
    specialDateBtn.hidden=YES;
    specialTimeBtn.hidden=YES;
    
    
}
#pragma mark -Button Clicked Methods

- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    // [self.navigationController popViewControllerAnimated:YES];
    /*if([_comingFrom isEqualToString:@"CARTVC"])
     {
     [self.navigationController popViewControllerAnimated:YES];
     }
     else
     {
     [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
     }*/
}

- (IBAction)optionBtnPress:(id)sender {
    
    NSArray * optionArray = [[NSArray alloc]initWithObjects:@"15.00", @"30.00",nil]; //NEED TO GIVE array
    
    
    [MOSLPickerView showPickerViewInView:self.view withDelegate:self withArray:optionArray withSelectedIndex:(int) self.selectedIndex completion:^(int selectedValue) {
            self.selectedIndex = selectedValue;
        
            shippingInfoChargeLbl.text = [[NSArray alloc] initWithArray:optionArray][(NSUInteger) selectedValue];
            
//            [self.selectOptionBtn setTitle:[[NSArray alloc] initWithArray:optionArray][(NSUInteger) selectedValue] forState:UIControlStateNormal];
//            [self.selectOptionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        
        }];
    
}
#pragma mark -Shipping addresses Methods
/*
 - (IBAction)shippingAddressBtnClicked:(id)sender
 {
 [timeBtn setTitle:@"Select Time Slot" forState:UIControlStateNormal];
 [dateBtn setTitle:@"Select Date" forState:UIControlStateNormal];
 [dtPicker removeFromSuperview];
 shippingInfoContinueBtn.tag=0;
 [self initialDeliveryView];
 
 payTypeBtn.hidden=YES;
 [self showShippingAddressViewMethod];
 [self initialBtnTags];
 }
 - (IBAction)billToThisAddressBtn_Clicked:(id)sender
 {
 if(selectShippingAddressBtn.tag==1)
 {
 [selectBillingAddressBtn setTitle:selectedLocationIndex forState:UIControlStateNormal];
 ChkAPICall = @"getShippingCharge";
 [self getShippingInformationAPI];
 billToThisAddressBtn.tag=1;
 [self showShippingInformationViewMethod];
 
 }
 else
 {
 NSLog(@"Select Address First");
 [self UIAlertViewControllerMethodTitle:@"" message:@"Select Address First"];
 }
 }
 
 - (IBAction)billToDifferentAddressBtn_Clicked:(id)sender
 {
 if(selectShippingAddressBtn.tag==1)
 {
 billToDifferentAddressBtn.tag=1;
 [self showBillingAddressViewMethod];
 }
 else
 {
 NSLog(@"Select Address First");
 [self UIAlertViewControllerMethodTitle:@"" message:@"Select Address First"];
 }
 }
 */
//- (IBAction)selectShippingAddressBtn_Clicked:(id)sender
//{
//
//    chkPickerView=@"shippingAddress";
////    selectShippingAddressBtn.tag=1;
//    [self getAddressesAPI];
//
//   /* if ([addressesIDArr count]>0)
//    {
//        selectShippingAddressBtn.tag=1;
//
//        [self addPickerView];
//
//    }
//    else
//    {
//        [ProgressHUD showError:@"No address found.Please add address." Interaction:NO];
//    }*/
//}
//
//- (IBAction)addShippingAddressBtn_Clicked:(id)sender
//{
//    [[NSUserDefaults standardUserDefaults]setObject:@"AddAddress" forKey:@"AddressView"];
//    [self loadStoryBoardWithIdentifier:@"UpdateAddressVC"];
//}

-(void)showShippingAddressViewMethod
{
    
    //    [shippingAddressBtn setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:48.0f/255.0f alpha:1.0f]];
    //    [billingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
    [shippingInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
    [deliveryInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
    [paymentInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    //    shippingAddressViewHeight.constant=85;
    //    billingAddressViewHeight.constant=0;
    shippingInformationViewHeight.constant=0;
    deliveryInformationViewHeight.constant=0;
    paymentInformationViewHeight.constant=0;
    
    //    shippingAddressView.hidden=NO;
    //    billingAddressView.hidden=YES;
    shippingInformationView.hidden=YES;
    deliveryInformationView.hidden=YES;
    paymentInformationView.hidden=YES;
    
    //    shippingAddressBtn.tag=1;
}
//#pragma mark -Billing Address Methods
//- (IBAction)billingAddressBtnClicked:(id)sender
//{
//    [timeBtn setTitle:@"Select Time Slot" forState:UIControlStateNormal];
//    [dateBtn setTitle:@"Select Date" forState:UIControlStateNormal];
//    [dtPicker removeFromSuperview];
//    shippingInfoContinueBtn.tag=0;
//    [self initialDeliveryView];
//
//    payTypeBtn.hidden=YES;
////    billToThisAddressBtn.tag=0;
//    [self showBillingAddressViewMethod];
//}
//- (IBAction)selectBillingAddressBtn_Clicked:(id)sender
//{
//    chkPickerView=@"billingAddress";
//     selectBillingAddressBtn.tag=2;
//    [self getAddressesAPI];
//
//    /*if ([addressesIDArr count]>0) {
//        selectBillingAddressBtn.tag=2;
//
//        [self addPickerView];
//
//    }
//    else
//    {
//        [ProgressHUD showError:@"No address found.Please add address." Interaction:NO];
//    }*/
//}

//- (IBAction)addBillingAddressBtn_Clicked:(id)sender
//{
//    [[NSUserDefaults standardUserDefaults]setObject:@"AddAddress" forKey:@"AddressView"];
//    [self loadStoryBoardWithIdentifier:@"UpdateAddressVC"];
//
//}

//-(void)showBillingAddressViewMethod
//{
//    if(billToDifferentAddressBtn.tag==1)
//    {
//        [billingAddressBtn setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:48.0f/255.0f alpha:1.0f]];
//        [shippingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
//
//        billingAddressViewHeight.constant=40;
//        shippingAddressViewHeight.constant=0;
//        shippingInformationViewHeight.constant=0;
//        deliveryInformationViewHeight.constant=0;
//        paymentInformationViewHeight.constant=0;
//
//        shippingAddressView.hidden=YES;
//        billingAddressView.hidden=NO;
//        shippingInformationView.hidden=YES;
//        deliveryInformationView.hidden=YES;
//        paymentInformationView.hidden=YES;
//
//        billingAddressBtn.tag=1;
//        //billToDifferentAddressBtn.tag=0;
//    }
//
//}
#pragma mark -shipping Information Methods
- (IBAction)shippingInformationBtnClicked:(id)sender
{
    [timeBtn setTitle:@"Select Time Slot" forState:UIControlStateNormal];
    [dateBtn setTitle:@"Select Date" forState:UIControlStateNormal];
    [dtPicker removeFromSuperview];
    shippingInfoContinueBtn.tag=0;
    [self initialDeliveryView];
    
    payTypeBtn.hidden=YES;
    [self showShippingInformationViewMethod];
    shippingInfoContinueBtn.tag=0;
}

- (IBAction)shippingInfoContinueBtn_Clicked:(id)sender{
    shippingInfoContinueBtn.tag=1;
    [self showDeliveryInformationViewMethod];
}

-(void)showShippingInformationViewMethod    {
    //    if(billToThisAddressBtn.tag==1)
    //    {
    // shippingInfoChargeLbl.text=shippingChrages;
    [shippingInformationBtn setBackgroundColor:TopBarTextColor];
    [shippingInformationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [deliveryInformationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [paymentInformationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    //        [billingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
    //        [shippingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
    [deliveryInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
    [paymentInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    shippingInformationViewHeight.constant=100;
    //        billingAddressViewHeight.constant=0;
    //        shippingAddressViewHeight.constant=0;
    deliveryInformationViewHeight.constant=0;
    paymentInformationViewHeight.constant=0;
    
    //        shippingAddressView.hidden=YES;
    //        billingAddressView.hidden=YES;
    shippingInformationView.hidden=NO;
    deliveryInformationView.hidden=YES;
    paymentInformationView.hidden=YES;
    
    shippingInformationBtn.tag=1;
    
    //         billToDifferentAddressBtn.tag=1;
    //   }
    
}
#pragma mark -Delivery Information Methods
- (IBAction)deliveryInformationBtnClicked:(id)sender
{
    payTypeBtn.hidden=YES;
    [self showDeliveryInformationViewMethod];
}
- (IBAction)dateBtn_Clicked:(id)sender
{
    //call datepicker
    dateBtnClick=@"dateBtn";
    [timeBtn setTitle:@"Select Time Slot" forState:UIControlStateNormal];
    ChkAPICall = @"getDeliveryInfo";
    [self getShippingInformationAPI];
    
    dateBtn.tag=1;
    
    [self initialDeliveryView];
    
}
-(void)initialDeliveryView
{
    SpecialProductInfoLbl.text=[NSString stringWithFormat:@""];
    specialProductInfoLblHeight.constant=0;
    specialDateBtnHeight.constant=0;
    specialTimeBtnHeight.constant=0;
    specialTimeBtn.hidden=YES;
    dateLblHeight.constant=0;
    timeLblHeight.constant=0;
    
    deliveryInfoContinueBtn.hidden=YES;
    if(shippingInfoContinueBtn.tag==1)
    {
//        deliveryInformationViewHeight.constant=130; CHANGED
        deliveryInformationViewHeight.constant=0;
    }
    else
    {
        deliveryInformationViewHeight.constant=0;
    }
    specialDateBtn.hidden=YES;
    specialTimeBtn.tag=0;
    timeBtn.hidden=YES;
    specialDateBtn.tag=0;
    deliveryInfoContinueBtn.tag=0;
    
    [specialDateBtn setTitle:@"Select Date" forState:UIControlStateNormal];
}
-(void)showCustomProductIndeliveryView
{
    
}
- (IBAction)timeBtnClicked:(id)sender
{
    chkPickerView=@"TimeSlot";
    if(dateBtn.tag==1)
    {
        [self addPickerView];
        //select time slot view
    }
    timeBtn.tag=1;
}

- (IBAction)deliveryInfoContinueBtn_Clicked:(id)sender
{
    deliveryInfoContinueBtn.tag=1;
    [self showPaymentInformationViewMethod];
}

- (IBAction)specialDateBtn_Clicked:(id)sender
{
    dateBtnClick=@"specialDateBtn";
    [specialTimeBtn setTitle:@"Select Time Slot" forState:UIControlStateNormal];
    //[self DatePickerFunction];
    [self addDatePicker];
    specialDateBtn.tag=1;
    
}

- (IBAction)specialTimeBtn_Clicked:(id)sender
{
    chkPickerView=@"SpecialTimeSlot";
    if(specialDateBtn.tag==1)
    {
        [self addPickerView];
        //select time slot view
    }
    specialTimeBtn.tag=1;
}
-(void)showDeliveryInformationViewMethod
{
    if(shippingInfoContinueBtn.tag==1)
    {
        [deliveryInformationBtn setBackgroundColor:TopBarTextColor];
        [deliveryInformationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [shippingInformationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [paymentInformationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        //        [billingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
        [shippingInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
        //        [shippingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
        [paymentInformationBtn setBackgroundColor:TopBarTextColor];
        
        
        /* if(specialTimeBtn.tag==1)
         {
         deliveryInformationViewHeight.constant=265;
         deliveryInfoContinueBtn.tag=0;
         }
         else
         {
         if(deliveryInfoContinueBtn.tag==1)
         {
         //deliveryInformationViewHeight.constant=165;
         deliveryInformationViewHeight.constant=235;
         deliveryInfoContinueBtn.tag=0;
         }
         else
         {
         deliveryInformationViewHeight.constant=130;
         }
         }*/
        if(deliveryInfoContinueBtn.tag==1){
            if(foodtype && [finalDateString1 isEqualToString:finalDateString2]){
//                deliveryInformationViewHeight.constant=300;CHANGED
                deliveryInformationViewHeight.constant=0;
                deliveryInfoContinueBtn.tag=0;
            }
            else{
                //deliveryInformationViewHeight.constant=200;CHANGED
                deliveryInformationViewHeight.constant=0;
                deliveryInfoContinueBtn.tag=0;
            }
        }else{
            //deliveryInformationViewHeight.constant=130; //CHANGED
            deliveryInformationViewHeight.constant=0;
            
        }
        
        
        //        shippingAddressViewHeight.constant=0;
        //        billingAddressViewHeight.constant=0;
        shippingInformationViewHeight.constant=0;
        paymentInformationViewHeight.constant=0;
        
        //        shippingAddressView.hidden=YES;
        //        billingAddressView.hidden=YES;
        shippingInformationView.hidden=YES;
        deliveryInformationView.hidden=YES;
        paymentInformationView.hidden=NO;
        
    }
}

#pragma mark -Payment Information Methods
- (IBAction)paymentInformationBtnClicked:(id)sender
{
    [self showPaymentInformationViewMethod];
}
-(void)showPaymentInformationViewMethod
{
    if(deliveryInfoContinueBtn.tag==1)
    {
        [paymentInformationBtn setBackgroundColor:TopBarTextColor];
        [paymentInformationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [shippingInformationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [deliveryInformationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        //        [billingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
        [shippingInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
        [deliveryInformationBtn setBackgroundColor:[UIColor lightGrayColor]];
        //        [shippingAddressBtn setBackgroundColor:[UIColor lightGrayColor]];
        
        paymentInformationViewHeight.constant=90;
        //        shippingAddressViewHeight.constant=0;
        //        billingAddressViewHeight.constant=0;
        shippingInformationViewHeight.constant=0;
        deliveryInformationViewHeight.constant=0;
        
        //        shippingAddressView.hidden=YES;
        //        billingAddressView.hidden=YES;
        shippingInformationView.hidden=YES;
        deliveryInformationView.hidden=YES;
        paymentInformationView.hidden=NO;
        
        
        deliveryInfoContinueBtn.tag=0;
        //[self initialBtnTags];
        
    }
    
}
- (IBAction)onlinePaymentBtnClicked:(id)sender
{
    onlinePaymentBtn.tag=1;
    cashOnDeliveryBtn.tag=0;
    payTypeBtn.hidden=NO;
    selectedPayType=@"1";
    [payTypeBtn setTitle:@"Pay Now" forState:UIControlStateNormal];
    
    onlinePaymentImgView.image=[UIImage imageNamed:@"RadioButton.png"];
    CODImgView.image=[UIImage imageNamed:@"RadioCircle.png"];
}

- (IBAction)cashOnDeliveryBtn_Clicked:(id)sender
{
    cashOnDeliveryBtn.tag=1;
    onlinePaymentBtn.tag=0;
    payTypeBtn.hidden=NO;
    selectedPayType=@"0";
    
    onlinePaymentImgView.image=[UIImage imageNamed:@"RadioCircle.png"];
    CODImgView.image=[UIImage imageNamed:@"RadioButton.png"];
    
    [payTypeBtn setTitle:@"Confirm Order" forState:UIControlStateNormal];
}

- (IBAction)paymentTypeBtn_Clicked:(id)sender
{    
    if([isPayNowClicked isEqualToString:@"YES"])
    {
    }
    else
    {
        isPayNowClicked = @"YES";
        payTypeBtn.userInteractionEnabled = YES;
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait while submitting order.";

        
        [self submitOrderData];
    }
    
}
#pragma mark Paynow API

-(void)submitOrderData
{
    
    
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        HelperClass *s=[[HelperClass alloc]init];
      
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            [HUD hide:YES];
            
            UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Connection Lost" message:@"Please check your internet connection!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
            
            payTypeBtn.userInteractionEnabled = YES;
            isPayNowClicked = @"NO";

            [alrt addAction:ok];
            [self presentViewController:alrt animated:YES completion:nil];
            
            
        }
        else
        {
            @try
            {
//                HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                HUD.dimBackground = YES;
//                HUD.labelText = @"Please wait while submitting order.";
               
                
                NSArray *shipData=[object fetch_ShippingOrderValue];
                NSArray *shipVal=[shipData valueForKey:@"shipping_value"];
                
                NSString *userid=[[NSUserDefaults standardUserDefaults] valueForKey:USERID];
                //                NSString *shippingaddress_id=selectedShippingAddrId;
                NSString *shippingaddress_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"shipid"];
                
                if ([isBillAddressSame isEqualToString:@"YES"]) {
                    selectedBillingAddrId = shippingaddress_id;
                }
                else{
                    selectedBillingAddrId = [[NSUserDefaults standardUserDefaults] valueForKey:@"billingid"];
                }
                //if (selectedBillingAddrId == nil || selectedBillingAddrId.length<=0)
                //{
                //selectedBillingAddrId=selectedShippingAddrId;
                //}
                NSString *billingaddress_id=selectedBillingAddrId;
                
                NSString *shippingrate=[shipVal objectAtIndex:0];
                NSString *comment=[NSString stringWithFormat:@"%@-Mobile Order",instructionTF.text];
                NSString *delivery_date = dateBtn.titleLabel.text;
                NSString *timeslot = timeBtn.titleLabel.text;
                NSString *payment_type=selectedPayType;
                NSString *custom_delivery_date,*custom_timeslot;
                
                
                NSString * strHashTagPoints = [[arrayRedeemOrder valueForKey:@"points"] stringValue];
                NSString * strHashTagAmount = [[arrayRedeemOrder valueForKey:@"value"]stringValue];
                NSString * strHashTagId = [[arrayRedeemOrder valueForKey:@"id"]stringValue];
                NSString * strHashTagName = [arrayRedeemOrder valueForKey:@"name"];
                
                
                NSMutableDictionary *dict=[NSMutableDictionary dictionary];
                //                    dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:CustId,@"customer_id",couponTxt,@"coupon_code", nil];
                
                
                if ([strHashTagPoints isEqualToString:@""] || strHashTagPoints == nil){
                    [dict setObject:@"" forKey:@"hashtag_points"];
                }else{
                    [dict setObject:strHashTagPoints forKey:@"hashtag_points"];
                }
                
                if ([strHashTagAmount isEqualToString:@""] || strHashTagAmount == nil){
                    [dict setObject:@"" forKey:@"hashtag_amount"];
                }else{
                    [dict setObject:strHashTagAmount forKey:@"hashtag_amount"];
                }
                
                if ([strHashTagId isEqualToString:@""] || strHashTagId == nil){
                    [dict setObject:@"" forKey:@"hashtag_id"];
                }else{
                    [dict setObject:strHashTagId forKey:@"hashtag_id"];
                }
                
                if ([strHashTagName isEqualToString:@""] || strHashTagName == nil){
                    [dict setObject:@"" forKey:@"hashtag_name"];
                }else{
                    [dict setObject:strHashTagName forKey:@"hashtag_name"];
                }
                
                
                
                [dict setObject:userid forKey:@"customer_id"];
                
                if(replaceSpaceFetchedCoupon == nil){
                    NSLog(@"No Coupon");
                    
                }else{
                    [dict setObject:replaceSpaceFetchedCoupon forKey:@"coupon_code"];
                }
                
                if (specialDateBtn.tag == 1){
                    custom_delivery_date=specialDateBtn.titleLabel.text;
                    [dict setObject:custom_delivery_date forKey:@"custom_delivery_date"];
                    
                }
                
                if (specialTimeBtn.tag == 1){
                    custom_timeslot=specialTimeBtn.titleLabel.text;
                    [dict setObject:custom_timeslot forKey:@"custom_timeslot"];
                    
                }
                
                if(shippingaddress_id != nil){
                    [dict setObject:shippingaddress_id forKey:@"shippingaddress_id"];
                }
                
                if(billingaddress_id != nil){
                    [dict setObject:billingaddress_id forKey:@"billingaddress_id"];
                }
                
                if(shippingrate != nil){
                    [dict setObject:shippingrate forKey:@"shipping_method"];
                }
                
//                if(shippingrate != nil){
//                    [dict setObject:shippingrate forKey:@"shipping_method"];//NEW
//                }
//
//                if(shippingrate != nil){
//                    [dict setObject:shippingrate forKey:@"payment_method"];//NEW
//                }
//
//                [dict setObject:@"Kinnar" forKey:@"firstname"];
//                [dict setObject:@"Parikh" forKey:@"lastname"];
//
                
                if(comment!=nil){
                    [dict setObject:comment forKey:@"comment"];
                }
                
                if(delivery_date!=nil){
                    [dict setObject:delivery_date forKey:@"delivery_date"];
                }
                
                if(timeslot!=nil){
                    [dict setObject:timeslot forKey:@"timeslot"];
                }
                
                if(payment_type!=nil){
                    [dict setObject:payment_type forKey:@"payment_type"];
                }
                
                for (int j=0; j<[prodIdKeyArr count]; j++)
                {
                    [dict setObject:[prodIdValArr objectAtIndex:j] forKey:[prodIdKeyArr objectAtIndex:j]];
                    [dict setObject:[prodQtyValArr objectAtIndex:j] forKey:[prodQtyKeyArr objectAtIndex:j]];
                    
                    if (!([[prod1IdVal objectAtIndex:j] isEqualToString:@"0"]))
                    {
                        [dict setObject:[prod1IdVal objectAtIndex:j] forKey:[prod1Idkey objectAtIndex:j]];
                        [dict setObject:[Prod1Value objectAtIndex:j] forKey:[prod1ValKey objectAtIndex:j]];
                        
                    }
                    if (!([[prod2IdVal objectAtIndex:j] isEqualToString:@"0"]))
                    {
                        [dict setObject:[prod2IdVal objectAtIndex:j] forKey:[prod2Idkey objectAtIndex:j]];
                        [dict setObject:[Prod2Value objectAtIndex:j] forKey:[prod2ValKey objectAtIndex:j]];
                        
                    }
                    if (!([[prod3IdVal objectAtIndex:j] isEqualToString:@"0"]))
                    {
                        [dict setObject:[prod3IdVal objectAtIndex:j] forKey:[prod3Idkey objectAtIndex:j]];
                        [dict setObject:[Prod3Value objectAtIndex:j] forKey:[prod3ValKey objectAtIndex:j]];
                        
                    }
                }
                
//                NSString *url=[NSString stringWithFormat:@"%@","Google.Com"];
                NSString *url=[NSString stringWithFormat:@"%@",API_POST_ORDER];
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:dict];

                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                if ([getJsonData count] >0)
                {
                    if (success)
                    {
                        
                        NSString *orderId=[getJsonData valueForKey:@"order_id"];
                        
                        if ([payment_type isEqualToString:@"1"])
                        {
                            payTypeBtn.userInteractionEnabled = NO;
                            isPayNowClicked = @"NO";

                            dispatch_async(dispatch_get_main_queue(), ^{
                                HUD.hidden=YES;
                                PaymentController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"PaymentController"];
                                viw.fetchedOrderId=orderId;
                                viw.orderparaDict = dict;
                                [[NSUserDefaults standardUserDefaults]setObject:orderId forKey:@"ORDERID"];

                                [self.navigationController pushViewController:viw animated:YES];
                            });
                            
                            
                        }
                        else
                        {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                payTypeBtn.userInteractionEnabled = NO;
                                isPayNowClicked = @"NO";

                                [object Delete_Cart_Data:@"ALL" AndCutLab:@"" AndWeightlab:@"" AndTypeLab:@""];
                                NSString *custOrderID=[getJsonData objectForKey:@"order_id"];
                                
                                
                                HUD.hidden=YES;
                            thankYouViewController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"thankYouViewController"];
                            viw.orderIDStr = custOrderID;
                            [self.navigationController pushViewController:viw animated:YES];
                                
                            });
                            /*NSString *message=[NSString stringWithFormat:@"Order placed successfully.\n Order Id: %@",custOrderID];
                            [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"ShowCustomiseNote"];
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@""
                                                          message:message
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action) {
                                                                           //Do Some action here
                                                                           
                                                                           ViewController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                                           
                                                                           [self.navigationController pushViewController:viw animated:YES];
                                                                           
                                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                                           
                                                                       }];
                            
                            [alert addAction:ok];
                            
                            [self presentViewController:alert animated:YES completion:nil];*/
                            
                            
                        }
                    }else{
                       
                        dispatch_async(dispatch_get_main_queue(), ^{
                            payTypeBtn.userInteractionEnabled = YES;
                                                   isPayNowClicked = @"NO";

                                                   NSString *msg=[getJsonData valueForKey:@"message"];
                                                   [ProgressHUD showError:msg  Interaction:NO];
                        HUD.hidden=YES;
                        });
                        
                    }
                    selectedBillingAddrId=nil;
                }
                else
                {
                    
                     dispatch_async(dispatch_get_main_queue(), ^{
                         payTypeBtn.userInteractionEnabled = YES;
                         isPayNowClicked = @"NO";

                                           HUD.hidden=YES;
                         [ProgressHUD showError:@"Order not placed. Please Try again!" Interaction:NO];
                                           });
                    
                    selectedBillingAddrId=nil;
                }
            }@catch (NSException *exception)
            {
                
                 dispatch_async(dispatch_get_main_queue(), ^{
                     payTypeBtn.userInteractionEnabled = YES;
                     isPayNowClicked = @"NO";

                                       HUD.hidden=YES;
                     [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                       });
                
                
            }
        }
    });
}

#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -Custom Picker View
-(void)addPickerView
{
    userPickerView.hidden=NO;
    alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    userPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    userPickerView.backgroundColor=[UIColor whiteColor];
    [userPickerView setDataSource: self];
    [userPickerView setDelegate: self];
    NSUInteger i=1;
    userPickerView.tag=i;
    userPickerView.showsSelectionIndicator = YES;
    [alertController.view addSubview:userPickerView];
    
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     @try
                                     {
                                         if (selectedLocationIndex.length>0 || !([selectedLocationIndex isEqualToString:@"0"]))
                                         {
                                             if([chkPickerView isEqualToString:@"shippingAddress"])//selectShippingAddressBtn.tag==1)
                                             {
                                                 //                    if(selectedLocationIndex!=NULL)
                                                 //                    {
                                                 //                        [selectShippingAddressBtn setTitle:selectedLocationIndex forState:UIControlStateNormal];
                                                 //                        selectShippingAddressBtn.tag=1;
                                                 //
                                                 //                    }
                                                 //                    else
                                                 //                    {
                                                 //                        [selectShippingAddressBtn setTitle:@"Select Shipping Address" forState:UIControlStateNormal];
                                                 //                    }
                                             }
                                             if([chkPickerView isEqualToString:@"billingAddress"])//selectBillingAddressBtn.tag==1)
                                             {
                                                 if(selectedLocationIndex!=NULL)
                                                 {
                                                     //                    ChkAPICall = @"getShippingCharge";
                                                     //                    [selectBillingAddressBtn setTitle:selectedLocationIndex forState:UIControlStateNormal];
                                                     //                    [self getShippingInformationAPI];
                                                     //                    billToThisAddressBtn.tag=1;
                                                 }
                                                 else
                                                 {
                                                     //                    [selectBillingAddressBtn setTitle:@"Select Billing Address" forState:UIControlStateNormal];
                                                 }
                                                 
                                             }
                                             if([chkPickerView isEqualToString:@"TimeSlot"])
                                             {
                                                 if(selectedLocationIndex1 == nil)
                                                 {
                                                     [timeBtn setTitle:[timeArr objectAtIndex:0] forState:UIControlStateNormal];
                                                 }
                                                 else
                                                 {
                                                     [timeBtn setTitle:selectedLocationIndex1 forState:UIControlStateNormal];
                                                 }
                                                 // [timeBtn setTitle:@"4.00pm - 6.00pm" forState:UIControlStateNormal];
                                                 
                                                 NSString *cust_name=[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userFName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"userLName"]];
                                                 //                NSString *product_names=@"xyx abc";
                                                 
                                                 
                                                 /* if(foodtype && [finalDateString1 isEqualToString:finalDateString2])
                                                  {
                                                  NSString *comSepStr=[nameArr componentsJoinedByString:@","];
                                                  SpecialProductInfoLbl.text=[NSString stringWithFormat:@"Dear %@,\nYou have %@ in your cart.Availability of these products can be possible in next day.Please select different delivery date & Time for mentioned products.",cust_name,comSepStr];
                                                  
                                                  [SpecialProductInfoLbl sizeToFit];
                                                  CGSize constraint = CGSizeMake(SpecialProductInfoLbl.frame.size.width, CGFLOAT_MAX);
                                                  CGSize size;
                                                  
                                                  NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
                                                  CGSize boundingBox = [SpecialProductInfoLbl.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:SpecialProductInfoLbl.font}
                                                  context:context].size;
                                                  
                                                  size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
                                                  
                                                  
                                                  specialProductInfoLblHeight.constant=size.height;;
                                                  
                                                  //                    specialDateBtnHeight.constant=22;
                                                  //                    specialTimeBtnHeight.constant=22;
                                                  //                    dateLblHeight.constant=22;
                                                  //                    timeLblHeight.constant=22;
                                                  //
                                                  //                    if(specialTimeBtn.tag==1)
                                                  //                    {
                                                  //                        deliveryInformationViewHeight.constant=273;
                                                  //                    }
                                                  //                    else
                                                  //                    {
                                                  //                        deliveryInformationViewHeight.constant=235;
                                                  //                    }
                                                  //                    specialDateBtn.hidden=NO;
                                                  
                                                  deliveryInfoContinueBtn.hidden=NO;
                                                  deliveryInformationViewHeight.constant=235;
                                                  
                                                  }
                                                  else
                                                  {
                                                  if(specialTimeBtn.tag==1)
                                                  {
                                                  deliveryInformationViewHeight.constant=273;
                                                  }
                                                  else
                                                  {
                                                  //deliveryInformationViewHeight.constant=160;
                                                  deliveryInformationViewHeight.constant=160;
                                                  }
                                                  deliveryInfoContinueBtn.hidden=NO;
                                                  }
                                                  */
                                                 if(foodtype && [finalDateString1 isEqualToString:finalDateString2])
                                                 {
                                                     /*SpecialProductInfoLbl.text=[NSString stringWithFormat:@"Note: Kindly note that you have chosen one or more items in your cart that may not be available in your slot selected. Our customer care team shall get back to you in 60mins would there be any availability concerns with respect to the items ordered by you."];
                                                     
                                                     [SpecialProductInfoLbl sizeToFit];
                                                     CGSize constraint = CGSizeMake(SpecialProductInfoLbl.frame.size.width, CGFLOAT_MAX);
                                                     CGSize size;
                                                     
                                                     NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
                                                     CGSize boundingBox = [SpecialProductInfoLbl.text boundingRectWithSize:constraint
                                                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                                attributes:@{NSFontAttributeName:SpecialProductInfoLbl.font}
                                                                                                                   context:context].size;
                                                     
                                                     size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
                                                     
                                                     
                                                     specialProductInfoLblHeight.constant=size.height;;
                                                      deliveryInformationViewHeight.constant=235;
                                                     */
                                                     deliveryInfoContinueBtn.hidden=YES;//CHANGEF
                                                     deliveryInformationViewHeight.constant= 0; // 180; CHANGED
                                                     
                                                 }
                                                 else
                                                 {
                                                     if(specialTimeBtn.tag==1)
                                                     {
                                                         deliveryInformationViewHeight.constant= 0; //300;CHANGED
                                                     }
                                                     else
                                                     {
                                                         //deliveryInformationViewHeight.constant=160;
                                                         deliveryInformationViewHeight.constant = 0;//180;CHANGED
                                                     }
                                                     deliveryInfoContinueBtn.hidden=YES;//CHANGED
                                                     
                                                 }
                                                 
                                                 
                                                 //                deliveryInformationViewHeight.constant=160;
                                                 //                deliveryInfoContinueBtn.hidden=NO;
                                                 
                                             }
                                             if([chkPickerView isEqualToString:@"SpecialTimeSlot"])
                                             {
                                                 if(selectedLocationIndex2 == nil)
                                                 {
                                                     [specialTimeBtn setTitle:[specialTimeArr objectAtIndex:0] forState:UIControlStateNormal];
                                                 }
                                                 else
                                                 {
                                                     [specialTimeBtn setTitle:selectedLocationIndex2 forState:UIControlStateNormal];
                                                 }
                                                 
                                                 
                                                 deliveryInfoContinueBtn.hidden=YES; //CHANGED
                                                 deliveryInformationViewHeight.constant= 0;//270;CHANGED
                                             }
                                             
                                             // selectedLocationIndex=@"0";
                                             
                                         }
                                         else
                                         {
                                         }
                                         
                                     } @catch (NSException *exception)
                                     {
                                         //[ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                     }
                                 }];
        action;
    })];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                          }];

    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController  animated:YES completion:nil];
}
-(void)addDatePicker
{
    dtPicker.hidden=NO;
    alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    dtPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    dtPicker.datePickerMode=UIDatePickerModeDate;
    dtPicker.backgroundColor=[UIColor whiteColor];
    [dtPicker setValue:[UIColor blackColor] forKey:@"textColor"];

    NSUInteger i=1;
    dtPicker.tag=i;
    
    NSDate *currentTime = [NSDate date];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH"];
    
    NSString *hourCountString = [timeFormatter stringFromDate:currentTime];
    
    int hourCountInt = [hourCountString intValue];
    
    NSLog(@"hourCountInt=%d",hourCountInt);
    //time between 12PM - 12AM.
    /*if(hourCountInt >= 12 && hourCountInt < 23)
     {
     NSLog(@"display night-time images");
     
     NSString *dateString = [deliveryDateCustomproductArr objectAtIndex:0];
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
     
     
     [dtPicker setMinimumDate:orignalDate];
     dtPicker.date = orignalDate;
     }*/
    
    //new code
    if([dateBtnClick isEqualToString:@"dateBtn"])
    {
        if([[deliveryTimeSlotArr objectAtIndex:0] count]>0)
        {
            NSString *dateString = [deliveryDateArr objectAtIndex:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
            
            
            [dtPicker setMinimumDate:orignalDate];
            dtPicker.date = orignalDate;
        }
        else if([deliveryTimeSlotCustomArr count]>0)
        {
            NSString *dateString = [deliveryDateCustomproductArr objectAtIndex:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
            
            
            [dtPicker setMinimumDate:orignalDate];
            dtPicker.date = orignalDate;
        }
    }
    else if ([dateBtnClick isEqualToString:@"specialDateBtn"])
    {
        if([deliveryTimeSlotCustomArr count]>0)
        {
            NSString *dateString = [deliveryDateCustomproductArr objectAtIndex:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
            
            
            [dtPicker setMinimumDate:orignalDate];
            dtPicker.date = orignalDate;
        }
        
    }
    
    
    
    
    
    /*   //current code
     if(foodtype && [[deliveryTimeSlotArr objectAtIndex:0] count]==0)
     {
     NSLog(@"display night-time images");
     
     NSString *dateString = [deliveryDateCustomproductArr objectAtIndex:0];
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
     
     
     [dtPicker setMinimumDate:orignalDate];
     dtPicker.date = orignalDate;
     }
     else
     {
     if([dateBtnClick isEqualToString:@"dateBtn"])
     {
     [dtPicker setMinimumDate:[NSDate date]];
     dtPicker.date = [NSDate date];
     }
     else if ([dateBtnClick isEqualToString:@"specialDateBtn"])
     {
     NSString *dateString = [deliveryDateCustomproductArr objectAtIndex:0];
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
     
     
     [dtPicker setMinimumDate:orignalDate];
     dtPicker.date = orignalDate;
     }
     
     }*/
    
    [alertController.view addSubview:dtPicker];
    
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     @try
                                     {
                                         timeBtn.hidden=NO;
                                         dtPicker.hidden=YES;
                                         [dtPicker removeFromSuperview];
                                         
                                         if([dateBtnClick isEqualToString:@"dateBtn"])
                                         {
                                             NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
                                             [formatter setDateFormat:@"dd-MM-yyyy"];
                                             formatter.dateStyle = NSDateFormatterMediumStyle;
                                             NSDate *date1=dtPicker.date;
                                             [formatter setDateFormat:@"dd-MM-yyyy"];
                                             finalDateString1 = [formatter stringFromDate:date1];
                                             
                                             NSString *dateString = [deliveryDateArr objectAtIndex:0];
                                             NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                             [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                                             NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
                                             [dateFormatter setDateFormat:@"dd-MM-yyyy"];
                                             finalDateString2 = [dateFormatter stringFromDate:orignalDate];
                                             
                                             [dateBtn setTitle:[NSString stringWithFormat:@"%@",finalDateString1] forState:UIControlStateNormal];
                                             
                                             //new code
                                             
                                             if(deliveryTimeSlotArr.count>0 && [finalDateString1 isEqualToString:finalDateString2])
                                             {
                                                 timeArr=[deliveryTimeSlotArr objectAtIndex:0];
                                             }
                                             else if(deliveryTimeSlotCustomArr.count>0)
                                             {
                                                 timeArr=[deliveryTimeSlotCustomArr objectAtIndex:0];
                                             }
                                             
                                             
                                             
                                             /*  //current code
                                              
                                              if(foodtype && [finalDateString1 isEqualToString:finalDateString2])
                                              {
                                              if(deliveryTimeSlotArr.count>0)
                                              {
                                              timeArr=[deliveryTimeSlotArr objectAtIndex:0];
                                              }
                                              
                                              }
                                              else
                                              {
                                              if(deliveryTimeSlotArr.count>0)
                                              {
                                              timeArr=[deliveryTimeSlotArr objectAtIndex:1];
                                              }
                                              }*/
                                         }
                                         else if ([dateBtnClick isEqualToString:@"specialDateBtn"])
                                         {
                                             NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
                                             [formatter setDateFormat:@"dd-MM-yyyy"];
                                             formatter.dateStyle = NSDateFormatterMediumStyle;
                                             
                                             timeBtn.hidden=NO;
                                             dtPicker.hidden=YES;
                                             [dtPicker removeFromSuperview];
                                             
                                             NSDate *date1=dtPicker.date;
                                             [formatter setDateFormat:@"dd-MM-yyyy"];
                                             NSString *finalDateString3 = [formatter stringFromDate:date1];
                                             
                                             [specialDateBtn setTitle:[NSString stringWithFormat:@"%@",finalDateString3] forState:UIControlStateNormal];
                                             specialTimeBtn.hidden=NO;
                                             
                                             if(deliveryTimeSlotCustomArr.count>0)
                                             {
                                                 specialTimeArr=[deliveryTimeSlotCustomArr objectAtIndex:0];
                                             }
                                             
                                         }
                                         
                                     } @catch (NSException *exception)
                                     {
                                         //[ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                     }
                                 }];
        action;
    })];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                           }];
    
    [alertController addAction:cancelAction];

    [self presentViewController:alertController  animated:YES completion:nil];
}


#pragma mark - API Methods
-(void)getAddressesAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    ChkAPICall = @"getAddress";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            //[HUD hide:YES];
            
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }
        else
        {
            @try
            {
                NSString *userId=[[NSUserDefaults standardUserDefaults] objectForKey:USERID];
                
                NSString *url=API_GET_ADDRESSES;
                
                NSMutableDictionary *parameterDict;
                
                parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:userId,@"customer_id", nil];
                
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                
                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    addressesArr=[[NSMutableArray alloc]init];
                    addressesArr2=[[NSMutableArray alloc]init];
                    addressesIDArr=[[NSMutableArray alloc]init];
                    decodedAddressesArr=[[NSMutableArray alloc]init];
                    streerAddressArr=[[NSMutableArray alloc]init];
                    decodeStreetArr=[[NSMutableArray alloc]init];
                    finalAddressArr=[[NSMutableArray alloc]init];
                    telephoneArr=[[NSMutableArray alloc]init];
                    cityArr=[[NSMutableArray alloc]init];
                    ChkDefaultArr=[[NSMutableArray alloc]init];
                    
                    if (success)
                    {
                        [addressesArr addObject:[getJsonData valueForKey:@"addresses"]];
                        
                        for(int i=0;i<addressesArr.count;i++)
                        {
                            for (int j=0; j<[[addressesArr objectAtIndex:i] count]; j++)
                            {
                                [addressesArr2 addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"postcode"]];
                                [streerAddressArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"street"]];
                                [addressesIDArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"entity_id"]];
                                [telephoneArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"telephone"]];
                                [cityArr addObject:[[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"addresses"] valueForKey:@"city"]];
                                
                                [ChkDefaultArr addObject:[[[addressesArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"default"]];
                            }
                            
                        }
                        // NSLog(@"Default Address:%@", ChkDefaultArr);
                        
                        
                        //Decode postcode address array
                        for(int i=0;i<addressesArr2.count;i++)
                        {
                            NSString *encoded = [addressesArr2 objectAtIndex:i];
                            NSString *decoded = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)encoded, CFSTR(""), kCFStringEncodingUTF8);
                            [decodedAddressesArr addObject:decoded];
                            
                        }
                        //decode street array
                        for(int i=0;i<streerAddressArr.count;i++)
                        {
                            NSString *decode=[[streerAddressArr objectAtIndex:i] stringByReplacingOccurrencesOfString:@"\n" withString:@","];
                            [decodeStreetArr addObject:decode];
                            
                        }
                        //Concating addressesArr and Street Array
                        for(int i=0;i<decodeStreetArr.count && i<decodedAddressesArr.count && i<cityArr.count ;i++)
                        {
                            NSString *finalAddressStr=[NSString stringWithFormat:@"%@,%@,%@",[decodeStreetArr objectAtIndex:i],[decodedAddressesArr objectAtIndex:i],[cityArr objectAtIndex:i]];
                            
                            [finalAddressArr addObject:finalAddressStr];
                            
                        }
                        // NSLog(@"%@",finalAddressArr);
                        
                        if (finalAddressArr.count > 0)
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"MoreThanOneAddress" forKey:@"chkAddressCount"];
                        }
                        else
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"ZeroAddress" forKey:@"chkAddressCount"];
                        }
                        
                        
                        [self addPickerView];
                        
                    }
                    else
                    {
                        [ProgressHUD showError:@"No address found.Please add address." Interaction:NO];
                    }
                    
                }
                else
                {
                    [ProgressHUD showError:@"Error." Interaction:NO];
                }
                
                HUD.hidden=YES;
                
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
        
    });
    
}
-(void)getShippingInformationAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";

    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection){
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }else{
            @try
            {
                if([ChkAPICall isEqualToString:@"getShippingCharge"])
                {
                    shippingInfoChargeLbl.text=@"";
                    NSString *ShippingLocation=[[NSUserDefaults standardUserDefaults] objectForKey:@"postcode"];
                    // NSString *cartTotal=fetchedCartTotal;
                    NSString *cartTotal=[[NSUserDefaults standardUserDefaults]objectForKey:@"grandTotal"];
                    
                    NSString *url = API_SEND_LOCATION;
                    
                    NSMutableDictionary *parameterDict;
                    
                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:ShippingLocation,@"location", nil];
                    
                    NSString *post=[NSString stringWithFormat:@"%@", url];
                    NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
                    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                    manager.responseSerializer=[AFJSONResponseSerializer serializer];
                    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
                    
                    NSError *error = nil;
                    dictObj = [manager syncGET:post parameters:parameterDict operation:NULL error:&error];
                    
                    if (!connection){
                        [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
                    }
                    else
                    {
                        NSLog(@"%@",dictObj);
                        BOOL success=[[dictObj valueForKey:@"success"] boolValue];
                        if(success)
                        {
                            int myInteger = [[dictObj valueForKey:@"value"] intValue];
                            NSString* strRate = [NSString stringWithFormat:@"%i", myInteger];

                            //|| [[dictObj objectForKey:@"value"] isEqual:@"(null)"] || [[dictObj objectForKey:@"value"] isEqualToString:@"<null>"]
                            if([strRate isEqualToString:@"0"])
                            {
                                shippingInfoChargeLbl.text=[NSString stringWithFormat:@"₹ 0.0"];
                                
                            }else{
                                shippingChrages=[dictObj objectForKey:@"value"];
                                
                                NSString *msg=[dictObj objectForKey:@"message"];
                                [ProgressHUD showSuccess:msg Interaction:YES];
                                
                                
                                
                                int shippingChargesValue=[shippingChrages intValue];
                                int cartTotalValue=[cartTotal intValue];
                                
                                NSLog(@"shippingChrages=%d\ncartTotal=%d",shippingChargesValue,cartTotalValue);
                                
                                if(cartTotalValue<=shippingChargesValue){
                                    [[NSUserDefaults standardUserDefaults]setObject:shippingChrages forKey:SELECTEDLOCATIONPRICE];
                                    
                                    [self loadStoryBoardWithIdentifier:@"CartController"];
                                }
                                else
                                {
                                    NSString *urlShippingCharges=API_GET_SHIPPING_CHARGES;
                                    
                                    NSMutableDictionary *parameterDict2;
                                    
                                    parameterDict2=[NSMutableDictionary dictionaryWithObjectsAndKeys:ShippingLocation,@"location_name",cartTotal,@"cart_total", nil];
                                    
                                    
                                    WebservicesClass *web=[[WebservicesClass alloc] init];
                                    
                                    NSDictionary *getJsonData=[web PostTopviewProductWithUrl:urlShippingCharges withParameter:parameterDict2];
                                    
                                    NSLog(@"%@",getJsonData);
                                    
                                    if([getJsonData count]>0)
                                    {
                                        
                                        if([getJsonData objectForKey:@"value"]!=nil || ![[getJsonData objectForKey:@"value"] isEqual:@"(null)"] || [[getJsonData objectForKey:@"value"] isEqual:@"<null>"])
                                        {
                                            NSString  *shippingChragesForSelectedArea=[NSString stringWithFormat:@"₹ %.f.0",[[getJsonData objectForKey:@"value"] floatValue]];
                                            shippingInfoChargeLbl.text=shippingChragesForSelectedArea;
                                        }
                                        else
                                        {
                                            shippingInfoChargeLbl.text=@"₹ 0.0";
                                            
                                        }
                                    }
                                }
                            }

                            [self showShippingInformationViewMethod];
                            // [self UIAlertViewControllerMethodTitle:@"" message:@"Location get successfully!"];
                            HUD.hidden=YES;
                        }else{
                            NSString *exc=[NSString stringWithFormat:@"%@",[dictObj valueForKey:@"message"]];
                            //[ProgressHUD showError:exc Interaction:NO];
                            [self UIAlertViewControllerMethodTitle:@"Error" message:exc];
                            HUD.hidden=YES;
                        }
                    }
                }
                else if([ChkAPICall isEqualToString:@"getDeliveryInfo"])
                {
                    HUD.hidden=YES;
                    NSString *ShippingLocation=[[NSUserDefaults standardUserDefaults] objectForKey:@"postcode"];
                    
                    NSString *url=API_DELIVERY_TIMING;
                    
                    NSMutableDictionary *parameterDict;
                    


                    NSString * strProductId = [cartIdArray componentsJoinedByString:@","];
                    NSString * strDeviceId = @"0";
                    
                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:ShippingLocation,@"location_name", strProductId, @"product_id",strDeviceId,@"device_id", nil];
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    NSLog(@"%@",getJsonData);
                    
                    BOOL success=[getJsonData valueForKey:@"success"];
                    
                    if (getJsonData.count>0)
                    {
                        if(success)
                        {
                            valueArr=[[NSMutableArray alloc]init];
                            deliveryDateArr=[[NSMutableArray alloc]init];
                            deliveryDateCustomproductArr=[[NSMutableArray alloc]init];
                            deliveryTimeSlotArr=[[NSMutableArray alloc]init];
                            deliveryTimeSlotCustomArr=[[NSMutableArray alloc]init];
                            
                            [valueArr addObject:[getJsonData valueForKey:@"value"]];
                            
                            
                            for(int i=0;i<valueArr.count;i++)
                            {
                                for(int j=0;j<[[valueArr objectAtIndex:i] count];j++)
                                {
                                    [deliveryDateArr addObject:[[[valueArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"delivery_date"]];
                                    [deliveryDateCustomproductArr addObject:[[[valueArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"delivery_date_custom_product"]];
                                    [deliveryTimeSlotArr addObject:[[[valueArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"delivery_timeslot"]];
                                    [deliveryTimeSlotCustomArr addObject:[[[valueArr objectAtIndex:i]objectAtIndex:j] valueForKey:@"display_timeslot_custom_array"]];
                                }
                                
                                
                            }
                            NSLog(@"%@",[deliveryTimeSlotArr objectAtIndex:0]);
                            //[self DatePickerFunction];
                            [self addDatePicker];
                        }
                    }
                }
            } @catch (NSException *exception){
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //[ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
                HUD.hidden=YES;
            }
        }
    });
}

#pragma mark Picker View Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if([chkPickerView isEqualToString:@"TimeSlot"])
    {
        if(timeArr.count>0)
        {
            return timeArr.count;
        }
        else
        {
            return 0;
        }
        /* if(foodtype)
         {
         if(timeArr.count>0)
         {
         return timeArr.count;
         }
         else
         {
         return 0;
         }
         }
         else
         {
         if(specialTimeArr.count>0)
         {
         return specialTimeArr.count;
         }
         else
         {
         return 0;
         }
         }*/
        
        
    }
    else if([chkPickerView isEqualToString:@"SpecialTimeSlot"])
    {
        if(specialTimeArr.count>0)
        {
            return specialTimeArr.count;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if(finalAddressArr.count>0)
        {
            return finalAddressArr.count;
        }
        else
        {
            return 0;
        }
    }
    
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,userPickerView.bounds.size.width, 44.0f)];
    NSString *rowItem;
    if([chkPickerView isEqualToString:@"TimeSlot"])
    {
        /*if(foodtype)
         {
         rowItem = [timeArr objectAtIndex:row];
         }
         else
         {
         rowItem = [specialTimeArr objectAtIndex:row];
         }*/
        rowItem = [timeArr objectAtIndex:row];
        
    }
    else if([chkPickerView isEqualToString:@"SpecialTimeSlot"])
    {
        rowItem = [specialTimeArr objectAtIndex:row];
    }
    else
    {
        rowItem = [finalAddressArr objectAtIndex:row];
    }
    
    [locationLbl setTextAlignment:NSTextAlignmentCenter];
    [locationLbl setTextColor: [UIColor blackColor]];
    [locationLbl setText:rowItem];
    [locationLbl setBackgroundColor:[UIColor clearColor]];
    
    return locationLbl;
    
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if([chkPickerView isEqualToString:@"TimeSlot"])
    {
        /*if(foodtype)
         {
         if(timeArr.count>0)
         {
         selectedLocationIndex1=[timeArr objectAtIndex:row];
         }
         }
         else
         {
         if(specialTimeArr.count>0)
         {
         selectedLocationIndex1=[specialTimeArr objectAtIndex:row];
         }
         }*/
        if(timeArr.count>0)
        {
            selectedLocationIndex1=[timeArr objectAtIndex:row];
        }
        
        
        
    }
    else if([chkPickerView isEqualToString:@"SpecialTimeSlot"])
    {
        if(specialTimeArr.count>0)
        {
            selectedLocationIndex2=[specialTimeArr objectAtIndex:row];
        }
    }
    else
    {
        if ([addressesIDArr count] > 0)
        {
            selectedLocationIndex=[finalAddressArr objectAtIndex:row];
            [[NSUserDefaults standardUserDefaults]setObject:[decodedAddressesArr objectAtIndex:row] forKey:@"postcodeLoc"];
            
            if([chkPickerView isEqualToString:@"shippingAddress"])//selectShippingAddressBtn.tag==1)
            {
                selectedShippingAddrId=[addressesIDArr objectAtIndex:row];
            }
            else if([chkPickerView isEqualToString:@"billingAddress"])//selectBillingAddressBtn.tag==1)
            {
                selectedBillingAddrId=[addressesIDArr objectAtIndex:row];
            }
            /*if (selectShippingAddressBtn.tag ==1)
             {
             selectedShippingAddrId=[addressesIDArr objectAtIndex:row];
             
             }
             else if(selectShippingAddressBtn.tag ==2)
             {
             selectedBillingAddrId=[addressesIDArr objectAtIndex:row];
             }*/
            
        }
        
    }
    
}

/*#pragma mark - DatePickerMethods
 -(void)DatePickerFunction
 {
 dtPicker.hidden = NO;
 
 NSDate *currentTime = [NSDate date];
 
 NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
 [timeFormatter setDateFormat:@"HH"];
 
 NSString *hourCountString = [timeFormatter stringFromDate:currentTime];
 
 int hourCountInt = [hourCountString intValue];
 
 //time between 12PM - 12AM.
 if(hourCountInt > 12 || hourCountInt < 12)
 {
 NSLog(@"display night-time images");
 
 NSString *dateString = [deliveryDateArr objectAtIndex:1];
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
 [dateFormatter setDateFormat:@"yyyy-MM-dd"];
 NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
 
 
 [dtPicker setMinimumDate:orignalDate];
 dtPicker.date = orignalDate;
 }
 else
 {
 if([dateBtnClick isEqualToString:@"dateBtn"])
 {
 [dtPicker setMinimumDate:[NSDate date]];
 dtPicker.date = [NSDate date];
 }
 else if ([dateBtnClick isEqualToString:@"specialDateBtn"])
 {
 NSString *dateString = [deliveryDateArr objectAtIndex:1];
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
 [dateFormatter setDateFormat:@"yyyy-MM-dd"];
 NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
 
 
 [dtPicker setMinimumDate:orignalDate];
 dtPicker.date = orignalDate;
 }
 
 }
 
 
 [dtPicker addTarget:self
 action:@selector(ShowSelectedDate:)
 forControlEvents:UIControlEventValueChanged];
 [self.view addSubview:dtPicker];
 
 }
 
 -(void)ShowSelectedDate:(id)sender
 {
 timeBtn.hidden=NO;
 dtPicker.hidden=YES;
 [dtPicker removeFromSuperview];
 
 if([dateBtnClick isEqualToString:@"dateBtn"])
 {
 NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
 [formatter setDateFormat:@"dd-MM-yyyy"];
 formatter.dateStyle = NSDateFormatterMediumStyle;
 NSDate *date1=dtPicker.date;
 [formatter setDateFormat:@"dd-MM-yyyy"];
 finalDateString1 = [formatter stringFromDate:date1];
 
 NSString *dateString = [deliveryDateArr objectAtIndex:0];
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
 [dateFormatter setDateFormat:@"yyyy-MM-dd"];
 NSDate *orignalDate   =  [dateFormatter dateFromString:dateString];
 [dateFormatter setDateFormat:@"dd-MM-yyyy"];
 finalDateString2 = [dateFormatter stringFromDate:orignalDate];
 
 [dateBtn setTitle:[NSString stringWithFormat:@"%@",finalDateString1] forState:UIControlStateNormal];
 if([finalDateString1 isEqualToString:finalDateString2])
 {
 if(deliveryTimeSlotArr.count>0)
 {
 timeArr=[deliveryTimeSlotArr objectAtIndex:0];
 }
 
 }
 else
 {
 if(deliveryTimeSlotArr.count>0)
 {
 timeArr=[deliveryTimeSlotArr objectAtIndex:1];
 }
 }
 }
 else if ([dateBtnClick isEqualToString:@"specialDateBtn"])
 {
 NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
 [formatter setDateFormat:@"dd-MM-yyyy"];
 formatter.dateStyle = NSDateFormatterMediumStyle;
 
 timeBtn.hidden=NO;
 dtPicker.hidden=YES;
 [dtPicker removeFromSuperview];
 
 NSDate *date1=dtPicker.date;
 [formatter setDateFormat:@"dd-MM-yyyy"];
 NSString *finalDateString3 = [formatter stringFromDate:date1];
 
 [specialDateBtn setTitle:[NSString stringWithFormat:@"%@",finalDateString3] forState:UIControlStateNormal];
 specialTimeBtn.hidden=NO;
 specialTimeArr=[deliveryTimeSlotArr objectAtIndex:1];
 }
 
 //NSLog(@"time arr=%@",timeArr);
 
 }
 */
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    // [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
