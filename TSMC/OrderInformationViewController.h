//
//  OrderInformationViewController.h
//  TSMC
//
//  Created by user on 07/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"
@interface OrderInformationViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
{
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *titleText;
  
    MBProgressHUD *HUD;

    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *subView;
    
    //Shipping Address View
//    IBOutlet UIView *shippingAddressView;
//    IBOutlet UIButton *shippingAddressBtn;
//    IBOutlet UIButton *selectShippingAddressBtn;
//    IBOutlet UIButton *addShippingAddressBtn;
//    IBOutlet UIButton *billToThisAddressBtn;
//    IBOutlet UIButton *billToDifferentAddressBtn;
//    
//    //Billing Address View
//    IBOutlet UIView *billingAddressView;
//    IBOutlet UIButton *billingAddressBtn;
//    IBOutlet UIButton *selectBillingAddressBtn;
//    IBOutlet UIButton *addBillingAddressBtn;
    
    //Shipping Information View
    IBOutlet UIView *shippingInformationView;
    IBOutlet UIButton *shippingInformationBtn;
    IBOutlet UILabel *shippingInformationLbl;
    IBOutlet UILabel *shippingInfoChargeLbl;
    IBOutlet UIButton *shippingInfoContinueBtn;
    
    //Delivery Information View
    IBOutlet UIView *deliveryInformationView;
    IBOutlet UIButton *deliveryInformationBtn;
    IBOutlet UIButton *dateBtn;
    IBOutlet UIButton *timeBtn;
    IBOutlet UITextField *instructionTF;
    IBOutlet UILabel *deliveryLineLbl;
    IBOutlet UIButton *deliveryInfoContinueBtn;
    
    
    IBOutlet UILabel *SpecialProductInfoLbl;
    IBOutlet UIButton *specialDateBtn;
    IBOutlet UIButton *specialTimeBtn;
    
    //Payment Information View
    IBOutlet UIView *paymentInformationView;
    IBOutlet UIButton *paymentInformationBtn;
    IBOutlet UIButton *onlinePaymentBtn;
    IBOutlet UIButton *cashOnDeliveryBtn;
    IBOutlet UIImageView *onlinePaymentImgView;
    IBOutlet UIImageView *CODImgView;
    
    //Constraints Outlet
//    IBOutlet NSLayoutConstraint *shippingAddressViewHeight;
//    IBOutlet NSLayoutConstraint *billingAddressViewHeight;
    IBOutlet NSLayoutConstraint *shippingInformationViewHeight;
    IBOutlet NSLayoutConstraint *deliveryInformationViewHeight;
    IBOutlet NSLayoutConstraint *paymentInformationViewHeight;
    IBOutlet UIButton *payTypeBtn;
    
    IBOutlet NSLayoutConstraint *specialProductInfoLblHeight;
    IBOutlet NSLayoutConstraint *dateLblHeight;
    IBOutlet NSLayoutConstraint *specialDateBtnHeight;
    IBOutlet NSLayoutConstraint *timeLblHeight;
    IBOutlet NSLayoutConstraint *specialTimeBtnHeight;
    
    
    UIPickerView *userPickerView;
    
    NSUserDefaults *defaults;
    NSMutableArray *addressesArr,*addressesArr2,*addressesIDArr,*decodedAddressesArr,*streerAddressArr,*decodeStreetArr,*finalAddressArr,*cityArr,*telephoneArr,*ChkDefaultArr,*timeArr,*specialTimeArr,*valueArr,*deliveryDateArr,*deliveryDateCustomproductArr,*deliveryTimeSlotArr,*deliveryTimeSlotCustomArr;
    
    NSString *checkedData,*ChkAPICall,*selectedLocationIndex,*selectedLocationIndex1,*selectedLocationIndex2,*chkPickerView,*shippingChrages,*dateBtnClick,*finalDateString1,*finalDateString2,*replaceSpaceFetchedCoupon;
    
    
    UIAlertController *alertController;
    UIDatePicker *dtPicker;
    
    NSString *selectedShippingAddrId,*selectedBillingAddrId,*selectedPayType;
    
    BOOL foodtype;
}
- (IBAction)backBtnClicked:(id)sender;

- (IBAction)shippingAddressBtnClicked:(id)sender;
- (IBAction)selectShippingAddressBtn_Clicked:(id)sender;
- (IBAction)addShippingAddressBtn_Clicked:(id)sender;
- (IBAction)billToThisAddressBtn_Clicked:(id)sender;
- (IBAction)billToDifferentAddressBtn_Clicked:(id)sender;

- (IBAction)billingAddressBtnClicked:(id)sender;
- (IBAction)selectBillingAddressBtn_Clicked:(id)sender;
- (IBAction)addBillingAddressBtn_Clicked:(id)sender;

- (IBAction)shippingInformationBtnClicked:(id)sender;
- (IBAction)shippingInfoContinueBtn_Clicked:(id)sender;

- (IBAction)deliveryInformationBtnClicked:(id)sender;
- (IBAction)dateBtn_Clicked:(id)sender;
- (IBAction)timeBtnClicked:(id)sender;
- (IBAction)deliveryInfoContinueBtn_Clicked:(id)sender;
- (IBAction)specialDateBtn_Clicked:(id)sender;
- (IBAction)specialTimeBtn_Clicked:(id)sender;

- (IBAction)paymentInformationBtnClicked:(id)sender;
- (IBAction)onlinePaymentBtnClicked:(id)sender;
- (IBAction)cashOnDeliveryBtn_Clicked:(id)sender;
- (IBAction)paymentTypeBtn_Clicked:(id)sender;
- (IBAction)optionBtnPress:(id)sender;

@property (nonatomic) NSInteger selectedIndex;

@property(weak,nonatomic)NSString *fetchedCoupon,*comingFrom,*fetchedCartTotal,*isBillAddressSame;
@property(weak,nonatomic)NSMutableArray * arrayRedeemOrder;
@end
