//
//  SearchController.m
//  TSMC
//
//  Created by Rahul Lekurwale on 10/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "SearchController.h"
#import "OfflineDb.h"
#import "HelperClass.h"
#import "WebservicesClass.h"
#import "AppConstant.h"
#import "ItemDetailsController.h"
#import "CartController.h"
#import "FilterController.h"
#import "wishListHelper.h"
#import "UIBarButtonItem+Badge.h"

@interface SearchController ()
{
    wishListHelper *wishObj;

    NSMutableArray *nameArray,*idArray,*imagesArray,*categoryIdArr,*categoryNameArr,*imageMainArray,*imageThumbArray,*imageSmallArray,*weightArray,*brandIndexArr,*brandValueArr;
    int page,limit,tempLimit;
    NSMutableArray *descriptionArray,*skuArray,*shortDescArray,*priceArray,*idContainInLocal,*localQtyArr,*foodTypeArr,*mirchImagesArr,*mirchIndexArr;
    NSMutableArray *localCutOpIdArray,*localCutOpValArray,*localCutOpPriceArray,*localWeightOpIdArray,*localWeightOpValArray,*localWeightOpPriceArray,*localTypeOpIdArray,*localTypeOpValArray,*localTypeOpPriceArray,*optionIdArray,*localTypeOpLabelArray,*localWeightOpLabelArray,*localCutOpLabelArray;
    int quntity;
    NSString *yesrr;
    NSMutableArray *dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray,*spicelevelArray,*messagesArray,*newPriceArray,*wlArr,*wvArr,*wpArr,*clArr,*cvArr,*cpArr,*tlArr,*tvArr,*tpArr;
    NSString *categoryId;
    NSMutableArray *wishlistIdArray,*containInWishListArray;

    int indexToShow;
    UILabel *lbl_line;
    OfflineDb *object;
    
    
    //Wight required array's
    NSMutableArray *weightlabelArray,*weightOptionIdArray;
    NSMutableArray *weightValueArray;
    NSMutableArray *weightpriceArray;
    NSMutableArray *pickerWeightLabelArr,*pickerWeightValueArr,*pickerWeightPriceArr;
    NSMutableArray *selectedWeightLabelIndex,*selectedWeightValueIndex,*selectedWeightPriceIndex;
    
    
    //Cut required array's
    NSMutableArray *cutlabelArray,*cutOptionIdArray;
    NSMutableArray *cutValueArray;
    NSMutableArray *cutpriceArray;
    NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
    NSMutableArray *selectedCutLabelIndex,*selectedCutValueIndex,*selectedCutPriceIndex;
    
    //Type required array's
    NSMutableArray *typelabelArray,*typeOptionIdArray;
    NSMutableArray *typeValueArray;
    NSMutableArray *typepriceArray;
    NSMutableArray *pickerTypeLabelArr,*pickerTypeValueArr,*pickerTypePriceArr;
    NSMutableArray *selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    NSString *selectedLabel,*selectedValue,*selectedPrice;
    
    
}


@end

@implementation SearchController
@synthesize spicesPicker;

-(void)viewWillAppear:(BOOL)animated{
    
    page=1;
    limit=50;
    
     _txt_search.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Text" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    _pageTableView.backgroundColor = [UIColor whiteColor];

    [self checkProgressBar];

    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    
    
    weightlabelArray=[[NSMutableArray alloc] init];
    weightValueArray=[[NSMutableArray alloc] init];
    weightpriceArray=[[NSMutableArray alloc] init];
    
    cutlabelArray=[[NSMutableArray alloc] init];
    cutValueArray=[[NSMutableArray alloc] init];
    cutpriceArray=[[NSMutableArray alloc] init];
    
    typelabelArray=[[NSMutableArray alloc] init];
    typeValueArray=[[NSMutableArray alloc] init];
    typepriceArray=[[NSMutableArray alloc] init];
    
    localCutOpIdArray=[[NSMutableArray alloc] init];
    localCutOpValArray=[[NSMutableArray alloc] init];
    localCutOpPriceArray=[[NSMutableArray alloc] init];
    
    localWeightOpIdArray=[[NSMutableArray alloc] init];
    localWeightOpValArray=[[NSMutableArray alloc] init];
    localWeightOpPriceArray=[[NSMutableArray alloc] init];
    
    localTypeOpIdArray=[[NSMutableArray alloc] init];
    localTypeOpValArray=[[NSMutableArray alloc] init];
    localTypeOpPriceArray=[[NSMutableArray alloc] init];
    brandIndexArr=[[NSMutableArray alloc] init];
    brandValueArr=[[NSMutableArray alloc] init];
    weightArray=[[NSMutableArray alloc] init];
    optionIdArray=[[NSMutableArray alloc] init];
    imageMainArray=[[NSMutableArray alloc] init];
    imageThumbArray=[[NSMutableArray alloc] init];
    imageSmallArray=[[NSMutableArray alloc] init];
    
    typeOptionIdArray=[[NSMutableArray alloc] init];
    weightOptionIdArray=[[NSMutableArray alloc] init];
    cutOptionIdArray=[[NSMutableArray alloc] init];
    localCutOpLabelArray=[[NSMutableArray alloc] init];
    localWeightOpLabelArray=[[NSMutableArray alloc] init];
    localTypeOpLabelArray=[[NSMutableArray alloc] init];
    selectedWeightLabelIndex=[[NSMutableArray alloc] init];
    selectedWeightValueIndex=[[NSMutableArray alloc] init];
    selectedWeightPriceIndex=[[NSMutableArray alloc] init];
    
    selectedCutLabelIndex=[[NSMutableArray alloc] init];
    selectedCutPriceIndex=[[NSMutableArray alloc] init];
    selectedCutValueIndex=[[NSMutableArray alloc] init];
    selectedTypeLabelIndex=[[NSMutableArray alloc] init];
    selectedTypeValueIndex=[[NSMutableArray alloc] init];
    selectedTypePriceIndex=[[NSMutableArray alloc] init];
    newPriceArray=[[NSMutableArray alloc] init];
    
    wlArr=[[NSMutableArray alloc] init];
    wvArr=[[NSMutableArray alloc] init];
    wpArr=[[NSMutableArray alloc] init];
    clArr=[[NSMutableArray alloc] init];
    cvArr=[[NSMutableArray alloc] init];
    cpArr=[[NSMutableArray alloc] init];
    tlArr=[[NSMutableArray alloc] init];
    tvArr=[[NSMutableArray alloc] init];
    tpArr=[[NSMutableArray alloc] init];

    wishlistIdArray=[[NSMutableArray alloc] init];
    containInWishListArray=[[NSMutableArray alloc] init];

    
    
    NSArray *str=[object fetch_CartDetails];
    NSLog(@"%@",str);
    NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
    if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] )
    {
        self.navigationItem.rightBarButtonItem.badge.hidden = YES;
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
        
    }
    else
    {
        self.navigationItem.rightBarButtonItem.badge.hidden = NO;
        [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
        
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
            [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
        
//        _lbl_badgeCount.text=cartCount;
//        _lbl_badgeCount.layer.masksToBounds=YES;
//        _lbl_badgeCount.layer.cornerRadius=_lbl_badgeCount.frame.size.width/2;

        
    }
    
    if (_txt_search.text==nil || _txt_search.text.length<=0)
    {
        
    }else{
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:nil animated:YES];

    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *border = [UIView new];
    border.backgroundColor = ButtonBlueColor;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(0, 0, self.progressBackView.frame.size.width, 2.0);
    [self.progressBackView addSubview:border];
    
    // Do any additional setup after loading the view.
    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    categoryIdArr=[[NSMutableArray alloc] init];
    categoryNameArr=[[NSMutableArray alloc] init];
    
    object=[[OfflineDb alloc] init];
    wishObj=[wishListHelper sharedInstance];

//    [self checkProgressBar];
//    _titleBack.backgroundColor=TopBarColor;
    self.title = @"Search".uppercaseString;
//    _lbl_titleText.font=TopBarFont;
//    _lbl_titleText.textColor=TopBarTextColor;
//    _lbl_badgeCount.textColor=cartIconTextColor;
//    _lbl_badgeCount.backgroundColor=cartIconBackColor;

    
    
    //    weightlabelArray=[[NSMutableArray alloc] init];
    //    weightValueArray=[[NSMutableArray alloc] init];
    //    weightpriceArray=[[NSMutableArray alloc] init];
    
    
    
//    self.lbl_badgeCount.layer.masksToBounds=YES;
//    self.lbl_badgeCount.layer.cornerRadius=self.lbl_badgeCount.frame.size.width/2;
    
    _loadMoreHeight.constant=0;
    _btn_loadMore.hidden=YES;
    
    [self.pageTableView bringSubviewToFront:_img_BackIcon];
    _img_BackIcon.hidden=NO;
    _pageTableView.hidden=YES;
//    [_pageTableView reloadData];

}


-(void)GetTopviewProduct:(NSString *)catId
{
    
    // ,*selectedCutValueIndex,*selectedCutPriceIndex,selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    // NSLog(@"Idarray1=%@",idArray);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        HelperClass *s=[[HelperClass alloc]init];
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection){
            [HUD hide:YES];
            
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }else{
            @try {

                _pageTableView.delegate = self;
                _pageTableView.dataSource = self;
                [_pageTableView reloadData];
                
                
                NSString *searchText=_txt_search.text;
                NSString *str_pg=[NSString stringWithFormat:@"%d",page];
                NSString *str_lim=[NSString stringWithFormat:@"%d",limit];
                
                NSString *tempUrl;
                tempUrl=[NSString stringWithFormat:@"%@",API_MENU_SEARCH];
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                
                NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
                
                if ([isLog isEqualToString:@"YES"]) {
                    NSMutableDictionary *wishDict=[wishObj getAllWishData];
                    BOOL success=[[wishDict valueForKey:@"success"] boolValue];
                    if (success) {
                        [wishlistIdArray addObjectsFromArray:[[wishDict valueForKey:@"message"] valueForKey:@"entity_id"]];
                    }
                    
                }

                NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:str_pg,@"page",str_lim,@"limit",searchText,@"query", nil];

                //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
                NSDictionary *jsonDict=[web PostTopviewProductWithUrl:tempUrl withParameter:dict];
                NSLog(@"%@",jsonDict);

                if ([jsonDict count] > 0)
                {
                    tempLimit=(int)[jsonDict count];
                    _img_BackIcon.hidden=YES;
                    _pageTableView.hidden=NO;

                    
                    NSArray *idArr=[jsonDict valueForKey:@"entity_id"];
                    if ([idArray containsObject:[idArr firstObject]])
                    {
                       // _img_BackIcon.hidden=NO;
                       // _pageTableView.hidden=YES;
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:@"Alert"
                                                      message:@"No data found."
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       //Do Some action here
                                                                       [HUD hide:YES];
                                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                                       
                                                                   }];
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];

                    }else{
                    
                        NSArray *name=[jsonDict valueForKey:@"name"];
                        //                NSArray *skuArr=[jsonDict valueForKey:@"sku"];
                        NSArray *descriptionArr=[jsonDict valueForKey:@"description"];
                        NSArray *shortDescArr=[jsonDict valueForKey:@"short_description"];
                        NSArray *priceArr=[jsonDict valueForKey:@"final_price_with_tax"];
                        //                NSArray *isSaleableArr=[jsonDict valueForKey:@"is_saleable"];
                        NSArray *foodType=[jsonDict valueForKey:@"food_type"];
                        
                        NSArray *imagesArr=[jsonDict valueForKey:@"image_url"];
                        NSArray *imagesThumb=[jsonDict valueForKey:@"image_thumb"];
                        NSArray *imagesMainUrl=[jsonDict valueForKey:@"main_url"];
                        NSArray *imagesSmall=[jsonDict valueForKey:@"image_small"];
                        NSArray *brand=[jsonDict valueForKey:@"brand"];
                        NSArray *origin=[jsonDict valueForKey:@"origin"];
                        NSArray *weight=[jsonDict valueForKey:@"weight"];

                        // dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray;
                        NSArray *attrDict=[jsonDict valueForKey:@"attributes"];
                        NSArray *InputTypeArray=[attrDict valueForKey:@"InputType"];
                        NSArray *opIdArr=[attrDict valueForKey:@"option_id"];
                        NSDictionary *attArr=[attrDict valueForKey:@"att"];
                        NSArray *labelArr=[attArr valueForKey:@"label"];
                        NSArray *valueArr=[attArr valueForKey:@"value"];
                        NSArray *priArr=[attArr valueForKey:@"price"];
                        NSArray *InputType=[attArr valueForKey:@"InputType"];

                        [self getRangeValuesOfPrice:priceArr];

                        
                        NSString *isfilter=[[NSUserDefaults standardUserDefaults] valueForKey:ISFILTER];
                        int filterMin=[[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMINPRICE] intValue];
                        int filterMax=[[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMAXPRICE] intValue];
                        NSString *filterBrand=[[NSUserDefaults standardUserDefaults] valueForKey:BRANDVALUE];

                        
                        /****Set attributes*****/
                        
                        NSLog(@"%@",APP_DELEGATE.selectBarndArray);
                        if ([isfilter isEqualToString:@"YES"]) {
                            for (int i=0; i<[idArr count]; i++) {
                                int currPri=[[priceArr objectAtIndex:i] intValue];
                                if ((currPri >= filterMin) && (currPri <=filterMax)) {
                                    NSArray *appArr=APP_DELEGATE.selectBarndArray;
                                    if ([appArr containsObject:[brand objectAtIndex:i]]) {
                                        
                                        if ([[attrDict objectAtIndex:i] count]>0) {
                                            
                                            if ([[InputType objectAtIndex:i] count]>0) {
                                                
                                                for (int m=0; m<[[InputType objectAtIndex:i] count]; m++) {
                                                    NSArray *arr=[[InputType objectAtIndex:i] objectAtIndex:m];
                                                    for (int n=0; n<[arr count]; n++) {
                                                        if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"]) {
                                                            NSString *str_wlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                            NSString *str_wvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                            NSString *str_wpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                            [wlArr addObject:str_wlArr];
//                                                            [wvArr addObject:str_wvArr];
//                                                            [wpArr addObject:str_wpArr];
                                                            
//                                                            int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
//                                                            [wlArr addObject:str_wlArr];
//                                                            [wvArr addObject:str_wvArr];
//                                                            [wpArr addObject:@(str_wpArr)];

                                                            int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
                                                            NSString *weightStr = [NSString stringWithFormat:@"%d",str_wpArr];
                                                            [wlArr addObject:str_wlArr];
                                                            [wvArr addObject:str_wvArr];
                                                            [wpArr addObject:weightStr];
                                                        }
                                                        
                                                        if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                                            NSString *str_clArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                            NSString *str_cvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                            NSString *str_cpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                            [clArr addObject:str_clArr];
                                                            [cvArr addObject:str_cvArr];
                                                            [cpArr addObject:str_cpArr];
                                                            
                                                        }
                                                        
                                                        if ([[arr objectAtIndex:n] isEqualToString:@"Type"]) {
                                                            NSString *str_tlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                            NSString *str_tvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                            NSString *str_tpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                            [tlArr addObject:str_tlArr];
                                                            [tvArr addObject:str_tvArr];
                                                            [tpArr addObject:str_tpArr];
                                                            
                                                        }
                                                        
                                                    }
                                                    
                                                    
                                                    
                                                }
                                                [weightlabelArray addObject:wlArr];
                                                [weightValueArray addObject:wvArr];
                                                [weightpriceArray addObject:wpArr];
                                                [cutlabelArray addObject:clArr];
                                                [cutValueArray addObject:cvArr];
                                                [cutpriceArray addObject:cpArr];
                                                [typelabelArray addObject:tlArr];
                                                [typeValueArray addObject:tvArr];
                                                [typepriceArray addObject:tpArr];
                                                
                                                
                                            }
                                            
                                            
                                            NSArray *labArr=[opIdArr objectAtIndex:i];
                                            if ( (NSArray *)[NSNull null] == labArr )
                                            {
                                                NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                                [optionIdArray addObject:emptyArray];
                                                
                                            }
                                            else
                                            {
                                                //[dropLabelArr addObject:[labArr objectAtIndex:1]];
                                                [optionIdArray addObject:[opIdArr objectAtIndex:i]];
                                                
                                            }
                                            
                                            NSArray *valArr=[InputTypeArray objectAtIndex:i];
                                            if ( (NSArray *)[NSNull null] == valArr )
                                            {
                                                
                                            }
                                            else
                                            {
                                                [dropLabelArr addObject:valArr];
                                                
                                                //                            [dropValueArr addObject:[valArr objectAtIndex:1]];
                                            }
                                            NSLog(@"dropLabelArr=%@",dropLabelArr);
                                            
                                        }
                                        else{
                                            
                                            NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                            [dropLabelArr addObject:emptyArray];
                                            [optionIdArray addObject:emptyArray];
                                            [weightlabelArray addObject:emptyArray];
                                            [weightValueArray addObject:emptyArray];
                                            [weightpriceArray addObject:emptyArray];
                                            [cutlabelArray addObject:emptyArray];
                                            [cutValueArray addObject:emptyArray];
                                            [cutpriceArray addObject:emptyArray];
                                            [typelabelArray addObject:emptyArray];
                                            [typeValueArray addObject:emptyArray];
                                            [typepriceArray addObject:emptyArray];
                                            
                                        }
                                        
                                        [newPriceArray addObject:[priceArr objectAtIndex:i]];
                                        [idArray addObject:[idArr objectAtIndex:i]];
                                        [nameArray addObject:[name objectAtIndex:i]];
                                        [descriptionArray addObject:[descriptionArr objectAtIndex:i]];
                                        [imagesArray addObject:[imagesArr objectAtIndex:i]];
                                        [foodTypeArr addObject:[foodType objectAtIndex:i]];
                                        
                                        //                        [skuArray addObject:[skuArr objectAtIndex:i]];
                                        [shortDescArray addObject:[shortDescArr objectAtIndex:i]];
                                        
                                        
                                        [brandIndexArr addObject:[brand objectAtIndex:i]];
                                        [imageMainArray addObject:[imagesMainUrl objectAtIndex:i]];
                                        [imageSmallArray addObject:[imagesSmall objectAtIndex:i]];
                                        [imageThumbArray addObject:[imagesThumb objectAtIndex:i]];
                                        [weightArray addObject:[weight objectAtIndex:i]];
                                        NSArray *getAttr=[object fetch_BrandValue:@"brand" AndValue:[brand objectAtIndex:i]];
                                        if ([getAttr count]>0) {
                                            [brandValueArr addObject:[[getAttr valueForKey:@"attribute_label"] objectAtIndex:0]];
                                            
                                        }
                                        else{
                                            [brandValueArr addObject:@"0"];
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            
                            
                        }
                        else{
                            
                            for (int i=0; i<[idArr count]; i++) {
                                if ([[attrDict objectAtIndex:i] count]>0) {
                                    
                                    if ([[InputType objectAtIndex:i] count]>0)
                                    {
                                        
                                        wlArr=[[NSMutableArray alloc] init];
                                        wvArr=[[NSMutableArray alloc] init];
                                        wpArr=[[NSMutableArray alloc] init];
                                        clArr=[[NSMutableArray alloc] init];
                                        cvArr=[[NSMutableArray alloc] init];
                                        cpArr=[[NSMutableArray alloc] init];
                                        tlArr=[[NSMutableArray alloc] init];
                                        tvArr=[[NSMutableArray alloc] init];
                                        tpArr=[[NSMutableArray alloc] init];
                                        
                                        for (int m=0; m<[[InputType objectAtIndex:i] count]; m++)
                                        {
                                            NSArray *arr=[[InputType objectAtIndex:i] objectAtIndex:m];
                                            for (int n=0; n<[arr count]; n++)
                                            {
                                                if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"])
                                                {
                                                    NSString *str_wlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_wvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_wpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    [wlArr addObject:str_wlArr];
//                                                    [wvArr addObject:str_wvArr];
//                                                    [wpArr addObject:str_wpArr];
                                                    
//                                                    int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
//                                                    [wlArr addObject:str_wlArr];
//                                                    [wvArr addObject:str_wvArr];
//                                                    [wpArr addObject:@(str_wpArr)];

                                                    int str_wpArr=[[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n] intValue] + [[priceArr objectAtIndex:i] intValue];
                                                    NSString *weightStr = [NSString stringWithFormat:@"%d",str_wpArr];
                                                    [wlArr addObject:str_wlArr];
                                                    [wvArr addObject:str_wvArr];
                                                    [wpArr addObject:weightStr];
                                                }
                                                
                                                if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"])
                                                {
                                                    NSString *str_clArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_cvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_cpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    [clArr addObject:str_clArr];
                                                    [cvArr addObject:str_cvArr];
                                                    [cpArr addObject:str_cpArr];
                                                    
                                                }
                                                
                                                if ([[arr objectAtIndex:n] isEqualToString:@"Type"])
                                                {
                                                    NSString *str_tlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_tvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    NSString *str_tpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
                                                    [tlArr addObject:str_tlArr];
                                                    [tvArr addObject:str_tvArr];
                                                    [tpArr addObject:str_tpArr];
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            
                                        }
                                        [weightlabelArray addObject:wlArr];
                                        [weightValueArray addObject:wvArr];
                                        [weightpriceArray addObject:wpArr];
                                        [cutlabelArray addObject:clArr];
                                        [cutValueArray addObject:cvArr];
                                        [cutpriceArray addObject:cpArr];
                                        [typelabelArray addObject:tlArr];
                                        [typeValueArray addObject:tvArr];
                                        [typepriceArray addObject:tpArr];
                                        
                                        
                                    }
                                    
                                    
                                    NSArray *labArr=[opIdArr objectAtIndex:i];
                                    if ( (NSArray *)[NSNull null] == labArr )
                                    {
                                        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                        [optionIdArray addObject:emptyArray];
                                        
                                    }
                                    else
                                    {
                                        //[dropLabelArr addObject:[labArr objectAtIndex:1]];
                                        [optionIdArray addObject:[opIdArr objectAtIndex:i]];
                                        
                                    }
                                    
                                    NSArray *valArr=[InputTypeArray objectAtIndex:i];
                                    if ( (NSArray *)[NSNull null] == valArr )
                                    {
                                        
                                    }
                                    else
                                    {
                                        [dropLabelArr addObject:valArr];
                                        
                                        //                            [dropValueArr addObject:[valArr objectAtIndex:1]];
                                    }
                                    NSLog(@"dropLabelArr=%@",dropLabelArr);
                                    
                                }
                                else{
                                    
                                    NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                                    [dropLabelArr addObject:emptyArray];
                                    [optionIdArray addObject:emptyArray];
                                    [weightlabelArray addObject:emptyArray];
                                    [weightValueArray addObject:emptyArray];
                                    [weightpriceArray addObject:emptyArray];
                                    [cutlabelArray addObject:emptyArray];
                                    [cutValueArray addObject:emptyArray];
                                    [cutpriceArray addObject:emptyArray];
                                    [typelabelArray addObject:emptyArray];
                                    [typeValueArray addObject:emptyArray];
                                    [typepriceArray addObject:emptyArray];
                                    
                                    
                                    
                                    
                                }
                                
                                [newPriceArray addObject:[priceArr objectAtIndex:i]];
                                
                                [idArray addObject:[idArr objectAtIndex:i]];
                                [nameArray addObject:[name objectAtIndex:i]];
                                [descriptionArray addObject:[descriptionArr objectAtIndex:i]];
                                [imagesArray addObject:[imagesArr objectAtIndex:i]];
                                [foodTypeArr addObject:[foodType objectAtIndex:i]];
                                
                                //                        [skuArray addObject:[skuArr objectAtIndex:i]];
                                [shortDescArray addObject:[shortDescArr objectAtIndex:i]];
                                
                                
                                [brandIndexArr addObject:[brand objectAtIndex:i]];
                                [imageMainArray addObject:[imagesMainUrl objectAtIndex:i]];
                                [imageSmallArray addObject:[imagesSmall objectAtIndex:i]];
                                [imageThumbArray addObject:[imagesThumb objectAtIndex:i]];
                                [weightArray addObject:[weight objectAtIndex:i]];
                                NSArray *getAttr=[object fetch_BrandValue:@"brand" AndValue:[brand objectAtIndex:i]];
                                if ([getAttr count]>0) {
                                    [brandValueArr addObject:[[getAttr valueForKey:@"attribute_label"] objectAtIndex:0]];
                                    
                                }
                                else{
                                    [brandValueArr addObject:@"0"];
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                        }
                        
                        
                        
                        
                        /****END of attributes*********/
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        //                    if ([[isSaleableArr objectAtIndex:i] boolValue] == YES) {
                        
                        
                        
                        
                        //                    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
                        priceArray=[[NSMutableArray alloc] init];
                        cutOptionIdArray=[[NSMutableArray alloc] init];
                        weightOptionIdArray=[[NSMutableArray alloc] init];
                        typeOptionIdArray=[[NSMutableArray alloc] init];
                        selectedWeightLabelIndex=[[NSMutableArray alloc] init];
                        selectedWeightValueIndex=[[NSMutableArray alloc] init];
                        selectedWeightPriceIndex=[[NSMutableArray alloc] init];
                        selectedCutLabelIndex=[[NSMutableArray alloc] init];
                        selectedCutValueIndex=[[NSMutableArray alloc] init];
                        selectedCutPriceIndex=[[NSMutableArray alloc] init];
                        selectedTypeLabelIndex=[[NSMutableArray alloc] init];
                        selectedTypeValueIndex=[[NSMutableArray alloc] init];
                        selectedTypePriceIndex=[[NSMutableArray alloc] init];
                        
                        for (int m=0; m<[optionIdArray count]; m++) {
                            int a,b,c;
                            a=0;b=0;c=0;
                            if([[optionIdArray objectAtIndex:m] count]>0){
                                
                                for (int n=0; n<[[optionIdArray objectAtIndex:m] count]; n++) {
                                    if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                        [cutOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                        a=1;
                                    }
                                    if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight"]) {
                                        [weightOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                        b=1;
                                    }
                                    if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Type"]) {
                                        [typeOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
                                        c=1;
                                    }
                                    
                                    
                                }
                                if (a==0) {
                                    [cutOptionIdArray addObject:@"0"];
                                }
                                if (b==0) {
                                    [weightOptionIdArray addObject:@"0"];
                                }
                                if (c==0) {
                                    [typeOptionIdArray addObject:@"0"];
                                }
                                
                                
                                
                            }
                            else{
                                
                                [cutOptionIdArray addObject:@"0"];
                                [typeOptionIdArray addObject:@"0"];
                                [weightOptionIdArray addObject:@"0"];
                                
                            }
                            
                            
                            NSString *concatedPrice=[NSString stringWithFormat:@"%@",[newPriceArray objectAtIndex:m]];
                            int conPri=[concatedPrice intValue];
                            
                            if (conPri == 0) {
                                int sum;
                                sum=0;
                                if([[weightpriceArray objectAtIndex:m] count]>0){
                                    
                                    sum=+[[[weightpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                                }
                                if([[cutpriceArray objectAtIndex:m] count]>0){
                                    sum=sum+[[[cutpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                                }
                                if([[typepriceArray objectAtIndex:m] count]>0){
                                    sum=sum+[[[typepriceArray objectAtIndex:m] objectAtIndex:0] intValue];
                                }
                                NSString *calcSum=[NSString stringWithFormat:@"%d",sum];
                                [priceArray addObject:calcSum];
                            }
                            else{
                                [priceArray addObject:concatedPrice];
                            }
                            
                            if([[weightlabelArray objectAtIndex:m] count]>0){
                                [selectedWeightLabelIndex addObject:[[weightlabelArray objectAtIndex:m] objectAtIndex:0]];
                                [selectedWeightValueIndex addObject:[[weightValueArray objectAtIndex:m] objectAtIndex:0]];
                                [selectedWeightPriceIndex addObject:[[weightpriceArray objectAtIndex:m] objectAtIndex:0]];
                                
                            }
                            else{
                                [selectedWeightLabelIndex addObject:@"0"];
                                [selectedWeightValueIndex addObject:@"0"];
                                [selectedWeightPriceIndex addObject:@"0"];
                                
                            }
                            if([[cutlabelArray objectAtIndex:m] count]>0){
                                [selectedCutLabelIndex addObject:[[cutlabelArray objectAtIndex:m] objectAtIndex:0]];
                                [selectedCutValueIndex addObject:[[cutValueArray objectAtIndex:m] objectAtIndex:0]];
                                [selectedCutPriceIndex addObject:[[cutpriceArray objectAtIndex:m] objectAtIndex:0]];
                                
                            }
                            else{
                                [selectedCutLabelIndex addObject:@"0"];
                                [selectedCutValueIndex addObject:@"0"];
                                [selectedCutPriceIndex addObject:@"0"];
                                
                            }
                            if([[typelabelArray objectAtIndex:m] count]>0){
                                [selectedTypeLabelIndex addObject:[[typelabelArray objectAtIndex:m] objectAtIndex:0]];
                                [selectedTypeValueIndex addObject:[[typeValueArray objectAtIndex:m] objectAtIndex:0]];
                                [selectedTypePriceIndex addObject:[[typepriceArray objectAtIndex:m] objectAtIndex:0]];
                                
                            }
                            else{
                                [selectedTypeLabelIndex addObject:@"0"];
                                [selectedTypeValueIndex addObject:@"0"];
                                [selectedTypePriceIndex addObject:@"0"];
                                
                            }
                            
                        }
                        
                        [self fillContainwishArray:idArray AndWishIdArray:wishlistIdArray];

                        
                        
                        
                        
                        //                OfflineDb *object=[[OfflineDb alloc]init];
                        
//                        [self getRangeValuesOfPrice:priceArray];
                        
                        [HUD hide:YES];
                        
                        
                        NSLog(@"Idarray1=%@",idArray);
                        
                        if ([idArray count] > 0)
                        {
                            _pageTableView.delegate = self;
                            _pageTableView.dataSource = self;
                            [self.pageTableView reloadData];
                            
                        }
                        else
                        {
                            
                            _img_BackIcon.hidden=NO;
                            _pageTableView.hidden=YES;
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@"Alert"
                                                          message:@"No data found."
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action) {
                                                                           //Do Some action here
                                                                           [HUD hide:YES];
                                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                                           
                                                                       }];
                            [alert addAction:ok];
                            
                            [self presentViewController:alert animated:YES completion:nil];
                            
                            
                        }
                    
                    }
                    
                    

                    
                    
                    
                    
                    
                   
                    
                    
                }
                else{
                    
                    
                    _img_BackIcon.hidden=NO;
                    _pageTableView.hidden=YES;

                    [HUD hide:YES];
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Alert"
                                                  message:@"No data found."
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   //Do Some action here
                                                                   [HUD hide:YES];
                                                                   nameArray=[[NSMutableArray alloc] init];
                                                                   idArray=[[NSMutableArray alloc] init];
                                                                   imagesArray=[[NSMutableArray alloc] init];
                                                                   idArray=[[NSMutableArray alloc] init];
                                                                   nameArray=[[NSMutableArray alloc] init];
                                                                   descriptionArray=[[NSMutableArray alloc] init];
                                                                   imagesArray=[[NSMutableArray alloc] init];
                                                                   skuArray=[[NSMutableArray alloc] init];
                                                                   priceArray=[[NSMutableArray alloc] init];
                                                                   shortDescArray=[[NSMutableArray alloc] init];
                                                                   idContainInLocal=[[NSMutableArray alloc] init];
                                                                   localQtyArr=[[NSMutableArray alloc] init];
                                                                   foodTypeArr=[[NSMutableArray alloc] init];
                                                                   
                                                                   localCutOpIdArray=[[NSMutableArray alloc] init];
                                                                   localCutOpValArray=[[NSMutableArray alloc] init];
                                                                   localCutOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   localWeightOpIdArray=[[NSMutableArray alloc] init];
                                                                   localWeightOpValArray=[[NSMutableArray alloc] init];
                                                                   localWeightOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   localTypeOpIdArray=[[NSMutableArray alloc] init];
                                                                   localTypeOpValArray=[[NSMutableArray alloc] init];
                                                                   localTypeOpPriceArray=[[NSMutableArray alloc] init];
                                                                   
                                                                   NSLog(@"Idarray1=%@",idArray);
                                                                   [_pageTableView reloadData];
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                                   
                                                               }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                
            } @catch (NSException *exception) {
                NSLog(@"Exception = %@",exception);
                [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                
            }
            
        }
        
    });
    
    
}

-(void)fillContainwishArray:(NSMutableArray *)prodIdArr AndWishIdArray:(NSMutableArray *)wIdArr{
    for (int i=0; i<[prodIdArr count]; i++) {
        if ([wIdArr containsObject:[prodIdArr objectAtIndex:i]]) {
            [containInWishListArray addObject:@"1"];
        }
        else
        {
            [containInWishListArray addObject:@"0"];
            
        }
    }
    
}

-(void)getRangeValuesOfPrice:(NSArray *)priArr{
    NSMutableArray *integerArrayFor=[[NSMutableArray alloc] init];
    [integerArrayFor addObjectsFromArray:priArr];
    for (int i=0; i<[integerArrayFor count]; i++) {
        NSString *a=0;
        
        for(int j=i+1 ;j<[integerArrayFor count]; j++)
        {
            if ([[integerArrayFor objectAtIndex:i] intValue]> [[integerArrayFor objectAtIndex:j] intValue]) {
                a=[integerArrayFor objectAtIndex:i] ;
                [integerArrayFor replaceObjectAtIndex:i withObject:[integerArrayFor objectAtIndex:j]];
                [integerArrayFor replaceObjectAtIndex:j withObject:a];

                
            }
        
        }
    }
    
//    NSSortDescriptor *sortDescriptor;
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil
//                                                 ascending:YES] ;
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    NSArray *sortedArray;
//    sortedArray = [integerArrayFor sortedArrayUsingDescriptors:sortDescriptors];
    NSString *min,*max;

    if ([integerArrayFor count]==1) {
        _btn_filter.hidden=YES;
        
        //        min=[NSString stringWithFormat:@"%d",[[sortedArray firstObject] intValue]];
        //        max=[NSString stringWithFormat:@"%d",[[sortedArray lastObject] intValue]];
    }
    else if([integerArrayFor count]==0){
        
        _btn_filter.hidden=YES;
    }
    else{
        _btn_filter.hidden=NO;
        
        min=[NSString stringWithFormat:@"%d",[[integerArrayFor firstObject] intValue]];
        max=[NSString stringWithFormat:@"%d",[[integerArrayFor lastObject] intValue]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] ==nil || [[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] length] <=0) {
        [[NSUserDefaults standardUserDefaults] setObject:min forKey:MINPRICE];
        
    }
    else{
        int compMin=[[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] intValue];
        if (compMin<[min intValue]) {
            
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:min forKey:MINPRICE];
            
        }
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] ==nil || [[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] length] <=0) {
        [[NSUserDefaults standardUserDefaults] setObject:max forKey:MAXPRICE];
        
    }
    else{
        int compMin=[[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] intValue];
        if (compMin>[max intValue]) {
            
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:max forKey:MAXPRICE];
            
        }
    }
    
    
    
}

#pragma mark UITableviewDelegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableVw
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return idArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString *simpleTableIdentifier1 = @"CustomTableViewCell";
    //
    //    CustomTableViewCell *cell1 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier1];
    
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell2 = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    if (cell2 == nil)
    {
        cell2 = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    //cell.img_product.backgroundColor=[UIColor greenColor];
    //cell.img_product.image=[UIImage imageNamed:[_detailArr objectAtIndex:indexPath.row]];
    
       [cell2.qtyBackView.layer setCornerRadius:15.0f];
       cell2.qtyBackView.layer.borderColor = [UIColor colorWithRed:189.0/255.0f green:189.0/255.0f blue:189.0/255.0f alpha:1.0].CGColor;
       cell2.qtyBackView.layer.borderWidth = 1.0f;
       [cell2.qtyBackView.layer setMasksToBounds:YES];
    
    @try
    {
        
        cell2.lbl_name.text=[nameArray objectAtIndex:indexPath.row];
//        [cell2.lbl_name sizeToFit];
        
        cell2.lbl_shortDesc.text=[shortDescArray objectAtIndex:indexPath.row];
        NSString *image=[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]];
        [cell2.img_product sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            [HUD hide:YES];
            
            
        }];
        
        //        [cell2.lbl_shortDesc sizeToFit];
        
        CGSize constraint = CGSizeMake(cell2.lbl_shortDesc.frame.size.width, CGFLOAT_MAX);
        CGSize size;
        
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [cell2.lbl_shortDesc.text boundingRectWithSize:constraint
                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                 attributes:@{NSFontAttributeName:cell2.lbl_shortDesc.font}
                                                                    context:context].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        
        cell2.shortDescHeight.constant=size.height;
        
        cell2.lbl_qty.text=@"1";
        
        if([[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"0"] || [[brandValueArr objectAtIndex:indexPath.row] isEqualToString:@"(null)"] || [brandValueArr objectAtIndex:indexPath.row] == nil)
        {
            cell2.lbl_brandName.text=@"";
            cell2.brandHeight.constant=0;
            cell2.lbl_brandName.hidden=YES;
        }
        else
        {
            cell2.lbl_brandName.hidden=NO;
            cell2.brandHeight.constant=23;
            cell2.lbl_brandName.text=[brandValueArr objectAtIndex:indexPath.row];
        }
        //[self.pageTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];

        cell2.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[[priceArray objectAtIndex:indexPath.row] floatValue]];
        
        
        NSString *weiarrr=[weightArray objectAtIndex:indexPath.row];
        if ((NSString *)[NSNull null] == weiarrr)
        {
            [weightArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        
        
        cell2.lbl_noAttrWeight.text=[NSString stringWithFormat:@"Weight: %.f gm",[[weightArray objectAtIndex:indexPath.row] floatValue]];
        
        if ([[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0.0000"] || [[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
        {
            cell2.weightLblHeight.constant=0;
            cell2.lbl_noAttrWeightHeight.constant=0;
        }
        else
        {
            cell2.weightLblHeight.constant=15;
            cell2.lbl_noAttrWeightHeight.constant=15;
            
        }
        
        if ([[containInWishListArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            [cell2.btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
        }
        else{
            
            [cell2.btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
        }

        
        cell2.btn_add.tag=indexPath.row;
        cell2.btn_sub.tag=indexPath.row;
        cell2.btn_addCart.tag=indexPath.row;
        cell2.btn_weight.tag=indexPath.row;
        cell2.btn_cut.tag=indexPath.row;
        cell2.btn_type.tag=indexPath.row;
        cell2.btn_wish.tag=indexPath.row;
        cell2.btn_weightHeight.constant=0;
        cell2.lbl_weightHeight.constant=0;
        cell2.btn_cutHeight.constant=0;
        cell2.lbl_cutHeight.constant=0;
        cell2.btn_typeHeight.constant=0;
        cell2.lbl_typeHeight.constant=0;
        cell2.btn_weight.hidden=YES;
        cell2.lbl_weight.hidden=YES;
        cell2.btn_cut.hidden=YES;
        cell2.lbl_cut.hidden=YES;
        cell2.btn_type.hidden=YES;
        cell2.lbl_type.hidden=YES;
        
        [cell2.contentBackView.layer setShadowOpacity:0.0];
        [cell2.contentBackView.layer setShadowRadius:1.0];
        [cell2.contentBackView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        
        if([[dropLabelArr objectAtIndex:indexPath.row] count ]>0)
        {
            
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            
            
            
            int a=40;
            
            if ([[dropLabelArr objectAtIndex:indexPath.row] count]>0)
            {
                // cell2.qtyBackViewConstraint.constant=30;
                
                for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
                {
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                    {
                        a=a+cell2.btn_cutHeight.constant;
                        
                        cell2.btn_cutHeight.constant=21;
                        cell2.lbl_cutHeight.constant=21;
                        cell2.btn_cut.hidden=NO;
                        cell2.lbl_cut.hidden=NO;
                        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
                        {
                            cell2.lbl_cut.text=@"Pieces";
                        }

                        
                        if([[cutlabelArray objectAtIndex:indexPath.row] count]>0){
                            // =[[[cutpriceArray objectAtIndex:i] objectAtIndex:0] intValue];
                            [cell2.btn_cut setTitle:[[cutlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                        }
                        
                        
                    }
                    else
                    {
                    }
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
                    {
                        a=a+cell2.btn_weightHeight.constant;
                        
                        cell2.btn_weightHeight.constant=21;
                        cell2.lbl_weightHeight.constant=21;
                        cell2.btn_weight.hidden=NO;
                        cell2.lbl_weight.hidden=NO;
                        if([[weightlabelArray objectAtIndex:indexPath.row] count]>0){
                            
                            [cell2.btn_weight setTitle:[[weightlabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                        }
                        
                    }
                    else
                    {
                    }
                    
                    if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
                    {
                        a=a+cell2.btn_typeHeight.constant;
                        
                        cell2.btn_typeHeight.constant=21;
                        cell2.lbl_typeHeight.constant=21;
                        cell2.btn_type.hidden=NO;
                        cell2.lbl_type.hidden=NO;
                        if([[typelabelArray objectAtIndex:indexPath.row] count]>0){
                            [cell2.btn_type setTitle:[[typelabelArray objectAtIndex:indexPath.row] objectAtIndex:0] forState:UIControlStateNormal];
                        }
                        
                        
                    }
                    else
                    {
                    }
                    
                }
            }
            else
            {
                
                cell2.btn_weightHeight.constant=0;
                cell2.lbl_weightHeight.constant=0;
                cell2.btn_cutHeight.constant=0;
                cell2.lbl_cutHeight.constant=0;
                cell2.btn_typeHeight.constant=0;
                cell2.lbl_typeHeight.constant=0;
                cell2.btn_weight.hidden=YES;
                cell2.lbl_weight.hidden=YES;
                cell2.btn_cut.hidden=YES;
                cell2.lbl_cut.hidden=YES;
                cell2.btn_type.hidden=YES;
                cell2.lbl_type.hidden=YES;
                
            }
            
            cell2.backViewConstraint.constant=cell2.nameHeight.constant+cell2.shortDescHeight.constant+cell2.brandHeight.constant+a+150;
            
            
        }
        else
        {
           cell2.backViewConstraint.constant=cell2.nameHeight.constant+cell2.shortDescHeight.constant+cell2.brandHeight.constant+cell2.lbl_noAttrWeightHeight.constant+150;
        }
        
        
    }
    @catch (NSException *exception)
    {
        [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
    }
    
    
    return cell2;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Section 0
    if (indexPath.section == 0) {
        
        // Is Last row?
        if ([idArray count] == (indexPath.row+1)) {
            //Yes
            if(tempLimit >= limit){
                _btn_loadMore.hidden=NO;
                _loadMoreHeight.constant=30;
            }
        }
        else{
            // other rows
            
            _btn_loadMore.hidden=YES;
            _loadMoreHeight.constant=0;
            
        }
        
        
    }
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomTableViewCell";
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    // long a=cell.nameHeight.constant+cell.brandHeight.constant+cell.cartWeightHeight.constant+70;
    
    
    //cell2.qtyBackViewConstraint.constant;
    
    cell.lbl_shortDesc.text = [shortDescArray objectAtIndex:indexPath.row];
    
    CGSize constraint = CGSizeMake(cell.lbl_shortDesc.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [cell.lbl_shortDesc.text boundingRectWithSize:constraint
                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{NSFontAttributeName:cell.lbl_shortDesc.font}
                                                                context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
//    CGSize constraint = CGSizeMake(cell.lbl_shortDesc.frame.size.height, CGFLOAT_MAX);
//    CGSize size;
//
//    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
//    CGSize boundingBox = [cell.lbl_shortDesc.text boundingRectWithSize:constraint
//                                                                options:NSStringDrawingUsesLineFragmentOrigin
//                                                             attributes:@{NSFontAttributeName:cell.lbl_shortDesc.font}
//                                                                context:context].size;
//
//    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    cell.shortDescHeight.constant=size.height;
    
//    if ((NSString *)[NSNull null] == [weightArray objectAtIndex:indexPath.row])
//    {
//        cell.lbl_noAttrWeightHeight.constant=0;
//    }
//    else
//    {
//       cell.lbl_noAttrWeightHeight.constant=15;
//    }
    
    if ((NSString *)[NSNull null] == [weightArray objectAtIndex:indexPath.row] || [[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0.0000"] || [[weightArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
    {
        cell.lbl_noAttrWeightHeight.constant=0;
    }
    else
    {
        cell.lbl_noAttrWeightHeight.constant=15;
        
    }
    
    long a=cell.nameHeight.constant+cell.brandHeight.constant+cell.shortDescHeight.constant+cell.lbl_noAttrWeightHeight.constant;
    
    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
    {
        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"] )
        {
            a=a+cell.btn_cutHeight.constant;
            
        }
        
        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Weight"])
        {
            a=a+cell.btn_weightHeight.constant;
            
            
        }
        
        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Type"])
        {
            a=a+cell.btn_typeHeight.constant;
            
        }
        
    }
    
     NSLog(@" Height Value = %ld",a);
    
            return a+100;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *allcutLabels=[[cutlabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allcutValues=[[cutValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allcutPrices=[[cutpriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    NSString *allweightLabels=[[weightlabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allweightValues=[[weightValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *allweightPrices=[[weightpriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    NSString *alltypeLabels=[[typelabelArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *alltypeValues=[[typeValueArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    NSString *alltypePrices=[[typepriceArray objectAtIndex:indexPath.row] componentsJoinedByString:@","];
    
    
    
    
    ItemDetailsController *viewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"ItemDetailsController"];
    viewController.fetchedItemId=[idArray objectAtIndex:indexPath.row];
    viewController.fetchedItemName=[nameArray objectAtIndex:indexPath.row];
    viewController.fetchedItemDesc=[descriptionArray objectAtIndex:indexPath.row];
    viewController.fetchedItemImage=[imagesArray objectAtIndex:indexPath.row];
    
    if (!((NSArray *)[NSNull null] == [foodTypeArr objectAtIndex:indexPath.row]))
    {
        viewController.fetchedFoodType=[foodTypeArr objectAtIndex:indexPath.row];
        
    }
    else
    {
        
        viewController.fetchedFoodType=@"0";
        
        
    }
    
   // viewController.fetchedFoodType=[foodTypeArr objectAtIndex:indexPath.row];
    //        viewController.fetchedSelectedStatus=[idContainInLocal objectAtIndex:indexPath.row];
    viewController.fetchedItemPrice=[priceArray objectAtIndex:indexPath.row];
    viewController.fetchedCategoryId=categoryId;
    viewController.fetchedName=_lbl_titleText.text;
    //        viewController.fetchedMsgId=[fieldIdArray objectAtIndex:indexPath.row];
    //        viewController.fetchedSpiceId=[dropDownIdArray objectAtIndex:indexPath.row];
    //        viewController.fetchedLevelIds=levelIds;
    
    viewController.fetchedBrandVal=[brandValueArr objectAtIndex:indexPath.row];
    viewController.fetchedBrandLabel=[brandIndexArr objectAtIndex:indexPath.row];
    viewController.allCutLabel=allcutLabels;
    viewController.allCutPrice=allcutPrices;
    viewController.allCutValues=allcutValues;
    viewController.allWeightLabel=allweightLabels;
    viewController.allWeightValues=allweightValues;
    viewController.allWeightPrice=allweightPrices;
    viewController.allTypeLabel=alltypeLabels;
    viewController.allTypeValues=alltypeValues;
    viewController.allTypePrice=alltypePrices;
    
    viewController.fetchedShortDesc=[shortDescArray objectAtIndex:indexPath.row];
    viewController.fetchedSmallImg=[imageSmallArray objectAtIndex:indexPath.row];
    viewController.fetchedMainImg=[imageMainArray objectAtIndex:indexPath.row];
    viewController.fetchedThumbImg=[imageThumbArray objectAtIndex:indexPath.row];
    viewController.fetchedWeight=[weightArray objectAtIndex:indexPath.row];
    viewController.fetchedCutOptionId=[cutOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedWeightOptionId=[weightOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedTypeOptionId=[typeOptionIdArray objectAtIndex:indexPath.row];
    viewController.fetchedWishStat=[containInWishListArray objectAtIndex:indexPath.row];
    for (int i=0; i<[[dropLabelArr objectAtIndex:indexPath.row] count]; i++)
    {
        if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
        {
            if ([[[dropLabelArr objectAtIndex:indexPath.row] objectAtIndex:i] isEqualToString:@"Pieces"])
            {
                viewController.fetchDropLabelName=@"Pieces:";
            }
            else
            {
                viewController.fetchDropLabelName=@"Cut:";
            }
            
        }
        else
        {
        }
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
    
    
    
    
    
}
//- (CGFloat)tableView:(UITableView *)tableView
//heightForFooterInSection:(NSInteger)section {
//    //differ between your sections or if you
//    //have only on section return a static value
//    if (idArray.count >0) {
//        
//        return 44;
//
//    }
//    else{
//        return 0;
//
//    }
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    
//    
//    CGRect screenBounds = [UIScreen mainScreen].bounds;
//    CGFloat width = screenBounds.size.width;
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 44)];
//    view.backgroundColor = [UIColor blackColor];
//
//    
//    UIButton *addProject = [[UIButton alloc] initWithFrame:CGRectMake(0, 0 , width,30)];
//    [addProject setTitle:@"Load More" forState:UIControlStateNormal];
//    [addProject addTarget:self action:@selector(loadMore:) forControlEvents:UIControlEventTouchUpInside];
//    [view addSubview:addProject];
//    
//    //return the view for the footer
//    return view;
//}

#pragma mark Other methods
- (IBAction)loadMore:(id)sender
{
    
    NSLog(@"Load More");
    
    page=page+1;
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:nil animated:YES];
    

}


- (IBAction)weightMethod:(id)sender
{
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    pickerWeightLabelArr=[[NSMutableArray alloc] init];
    pickerWeightValueArr=[[NSMutableArray alloc] init];
    pickerWeightPriceArr=[[NSMutableArray alloc] init];
    [pickerWeightLabelArr addObjectsFromArray:[weightlabelArray objectAtIndex:row]];
    [pickerWeightValueArr addObjectsFromArray:[weightValueArray objectAtIndex:row]];
    [pickerWeightPriceArr addObjectsFromArray:[weightpriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=2;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedWeightLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             
                                             [cell.btn_weight setTitle:[selectedWeightLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                             
                                                                                      }
                                         else{
                                             [selectedWeightLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedWeightValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedWeightPriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             
                                             [cell.btn_weight setTitle:[selectedWeightLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             

                                         }
                                         
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                         cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         
                                     }
                                     @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    

}

- (IBAction)typeMethod:(id)sender
{
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    
    pickerTypeLabelArr=[[NSMutableArray alloc] init];
    pickerTypeValueArr=[[NSMutableArray alloc] init];
    pickerTypePriceArr=[[NSMutableArray alloc] init];
    [pickerTypeLabelArr addObjectsFromArray:[typelabelArray objectAtIndex:row]];
    [pickerTypeValueArr addObjectsFromArray:[typeValueArray objectAtIndex:row]];
    [pickerTypePriceArr addObjectsFromArray:[typepriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=3;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedTypeLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             
                                             [cell.btn_type setTitle:[selectedTypeLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                            }
                                         else{
                                             
                                             [selectedTypeLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedTypeValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedTypePriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             [cell.btn_type setTitle:[selectedTypeLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             

                                             
                                             
                                         }
                                         
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
//                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
//                                         cell.lbl_price.text=strPrice;
//                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         if(calcPrice==0)
                                         {
                                             
                                         }
                                         else
                                         {
                                             NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                             cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                             [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         }
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    

}

- (IBAction)wishMethod:(id)sender {
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";

    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
    
    if ([isLog isEqualToString:@"YES"]) {
        
        NSLog(@"%ld",[sender tag]);
        
        HelperClass *s=[[HelperClass alloc]init];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            BOOL connection=s.getInternetStatus;
            
            if (!connection)
            {
                
                
                NSLog(@"Hello Net connectiion is not pres;ent....");
                
                [HUD hide:YES];
                
                UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Connection Lost" message:@"Please check your internet connection!" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
                
                
                [alrt addAction:ok];
                [self presentViewController:alrt animated:YES completion:nil];
                
                
            }
            else
            {
                @try {
                    long row = [sender tag];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
                    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
                    
                    
                    
                    if ([[containInWishListArray objectAtIndex:row] isEqualToString:@"0"]) {
                        NSArray *getdata=[wishObj insertIntoProductList:[idArray objectAtIndex:row]];
                        NSLog(@"%@",getdata);
                        if ([getdata count]>0) {
                            
                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
                            if (success) {
                                [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
                                [containInWishListArray replaceObjectAtIndex:row withObject:@"1"];
                                
                            }
                            else{
                                NSString *msg=[getdata valueForKey:@"message"];
                                [ProgressHUD showError:msg Interaction:NO];
                                
                            }
                        }
                        else{
                            
                            [ProgressHUD showError:@"Unable to add to wishlist" Interaction:NO];
                            
                        }
                        [HUD hide:YES];
 
                    }
                    else if ([[containInWishListArray objectAtIndex:row] isEqualToString:@"1"]){
                        
                        NSArray *getdata=[wishObj deleteFromWishList:[idArray objectAtIndex:row]];
                        NSLog(@"%@",getdata);
                        if ([getdata count]>0) {
                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
                            if (success) {
                                
                                [cell.btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
                                [containInWishListArray replaceObjectAtIndex:row withObject:@"0"];
                            }
                            else{
                                NSString *msg=[getdata valueForKey:@"message"];
                                [ProgressHUD showError:msg Interaction:NO];
                                
                            }
                        }
                        else{
                            
                            [ProgressHUD showError:@"Unable to delete from wishlist" Interaction:NO];
                            
                        }
                        [HUD hide:YES];

                        
                    }
                    
                    
                } @catch (NSException *exception) {
                    [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                }
                
            }
        });
        
        
    }
    else{
        [HUD hide:YES];

        UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Alert" message:@"You need to login first" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
        [alrt addAction:ok];
        [self presentViewController:alrt animated:YES completion:nil];
        
        
        
    }
    

    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cartMethod:(id)sender
{
    //    OfflineDb *object=[[OfflineDb alloc]init];
    NSArray *dataArray=[object fetch_CartDetails];
    NSLog(@"%@",dataArray);
    if ([dataArray count]>0)
    {
        CartController *viewController =[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
        viewController.comingFrom=@"SearchVC";
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    else{
        
        [ProgressHUD showError:@"Cart Empty" Interaction:NO];
    }

}
- (IBAction)cutMethod:(id)sender {
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    
    
    pickerCutLabelArr=[[NSMutableArray alloc] init];
    pickerCutValueArr=[[NSMutableArray alloc] init];
    pickerCutPriceArr=[[NSMutableArray alloc] init];
    [pickerCutLabelArr addObjectsFromArray:[cutlabelArray objectAtIndex:row]];
    [pickerCutValueArr addObjectsFromArray:[cutValueArray objectAtIndex:row]];
    [pickerCutPriceArr addObjectsFromArray:[cutpriceArray objectAtIndex:row]];
    
    //    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
    //    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"0"];
    //    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=1;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedCutLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             //                                     [spicelevelArray replaceObjectAtIndex:[sender tag] withObject:selectedSpiceIndex];
                                             [cell.btn_cut setTitle:[selectedCutLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                             
                                         }
                                         else{
                                             
                                             
                                             [selectedCutLabelIndex replaceObjectAtIndex:row withObject:selectedLabel];
                                             [selectedCutValueIndex replaceObjectAtIndex:row withObject:selectedValue];
                                             [selectedCutPriceIndex replaceObjectAtIndex:row withObject:selectedPrice];
                                             
                                             [cell.btn_cut setTitle:[selectedCutLabelIndex objectAtIndex:row] forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";

                                             
                                         }
                                         
                                         int calcPrice=([[selectedCutPriceIndex objectAtIndex:row] intValue]+[[selectedWeightPriceIndex objectAtIndex:row] intValue]+[[selectedTypePriceIndex objectAtIndex:row] intValue]);
                                         if(calcPrice==0)
                                         {
                                             
                                         }
                                         else
                                         {
                                             NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                             cell.lbl_price.text=[NSString stringWithFormat:@"₹ %.2f",[strPrice floatValue]];
                                             [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         }
                                         
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                     
                                     
                                     
                                     
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    

}

#pragma mark UIPickerDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// Total rows in our component.

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger i;
    if (pickerView.tag==1) {
        i=[pickerCutLabelArr count];
        
    }
    else if (pickerView.tag==2){
        i=[pickerWeightLabelArr count];
    }
    else if (pickerView.tag==3){
        i=[pickerTypeLabelArr count];
        
    }
    
    //    i=[mirchImagesArr count];
    return i;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 50;
}
// Display each row's data.

//-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    id i;
//    i=[mirchImagesArr objectAtIndex: row];
//    return i;
//}

// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (pickerView.tag==1) {
        
        selectedLabel=[pickerCutLabelArr objectAtIndex:row];
        selectedValue=[pickerCutValueArr objectAtIndex:row];
        selectedPrice=[pickerCutPriceArr objectAtIndex:row];
        
    }
    else if (pickerView.tag==2){
        selectedLabel=[pickerWeightLabelArr objectAtIndex:row];
        selectedValue=[pickerWeightValueArr objectAtIndex:row];
        selectedPrice=[pickerWeightPriceArr objectAtIndex:row];
        
    }
    else if (pickerView.tag==3){
        selectedLabel=[pickerTypeLabelArr objectAtIndex:row];
        selectedValue=[pickerTypeValueArr objectAtIndex:row];
        selectedPrice=[pickerTypePriceArr objectAtIndex:row];
        
    }
    
    //    selectedCustId=[custIdArray objectAtIndex:row];
    
    
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    // Get the text of the row.
    
    UILabel *lblRow = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,spicesPicker.bounds.size.width, 44.0f)];
    
    
    if (pickerView.tag==1) {
        NSString *rowItem = [pickerCutLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==2){
        NSString *rowItem = [pickerWeightLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==3){
        NSString *rowItem = [pickerTypeLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    
    
    
    
    
    return lblRow;
    
    
}
#pragma mark UITextFieldDelegates
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    globalvar=textField;
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{


//    long row = [globalvar tag];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
//    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];

//    [cell.btn_addCart setBackgroundImage:[UIImage imageNamed:@"i-c-empty140x60.png"] forState:UIControlStateNormal];
//    [idContainInLocal replaceObjectAtIndex:[globalvar tag] withObject:@"0"];
//    [localQtyArr replaceObjectAtIndex:[globalvar tag] withObject:cell.lbl_qty.text];

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [globalvar resignFirstResponder];

    
    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    
    
    weightlabelArray=[[NSMutableArray alloc] init];
    weightValueArray=[[NSMutableArray alloc] init];
    weightpriceArray=[[NSMutableArray alloc] init];
    
    cutlabelArray=[[NSMutableArray alloc] init];
    cutValueArray=[[NSMutableArray alloc] init];
    cutpriceArray=[[NSMutableArray alloc] init];
    
    typelabelArray=[[NSMutableArray alloc] init];
    typeValueArray=[[NSMutableArray alloc] init];
    typepriceArray=[[NSMutableArray alloc] init];
    
    localCutOpIdArray=[[NSMutableArray alloc] init];
    localCutOpValArray=[[NSMutableArray alloc] init];
    localCutOpPriceArray=[[NSMutableArray alloc] init];
    
    localWeightOpIdArray=[[NSMutableArray alloc] init];
    localWeightOpValArray=[[NSMutableArray alloc] init];
    localWeightOpPriceArray=[[NSMutableArray alloc] init];
    
    localTypeOpIdArray=[[NSMutableArray alloc] init];
    localTypeOpValArray=[[NSMutableArray alloc] init];
    localTypeOpPriceArray=[[NSMutableArray alloc] init];
    brandIndexArr=[[NSMutableArray alloc] init];
    brandValueArr=[[NSMutableArray alloc] init];
    weightArray=[[NSMutableArray alloc] init];
    optionIdArray=[[NSMutableArray alloc] init];
    imageMainArray=[[NSMutableArray alloc] init];
    imageThumbArray=[[NSMutableArray alloc] init];
    imageSmallArray=[[NSMutableArray alloc] init];
    
    typeOptionIdArray=[[NSMutableArray alloc] init];
    weightOptionIdArray=[[NSMutableArray alloc] init];
    cutOptionIdArray=[[NSMutableArray alloc] init];
    localCutOpLabelArray=[[NSMutableArray alloc] init];
    localWeightOpLabelArray=[[NSMutableArray alloc] init];
    localTypeOpLabelArray=[[NSMutableArray alloc] init];
    selectedWeightLabelIndex=[[NSMutableArray alloc] init];
    selectedWeightValueIndex=[[NSMutableArray alloc] init];
    selectedWeightPriceIndex=[[NSMutableArray alloc] init];
    
    selectedCutLabelIndex=[[NSMutableArray alloc] init];
    selectedCutPriceIndex=[[NSMutableArray alloc] init];
    selectedCutValueIndex=[[NSMutableArray alloc] init];
    selectedTypeLabelIndex=[[NSMutableArray alloc] init];
    selectedTypeValueIndex=[[NSMutableArray alloc] init];
    selectedTypePriceIndex=[[NSMutableArray alloc] init];
    newPriceArray=[[NSMutableArray alloc] init];
    wlArr=[[NSMutableArray alloc] init];
    wvArr=[[NSMutableArray alloc] init];
    wpArr=[[NSMutableArray alloc] init];
    clArr=[[NSMutableArray alloc] init];
    cvArr=[[NSMutableArray alloc] init];
    cpArr=[[NSMutableArray alloc] init];
    tlArr=[[NSMutableArray alloc] init];
    tvArr=[[NSMutableArray alloc] init];
    tpArr=[[NSMutableArray alloc] init];
    
    
    if ([_txt_search.text isEqualToString:@""])
    {
        _img_BackIcon.hidden=NO;
        _pageTableView.hidden=YES;
        NSLog(@"%@",_txt_search.text);
        [_pageTableView reloadData];
    }
    else
    {

        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:nil animated:YES];

    }
    return [textField resignFirstResponder];


}






- (IBAction)backMethod:(id)sender {
    NSArray *brandListArr=[[NSMutableArray alloc] init];
    NSArray *brandIdArr=[[NSMutableArray alloc] init];
    NSArray *brandData=[object fetch_AttributeValue:@"brand"];
    brandListArr=[brandData valueForKey:@"attribute_label"];
    brandIdArr=[brandData valueForKey:@"attribute_value"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMAXPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:BRANDVALUE];
    [APP_DELEGATE.selectBarndArray addObjectsFromArray:brandIdArr];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISFILTER];

    [self.navigationController popViewControllerAnimated:YES];
    
}



- (IBAction)addCartMethod:(id)sender {
   
    
    NSLog(@"%ld",[sender tag]);
    
    
    //  if ([self validations:[sender tag]]) {
    NSString *name=[nameArray objectAtIndex:[sender tag]];
    //    NSString *sk=[skuArray objectAtIndex:[sender tag]];
    NSString *idStr=[idArray objectAtIndex:[sender tag]];
    NSString *catId=categoryId;
    NSString *img=[imagesArray objectAtIndex:[sender tag]];
    NSString *imgSmall=[imageSmallArray objectAtIndex:[sender tag]];
    NSString *imgMain=[imageMainArray objectAtIndex:[sender tag]];
    NSString *imgThumb=[imageThumbArray objectAtIndex:[sender tag]];
    NSString *desc=[descriptionArray objectAtIndex:[sender tag]];
    NSString *shortDesc=[shortDescArray objectAtIndex:[sender tag]];
    NSString *weight=[weightArray objectAtIndex:[sender tag]];
    
    NSString *type;
    if (!((NSArray *)[NSNull null] == [foodTypeArr objectAtIndex:[sender tag]]))
    {
       type=[foodTypeArr objectAtIndex:[sender tag]];
        
    }
    else
    {
        
         type=@"0";
        
        
    }
    //NSString *type=[foodTypeArr objectAtIndex:[sender tag]];

    NSString *price=[priceArray objectAtIndex:[sender tag]];
    NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
    NSString *currIndQty;
            CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
        currIndQty=cell.lbl_qty.text;
    
    //    NSString *msg=cell.txt_msg.text;
    //        NSString *msgId=[fieldIdArray objectAtIndex:[sender tag]];
    //                        [cutOptionIdArray addObject:@"0"];
    // [typeOptionIdArray addObject:@"0"];
    //[weightOptionIdArray addObject:@"0"];
    
    NSString *opid=[optionIdArray objectAtIndex:[sender tag]];
    NSString *brand=[brandValueArr objectAtIndex:[sender tag]];
    NSString *cutopid=[cutOptionIdArray objectAtIndex:[sender tag]];
    NSString *cutopval=[selectedCutValueIndex objectAtIndex:row];
    NSString *cutoppri=[selectedCutPriceIndex objectAtIndex:row];
    NSString *weiopid=[weightOptionIdArray objectAtIndex:[sender tag]];
    NSString *weiopval=[selectedWeightValueIndex objectAtIndex:row];
    NSString *weioppri=[selectedWeightPriceIndex objectAtIndex:row];
    NSString *typopid=[typeOptionIdArray objectAtIndex:[sender tag]];
    NSString *typopval=[selectedTypeValueIndex objectAtIndex:row];
    NSString *typoppri=[selectedTypePriceIndex objectAtIndex:row];
    NSString *cutoplabel=[selectedCutLabelIndex objectAtIndex:row];
    NSString *weioplabel=[selectedWeightLabelIndex objectAtIndex:row];
    NSString *typoplabel=[selectedTypeLabelIndex objectAtIndex:row];
    
    //For storing All data in database
    NSString *allcutLabels=[[cutlabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allcutValues=[[cutValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allcutPrices=[[cutpriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    NSString *allweightLabels=[[weightlabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allweightValues=[[weightValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *allweightPrices=[[weightpriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    NSString *alltypeLabels=[[typelabelArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *alltypeValues=[[typeValueArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    NSString *alltypePrices=[[typepriceArray objectAtIndex:[sender tag]] componentsJoinedByString:@","];
    
    
    //
    //        NSString *spiceLevel,*spiceId,*levelIds;
    //        NSString *selectecdLevelId;
    //        NSMutableArray *getSpiceVals=[[NSMutableArray alloc] init];
    //        int level=[[spicelevelArray objectAtIndex:[sender tag]] intValue];
    //        [getSpiceVals addObject:[dropValueArr objectAtIndex:[sender tag]]];
    //        if ([[getSpiceVals objectAtIndex:0] count]<=0) {
    //            spiceLevel=@"0";
    //            levelIds=@"0";
    //            selectecdLevelId=@"0";
    //
    //        }
    //        else{
    //
    //            if (level==0) {
    //                spiceLevel=[[getSpiceVals objectAtIndex:0] objectAtIndex:0];
    //                levelIds=[[getSpiceVals objectAtIndex:0] objectAtIndex:0];
    //                selectecdLevelId=@"1";
    //
    //
    //            }
    //            else{
    //                spiceLevel=[[getSpiceVals objectAtIndex:0] objectAtIndex:level-1];
    //                levelIds=[[getSpiceVals objectAtIndex:0] componentsJoinedByString:@","];
    //                selectecdLevelId=[spicelevelArray objectAtIndex:[sender tag]];
    //
    //            }
    //        }
    //        spiceId=[dropDownIdArray objectAtIndex:[sender tag]];
    NSString *qty=currIndQty;
    //        NSString *type=[foodTypeArr objectAtIndex:[sender tag]];
    
    if ([qty isEqualToString:@"0"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:@"Please select Quantity."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        
        float total=[qty floatValue]*[price floatValue];
        NSString *tot=[NSString stringWithFormat:@"%.2f",total];
        
        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
        
        ///Add in cart
        
        NSMutableArray *cartDbArray=[[NSMutableArray alloc]init];
        testDB_Obj.cart_id=idStr;
        testDB_Obj.cart_category_id=catId;
        testDB_Obj.cart_name=name;
        testDB_Obj.cart_image_url=img;
        testDB_Obj.cart_image_thumb=imgThumb;
        testDB_Obj.cart_image_small=imgSmall;
        testDB_Obj.cart_image_main=imgMain;
        testDB_Obj.cart_price=price;
        testDB_Obj.cart_qty=qty;
        testDB_Obj.cart_description=desc;
        testDB_Obj.cart_short_desc=shortDesc;
        testDB_Obj.cart_food_type=type;
        testDB_Obj.cart_total=tot;
        testDB_Obj.cart_origin=@"";
        
        testDB_Obj.cart_brand=brand;
        testDB_Obj.cart_weight=weight;
        testDB_Obj.cart_option_id=@"";
        testDB_Obj.cart_cut_option_id=cutopid;
        testDB_Obj.cart_cut_option_value=cutopval;
        testDB_Obj.cart_cut_option_price=cutoppri;
        testDB_Obj.cart_weight_option_id=weiopid;
        testDB_Obj.cart_weight_option_value=weiopval;
        testDB_Obj.cart_weight_option_price=weioppri;
        testDB_Obj.cart_type_option_id=typopid;
        testDB_Obj.cart_type_option_value=typopval;
        testDB_Obj.cart_type_option_price=typoppri;
        testDB_Obj.cart_cut_option_label=cutoplabel;
        testDB_Obj.cart_weight_option_label=weioplabel;
        testDB_Obj.cart_type_option_label=typoplabel;
        
        //For cart master
        testDB_Obj.cm_cart_id=idStr;
        testDB_Obj.cm_category_id=catId;
        testDB_Obj.cm_all_cut_values=allcutValues;
        testDB_Obj.cm_all_cut_labels=allcutLabels;
        testDB_Obj.cm_all_cut_prices=allcutPrices;
        testDB_Obj.cm_all_weight_values=allweightValues;
        testDB_Obj.cm_all_weight_labels=allweightLabels;
        testDB_Obj.cm_all_weight_prices=allweightPrices;
        testDB_Obj.cm_all_type_values=alltypeValues;
        testDB_Obj.cm_all_type_labels=alltypeLabels;
        testDB_Obj.cm_all_type_prices=alltypePrices;
        
        [cartDbArray addObject:testDB_Obj];
        [testDB_Obj checkCartEntry:cartDbArray];
        
        
        
        //                    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"1"];
        //                    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
        //                    [localCutOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_cut.titleLabel.text];
        //                    [localWeightOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_weight.titleLabel.text];
        //                    [localTypeOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_type.titleLabel.text];
        
        //                [messagesArray replaceObjectAtIndex:[sender tag] withObject:cell.txt_msg.text];
        
        //                    NSLog(@"%@",idContainInLocal);
        //                    NSLog(@"%@",localQtyArr);
        
        dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *str=[object fetch_CartDetails];
        NSLog(@"%@",str);
        NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
        if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
            self.navigationItem.rightBarButtonItem.badge.hidden = true;
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
            
        }
        else{
            self.navigationItem.rightBarButtonItem.badge.hidden = false;
            [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
            
            UIImage *image = [UIImage imageNamed:@"CartNew"];
           UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
           button.frame = CGRectMake(0,0,image.size.width, image.size.height);
               [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
           [button setBackgroundImage:image forState:UIControlStateNormal];
           
           UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
           self.navigationItem.rightBarButtonItem = navLeftButton;
           self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
            
//            _lbl_badgeCount.text=cartCount;
//            _lbl_badgeCount.layer.masksToBounds=YES;
//            _lbl_badgeCount.layer.cornerRadius=_lbl_badgeCount.frame.size.width/2;
            
            }
        });
        
        [self checkProgressBar];
//        self.lbl_badgeCount.text=cartCount;
        
        [ProgressHUD showSuccess:@"Item successfully added to cart" Interaction:NO];
        
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        
        [HUD hide:YES afterDelay:5.0];
        
        NSIndexPath *inde=[NSIndexPath indexPathForRow:[sender tag] inSection:0];
        [self.pageTableView beginUpdates];
        [self.pageTableView reloadRowsAtIndexPaths:@[inde] withRowAnimation:UITableViewRowAnimationNone];
        [self.pageTableView endUpdates];
        //                    [self.pageTableView reloadData];
        
        
        
        
        
        
        
        
    }
    

}

- (IBAction)filterMethod:(id)sender
{
    if([idArray count]>0)
    {
        FilterController *lo=[self.storyboard instantiateViewControllerWithIdentifier:@"FilterController"];
        [self.navigationController pushViewController:lo animated:YES];
    }
    else
    {
        [ProgressHUD showError:@"No data to filter" Interaction:NO];
    }
    

}


- (IBAction)addQtyMethod:(id)sender {
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
        CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
        NSString *currIndQty=cell.lbl_qty.text;
        quntity=[currIndQty intValue];
        if (quntity < 99)
        {
            quntity++;
            NSString *str=[NSString stringWithFormat:@"%d",quntity];
            cell.lbl_qty.text=str;
        }

}

- (IBAction)loadMoreMethod:(id)sender {
    page=page+1;
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct:) onTarget:self withObject:nil animated:YES];

}

- (IBAction)subQtyMethod:(id)sender {
    
    
    long row = [sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    
        CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
        NSString *currIndQty=cell.lbl_qty.text;
        quntity=[currIndQty intValue];
        if (quntity > 0)
        {
            quntity--;
            cell.lbl_qty.text=[NSString stringWithFormat:@"%d",quntity];
        }
    

}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Close App" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    //[alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}



-(void)checkProgressBar{
    
    NSArray *getTotalAmountArray=[object fetch_Total_Amount];
    NSString *amt=[[getTotalAmountArray valueForKey:@"total_cnt"] objectAtIndex:0];
    if (amt.length<=0 || [amt isEqualToString:@"(null)"]) {
        NSLog(@"nill");
        _progressBackViewHeight.constant=0;
        _progressView.hidden=YES;
        _lbl_progressMax.hidden=YES;
        _lbl_progressMin.hidden=YES;
        
        _progressBackView.hidden=YES;
        
    }
    else{
        _progressBackViewHeight.constant=60;
        _progressView.hidden=NO;
        _lbl_progressMax.hidden=NO;
        _lbl_progressMin.hidden=NO;
        
        _progressBackView.hidden=NO;
        
        _lbl_progressMin.text=@"0";
        //total_cnt
        NSString *storedAmt=[[NSUserDefaults standardUserDefaults] valueForKey:MAXLIMIT];
        _lbl_progressMax.text=storedAmt;
        
        if ([amt floatValue]<[_lbl_progressMax.text floatValue]) {
            _lbl_progressMessage.text=FREEFORSHIPPING;
            
            
            _progressBackView.hidden=NO;
            float maxValue = 1.0;
            float minValue = 0.0;
            
            CGFloat diff = ([amt floatValue] - 0);
            CGFloat scope = ([_lbl_progressMax.text floatValue] - 0);
            CGFloat progress;
            
            if(diff != 0.0) {
                progress = diff / scope;
            } else {
                progress = 0.0f;
            }
            
            
            
            float myValueConstrained = MAX(minValue, MIN(maxValue, progress)); // 1.0
            NSLog(@"%f",myValueConstrained);
            _progressView.progress=myValueConstrained;
            
            
            
            
        }
        else{
            //            _progressBackView.hidden=YES;
            _lbl_progressMessage.text=ELIGIBLE;
            _progressBackViewHeight.constant=30;
            _minHeight.constant=0;
            _maxHeight.constant=0;
            _barHeight.constant=0;
            _progressView.hidden=YES;
            _lbl_progressMax.hidden=YES;
            _lbl_progressMin.hidden=YES;
            
        }
        
        
    }
    
    
}

@end
