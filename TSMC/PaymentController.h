//
//  PaymentController.h
//  TSMC
//
//  Created by Rahul Lekurwale on 12/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface PaymentController : UIViewController
{
    MBProgressHUD *HUD;


}
@property NSMutableArray *orderparaDict;
@property (strong, nonatomic) IBOutlet UILabel *titleBack;
@property (strong, nonatomic) IBOutlet UIButton *btn_back;
- (IBAction)backMethod:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_titleText;
@property (strong, nonatomic) IBOutlet UIWebView *payView;

@property(weak,nonatomic)NSString *fetchedOrderId;
@end
