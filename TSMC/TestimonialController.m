//
//  TestimonialController.m
//  TSMC
//
//  Created by Rahul Lekurwale on 04/09/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "TestimonialController.h"
#import "UIImageView+WebCache.h"
#import "ViewController.h"
#import "AppConstant.h"
#import "thankYouViewController.h"

@interface TestimonialController ()
{
    UISwipeGestureRecognizer *gestureRight,*gestureLeft;
    //int indexToShow;
    NSInteger currentIndex;
    NSArray *imgArr;
    NSUInteger indexToShow;


}
@end

@implementation TestimonialController
@synthesize nameArray,imageArray,messageArray;
-(void)viewWillAppear:(BOOL)animated{
   [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    indexToShow=0;

    _btn_skip.layer.cornerRadius=10.0f;
    
    
    _lbl_msg.text=[messageArray objectAtIndex:indexToShow];
    _lbl_name.text=[NSString stringWithFormat:@"-%@",[nameArray objectAtIndex:indexToShow]];

    [_pageControl setTag:12];
    _pageControl.numberOfPages=[imageArray count];
    
    NSString *image=[NSString stringWithFormat:@"%@",[imageArray objectAtIndex:indexToShow]];

    [_img_background sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
    }];
    
    gestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    [gestureRight setNumberOfTouchesRequired:1];
    
    [gestureRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    gestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    [gestureLeft setNumberOfTouchesRequired:1];
    
    [gestureLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    
    gestureRight.delegate=self;
    gestureLeft.delegate=self;
    
    _img_background.userInteractionEnabled=YES;
    
    [_img_background addGestureRecognizer:gestureRight];
    [_img_background addGestureRecognizer:gestureLeft];

}

#pragma Mark SwipeGesturesMethods
- (void)swipeRight:(UISwipeGestureRecognizer *)gesture
{
    if (indexToShow > 0)
    {
        
        indexToShow--;
        // [self.segmentedControl3 setSelectedSegmentIndex:indexToShow animated:YES];
        
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:
        //        ^{
        
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [_img_background.layer addAnimation:transition forKey:@"transition" ];
        
        _pageControl.currentPage=indexToShow;
        
        _lbl_msg.text=[messageArray objectAtIndex:indexToShow];
        _lbl_name.text=[NSString stringWithFormat:@"-%@",[nameArray objectAtIndex:indexToShow]];
        if (indexToShow ==[imageArray count]-1) {
            [_btn_skip setTitle:@"DONE" forState:UIControlStateNormal];
        }else{
            
            [_btn_skip setTitle:@"SKIP" forState:UIControlStateNormal];
            
        }

        NSString *image=[NSString stringWithFormat:@"%@",[imageArray objectAtIndex:indexToShow]];
        [_img_background sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//            [HUD hide:YES];
            
            
        }];
        //        }
        //        completion:nil];
        
    }
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)gesture
{
    if (indexToShow < imageArray.count - 1)
    {
        indexToShow++;
        
        //        [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:
        //         ^{
        
        CATransition *transition = [CATransition new];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [_img_background.layer addAnimation:transition forKey:@"transition" ];
        _pageControl.currentPage=indexToShow;
        
        
        if (indexToShow ==[imageArray count]-1) {
            [_btn_skip setTitle:@"DONE" forState:UIControlStateNormal];
        }else{
        
            [_btn_skip setTitle:@"SKIP" forState:UIControlStateNormal];

        }
        
        _lbl_msg.text=[messageArray objectAtIndex:indexToShow];
        _lbl_name.text=[NSString stringWithFormat:@"-%@",[nameArray objectAtIndex:indexToShow]];

        NSString *image=[NSString stringWithFormat:@"%@",[imageArray objectAtIndex:indexToShow]];
        [_img_background sd_setImageWithURL:[NSURL URLWithString:image]placeholderImage:[UIImage imageNamed:@"newPlacholder"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//            [HUD hide:YES];
            
        }];
        //            } completion:nil];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (IBAction)skipMethod:(id)sender
{
    ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:vc animated:YES];
   
}
@end
