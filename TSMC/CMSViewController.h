//
//  CMSViewController.h
//  TSMC
//
//  Created by user on 10/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface CMSViewController : UIViewController
{
    IBOutlet UILabel *_titleLbl;
    IBOutlet UILabel *_titleText;
    
    IBOutlet UIWebView *webView;
    
    MBProgressHUD *HUD;
    
    NSString *urlStr;
    NSURL *url;
}
- (IBAction)MenuBtn_Clicked:(id)sender;
@end
