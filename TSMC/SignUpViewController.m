//
//  SignUpViewController.m
//  TSMC
//
//  Created by user on 01/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "SignUpViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "ProfileUpdateViewController.h"
#import "OrderInformationViewController.h"
#import "ViewController.h"
#import "CartController.h"
#import "ShippingAddressController.h"
@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize goBackTo,comingFrom;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
        EmailTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        passwordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        confirmPassTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        FirstNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        LastNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
   
    
    
    [self initObjects];
    [self InitAttributeFrame];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init Methods

-(void)initObjects
{
    FirstNameTF.tag=104;
    LastNameTF.tag=105;
}
-(void)InitAttributeFrame
{
    [scrollView setContentSize:CGSizeMake(375, 800)];
    
    self.navigationController.navigationBarHidden=NO;
    
//    _titleBack.backgroundColor=TopBarColor;
    self.title = @"Sign Up".uppercaseString;
//    _titleText.font=TopBarFont;
//    _titleText.textColor=TopBarTextColor;
    
    
    
}
#pragma mark - Button Clicked Methods
- (IBAction)BackBtn_Clicked:(id)sender
{
    // [self.navigationController popViewControllerAnimated:YES];
    [self loadStoryBoardWithIdentifier:@"LoginVC"];
}

- (IBAction)CancelBtn_Clicked:(id)sender
{
    [LastNameTF resignFirstResponder];
}

- (IBAction)SignUpBtn_Clicked:(id)sender
{
    [LastNameTF resignFirstResponder];
    
    if(EmailTF.text.length>0)
    {
       if([self validEmail:EmailTF.text])
       {
           if(passwordTF.text.length>0)
           {
               if(!(passwordTF.text.length < 6) && !(passwordTF.text.length > 15))
                {
                    if(confirmPassTF.text.length>0)
                    {
                        if([confirmPassTF.text isEqualToString:passwordTF.text])
                        {
                            if(FirstNameTF.text.length>0)
                            {
                                if(LastNameTF.text.length>0)
                                {
                                    if((txtMobileNumber.text.length > 0) && (txtMobileNumber.text.length == 10))
                                    {
                                        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                        HUD.hidden=NO;
                                        HUD.dimBackground = YES;
                                        HUD.labelText = @"Please wait...";
                                    
                                        [self SignupAPI];
                                        
                                    } else {
                                        [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Enter Mobile Number!"];
                                    
                                    }
                                    
                                } else {
                                    [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Enter Last Name!"];
                                    //enter lname
                                }
                            }
                            else
                            {
                                [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Enter First Name!"];
                                //enter fname
                            }
                        }
                        else
                        {
                            [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Password and confirm password must be same!"];
                            //cp and pwrd must same
                        }
                        
                    }
                    else
                    {
                        [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Enter Confirm Password!"];
                        //enter cp
                    }
                }
                else
                {
                    [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Password must have atleast 6 characters!"];
                     //need 6 character long password
                }
           }
           else
           {
               [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Enter Password!"];
               //enter password
           }
       }
       else
       {
           [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Enter valid Email Address!"];
            //invalid email
       }
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:@"Enter Email!"];
        //enter email
    }
    
}


#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}
#pragma mark - API Call Methods

-(void)SignupAPI
{

    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        }else{
            
            @try{
                /*   
                 email = "sne1234@gmail.com";
                 firstname = Snehal;
                 lastname = Snehal;
                 password = snehal;
                 https://seafoodandmeatco.in/api/customer.php?op=regi

                 message = "This customer email already exists";
                 success = 0;
                 ////////////////
                 email = "sne12345@gmail.com";
                 firstname = Snehal;
                 lastname = Snehal;
                 password = snehal;
                 https://seafoodandmeatco.in/api/customer.php?op=regi
                 https://seafoodandmeatco.in/api/customer.php?op=regi

                */
                NSString *email=EmailTF.text;
                NSString *pass=passwordTF.text;
                NSString *fname=FirstNameTF.text;
                NSString *lname=LastNameTF.text;
                NSString *mobile = txtMobileNumber.text;
                
                NSString *url=API_SIGN_UP;
                
                NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:email,@"email",pass,@"password",fname,@"firstname",lname,@"lastname",mobile,@"mobile", nil];
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];

                BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                
                if ([getJsonData count] >0)
                {
                    if (success)
                    {
                        defaults=[NSUserDefaults standardUserDefaults];
                        
                        NSDictionary *userDict=[getJsonData valueForKey:@"customer"];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:@"ZeroAddress" forKey:@"chkAddressCount"];
                        [defaults setObject:[userDict valueForKey:@"email"] forKey:USEREMAIL];;
                        [defaults setObject:[userDict valueForKey:@"firstname"] forKey:USERFIRSTNAME];
                        [defaults setObject:[userDict valueForKey:@"lastname"] forKey:USERLASTNAME];
                        [defaults setObject:[userDict valueForKey:@"password_hash"] forKey:@"userPassword"];
                        [defaults setObject:[userDict valueForKey:@"entity_id"] forKey:USERID];
                        [defaults setObject:[userDict valueForKey:@"mobile"] forKey:USERMOBILE];
                        [defaults setObject:@"YES" forKey:ISLOGIN];
                        
                        
                        //adding fcm id against userid
                        NSString *tok=[[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"];
                        if (tok !=nil) {
                            HelperClass *helpObj=[[HelperClass alloc] init];
                            [helpObj addFCMTokenToServer];
                            
                        }
                        if ([comingFrom isEqualToString:@"PROFILE"]) {
                            ProfileUpdateViewController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileUpdateVC"];
                            //                            vie.goBackTo=goBackTo;
                            [self.navigationController pushViewController:vie animated:YES];
                            
                        }else if([comingFrom isEqualToString:@"CART"]){
                            ShippingAddressController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingAddressController"];
                            vie.comingFrom=@"LoginVC";
                            [self.navigationController pushViewController:vie animated:YES];

                        }else{
                            ViewController *vie=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                            [self.navigationController pushViewController:vie animated:YES];
                        }
                    }else{
                        NSString *msg=[NSString stringWithFormat:@"%@",[getJsonData valueForKey:@"message"]];
                        [self UIAlertViewControllerMethodTitle:@"Sign Up Error" message:msg];
                        
                    }
                    
                    HelperClass *obj=[[HelperClass alloc] init];
                    [obj addFCMTokenToServer];
                    HUD.hidden=YES;
                }
            } @catch (NSException *exception){
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            
            }
        }
    });
}

#pragma mark - TextField Delegates

- (IBAction)EmailTFDidEndEditing:(id)sender
{
   /* BOOL email=[self validEmail:EmailTF.text];
    if(email)
    {
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Error" message:@"Invalid Email"];
    }*/
}

- (IBAction)PasswordTFDidEndEditing:(id)sender
{
   /* if(!(passwordTF.text.length < 6))
    {
        if(!(passwordTF.text.length > 15))
        {
            passwordTF.tag=1;
        }
        else
        {
            [self UIAlertViewControllerMethodTitle:@"Error" message:@"Large Password"];
        }
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Error" message:@"Short Password"];
    }*/
    
}

- (IBAction)confirmPasswordTFDidEndEditing:(id)sender
{
    /*if([confirmPassTF.text isEqualToString:passwordTF.text])
    {
        
    }
    else
    {
        [self UIAlertViewControllerMethodTitle:@"Error" message:@"Password Mismatch"];
    }*/
}

- (IBAction)FirstNameDidEndEditing:(id)sender
{
}

- (IBAction)LastNameTFDidEndEditing:(id)sender
{
    //[LastNameTF resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    ///Restrict user to enter numbers in First Name and Last Name
    if (textField.tag==104) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    if (textField.tag==105) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    
    ///Restrict user to enter Charaters in Mobile textfield
    if (textField.tag==106) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return YES;
            }
            else{
                return NO;
            }
        }
    }
    
    if (textField==txtMobileNumber) {
           NSUInteger newLength = [txtMobileNumber.text length] + [string length] - range.length;
           return (newLength > 10) ? NO : YES;
       }
    
    return YES;
}

#pragma mark - ValidationMethods

- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}



#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           HUD.hidden=YES;
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               HUD.hidden=YES;
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
