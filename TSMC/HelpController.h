//
//  HelpController.h
//  TSMC
//
//  Created by Rahul Lekurwale on 15/09/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btn_skip;
- (IBAction)skipMethod:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *img_Back;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;



@property(nonatomic,strong)NSArray *nameArray,*imageArray,*messageArray;

@property(nonatomic,strong)NSString *comingFrom;
@end
