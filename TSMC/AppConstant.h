//
//  AppConstant.h
//  TSMC
//
//  Created by Rahul Lekurwale on 22/06/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#ifndef AppConstant_h
#define AppConstant_h
#import "AppDelegate.h"


#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication] delegate])


//#define URL_BASE @"http://tsmc.demowebapps.com/api3/"
//#define URL_BASE @"http://seafoodandmeatco.in/"
//#define URL_BASE @"https://seafoodandmeatco.in/api3/"

//#define URL_BASE @"https://fresh-pick.in/api3/"

#define URL_BASE  @"http://web.fruitbucket.in/api4/"//@"http://fruitbucket.in/api4/"


#define API_SIGN_UP                 [URL_BASE stringByAppendingString:@"customer.php?op=register"]
#define API_SIGN_IN                 [URL_BASE stringByAppendingString:@"customer.php?op=login"]
#define API_EDIT_PROFILE            [URL_BASE stringByAppendingString:@"customer.php?op=edit"]
#define API_CATEGORY_DATA           [URL_BASE stringByAppendingString:@"category.php"]
#define API_SHIPPING_METHOD         [URL_BASE stringByAppendingString:@"order.php?op=shippingmethod"]
#define API_MENU_ITEMS              [URL_BASE stringByAppendingString:@"productlist.php?"]
#define API_MENU_SEARCH             [URL_BASE stringByAppendingString:@"customer.php?op=search"]
#define API_SELECT_LOCATIONS        [URL_BASE stringByAppendingString:@"location.php"]
#define API_SEND_LOCATION           [URL_BASE stringByAppendingString:@"order.php?op=ordervalue"]
#define API_TIME_SLOT               [URL_BASE stringByAppendingString:@"order.php?op=ordertimeslot"]
#define API_DELIVERY_TIMING         [URL_BASE stringByAppendingString:@"timestamp.php"]
#define API_GET_ADDRESSES           [URL_BASE stringByAppendingString:@"customer.php?op=getCustomerAddresses"]
#define API_EDIT_ADDRESSES          [URL_BASE stringByAppendingString:@"customer.php?op=editadd"]
#define API_SET_DEFAULT_ADDRESSES   [URL_BASE stringByAppendingString:@"customer.php?op=defaultaddress"]
#define API_DELETE_ADDRESS          [URL_BASE stringByAppendingString:@"customer.php?op=deleteaddress"]
#define API_POST_ORDER              [URL_BASE stringByAppendingString:@"order.php?op=addOrder"]
#define API_ADD_ADDRESS             [URL_BASE stringByAppendingString:@"customer.php?op=addaddress"]
#define API_GET_HISTORY             [URL_BASE stringByAppendingString:@"customer.php?op=order"]
#define API_GET_HISTORY_DETAILS     [URL_BASE stringByAppendingString:@"customer.php?op=orderdetails"]
#define API_GET_ORDER_STATUS        [URL_BASE stringByAppendingString:@"customer.php?op=orderstatus"]
#define API_GET_BILLING_DETAILS     [URL_BASE stringByAppendingString:@"customer.php?op=billingdetails"]
#define API_GET_SHIPPING_CHARGES    [URL_BASE stringByAppendingString:@"order.php?op=shippingrate"]
#define API_GET_ATTRIBUTES          [URL_BASE stringByAppendingString:@"testatt.php"]
#define API_GET_OTP                 [URL_BASE stringByAppendingString:@"order.php?op=otpsend"]

#define API_FORGOT_PASSWORD         [URL_BASE stringByAppendingString:@"customer.php?op=forgetpassword"]

#define API_GET_COUNTRYLIST         [URL_BASE stringByAppendingString:@"location.php?type=countries"]
#define API_GET_STATELIST           [URL_BASE stringByAppendingString:@"location.php?type=states&id="]
#define API_GET_CITY                [URL_BASE stringByAppendingString:@"location.php?type=cities&id="]

#define API_APPLYCOUPON             [URL_BASE stringByAppendingString:@"coupon.php"]
#define API_TESTIMONIAL             [URL_BASE stringByAppendingString:@"whislist.php?op=testimonial"]
#define API_GET_ALL_WISHLIST        [URL_BASE stringByAppendingString:@"whislist.php?op=getallwhislist"]
#define API_INSERT_INTO_WISHLIST    [URL_BASE stringByAppendingString:@"whislist.php?op=getwhislist"]
#define API_DELETE_FROM_WISHLIST    [URL_BASE stringByAppendingString:@"whislist.php?op=deletewhislist"]

#define API_NOTIFICATION            [URL_BASE stringByAppendingString:@"whislist.php?op=fcmadd"]
#define API_PRODUCT_DETAIL          [URL_BASE stringByAppendingString:@"customer.php?op=productdetails"]
#define API_SOCIAL_LOGIN            [URL_BASE stringByAppendingString:@"sociallogin.php?op=sociallogin"]
#define API_REDEEMPOINT             [URL_BASE stringByAppendingString:@"hastagapi.php"]


#define ISLOGIN @"isLogin"
#define USERID  @"userId"
#define USEREMAIL  @"userEmail"
#define USERFIRSTNAME  @"userFName"
#define USERLASTNAME  @"userLName"
#define USERPASSWORD  @"userPassword"
#define USERMOBILE  @"userMobile"

#define ELIGIBLE @"Order eligible for FREE SHIPPING"
#define FREEFORSHIPPING @"For Free Shipping"
#define MAXLIMIT @"maxLimit"
#define HELPSESSION @"helpSession"

#define SHOWTESTIMONIAL     @"showTestimonial"

#define PARENTID         @"2"

#define YellowColor     [UIColor colorWithRed:(33.0/255.0) green:(155.0/255.0) blue:(144.0/255.0) alpha:1.0]
#define ButtonBlueColor     [UIColor colorWithRed:(37.0/255.0) green:(71.0/255.0) blue:(86.0/255.0) alpha:1.0]
//[UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:48.0f/255.0f alpha:1.0f]

//TopBar
#define TopBarColor     [UIColor colorWithRed:(169.0/255.0) green:(207.0/255.0) blue:(70.0/255.0) alpha:1.0]
#define TopBarFont     [UIFont fontWithName:@"HelveticaNeue" size:18]
#define TopBarTextColor    [UIColor colorWithRed:(202.0/255.0) green:(51.0/255.0) blue:(56.0/255.0) alpha:1.0]
#define cartIconBackColor   [UIColor redColor]
#define cartIconTextColor   [UIColor whiteColor]
#define unselectedSegmentTextColor   [UIColor lightGrayColor]


/////HomeScreen
#define HomeScreenLabelFont     [UIFont fontWithName:@"HelveticaNeue" size:15]
#define HomeScreenLabelColor     [UIColor blackColor]
#define HomeScreenTitleText     @"Fruit Bucket"


///sidebar Constants
#define LOCATION    @"Location"

/////Categories
#define CategoriesLabelFont     [UIFont fontWithName:@"HelveticaNeue" size:15]
#define CategoriesLabelColor     [UIColor whiteColor]
#define CategoriesTitleText     @"Menu"
#define CategoriesSubtitleTextColor     [UIColor whiteColor]
#define CategoriesSubtitleBackColor     [UIColor blackColor]
#define CategoriesSubtitleFont     [UIFont fontWithName:@"HelveticaNeue" size:15]


///USERDEFAULTS
#define SELECTEDLOCATION    @"selectedLocation"
#define SELECTEDLOCATIONPRICE       @"selectedLocationPrice"

///Products
#define SELECTCUT       @"Select Cut"
#define SELECTWEIGHT    @"Select Weight Range"
#define SELECTTYPE    @"Select Type"


///CartScreen
#define CartLabelFont     [UIFont fontWithName:@"HelveticaNeue" size:11]
#define CartLabelColor     [UIColor blackColor]
#define CartTitleText     @"ORDERS"


//Filters
#define MINPRICE       @"minPrice"
#define MAXPRICE    @"maxPrice"
#define SELECTEDMINPRICE    @"selectedMinPrice"
#define SELECTEDMAXPRICE    @"selectedMaxPrice"
#define ISFILTER    @"isFilter"
#define BRANDVALUE    @"brandValue"


#endif /* AppConstant_h */
