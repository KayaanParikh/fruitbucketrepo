//
//  HistoryDetailsViewController.m
//  TSMC
//
//  Created by user on 05/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "HistoryDetailsViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "CustomTableViewCell.h"
#import "CartController.h"

@interface HistoryDetailsViewController ()
{
    NSMutableArray *OptionHistoryArray,*HistIdArray;
    NSMutableArray *dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray,*spicelevelArray,*messagesArray,*newPriceArray,*wlArr,*wvArr,*wpArr,*clArr,*cvArr,*cpArr,*tlArr,*tvArr,*tpArr;
    NSMutableArray *localCutOpIdArray,*localCutOpValArray,*localCutOpPriceArray,*localWeightOpIdArray,*localWeightOpValArray,*localWeightOpPriceArray,*localTypeOpIdArray,*localTypeOpValArray,*localTypeOpPriceArray,*optionIdArray,*localTypeOpLabelArray,*localWeightOpLabelArray,*localCutOpLabelArray;
    NSMutableArray *descriptionArray,*skuArray,*shortDescArray,*priceArray,*idContainInLocal,*localQtyArr,*foodTypeArr,*mirchImagesArr,*mirchIndexArr;

    //Wight required array's
    NSMutableArray *weightlabelArray,*weightOptionIdArray;
    NSMutableArray *weightValueArray;
    NSMutableArray *weightpriceArray;
    NSMutableArray *pickerWeightLabelArr,*pickerWeightValueArr,*pickerWeightPriceArr;
    NSMutableArray *selectedWeightLabelIndex,*selectedWeightValueIndex,*selectedWeightPriceIndex;
    
    OfflineDb *object;

    //Cut required array's
    NSMutableArray *cutlabelArray,*cutOptionIdArray;
    NSMutableArray *cutValueArray;
    NSMutableArray *cutpriceArray;
    NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
    NSMutableArray *selectedCutLabelIndex,*selectedCutValueIndex,*selectedCutPriceIndex;
    
    //Type required array's
    NSMutableArray *typelabelArray,*typeOptionIdArray;
    NSMutableArray *typeValueArray;
    NSMutableArray *typepriceArray;
    NSMutableArray *pickerTypeLabelArr,*pickerTypeValueArr,*pickerTypePriceArr;
    NSMutableArray *selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    NSString *selectedLabel,*selectedValue,*selectedPrice;

    NSMutableArray *nameArray,*idArray,*imagesArray,*categoryIdArr,*categoryNameArr,*imageMainArray,*imageThumbArray,*imageSmallArray,*weightArray,*brandIndexArr,*brandValueArr;

    NSDictionary *demoDict;

}
@end

@implementation HistoryDetailsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    object=[[OfflineDb alloc] init];
    orderDetailsTblView.backgroundColor = [UIColor whiteColor];
    [self initialText];
    [self initObjects];
    [self InitAttributeFrame];
    [self historyDetailsAPI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Init methods
-(void)initialText
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";

    or_ID.text=@"";
    or_time1.text=@"";
    or_type.text=@"";
    or_status.text=@"";
    or_time2.text=@"";
    or_add.text=@"";
    or_comment.text=@"";
    
    orderIdLbl.text=@"";
    dateTimeLbl.text=@"";
    orderTypeLbl.text=@"";
    orderStatusLbl.text=@"";
    dateTimeLbl2.text=@"";
    orderAddressLbl.text=@"";
    orderCommentLbl.text=@"";
    grandTotalLbl.text=@"";
    grandTotalLbl.hidden=YES;
    backLbl.hidden=YES;
    or_totalLblName.hidden=YES;
    or_totalLblName.text=@"";
    or_grandTotal.text=@"";
    
}
-(void)initObjects{
   orderId=[[NSUserDefaults standardUserDefaults] objectForKey:@"orderId"];
}

-(void)InitAttributeFrame{
    
    self.navigationController.navigationBarHidden=FALSE;
//    titleLbl.backgroundColor=TopBarColor;
    self.title = @"History".uppercaseString;
//    titleText.font=TopBarFont;
//    titleText.textColor=TopBarTextColor;
    chkAPICall=@"getHistoryDetails";
    
    _btn_reOrder.layer.cornerRadius=5.0f;
    _btn_reOrder.clipsToBounds=YES;
}
#pragma mark - Show User Details
-(void)showOrderSummeryData
{
    
    or_ID.text=@"Order #:";
    or_time1.text=@"Order Time:";
    or_type.text=@"Order Type:";
    or_status.text=@"Order Status:";
    or_time2.text=@"Delivery Time:";
    or_add.text=@"Delivery Address:";
    or_comment.text=@"Comment:";

    orderIdLbl.text=[NSString stringWithFormat:@"%@",orderId];
    
    dateTimeLbl.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DateTime"]];
    CGSize maximumLabelSize = CGSizeMake(296,9999);
    CGSize expecteddateTimeLblSize = [[[NSUserDefaults standardUserDefaults]objectForKey:@"DateTime"] sizeWithFont:dateTimeLbl.font
                                                                                                constrainedToSize:maximumLabelSize
                                                                                                    lineBreakMode:dateTimeLbl.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect dateTimeLblFrame = dateTimeLbl.frame;
    dateTimeLblFrame.size.height = expecteddateTimeLblSize.height;
    dateTimeLbl.frame = dateTimeLblFrame;
    
    orderTypeLbl.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderType"]];
    CGSize expectedorderTypeLblSize2 = [[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderType"] sizeWithFont:orderTypeLbl.font
                                                                                                constrainedToSize:maximumLabelSize
                                                                                                    lineBreakMode:orderTypeLbl.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect orderTypeLblFrame = orderTypeLbl.frame;
    orderTypeLblFrame.size.height = expectedorderTypeLblSize2.height;
    orderTypeLbl.frame = orderTypeLblFrame;
    
    orderStatusLbl.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"Status"]];
    
    NSString *customDeliveryDate,*customDeliveryTime;
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customDeliveryDate"] isEqualToString:@""])
    {
        customDeliveryDate=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"customDeliveryDate"]];
    }
    else
    {
        customDeliveryDate=[NSString stringWithFormat:@""];
    }
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customDeliveryTime"] isEqualToString:@""])
    {
        customDeliveryTime=[NSString stringWithFormat:@"(%@)",[[NSUserDefaults standardUserDefaults]objectForKey:@"customDeliveryTime"]];
    }
    else
    {
        customDeliveryTime=[NSString stringWithFormat:@""];
    }
    
//    NSString *dateTime;
//    if([customDeliveryDate isEqualToString:@""] && [customDeliveryTime isEqualToString:@""])
//    {
//        dateTime=[NSString stringWithFormat:@"%@ (%@)",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderDate2"],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderTime2"]];
//    }
//    if(![customDeliveryDate isEqualToString:@""] && ![customDeliveryTime isEqualToString:@""])
//    {
//      dateTime=[NSString stringWithFormat:@"%@ (%@)\n%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderDate2"],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderTime2"],customDeliveryDate,customDeliveryTime];
//    }
//    if(![customDeliveryDate isEqualToString:@""])
//    {
//        dateTime=[NSString stringWithFormat:@"%@ (%@)\n%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderDate2"],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderTime2"],customDeliveryDate];
//    }
//    if(![customDeliveryTime isEqualToString:@""])
//    {
//        dateTime=[NSString stringWithFormat:@"%@ (%@)\n%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderDate2"],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderTime2"],customDeliveryTime];
//    }
   // NSString *dateTime=[NSString stringWithFormat:@"%@ (%@)\n%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderDate2"],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderTime2"],customDeliveryDate,customDeliveryTime];
//    dateTimeLbl2.text=dateTime;
//    CGSize expecteddateTimeLbl2Size2 = [dateTime sizeWithFont:dateTimeLbl2.font constrainedToSize:maximumLabelSize lineBreakMode:dateTimeLbl2.lineBreakMode];
    
    //adjust the label the the new height.
//    CGRect dateTimeLbl2Frame = orderCommentLbl.frame;
//    dateTimeLbl2Frame.size.height = expecteddateTimeLbl2Size2.height;
//    orderCommentLbl.frame = dateTimeLbl2Frame;
    
    orderAddressLbl.text=[NSString stringWithFormat:@"%@",finalAddress];
    CGSize expectedorderAddressLblSize = [finalAddress sizeWithFont:orderAddressLbl.font
                                      constrainedToSize:maximumLabelSize
                                          lineBreakMode:orderAddressLbl.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect orderAddressLblFrame = orderAddressLbl.frame;
    orderAddressLblFrame.size.height = expectedorderAddressLblSize.height;
    orderAddressLbl.frame = orderAddressLblFrame;
    //[orderAddressLbl sizeToFit];
    
//    orderCommentLbl.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderComment"]];
//    
//    CGSize expectedorderCommentLblSize2 = [[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderComment"] sizeWithFont:orderCommentLbl.font
//                                        constrainedToSize:maximumLabelSize
//                                            lineBreakMode:orderCommentLbl.lineBreakMode];
//    
//    //adjust the label the the new height.
//    CGRect orderCommentLblFrame = orderCommentLbl.frame;
//    orderCommentLblFrame.size.height = expectedorderCommentLblSize2.height;
//    orderCommentLbl.frame = orderCommentLblFrame;
   // [orderCommentLbl sizeToFit];
    
    NSString *couponname=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"]];
    
   // int shippingCharge=[[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] intValue];
    if(([[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0.0000"] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0"] ) && ([[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] isEqualToString:@"null"]))
    {
        backLbl.hidden=NO;
        or_totalLblName.hidden=NO;
        grandTotalLbl.hidden=NO;
        backLblHeightConstraints.constant=30;
        totalLblNameHeightConstraints.constant=30;
        grandTotalConstraintsHeight.constant=30;
        or_totalLblName.numberOfLines=1;
        grandTotalLbl.numberOfLines=1;
        or_totalLblName.text=[NSString stringWithFormat:@"Grand Total:₹"];
        grandTotalLbl.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderAmount"]];
    }
    else if(([[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0.0000"] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0"]) && !([[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] isEqualToString:@"null"]))
    {
        backLbl.hidden=NO;
        or_totalLblName.hidden=NO;
        grandTotalLbl.hidden=NO;
        backLblHeightConstraints.constant=75;
        totalLblNameHeightConstraints.constant=75;
        grandTotalConstraintsHeight.constant=75;
        or_totalLblName.numberOfLines=3;
        grandTotalLbl.numberOfLines=3;
        or_totalLblName.text=[NSString stringWithFormat:@"Food Total:₹\n%@:₹\nGrand Total:₹",[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"]];
        grandTotalLbl.text=[NSString stringWithFormat:@"%.2f\n%.2f\n%@",[[[NSUserDefaults standardUserDefaults]objectForKey:@"SubTotal"] floatValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponDiscount"] floatValue],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderAmount"]];
    }
    
    else if((![[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0.0000"] || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0"]) && ([[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] isEqualToString:@"null"]))
    {
        backLbl.hidden=NO;
        or_totalLblName.hidden=NO;
        grandTotalLbl.hidden=NO;
        backLblHeightConstraints.constant=75;
        totalLblNameHeightConstraints.constant=75;
        grandTotalConstraintsHeight.constant=75;
        or_totalLblName.numberOfLines=3;
        grandTotalLbl.numberOfLines=3;
        or_totalLblName.text=[NSString stringWithFormat:@"Food Total:₹\nShipping Charges:₹\nGrand Total:₹"];
        grandTotalLbl.text=[NSString stringWithFormat:@"%.2f\n%.2f\n%@",[[[NSUserDefaults standardUserDefaults]objectForKey:@"SubTotal"] floatValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] floatValue],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderAmount"]];
       // [grandTotalLbl sizeToFit];
    }
    else if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0.0000"] || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] isEqualToString:@"0"] || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] isEqualToString:@""] || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] isEqualToString:@"0"] || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] isEqualToString:@"0.00"] || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] isEqualToString:@"null"] || !([[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"] == nil) || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponDiscount"] isEqualToString:@"0.0000"])
    {
        backLbl.hidden=NO;
        or_totalLblName.hidden=NO;
        grandTotalLbl.hidden=NO;
        backLblHeightConstraints.constant=90;
        totalLblNameHeightConstraints.constant=90;
        grandTotalConstraintsHeight.constant=90;
        or_totalLblName.numberOfLines=4;
        grandTotalLbl.numberOfLines=4;
        or_totalLblName.text=[NSString stringWithFormat:@"Food Total:₹\nShipping Charges:₹\n%@:₹\nGrand Total:₹",[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponName"]];
        grandTotalLbl.text=[NSString stringWithFormat:@"%.2f\n%.2f\n%.2f\n%@",[[[NSUserDefaults standardUserDefaults]objectForKey:@"SubTotal"] floatValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"ShippingCharge"] floatValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"CouponDiscount"] floatValue],[[NSUserDefaults standardUserDefaults]objectForKey:@"OrderAmount"]];
    }

    
    HUD.hidden=YES;
}
#pragma mark -API Methods
-(void)historyDetailsAPI
{
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection){
            
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }else{
            @try
            {
                if([chkAPICall isEqualToString:@"getHistoryDetails"])
                {
                    NSString *url=API_GET_HISTORY_DETAILS;
                    
                    NSMutableDictionary *parameterDict;
                    
                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:orderId,@"order_id", nil];
                    
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    //NSLog(@"%@",getJsonData);
                    
                    BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                    
                    if ([getJsonData count] >0)
                    {
                        customerArr=[[NSMutableArray alloc]init];
                        dishNameArr=[[NSMutableArray alloc]init];
                        basePriceArr=[[NSMutableArray alloc]init];
                        qtyArr=[[NSMutableArray alloc]init];
                        basepriceQtyArr=[[NSMutableArray alloc]init];
                        totalPriceArr=[[NSMutableArray alloc]init];
                        totalPriceArr2=[[NSMutableArray alloc]init];
                        basePriceArr2=[[NSMutableArray alloc]init];
                        qtyArr2=[[NSMutableArray alloc]init];
                        HistIdArray=[[NSMutableArray alloc] init];
                        OptionHistoryArray=[[NSMutableArray alloc] init];
                        NSMutableArray *options=[[NSMutableArray alloc] init];
                        if (success)
                        {
                            [customerArr addObject:[getJsonData valueForKey:@"customer"]];
                            for(int i=0;i<customerArr.count;i++)
                            {
                                for (int j=0; j<[[customerArr objectAtIndex:i] count]; j++)
                                {
                                    [dishNameArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"name"] ];
                                    [basePriceArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"base_price"]];
                                    [qtyArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"qty_ordered"]];
                                    [totalPriceArr addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"row_total"]];
                                    [options addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"options"]];
                                    [HistIdArray addObject:[[[customerArr objectAtIndex:i] objectAtIndex:j] valueForKey:@"product_id"]];
//                                    NSArray *options [[[customerArr objectAtIndex:i] objectAtIndex:j]valueForKey:@"options"];
                                }
                            }
                            
                            [OptionHistoryArray addObject:[options valueForKey:@"options"]];

                            
                            
//                            NSString *cutopid=[cutOptionIdArray objectAtIndex:[sender tag]];
//                            NSString *cutopval=[selectedCutValueIndex objectAtIndex:row];
//                            NSString *cutoppri=[selectedCutPriceIndex objectAtIndex:row];
//                            NSString *weiopid=[weightOptionIdArray objectAtIndex:[sender tag]];
//                            NSString *weiopval=[selectedWeightValueIndex objectAtIndex:row];
//                            NSString *weioppri=[selectedWeightPriceIndex objectAtIndex:row];
//                            NSString *typopid=[typeOptionIdArray objectAtIndex:[sender tag]];
//                            NSString *typopval=[selectedTypeValueIndex objectAtIndex:row];
//                            NSString *typoppri=[selectedTypePriceIndex objectAtIndex:row];
//                            NSString *cutoplabel=[selectedCutLabelIndex objectAtIndex:row];
//                            NSString *weioplabel=[selectedWeightLabelIndex objectAtIndex:row];
//                            NSString *typoplabel=[selectedTypeLabelIndex objectAtIndex:row];

                            
                            

                            
                            for(int i=0;i<totalPriceArr.count && basePriceArr.count && i<qtyArr.count;i++)
                            {
                                CGFloat value=[[totalPriceArr objectAtIndex:i] floatValue];
                                NSString *str=[NSString stringWithFormat:@"%.2f",value];
                                [totalPriceArr2 addObject:str];
                                
                                CGFloat value1=[[basePriceArr objectAtIndex:i] floatValue];
                                NSString *str1=[NSString stringWithFormat:@"%.2f",value1];
                                [basePriceArr2 addObject:str1];
                                
                                CGFloat value2=[[qtyArr objectAtIndex:i] floatValue];
                                NSString *str2=[NSString stringWithFormat:@"0%.f",value2];
                                [qtyArr2 addObject:str2];
                                
                                
                            }
                           // NSLog(@"%@",totalPriceArr2);
                            //Concating addressesArr and Street Array
                            for(int i=0;i<basePriceArr2.count && i<qtyArr2.count;i++)
                            {
                                NSString *finalAddressStr=[NSString stringWithFormat:@"%@ * %@",[basePriceArr2 objectAtIndex:i],[qtyArr2 objectAtIndex:i]];
                                
                                [basepriceQtyArr addObject:finalAddressStr];
                                
                            }
                            [orderDetailsTblView reloadData];
                            chkAPICall=@"getAddressDetails";
                        }
                        
                    }

                    
                }
                if([chkAPICall isEqualToString:@"getAddressDetails"])
                {
                    NSString *url=API_GET_BILLING_DETAILS;
                    
                    NSMutableDictionary *parameterDict;
                    
                    parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:orderId,@"order_id", nil];
                    
                    
                    WebservicesClass *web=[[WebservicesClass alloc] init];
                    
                    NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                    
                    //NSLog(@"%@",getJsonData);
                    
                    BOOL success=[[getJsonData valueForKey:@"success"] boolValue];
                    
                    if ([getJsonData count] >0)
                    {
                        if(success)
                        {
                            NSString *street=[[getJsonData valueForKey:@"billingaddress"] valueForKey:@"street"];
                            decodeStreet=[street stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
                            postcode=[[getJsonData valueForKey:@"billingaddress"] valueForKey:@"postcode"];
                            city=[[getJsonData valueForKey:@"billingaddress"] valueForKey:@"city"];
                        }
                        finalAddress=[NSString stringWithFormat:@"%@,%@,%@",decodeStreet,postcode,city];
                    }
                    [self showOrderSummeryData];
                }
                HUD.hidden=YES;
                
            } @catch (NSException *exception)
            
            {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
            }
        }
        
    });
    
}
//-(void)historyDetailsAPI
//{
//    HelperClass *s=[[HelperClass alloc]init];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        BOOL connection=s.getInternetStatus;
//        
//        if (!connection)
//        {
//            NSLog(@"Hello Net connectiion is not pres;ent....");
//            
//            //[HUD hide:YES];
//            
//            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
//            
//        }
//        else
//        {
//            @try
//            {
//                NSString *urlShippingCharges=API_GET_SHIPPING_CHARGES;
//                
//                NSMutableDictionary *parameterDict2;
//                
//                parameterDict2=[NSMutableDictionary dictionaryWithObjectsAndKeys:ShippingLocation,@"location_name",cartTotal,@"cart_total", nil];
//                
//                
//                WebservicesClass *web=[[WebservicesClass alloc] init];
//                
//                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:urlShippingCharges withParameter:parameterDict2];
//                
//                NSLog(@"%@",getJsonData);
//                
//                if([getJsonData count]>0)
//                {
//                    
//                    if([getJsonData objectForKey:@"value"]!=nil || ![[getJsonData objectForKey:@"value"] isEqual:@"(null)"])
//                    {
//                        NSString  *shippingChragesForSelectedArea=[NSString stringWithFormat:@"₹ %.f.0",[[getJsonData objectForKey:@"value"] floatValue]];
//                        shippingInfoChargeLbl.text=shippingChragesForSelectedArea;
//                    }
//                    else
//                    {
//                        shippingInfoChargeLbl.text=@"₹ 0.0";
//                        
//                    }
//                }
//                else
//                {
//                    
//                }
//                
//                
//            } @catch (NSException *exception)
//            
//            {
//                NSLog(@"%@",exception);
//                NSString *exc=[NSString stringWithFormat:@"%@",exception];
//                //            [ProgressHUD showError:exc Interaction:NO];
//                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
//            }
//        }
//        
//    });
//    
//}


#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(dishNameArr.count>0)
    {
        return dishNameArr.count;
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"CustomTableViewCell";
    
    CustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil)
    {
        cell=[[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    cell.Lbl_DishName.text=[NSString stringWithFormat:@"%@",[dishNameArr objectAtIndex:indexPath.row]];
    [cell.Lbl_DishName sizeToFit];
    cell.Lbl_Qty.text=[NSString stringWithFormat:@"%@",[basepriceQtyArr objectAtIndex:indexPath.row]];
    cell.Lbl_Price.text=[NSString stringWithFormat:@"%@",[totalPriceArr2 objectAtIndex:indexPath.row]];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}


#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [HUD hide:YES];
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [HUD hide:YES];

                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

#pragma mark - Button Clicked Methods

- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)reOrderMethod:(id)sender
{
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait while submitting...";
    [HUD showWhileExecuting:@selector(GetTopviewProduct) onTarget:self withObject:Nil animated:YES];

//    [self GetTopviewProduct];
}
-(void)GetTopviewProduct
{
    
    // ,*selectedCutValueIndex,*selectedCutPriceIndex,selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    // NSLog(@"Idarray1=%@",idArray);
    
    nameArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    idArray=[[NSMutableArray alloc] init];
    nameArray=[[NSMutableArray alloc] init];
    descriptionArray=[[NSMutableArray alloc] init];
    imagesArray=[[NSMutableArray alloc] init];
    skuArray=[[NSMutableArray alloc] init];
    priceArray=[[NSMutableArray alloc] init];
    shortDescArray=[[NSMutableArray alloc] init];
    idContainInLocal=[[NSMutableArray alloc] init];
    localQtyArr=[[NSMutableArray alloc] init];
    foodTypeArr=[[NSMutableArray alloc] init];
    
    dropLabelArr=[[NSMutableArray alloc] init];
    dropValueArr=[[NSMutableArray alloc] init];
    fieldIdArray=[[NSMutableArray alloc] init];
    dropDownIdArray=[[NSMutableArray alloc] init];
    spicelevelArray=[[NSMutableArray alloc] init];
    messagesArray=[[NSMutableArray alloc] init];
    
    
    weightlabelArray=[[NSMutableArray alloc] init];
    weightValueArray=[[NSMutableArray alloc] init];
    weightpriceArray=[[NSMutableArray alloc] init];
    
    cutlabelArray=[[NSMutableArray alloc] init];
    cutValueArray=[[NSMutableArray alloc] init];
    cutpriceArray=[[NSMutableArray alloc] init];
    
    typelabelArray=[[NSMutableArray alloc] init];
    typeValueArray=[[NSMutableArray alloc] init];
    typepriceArray=[[NSMutableArray alloc] init];
    
    localCutOpIdArray=[[NSMutableArray alloc] init];
    localCutOpValArray=[[NSMutableArray alloc] init];
    localCutOpPriceArray=[[NSMutableArray alloc] init];
    
    localWeightOpIdArray=[[NSMutableArray alloc] init];
    localWeightOpValArray=[[NSMutableArray alloc] init];
    localWeightOpPriceArray=[[NSMutableArray alloc] init];
    
    localTypeOpIdArray=[[NSMutableArray alloc] init];
    localTypeOpValArray=[[NSMutableArray alloc] init];
    localTypeOpPriceArray=[[NSMutableArray alloc] init];
    brandIndexArr=[[NSMutableArray alloc] init];
    brandValueArr=[[NSMutableArray alloc] init];
    weightArray=[[NSMutableArray alloc] init];
    optionIdArray=[[NSMutableArray alloc] init];
    imageMainArray=[[NSMutableArray alloc] init];
    imageThumbArray=[[NSMutableArray alloc] init];
    imageSmallArray=[[NSMutableArray alloc] init];
    
    typeOptionIdArray=[[NSMutableArray alloc] init];
    weightOptionIdArray=[[NSMutableArray alloc] init];
    cutOptionIdArray=[[NSMutableArray alloc] init];
    localCutOpLabelArray=[[NSMutableArray alloc] init];
    localWeightOpLabelArray=[[NSMutableArray alloc] init];
    localTypeOpLabelArray=[[NSMutableArray alloc] init];
    selectedWeightLabelIndex=[[NSMutableArray alloc] init];
    selectedWeightValueIndex=[[NSMutableArray alloc] init];
    selectedWeightPriceIndex=[[NSMutableArray alloc] init];
    
    selectedCutLabelIndex=[[NSMutableArray alloc] init];
    selectedCutPriceIndex=[[NSMutableArray alloc] init];
    selectedCutValueIndex=[[NSMutableArray alloc] init];
    selectedTypeLabelIndex=[[NSMutableArray alloc] init];
    selectedTypeValueIndex=[[NSMutableArray alloc] init];
    selectedTypePriceIndex=[[NSMutableArray alloc] init];
    newPriceArray=[[NSMutableArray alloc] init];
    
    wlArr=[[NSMutableArray alloc] init];
    wvArr=[[NSMutableArray alloc] init];
    wpArr=[[NSMutableArray alloc] init];
    clArr=[[NSMutableArray alloc] init];
    cvArr=[[NSMutableArray alloc] init];
    cpArr=[[NSMutableArray alloc] init];
    tlArr=[[NSMutableArray alloc] init];
    tvArr=[[NSMutableArray alloc] init];
    tpArr=[[NSMutableArray alloc] init];

    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        
        HelperClass *s=[[HelperClass alloc]init];
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            
            
            NSLog(@"Hello Net connectiion is not pres;ent....");
            
            [HUD hide:YES];
            
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
            
        }
        else
        {
            @try {
                
                NSString *tempUrl;
                tempUrl=[NSString stringWithFormat:@"%@",API_MENU_SEARCH];
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                
                
                NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:orderId,@"order_id", nil];
                
                
                
                //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
                NSDictionary *jsonDict=[web PostTopviewProductWithUrl:tempUrl withParameter:dict];
                NSLog(@"%@",jsonDict);
                if ([jsonDict count] > 0)
                {

                        demoDict=jsonDict;
                    NSArray *idArr=[jsonDict valueForKey:@"entity_id"];
                    [idArray addObjectsFromArray:idArr];

                        [self addToCartMethod];

                }
//
//                if ([jsonDict count] > 0)
//                {
//                    
//                    
//
//                        NSArray *name=[jsonDict valueForKey:@"name"];
//                        //                NSArray *skuArr=[jsonDict valueForKey:@"sku"];
//                        NSArray *descriptionArr=[jsonDict valueForKey:@"description"];
//                        NSArray *shortDescArr=[jsonDict valueForKey:@"short_description"];
//                        NSArray *priceArr=[jsonDict valueForKey:@"final_price_with_tax"];
//                        //                NSArray *isSaleableArr=[jsonDict valueForKey:@"is_saleable"];
//                        NSArray *foodType=[jsonDict valueForKey:@"food_type"];
//                        
//                        NSArray *imagesArr=[jsonDict valueForKey:@"image_url"];
//                        NSArray *imagesThumb=[jsonDict valueForKey:@"image_thumb"];
//                        NSArray *imagesMainUrl=[jsonDict valueForKey:@"main_url"];
//                        NSArray *imagesSmall=[jsonDict valueForKey:@"image_small"];
//                        NSArray *brand=[jsonDict valueForKey:@"brand"];
//                        NSArray *origin=[jsonDict valueForKey:@"origin"];
//                        NSArray *weight=[jsonDict valueForKey:@"weight"];
//                        
//                        
//                        
//                        // dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray;
//                        NSArray *attrDict=[jsonDict valueForKey:@"attributes"];
//                        NSArray *InputTypeArray=[attrDict valueForKey:@"InputType"];
//                        NSArray *opIdArr=[attrDict valueForKey:@"option_id"];
//                        NSDictionary *attArr=[attrDict valueForKey:@"att"];
//                        NSArray *labelArr=[attArr valueForKey:@"label"];
//                        NSArray *valueArr=[attArr valueForKey:@"value"];
//                        NSArray *priArr=[attArr valueForKey:@"price"];
//                        NSArray *InputType=[attArr valueForKey:@"InputType"];
//                    
//                    
//                    
//                    
//                    
//                    
//                        
//                        //                    for (int j=0; j<[attrDict count]; j++) {
//                        //                                          }
//                        //
//                        
//                        
//                            for (int i=0; i<[idArr count]; i++) {
//                                if ([[attrDict objectAtIndex:i] count]>0) {
//                                    
//                                    if ([[InputType objectAtIndex:i] count]>0)
//                                    {
//                                        
//                                        wlArr=[[NSMutableArray alloc] init];
//                                        wvArr=[[NSMutableArray alloc] init];
//                                        wpArr=[[NSMutableArray alloc] init];
//                                        clArr=[[NSMutableArray alloc] init];
//                                        cvArr=[[NSMutableArray alloc] init];
//                                        cpArr=[[NSMutableArray alloc] init];
//                                        tlArr=[[NSMutableArray alloc] init];
//                                        tvArr=[[NSMutableArray alloc] init];
//                                        tpArr=[[NSMutableArray alloc] init];
//                                        
//                                        for (int m=0; m<[[InputType objectAtIndex:i] count]; m++)
//                                        {
//                                            NSArray *arr=[[InputType objectAtIndex:i] objectAtIndex:m];
//                                            for (int n=0; n<[arr count]; n++)
//                                            {
//                                                if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"])
//                                                {
//                                                    NSString *str_wlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_wvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_wpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    [wlArr addObject:str_wlArr];
//                                                    [wvArr addObject:str_wvArr];
//                                                    [wpArr addObject:str_wpArr];
//                                                }
//                                                
//                                                if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"])
//                                                {
//                                                    NSString *str_clArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_cvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_cpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    [clArr addObject:str_clArr];
//                                                    [cvArr addObject:str_cvArr];
//                                                    [cpArr addObject:str_cpArr];
//                                                    
//                                                }
//                                                
//                                                if ([[arr objectAtIndex:n] isEqualToString:@"Type"])
//                                                {
//                                                    NSString *str_tlArr=[[[labelArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_tvArr=[[[valueArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    NSString *str_tpArr=[[[priArr objectAtIndex:i] objectAtIndex:m] objectAtIndex:n];
//                                                    [tlArr addObject:str_tlArr];
//                                                    [tvArr addObject:str_tvArr];
//                                                    [tpArr addObject:str_tpArr];
//                                                    
//                                                }
//                                                
//                                            }
//                                            
//                                            
//                                            
//                                        }
//                                        [weightlabelArray addObject:wlArr];
//                                        [weightValueArray addObject:wvArr];
//                                        [weightpriceArray addObject:wpArr];
//                                        [cutlabelArray addObject:clArr];
//                                        [cutValueArray addObject:cvArr];
//                                        [cutpriceArray addObject:cpArr];
//                                        [typelabelArray addObject:tlArr];
//                                        [typeValueArray addObject:tvArr];
//                                        [typepriceArray addObject:tpArr];
//                                        
//                                        
//                                    }
//                                    
//                                    
//                                    NSArray *labArr=[opIdArr objectAtIndex:i];
//                                    if ( (NSArray *)[NSNull null] == labArr )
//                                    {
//                                        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
//                                        [optionIdArray addObject:emptyArray];
//                                        
//                                    }
//                                    else
//                                    {
//                                        //[dropLabelArr addObject:[labArr objectAtIndex:1]];
//                                        [optionIdArray addObject:[opIdArr objectAtIndex:i]];
//                                        
//                                    }
//                                    
//                                    NSArray *valArr=[InputTypeArray objectAtIndex:i];
//                                    if ( (NSArray *)[NSNull null] == valArr )
//                                    {
//                                        
//                                    }
//                                    else
//                                    {
//                                        [dropLabelArr addObject:valArr];
//                                        
//                                        //                            [dropValueArr addObject:[valArr objectAtIndex:1]];
//                                    }
//                                    NSLog(@"dropLabelArr=%@",dropLabelArr);
//                                    
//                                }
//                                else{
//                                    
//                                    NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
//                                    [dropLabelArr addObject:emptyArray];
//                                    [optionIdArray addObject:emptyArray];
//                                    [weightlabelArray addObject:emptyArray];
//                                    [weightValueArray addObject:emptyArray];
//                                    [weightpriceArray addObject:emptyArray];
//                                    [cutlabelArray addObject:emptyArray];
//                                    [cutValueArray addObject:emptyArray];
//                                    [cutpriceArray addObject:emptyArray];
//                                    [typelabelArray addObject:emptyArray];
//                                    [typeValueArray addObject:emptyArray];
//                                    [typepriceArray addObject:emptyArray];
//                                    
//                                    
//                                    
//                                    
//                                }
//                                
//                                [newPriceArray addObject:[priceArr objectAtIndex:i]];
//                                
//                                [idArray addObject:[idArr objectAtIndex:i]];
//                                [nameArray addObject:[name objectAtIndex:i]];
//                                [descriptionArray addObject:[descriptionArr objectAtIndex:i]];
//                                [imagesArray addObject:[imagesArr objectAtIndex:i]];
//                                [foodTypeArr addObject:[foodType objectAtIndex:i]];
//                                
//                                //                        [skuArray addObject:[skuArr objectAtIndex:i]];
//                                [shortDescArray addObject:[shortDescArr objectAtIndex:i]];
//                                
//                                
//                                [brandIndexArr addObject:[brand objectAtIndex:i]];
//                                [imageMainArray addObject:[imagesMainUrl objectAtIndex:i]];
//                                [imageSmallArray addObject:[imagesSmall objectAtIndex:i]];
//                                [imageThumbArray addObject:[imagesThumb objectAtIndex:i]];
//                                [weightArray addObject:[weight objectAtIndex:i]];
//                                NSArray *getAttr=[object fetch_BrandValue:@"brand" AndValue:[brand objectAtIndex:i]];
//                                if ([getAttr count]>0) {
//                                    [brandValueArr addObject:[[getAttr valueForKey:@"attribute_label"] objectAtIndex:0]];
//                                    
//                                }
//                                else{
//                                    [brandValueArr addObject:@"0"];
//                                    
//                                }
//                                
//                                
//                                
//                            }
//                            
//                            
//                            
//                        
//                        
//                        
//                        
//                        
//                        /****END of attributes*********/
//                        
//                        
//                        
//                        
//                        
//                        
//                        
//                        
//                        
//                        
//                        
//                        //                    if ([[isSaleableArr objectAtIndex:i] boolValue] == YES) {
//                        
//                        
//                        
//                        
//                        //                    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
//                        priceArray=[[NSMutableArray alloc] init];
//                        cutOptionIdArray=[[NSMutableArray alloc] init];
//                        weightOptionIdArray=[[NSMutableArray alloc] init];
//                        typeOptionIdArray=[[NSMutableArray alloc] init];
//                        selectedWeightLabelIndex=[[NSMutableArray alloc] init];
//                        selectedWeightValueIndex=[[NSMutableArray alloc] init];
//                        selectedWeightPriceIndex=[[NSMutableArray alloc] init];
//                        selectedCutLabelIndex=[[NSMutableArray alloc] init];
//                        selectedCutValueIndex=[[NSMutableArray alloc] init];
//                        selectedCutPriceIndex=[[NSMutableArray alloc] init];
//                        selectedTypeLabelIndex=[[NSMutableArray alloc] init];
//                        selectedTypeValueIndex=[[NSMutableArray alloc] init];
//                        selectedTypePriceIndex=[[NSMutableArray alloc] init];
//                        
//                        for (int m=0; m<[optionIdArray count]; m++) {
//                            int a,b,c;
//                            a=0;b=0;c=0;
//                            if([[optionIdArray objectAtIndex:m] count]>0){
//                                
//                                for (int n=0; n<[[optionIdArray objectAtIndex:m] count]; n++) {
//                                    if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Pieces"]) {
//                                        [cutOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
//                                        a=1;
//                                    }
//                                    if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight"]) {
//                                        [weightOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
//                                        b=1;
//                                    }
//                                    if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Type"]) {
//                                        [typeOptionIdArray addObject:[[optionIdArray objectAtIndex:m] objectAtIndex:n]];
//                                        c=1;
//                                    }
//                                    
//                                    
//                                }
//                                if (a==0) {
//                                    [cutOptionIdArray addObject:@"0"];
//                                }
//                                if (b==0) {
//                                    [weightOptionIdArray addObject:@"0"];
//                                }
//                                if (c==0) {
//                                    [typeOptionIdArray addObject:@"0"];
//                                }
//                                
//                                
//                                
//                            }
//                            else{
//                                
//                                [cutOptionIdArray addObject:@"0"];
//                                [typeOptionIdArray addObject:@"0"];
//                                [weightOptionIdArray addObject:@"0"];
//                                
//                            }
//                            
//                            
//                            NSString *concatedPrice=[NSString stringWithFormat:@"%@",[newPriceArray objectAtIndex:m]];
//                            int conPri=[concatedPrice intValue];
//                            
//                            if (conPri == 0) {
//                                int sum;
//                                sum=0;
//                                if([[weightpriceArray objectAtIndex:m] count]>0){
//                                    
//                                    sum=+[[[weightpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
//                                }
//                                if([[cutpriceArray objectAtIndex:m] count]>0){
//                                    sum=sum+[[[cutpriceArray objectAtIndex:m] objectAtIndex:0] intValue];
//                                }
//                                if([[typepriceArray objectAtIndex:m] count]>0){
//                                    sum=sum+[[[typepriceArray objectAtIndex:m] objectAtIndex:0] intValue];
//                                }
//                                NSString *calcSum=[NSString stringWithFormat:@"%d",sum];
//                                [priceArray addObject:calcSum];
//                            }
//                            else{
//                                [priceArray addObject:concatedPrice];
//                            }
//                            
//                            if([[weightlabelArray objectAtIndex:m] count]>0){
//                                [selectedWeightLabelIndex addObject:[[weightlabelArray objectAtIndex:m] objectAtIndex:0]];
//                                [selectedWeightValueIndex addObject:[[weightValueArray objectAtIndex:m] objectAtIndex:0]];
//                                [selectedWeightPriceIndex addObject:[[weightpriceArray objectAtIndex:m] objectAtIndex:0]];
//                                
//                            }
//                            else{
//                                [selectedWeightLabelIndex addObject:@"0"];
//                                [selectedWeightValueIndex addObject:@"0"];
//                                [selectedWeightPriceIndex addObject:@"0"];
//                                
//                            }
//                            if([[cutlabelArray objectAtIndex:m] count]>0){
//                                [selectedCutLabelIndex addObject:[[cutlabelArray objectAtIndex:m] objectAtIndex:0]];
//                                [selectedCutValueIndex addObject:[[cutValueArray objectAtIndex:m] objectAtIndex:0]];
//                                [selectedCutPriceIndex addObject:[[cutpriceArray objectAtIndex:m] objectAtIndex:0]];
//                                
//                            }
//                            else{
//                                [selectedCutLabelIndex addObject:@"0"];
//                                [selectedCutValueIndex addObject:@"0"];
//                                [selectedCutPriceIndex addObject:@"0"];
//                                
//                            }
//                            if([[typelabelArray objectAtIndex:m] count]>0){
//                                [selectedTypeLabelIndex addObject:[[typelabelArray objectAtIndex:m] objectAtIndex:0]];
//                                [selectedTypeValueIndex addObject:[[typeValueArray objectAtIndex:m] objectAtIndex:0]];
//                                [selectedTypePriceIndex addObject:[[typepriceArray objectAtIndex:m] objectAtIndex:0]];
//                                
//                            }
//                            else{
//                                [selectedTypeLabelIndex addObject:@"0"];
//                                [selectedTypeValueIndex addObject:@"0"];
//                                [selectedTypePriceIndex addObject:@"0"];
//                                
//                            }
//                            
//                        }
//                        
//                        
//                        
//                        
//                        
//                        
//                        //                OfflineDb *object=[[OfflineDb alloc]init];
//                        
//                        //                        [self getRangeValuesOfPrice:priceArray];
//                        
//                        [HUD hide:YES];
//                        
//                        
//                        NSLog(@"Idarray1=%@",idArray);
//                        
//                    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                    HUD.dimBackground = YES;
//                    HUD.labelText = @"Please wait while submitting...";
//                    [HUD showWhileExecuting:@selector(addToCartMethod) onTarget:self withObject:Nil animated:YES];
//   
//                    
////                    [self addToCartMethod];
//                    
//                    
//                    
//                    
//                    
//                    
//                    
//                    
//                    
//                    
//                }
//                else{
//                    
//                    
//                    
//                    [HUD hide:YES];
//                    UIAlertController * alert=   [UIAlertController
//                                                  alertControllerWithTitle:@"Alert"
//                                                  message:@"No data found."
//                                                  preferredStyle:UIAlertControllerStyleAlert];
//                    
//                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                               handler:^(UIAlertAction * action) {
//                                                                   //Do Some action here
//                                                                   [HUD hide:YES];
//                                                                   nameArray=[[NSMutableArray alloc] init];
//                                                                   idArray=[[NSMutableArray alloc] init];
//                                                                   imagesArray=[[NSMutableArray alloc] init];
//                                                                   idArray=[[NSMutableArray alloc] init];
//                                                                   nameArray=[[NSMutableArray alloc] init];
//                                                                   descriptionArray=[[NSMutableArray alloc] init];
//                                                                   imagesArray=[[NSMutableArray alloc] init];
//                                                                   skuArray=[[NSMutableArray alloc] init];
//                                                                   priceArray=[[NSMutableArray alloc] init];
//                                                                   shortDescArray=[[NSMutableArray alloc] init];
//                                                                   idContainInLocal=[[NSMutableArray alloc] init];
//                                                                   localQtyArr=[[NSMutableArray alloc] init];
//                                                                   foodTypeArr=[[NSMutableArray alloc] init];
//                                                                   
//                                                                   localCutOpIdArray=[[NSMutableArray alloc] init];
//                                                                   localCutOpValArray=[[NSMutableArray alloc] init];
//                                                                   localCutOpPriceArray=[[NSMutableArray alloc] init];
//                                                                   
//                                                                   localWeightOpIdArray=[[NSMutableArray alloc] init];
//                                                                   localWeightOpValArray=[[NSMutableArray alloc] init];
//                                                                   localWeightOpPriceArray=[[NSMutableArray alloc] init];
//                                                                   
//                                                                   localTypeOpIdArray=[[NSMutableArray alloc] init];
//                                                                   localTypeOpValArray=[[NSMutableArray alloc] init];
//                                                                   localTypeOpPriceArray=[[NSMutableArray alloc] init];
//                                                                   
//                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
//                                                                   
//                                                               }];
//                    [alert addAction:ok];
//                    
//                    [self presentViewController:alert animated:YES completion:nil];
//                    
//                }
//            
                
            } @catch (NSException *exception) {
                NSLog(@"Exception = %@",exception);
                [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                
            }
            
        }
        
    });
    
    
}


-(void)addToCartMethod
{
    NSMutableArray *demoOpArray=[NSMutableArray arrayWithArray:[OptionHistoryArray objectAtIndex:0]];
    
    
    for (int i=0; i<[HistIdArray count]; i++)
    {
        if (!([idArray containsObject:[HistIdArray objectAtIndex:i]]))
        {
            [HistIdArray removeObjectAtIndex:i];
            
            [demoOpArray removeObjectAtIndex:i];
            i=i-1;
        }
    }
    
    NSMutableArray *emtArray=[[NSMutableArray alloc] init];
    OptionHistoryArray=[[NSMutableArray alloc] init];
    [OptionHistoryArray addObject:emtArray];
    [[OptionHistoryArray objectAtIndex:0] addObjectsFromArray:demoOpArray];

    
    NSArray *newInputTypeArray=[OptionHistoryArray valueForKey:@"option_type"];
    NSArray *newopIdArr=[OptionHistoryArray valueForKey:@"option_id"];
    NSArray *newlabelArr=[OptionHistoryArray valueForKey:@"label"];
    NSArray *newvalueArr=[OptionHistoryArray valueForKey:@"value"];
    NSArray *newOptionvalueArr=[OptionHistoryArray valueForKey:@"option_value"];

    NSMutableArray *InputTypeArray=[[NSMutableArray alloc] init];
    NSMutableArray *opIdArr=[[NSMutableArray alloc] init];
    NSMutableArray *labelArr=[[NSMutableArray alloc] init];
    NSMutableArray *valueArr=[[NSMutableArray alloc] init];
    NSMutableArray *opValueArr=[[NSMutableArray alloc] init];

    NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
    NSMutableArray *emptyArray1=[[NSMutableArray alloc] init];
    NSMutableArray *emptyArray2=[[NSMutableArray alloc] init];
    NSMutableArray *emptyArray3=[[NSMutableArray alloc] init];
    NSMutableArray *emptyArray4=[[NSMutableArray alloc] init];

    [opIdArr addObject:emptyArray];
    [labelArr addObject:emptyArray1];
    [valueArr addObject:emptyArray2];
    [InputTypeArray addObject:emptyArray3];
    [opValueArr addObject:emptyArray4];

    for (int p=0; p<[HistIdArray count]; p++) {
        
                NSMutableArray *emptyArray=[[NSMutableArray alloc] init];

//                NSArray *labArr=[[newInputTypeArray objectAtIndex:0] objectAtIndex:s];
                if (!((NSArray *)[NSNull null] == [[newInputTypeArray objectAtIndex:0] objectAtIndex:p]))
                {
                    [[InputTypeArray objectAtIndex:0] addObject:[[newInputTypeArray objectAtIndex:0] objectAtIndex:p]];
                    
                }
                else{
                
                    [[InputTypeArray objectAtIndex:0] addObject:emptyArray];

                
                }

                
                if (!((NSArray *)[NSNull null] == [[newopIdArr objectAtIndex:0] objectAtIndex:p]))
                {
                    [[opIdArr objectAtIndex:0] addObject:[[newopIdArr objectAtIndex:0] objectAtIndex:p]];
                    
                }
                else{
                    
                    [[opIdArr objectAtIndex:0] addObject:emptyArray];
                    
                    
                }

                
                if (!((NSArray *)[NSNull null] == [[newlabelArr objectAtIndex:0] objectAtIndex:p]))
                {
                    [[labelArr objectAtIndex:0] addObject:[[newlabelArr objectAtIndex:0] objectAtIndex:p]];
                    
                }
                else{
                    
                    [[labelArr objectAtIndex:0] addObject:emptyArray];
                    
                    
                }


                if (!((NSArray *)[NSNull null] == [[newvalueArr objectAtIndex:0] objectAtIndex:p]))
                {
                    [[valueArr objectAtIndex:0] addObject:[[newvalueArr objectAtIndex:0] objectAtIndex:p]];
                    
                }
                else{
                    
                    [[valueArr objectAtIndex:0] addObject:emptyArray];
                    
                    
                }
                
                
                if (!((NSArray *)[NSNull null] == [[newOptionvalueArr objectAtIndex:0] objectAtIndex:p]))
                {
                    [[opValueArr objectAtIndex:0] addObject:[[newOptionvalueArr objectAtIndex:0] objectAtIndex:p]];
                    
                }
                else{
                    
                    [[opValueArr objectAtIndex:0] addObject:emptyArray];
                    
                    
                }


        
        
    }
    
    
    
    
    
    cutOptionIdArray=[[NSMutableArray alloc] init];
    weightOptionIdArray=[[NSMutableArray alloc] init];
    typeOptionIdArray=[[NSMutableArray alloc] init];

    selectedWeightLabelIndex =[[NSMutableArray alloc] init];
    selectedWeightValueIndex=[[NSMutableArray alloc] init];
    selectedCutLabelIndex=[[NSMutableArray alloc] init];
    selectedCutValueIndex=[[NSMutableArray alloc] init];
    selectedTypeLabelIndex=[[NSMutableArray alloc] init];
    selectedTypeValueIndex=[[NSMutableArray alloc] init];

//    NSArray *priArr=[OptionHistoryArray valueForKey:@"price"];
//    NSArray *InputType=[OptionHistoryArray valueForKey:@"InputType"];

    
    

    if ([opIdArr count]>0) {
        for (int j=0; j<[opIdArr count]; j++) {
            
            
            wlArr=[[NSMutableArray alloc] init];
            wvArr=[[NSMutableArray alloc] init];
            wpArr=[[NSMutableArray alloc] init];
            clArr=[[NSMutableArray alloc] init];
            cvArr=[[NSMutableArray alloc] init];
            cpArr=[[NSMutableArray alloc] init];
            tlArr=[[NSMutableArray alloc] init];
            tvArr=[[NSMutableArray alloc] init];
            tpArr=[[NSMutableArray alloc] init];
            

            for (int m=0; m<[[opIdArr objectAtIndex:j] count]; m++)
            {
                NSArray *arr=[[labelArr objectAtIndex:j] objectAtIndex:m];
                for (int n=0; n<[arr count]; n++)
                {
                    if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"])
                    {
                        NSString *str_wlArr=[[[valueArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        NSString *str_wvArr=[[[opValueArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        NSString *str_wopArr=[[[opIdArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        [wlArr addObject:str_wlArr];
                        [wvArr addObject:str_wvArr];
                        [wpArr addObject:str_wopArr];
                    }
                    
                    if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"])
                    {
                        NSString *str_clArr=[[[valueArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        NSString *str_cvArr=[[[opValueArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        NSString *str_copArr=[[[opIdArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        [clArr addObject:str_clArr];
                        [cvArr addObject:str_cvArr];
                        [cpArr addObject:str_copArr];
                        
                    }
                    
                    if ([[arr objectAtIndex:n] isEqualToString:@"Type"])
                    {
                        NSString *str_tlArr=[[[valueArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        NSString *str_tvArr=[[[opValueArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        NSString *str_topArr=[[[opIdArr objectAtIndex:j] objectAtIndex:m] objectAtIndex:n];
                        [tlArr addObject:str_tlArr];
                        [tvArr addObject:str_tvArr];
                        [tpArr addObject:str_topArr];
                        
                    }
                    
                }
                
                if (([wpArr count]-1)!=m) {
                    [wlArr addObject:@"0"];
                    [wvArr addObject:@"0"];
                    [wpArr addObject:@"0"];
                }
                if (([cpArr count]-1)!=m) {
                    [clArr addObject:@"0"];
                    [cvArr addObject:@"0"];
                    [cpArr addObject:@"0"];
                }
                if (([tpArr count]-1)!=m) {
                    [tlArr addObject:@"0"];
                    [tvArr addObject:@"0"];
                    [tpArr addObject:@"0"];                }
                
                
            }
            [selectedWeightLabelIndex addObjectsFromArray:wlArr];
            [selectedWeightValueIndex addObjectsFromArray:wvArr];
            //            [weightpriceArray addObject:wpArr];
            [selectedCutLabelIndex addObjectsFromArray:clArr];
            [selectedCutValueIndex addObjectsFromArray:cvArr];
            //            [cutpriceArray addObject:cpArr];
            [selectedTypeLabelIndex addObjectsFromArray:tlArr];
            [selectedTypeValueIndex addObjectsFromArray:tvArr];
            //            [typepriceArray addObject:tpArr];
            [cutOptionIdArray addObjectsFromArray:cpArr];
            [weightOptionIdArray addObjectsFromArray:wpArr];
            [typeOptionIdArray addObjectsFromArray:tpArr];

        
        
    }
   
        
        
        
    }
    else{
        
        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
        [dropLabelArr addObject:emptyArray];
        [optionIdArray addObject:emptyArray];
        [weightlabelArray addObject:emptyArray];
        [weightValueArray addObject:emptyArray];
        [weightpriceArray addObject:emptyArray];
        [cutlabelArray addObject:emptyArray];
        [cutValueArray addObject:emptyArray];
        [cutpriceArray addObject:emptyArray];
        [typelabelArray addObject:emptyArray];
        [typeValueArray addObject:emptyArray];
        [typepriceArray addObject:emptyArray];
        
        
        
        
    }
    
    
    


    [self getData];



/*
    for (int i=0; i<[idArray count]; i++) {
        //  if ([self validations:[sender tag]]) {
        NSString *name=[nameArray objectAtIndex:i];
        //    NSString *sk=[skuArray objectAtIndex:[sender tag]];
        NSString *idStr=[idArray objectAtIndex:i];
        NSString *catId=@"0";
        NSString *img=[imagesArray objectAtIndex:i];
        NSString *imgSmall=[imageSmallArray objectAtIndex:i];
        NSString *imgMain=[imageMainArray objectAtIndex:i];
        NSString *imgThumb=[imageThumbArray objectAtIndex:i];
        NSString *desc=[descriptionArray objectAtIndex:i];
        NSString *shortDesc=[shortDescArray objectAtIndex:i];
        NSString *weight=[weightArray objectAtIndex:i];
        NSString *type=[foodTypeArr objectAtIndex:i];
        
        NSString *price=[priceArray objectAtIndex:i];
        NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
        NSString *currIndQty;
        currIndQty=[qtyArr objectAtIndex:i];
        
        //    NSString *msg=cell.txt_msg.text;
        //        NSString *msgId=[fieldIdArray objectAtIndex:[sender tag]];
        //                        [cutOptionIdArray addObject:@"0"];
        // [typeOptionIdArray addObject:@"0"];
        //[weightOptionIdArray addObject:@"0"];
        
        
        
        
        
        
        
//        NSString *opid=[optionIdArray objectAtIndex:i];
        
        
        NSString *decodedSelectedCutValue=[self kv_decodeHTMLCharacterEntities:[selectedCutValueIndex objectAtIndex:i]];
        
        NSString *getCutPrices=[self getSelectedCutPrice:[cutlabelArray objectAtIndex:i] AndPriceArray:[cutpriceArray objectAtIndex:i] AndSelectedLabel:decodedSelectedCutValue];
        
        
        NSString *decodedSelectedWeightValue=[self kv_decodeHTMLCharacterEntities:[selectedWeightValueIndex objectAtIndex:i]];

        
        NSString *getWeightPrices=[self getSelectedCutPrice:[weightlabelArray objectAtIndex:i] AndPriceArray:[weightpriceArray objectAtIndex:i] AndSelectedLabel:decodedSelectedWeightValue];

        NSString *decodedSelectedTypeValue=[self kv_decodeHTMLCharacterEntities:[selectedTypeValueIndex objectAtIndex:i]];
        
        NSString *getTypePrices=[self getSelectedCutPrice:[typelabelArray objectAtIndex:i] AndPriceArray:[typepriceArray objectAtIndex:i] AndSelectedLabel:decodedSelectedTypeValue];

        if (!([[cutOptionIdArray objectAtIndex:i] isEqualToString:@"0"]) || !([[weightOptionIdArray objectAtIndex:i] isEqualToString:@"0"]) || !([[typeOptionIdArray objectAtIndex:i] isEqualToString:@"0"])) {
            float a=0.0;
            if (!(getCutPrices ==nil)) {
                a=a+[getCutPrices intValue];
            }
            if (!(getWeightPrices ==nil)) {
                a=a+[getWeightPrices intValue];
            }
            if (!(getTypePrices ==nil)) {
                a=a+[getTypePrices intValue];
            }
            
            NSString *addPrices=[NSString stringWithFormat:@"%.2f",a];
            [priceArray replaceObjectAtIndex:i withObject:addPrices];
        }
        
        

        NSString *brand=[brandValueArr objectAtIndex:i];
        NSString *cutopid=[cutOptionIdArray objectAtIndex:i];
        NSString *cutopval=decodedSelectedCutValue;
        NSString *cutoppri=[selectedCutPriceIndex objectAtIndex:i];
        NSString *weiopid=[weightOptionIdArray objectAtIndex:i];
        NSString *weiopval=decodedSelectedWeightValue;
        NSString *weioppri=[selectedWeightPriceIndex objectAtIndex:i];
        NSString *typopid=[typeOptionIdArray objectAtIndex:i];
        NSString *typopval=decodedSelectedTypeValue;
        NSString *typoppri=[selectedTypePriceIndex objectAtIndex:i];
        NSString *cutoplabel=[selectedCutLabelIndex objectAtIndex:i];
        NSString *weioplabel=[selectedWeightLabelIndex objectAtIndex:i];
        NSString *typoplabel=[selectedTypeLabelIndex objectAtIndex:i];
        
        //For storing All data in database
        NSString *allcutLabels=[[cutlabelArray objectAtIndex:i] componentsJoinedByString:@","];
        NSString *allcutValues=[[cutValueArray objectAtIndex:i] componentsJoinedByString:@","];
        NSString *allcutPrices=[[cutpriceArray objectAtIndex:i] componentsJoinedByString:@","];
        
        NSString *allweightLabels=[[weightlabelArray objectAtIndex:i] componentsJoinedByString:@","];
        NSString *allweightValues=[[weightValueArray objectAtIndex:i] componentsJoinedByString:@","];
        NSString *allweightPrices=[[weightpriceArray objectAtIndex:i] componentsJoinedByString:@","];
        
        NSString *alltypeLabels=[[typelabelArray objectAtIndex:i] componentsJoinedByString:@","];
        NSString *alltypeValues=[[typeValueArray objectAtIndex:i] componentsJoinedByString:@","];
        NSString *alltypePrices=[[typepriceArray objectAtIndex:i] componentsJoinedByString:@","];
        
        
          NSString *qty=currIndQty;
        
        float total=[qty floatValue]*[price floatValue];
        NSString *tot=[NSString stringWithFormat:@"%.2f",total];
        
        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
        
        ///Add in cart
        
        NSMutableArray *cartDbArray=[[NSMutableArray alloc]init];
        testDB_Obj.cart_id=idStr;
        testDB_Obj.cart_category_id=catId;
        testDB_Obj.cart_name=name;
        testDB_Obj.cart_image_url=img;
        testDB_Obj.cart_image_thumb=imgThumb;
        testDB_Obj.cart_image_small=imgSmall;
        testDB_Obj.cart_image_main=imgMain;
        testDB_Obj.cart_price=price;
        testDB_Obj.cart_qty=qty;
        testDB_Obj.cart_description=desc;
        testDB_Obj.cart_short_desc=shortDesc;
        testDB_Obj.cart_food_type=type;
        testDB_Obj.cart_total=tot;
        testDB_Obj.cart_origin=@"";
        
        testDB_Obj.cart_brand=brand;
        testDB_Obj.cart_weight=weight;
        testDB_Obj.cart_option_id=@"";
        testDB_Obj.cart_cut_option_id=cutopid;
        testDB_Obj.cart_cut_option_value=cutopval;
        testDB_Obj.cart_cut_option_price=cutoppri;
        testDB_Obj.cart_weight_option_id=weiopid;
        testDB_Obj.cart_weight_option_value=weiopval;
        testDB_Obj.cart_weight_option_price=weioppri;
        testDB_Obj.cart_type_option_id=typopid;
        testDB_Obj.cart_type_option_value=typopval;
        testDB_Obj.cart_type_option_price=typoppri;
        testDB_Obj.cart_cut_option_label=cutoplabel;
        testDB_Obj.cart_weight_option_label=weioplabel;
        testDB_Obj.cart_type_option_label=typoplabel;
        
        //For cart master
        testDB_Obj.cm_cart_id=idStr;
        testDB_Obj.cm_category_id=catId;
        testDB_Obj.cm_all_cut_values=allcutValues;
        testDB_Obj.cm_all_cut_labels=allcutLabels;
        testDB_Obj.cm_all_cut_prices=allcutPrices;
        testDB_Obj.cm_all_weight_values=allweightValues;
        testDB_Obj.cm_all_weight_labels=allweightLabels;
        testDB_Obj.cm_all_weight_prices=allweightPrices;
        testDB_Obj.cm_all_type_values=alltypeValues;
        testDB_Obj.cm_all_type_labels=alltypeLabels;
        testDB_Obj.cm_all_type_prices=alltypePrices;
        
        [cartDbArray addObject:testDB_Obj];
        [testDB_Obj checkCartEntry:cartDbArray];
        
        
        
        //                    [idContainInLocal replaceObjectAtIndex:[sender tag] withObject:@"1"];
        //                    [localQtyArr replaceObjectAtIndex:[sender tag] withObject:cell.lbl_qty.text];
        //                    [localCutOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_cut.titleLabel.text];
        //                    [localWeightOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_weight.titleLabel.text];
        //                    [localTypeOpLabelArray replaceObjectAtIndex:[sender tag] withObject:cell.btn_type.titleLabel.text];
        
        //                [messagesArray replaceObjectAtIndex:[sender tag] withObject:cell.txt_msg.text];
        
        //                    NSLog(@"%@",idContainInLocal);
        //                    NSLog(@"%@",localQtyArr);
        
        
        NSArray *str=[object fetch_CartDetails];
        NSLog(@"%@",str);
        NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
        if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
            
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
            
            
        }
        
        [ProgressHUD showSuccess:@"Item successfully added to cart" Interaction:NO];
        
        
        //                    [self.pageTableView reloadData];
        
        

    }
    
    [HUD hide:YES];
    CartController *cvc=[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
    [self.navigationController pushViewController:cvc animated:YES];
    
    */
}


- (NSString *)kv_decodeHTMLCharacterEntities:(NSString *)inputString {
    if ([inputString rangeOfString:@"&"].location == NSNotFound) {
        return inputString;
    } else {
        NSMutableString *escaped = [NSMutableString stringWithString:inputString];
        NSArray *codes = [NSArray arrayWithObjects:
                          @"&nbsp;", @"&iexcl;", @"&cent;", @"&pound;", @"&curren;", @"&yen;", @"&brvbar;",
                          @"&sect;", @"&uml;", @"&copy;", @"&ordf;", @"&laquo;", @"&not;", @"&shy;", @"&reg;",
                          @"&macr;", @"&deg;", @"&plusmn;", @"&sup2;", @"&sup3;", @"&acute;", @"&micro;",
                          @"&para;", @"&middot;", @"&cedil;", @"&sup1;", @"&ordm;", @"&raquo;", @"&frac14;",
                          @"&frac12;", @"&frac34;", @"&iquest;", @"&Agrave;", @"&Aacute;", @"&Acirc;",
                          @"&Atilde;", @"&Auml;", @"&Aring;", @"&AElig;", @"&Ccedil;", @"&Egrave;",
                          @"&Eacute;", @"&Ecirc;", @"&Euml;", @"&Igrave;", @"&Iacute;", @"&Icirc;", @"&Iuml;",
                          @"&ETH;", @"&Ntilde;", @"&Ograve;", @"&Oacute;", @"&Ocirc;", @"&Otilde;", @"&Ouml;",
                          @"&times;", @"&Oslash;", @"&Ugrave;", @"&Uacute;", @"&Ucirc;", @"&Uuml;", @"&Yacute;",
                          @"&THORN;", @"&szlig;", @"&agrave;", @"&aacute;", @"&acirc;", @"&atilde;", @"&auml;",
                          @"&aring;", @"&aelig;", @"&ccedil;", @"&egrave;", @"&eacute;", @"&ecirc;", @"&euml;",
                          @"&igrave;", @"&iacute;", @"&icirc;", @"&iuml;", @"&eth;", @"&ntilde;", @"&ograve;",
                          @"&oacute;", @"&ocirc;", @"&otilde;", @"&ouml;", @"&divide;", @"&oslash;", @"&ugrave;",
                          @"&uacute;", @"&ucirc;", @"&uuml;", @"&yacute;", @"&thorn;", @"&yuml;", nil];
        
        NSUInteger i, count = [codes count];
        
        // Html
        for (i = 0; i < count; i++) {
            NSRange range = [inputString rangeOfString:[codes objectAtIndex:i]];
            if (range.location != NSNotFound) {
                [escaped replaceOccurrencesOfString:[codes objectAtIndex:i]
                                         withString:[NSString stringWithFormat:@"%C", (unsigned short) (160 + i)]
                                            options:NSLiteralSearch
                                              range:NSMakeRange(0, [escaped length])];
            }
        }
        
        // The following five are not in the 160+ range
        
        // @"&amp;"
        NSRange range = [inputString rangeOfString:@"&amp;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&amp;"
                                     withString:[NSString stringWithFormat:@"%C", (unsigned short) 38]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&lt;"
        range = [inputString rangeOfString:@"&lt;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&lt;"
                                     withString:[NSString stringWithFormat:@"%C", (unsigned short) 60]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&gt;"
        range = [inputString rangeOfString:@"&gt;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&gt;"
                                     withString:[NSString stringWithFormat:@"%C", (unsigned short) 62]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&apos;"
        range = [inputString rangeOfString:@"&apos;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&apos;"
                                     withString:[NSString stringWithFormat:@"%C", (unsigned short) 39]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // @"&quot;"
        range = [inputString rangeOfString:@"&quot;"];
        if (range.location != NSNotFound) {
            [escaped replaceOccurrencesOfString:@"&quot;"
                                     withString:[NSString stringWithFormat:@"%C", (unsigned short) 34]
                                        options:NSLiteralSearch
                                          range:NSMakeRange(0, [escaped length])];
        }
        
        // Decimal & Hex
        NSRange start, finish, searchRange = NSMakeRange(0, [escaped length]);
        i = 0;
        
        while (i < [escaped length]) {
            start = [escaped rangeOfString:@"&#"
                                   options:NSCaseInsensitiveSearch
                                     range:searchRange];
            
            finish = [escaped rangeOfString:@";"
                                    options:NSCaseInsensitiveSearch
                                      range:searchRange];
            
            if (start.location != NSNotFound && finish.location != NSNotFound &&
                finish.location > start.location) {
                NSRange entityRange = NSMakeRange(start.location, (finish.location - start.location) + 1);
                NSString *entity = [escaped substringWithRange:entityRange];
                NSString *value = [entity substringWithRange:NSMakeRange(2, [entity length] - 2)];
                
                [escaped deleteCharactersInRange:entityRange];
                
                if ([value hasPrefix:@"x"]) {
                    unsigned tempInt = 0;
                    NSScanner *scanner = [NSScanner scannerWithString:[value substringFromIndex:1]];
                    [scanner scanHexInt:&tempInt];
                    [escaped insertString:[NSString stringWithFormat:@"%C", (unsigned short) tempInt] atIndex:entityRange.location];
                } else {
                    [escaped insertString:[NSString stringWithFormat:@"%C", (unsigned short) [value intValue]] atIndex:entityRange.location];
                } i = start.location;
            } else { i++; }
            searchRange = NSMakeRange(i, [escaped length] - i);
        }
        
        return escaped;    // Note this is autoreleased
    }
}

-(NSString *)getSelectedCutPrice:(NSMutableArray *)inputCutLabArray AndPriceArray:(NSMutableArray *)inputPriceArray AndSelectedLabel:(NSString *)inputString{
    
    for (int i=0; i<[inputCutLabArray count]; i++) {
        if ([inputString isEqualToString:[inputCutLabArray objectAtIndex:i]]) {
            
            return [inputPriceArray objectAtIndex:i];
        }
    }
    return nil;
}



-(void)getData{

    
    @try
    {
        weightlabelArray=[[NSMutableArray alloc] init];
        weightValueArray=[[NSMutableArray alloc] init];
        weightpriceArray=[[NSMutableArray alloc] init];
        cutlabelArray=[[NSMutableArray alloc] init];
        cutValueArray=[[NSMutableArray alloc] init];
        cutpriceArray=[[NSMutableArray alloc] init];
        typelabelArray=[[NSMutableArray alloc] init];
        typeValueArray=[[NSMutableArray alloc] init];
        typepriceArray=[[NSMutableArray alloc] init];
        selectedCutPriceIndex=[[NSMutableArray alloc] init];
        selectedWeightPriceIndex=[[NSMutableArray alloc] init];
        selectedTypePriceIndex=[[NSMutableArray alloc] init];
        priceArray=[[NSMutableArray alloc] init];
        
        for (int i=0; i<[HistIdArray count]; i++) {
            
            
            
            for (NSArray *obj in demoDict)
            {
                NSLog(@"%@",obj);
                NSString *idArr=[obj valueForKey:@"entity_id"];
                if ([[HistIdArray objectAtIndex:i] isEqualToString:idArr]) {
                    
                    NSString *name=[obj valueForKey:@"name"];
                    //                NSArray *skuArr=[jsonDict valueForKey:@"sku"];
                    NSString *descriptionArr=[obj valueForKey:@"description"];
                    NSString *shortDescArr=[obj valueForKey:@"short_description"];
                    NSString *priceArr=[obj valueForKey:@"final_price_with_tax"];
                    //                NSArray *isSaleableArr=[jsonDict valueForKey:@"is_saleable"];
                    
                    NSString *foodType;
                    if (!((NSArray *)[NSNull null] == [obj valueForKey:@"food_type"]))
                    {
                        foodType=[obj valueForKey:@"food_type"];
                        //[foodTypeArr addObject:[foodType objectAtIndex:i]];
                        
                    }
                    else
                    {
                        
                        foodType=@"0";
                    }
                    
                    NSString *imagesArr=[obj valueForKey:@"image_url"];
                    NSString *imagesThumb=[obj valueForKey:@"image_thumb"];
                    NSString *imagesMainUrl=[obj valueForKey:@"main_url"];
                    NSString *imagesSmall=[obj valueForKey:@"image_small"];
                    NSString *brand=[obj valueForKey:@"brand"];
                    NSString *origin=[obj valueForKey:@"origin"];
                    
                    //NSString *weightArr=[obj valueForKey:@"weight"];
                    
                    NSString *weightArr;
                    if (!((NSArray *)[NSNull null] == [obj valueForKey:@"weight"]))
                    {
                        weightArr=[obj valueForKey:@"weight"];
                        //[foodTypeArr addObject:[foodType objectAtIndex:i]];
                        
                    }
                    else
                    {
                        
                        weightArr=@"0";
                    }
                        
                    
                    
                    // dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray;
                    NSArray *attrDict=[obj valueForKey:@"attributes"];
                    NSArray *InputTypeArray=[attrDict valueForKey:@"InputType"];
                    NSArray *opIdArr=[attrDict valueForKey:@"option_id"];
                    NSDictionary *attArr=[attrDict valueForKey:@"att"];
                    NSArray *labelArr=[attArr valueForKey:@"label"];
                    NSArray *valueArr=[attArr valueForKey:@"value"];
                    NSArray *priArr=[attArr valueForKey:@"price"];
                    NSArray *InputType=[attArr valueForKey:@"InputType"];
                    
                    
                    
                    
                    if ([attrDict count]>0) {
                        
                        if ([InputType  count]>0)
                        {
                            
                            wlArr=[[NSMutableArray alloc] init];
                            wvArr=[[NSMutableArray alloc] init];
                            wpArr=[[NSMutableArray alloc] init];
                            clArr=[[NSMutableArray alloc] init];
                            cvArr=[[NSMutableArray alloc] init];
                            cpArr=[[NSMutableArray alloc] init];
                            tlArr=[[NSMutableArray alloc] init];
                            tvArr=[[NSMutableArray alloc] init];
                            tpArr=[[NSMutableArray alloc] init];
                            
                            for (int m=0; m<[InputType  count]; m++)
                            {
                                NSArray *arr=[InputType objectAtIndex:m];
                                for (int n=0; n<[arr count]; n++)
                                {
                                    if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"])
                                    {
                                        NSString *str_wlArr=[[labelArr objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_wvArr=[[valueArr objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_wpArr=[[priArr objectAtIndex:m] objectAtIndex:n];
                                        [wlArr addObject:str_wlArr];
                                        [wvArr addObject:str_wvArr];
                                        [wpArr addObject:str_wpArr];
                                    }
                                    
                                    if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"])
                                    {
                                        NSString *str_clArr=[[labelArr objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_cvArr=[[valueArr objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_cpArr=[[priArr  objectAtIndex:m] objectAtIndex:n];
                                        [clArr addObject:str_clArr];
                                        [cvArr addObject:str_cvArr];
                                        [cpArr addObject:str_cpArr];
                                        
                                    }
                                    
                                    if ([[arr objectAtIndex:n] isEqualToString:@"Type"])
                                    {
                                        NSString *str_tlArr=[[labelArr objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_tvArr=[[valueArr objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_tpArr=[[priArr objectAtIndex:m] objectAtIndex:n];
                                        [tlArr addObject:str_tlArr];
                                        [tvArr addObject:str_tvArr];
                                        [tpArr addObject:str_tpArr];
                                        
                                    }
                                    
                                }
                                //                            if (([wpArr count]-1)!=m) {
                                //                                [wlArr addObject:@"0"];
                                //                                [wvArr addObject:@"0"];
                                //                                [wpArr addObject:@"0"];
                                //                            }
                                //                            if (([cpArr count]-1)!=m) {
                                //                                [clArr addObject:@"0"];
                                //                                [cvArr addObject:@"0"];
                                //                                [cpArr addObject:@"0"];
                                //                            }
                                //                            if (([tpArr count]-1)!=m) {
                                //                                [tlArr addObject:@"0"];
                                //                                [tvArr addObject:@"0"];
                                //                                [tpArr addObject:@"0"];
                                //                            }
                                
                                
                                
                            }
                            [weightlabelArray addObject:wlArr];
                            [weightValueArray addObject:wvArr];
                            [weightpriceArray addObject:wpArr];
                            [cutlabelArray addObject:clArr];
                            [cutValueArray addObject:cvArr];
                            [cutpriceArray addObject:cpArr];
                            [typelabelArray addObject:tlArr];
                            [typeValueArray addObject:tvArr];
                            [typepriceArray addObject:tpArr];
                            
                            
                        }
                        
                        
                        //                    NSArray *labArr=[opIdArr objectAtIndex:i];
                        //                    if ( (NSArray *)[NSNull null] == labArr )
                        //                    {
                        //                        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                        //                        [optionIdArray addObject:emptyArray];
                        //
                        //                    }
                        //                    else
                        //                    {
                        //                        //[dropLabelArr addObject:[labArr objectAtIndex:1]];
                        //                        [optionIdArray addObject:[opIdArr objectAtIndex:i]];
                        //
                        //                    }
                        //
                        //                    NSArray *valArr=[InputTypeArray objectAtIndex:i];
                        //                    if ( (NSArray *)[NSNull null] == valArr )
                        //                    {
                        //
                        //                    }
                        //                    else
                        //                    {
                        //                        [dropLabelArr addObject:valArr];
                        //
                        //                        //                            [dropValueArr addObject:[valArr objectAtIndex:1]];
                        //                    }
                        //                    NSLog(@"dropLabelArr=%@",dropLabelArr);
                        
                    }
                    else{
                        
                        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                        [dropLabelArr addObject:emptyArray];
                        [optionIdArray addObject:emptyArray];
                        [weightlabelArray addObject:emptyArray];
                        [weightValueArray addObject:emptyArray];
                        [weightpriceArray addObject:emptyArray];
                        [cutlabelArray addObject:emptyArray];
                        [cutValueArray addObject:emptyArray];
                        [cutpriceArray addObject:emptyArray];
                        [typelabelArray addObject:emptyArray];
                        [typeValueArray addObject:emptyArray];
                        [typepriceArray addObject:emptyArray];
                        
                        
                        
                        
                    }
                    
                    NSString *nam=name;
                    //    NSString *sk=[skuArray objectAtIndex:[sender tag]];
                    NSString *idStr=idArr;
                    NSString *catId=@"0";
                    NSString *img=imagesArr;
                    NSString *imgSmall=imagesSmall;
                    NSString *imgMain=imagesMainUrl;
                    NSString *imgThumb=imagesThumb;
                    NSString *desc=descriptionArr;
                    NSString *shortDesc=shortDescArr;
                    
                    NSString *weight=weightArr;
                    NSString *type=foodType;
                    
                    NSString *currIndQty;
                    currIndQty=[qtyArr objectAtIndex:i];
                    
                    
                    NSString *decodedSelectedCutValue=[self kv_decodeHTMLCharacterEntities:[selectedCutLabelIndex objectAtIndex:i]];
                    
                    NSString *getCutPrices=[self getSelectedCutPrice:[cutlabelArray objectAtIndex:i] AndPriceArray:[cutpriceArray objectAtIndex:i] AndSelectedLabel:decodedSelectedCutValue];
                    
                    
                    NSString *decodedSelectedWeightValue=[self kv_decodeHTMLCharacterEntities:[selectedWeightLabelIndex objectAtIndex:i]];
                    
                    
                    NSString *getWeightPrices=[self getSelectedCutPrice:[weightlabelArray objectAtIndex:i] AndPriceArray:[weightpriceArray objectAtIndex:i] AndSelectedLabel:decodedSelectedWeightValue];
                    
                    NSString *decodedSelectedTypeValue=[self kv_decodeHTMLCharacterEntities:[selectedTypeLabelIndex objectAtIndex:i]];
                    
                    NSString *getTypePrices=[self getSelectedCutPrice:[typelabelArray objectAtIndex:i] AndPriceArray:[typepriceArray objectAtIndex:i] AndSelectedLabel:decodedSelectedTypeValue];
                    
                    if (!([[cutOptionIdArray objectAtIndex:i] isEqualToString:@"0"]) || !([[weightOptionIdArray objectAtIndex:i] isEqualToString:@"0"]) || !([[typeOptionIdArray objectAtIndex:i] isEqualToString:@"0"])) {
                        float a=0.0;
                        if (!(getCutPrices ==nil)) {
                            a=a+[getCutPrices intValue];
                            [selectedCutPriceIndex addObject:getCutPrices];
                        }
                        else{
                            [selectedCutPriceIndex addObject:@"0"];
                            
                            
                        }
                        if (!(getWeightPrices ==nil)) {
                            a=a+[getWeightPrices intValue];
                            [selectedWeightPriceIndex addObject:getWeightPrices];
                        }
                        else{
                            [selectedWeightPriceIndex addObject:@"0"];
                            
                            
                        }
                        
                        if (!(getTypePrices ==nil))
                        {
                            a=a+[getTypePrices intValue];
                            [selectedTypePriceIndex addObject:getTypePrices];
                        }
                        else
                        {
                            [selectedTypePriceIndex addObject:@"0"];
                            
                            
                        }
                        
                        
                        NSString *addPrices=[NSString stringWithFormat:@"%.2f",a];
                        int Pri=[priceArr intValue]+[addPrices intValue];
                        [priceArray addObject:[NSString stringWithFormat:@"%d",Pri]];
                    }else{
                        [selectedTypePriceIndex addObject:@"0"];
                        [selectedWeightPriceIndex addObject:@"0"];
                        [selectedCutPriceIndex addObject:@"0"];
                        [priceArray addObject:priceArr];
                        
                    }
                    NSString *brnd;
                    NSArray *getBrandArr=[object fetch_BrandValue:@"brand" AndValue:brand ];
                    if ([getBrandArr count]>0) {
                        brnd=[[getBrandArr valueForKey:@"attribute_label"] objectAtIndex:0];
                        
                    }
                    else{
                        brnd=@"0";
                        
                    }
                    
                    
                    //                NSString *brand=[brandValueArr objectAtIndex:i];
                    NSString *cutopid=[cutOptionIdArray objectAtIndex:i];
                    NSString *cutopval=[selectedCutValueIndex objectAtIndex:i];
                    NSString *cutoppri=[selectedCutPriceIndex objectAtIndex:i];
                    NSString *weiopid=[weightOptionIdArray objectAtIndex:i];
                    NSString *weiopval=[selectedWeightValueIndex objectAtIndex:i];
                    NSString *weioppri=[selectedWeightPriceIndex objectAtIndex:i];
                    NSString *typopid=[typeOptionIdArray objectAtIndex:i];
                    NSString *typopval=[selectedTypeValueIndex objectAtIndex:i];
                    NSString *typoppri=[selectedTypePriceIndex objectAtIndex:i];
                    //NSString *cutoplabel=[selectedCutLabelIndex objectAtIndex:i];
                    NSString *cutoplabel=decodedSelectedCutValue;
                    NSString *weioplabel=decodedSelectedWeightValue;//[selectedWeightLabelIndex objectAtIndex:i];
                    NSString *typoplabel=decodedSelectedTypeValue;//[selectedTypeLabelIndex objectAtIndex:i];
                    
                    //For storing All data in database
                    NSString *allcutLabels=[[cutlabelArray objectAtIndex:i] componentsJoinedByString:@","];
                    NSString *allcutValues=[[cutValueArray objectAtIndex:i] componentsJoinedByString:@","];
                    NSString *allcutPrices=[[cutpriceArray objectAtIndex:i] componentsJoinedByString:@","];
                    
                    NSString *allweightLabels=[[weightlabelArray objectAtIndex:i] componentsJoinedByString:@","];
                    NSString *allweightValues=[[weightValueArray objectAtIndex:i] componentsJoinedByString:@","];
                    NSString *allweightPrices=[[weightpriceArray objectAtIndex:i] componentsJoinedByString:@","];
                    
                    NSString *alltypeLabels=[[typelabelArray objectAtIndex:i] componentsJoinedByString:@","];
                    NSString *alltypeValues=[[typeValueArray objectAtIndex:i] componentsJoinedByString:@","];
                    
                    NSString *alltypePrices=[[typepriceArray objectAtIndex:i] componentsJoinedByString:@","];
                    
                    
//                    if([[typepriceArray objectAtIndex:i] isEqual:@"0.0000"])
//                    {
//                        
//                    }
//                    else
//                    {
//                      
//                    }
                    
                    
                    NSString *price=[priceArray objectAtIndex:i];
                    NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
                    
                    NSString *qty=[NSString stringWithFormat:@"%d",[currIndQty intValue]];
                    
                    float total=[qty floatValue]*[price floatValue];
                    NSString *tot=[NSString stringWithFormat:@"%.2f",total];
                    
                    OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
                    
                    ///Add in cart
                    
                    NSMutableArray *cartDbArray=[[NSMutableArray alloc]init];
                    testDB_Obj.cart_id=idStr;
                    testDB_Obj.cart_category_id=catId;
                    testDB_Obj.cart_name=name;
                    testDB_Obj.cart_image_url=img;
                    testDB_Obj.cart_image_thumb=imgThumb;
                    testDB_Obj.cart_image_small=imgSmall;
                    testDB_Obj.cart_image_main=imgMain;
                    testDB_Obj.cart_price=price;
                    testDB_Obj.cart_qty=qty;
                    testDB_Obj.cart_description=desc;
                    testDB_Obj.cart_short_desc=shortDesc;
                    testDB_Obj.cart_food_type=type;
                    testDB_Obj.cart_total=tot;
                    testDB_Obj.cart_origin=@"";
                    
                    testDB_Obj.cart_brand=brnd;
                    testDB_Obj.cart_weight=weight;
                    testDB_Obj.cart_option_id=@"";
                    testDB_Obj.cart_cut_option_id=cutopid;
                    testDB_Obj.cart_cut_option_value=cutopval;
                    testDB_Obj.cart_cut_option_price=cutoppri;
                    testDB_Obj.cart_weight_option_id=weiopid;
                    testDB_Obj.cart_weight_option_value=weiopval;
                    testDB_Obj.cart_weight_option_price=weioppri;
                    testDB_Obj.cart_type_option_id=typopid;
                    testDB_Obj.cart_type_option_value=typopval;
                    testDB_Obj.cart_type_option_price=typoppri;
                    testDB_Obj.cart_cut_option_label=cutoplabel;
                    testDB_Obj.cart_weight_option_label=weioplabel;
                    testDB_Obj.cart_type_option_label=typoplabel;
                    
                    //For cart master
                    testDB_Obj.cm_cart_id=idStr;
                    testDB_Obj.cm_category_id=catId;
                    testDB_Obj.cm_all_cut_values=allcutValues;
                    testDB_Obj.cm_all_cut_labels=allcutLabels;
                    testDB_Obj.cm_all_cut_prices=allcutPrices;
                    testDB_Obj.cm_all_weight_values=allweightValues;
                    testDB_Obj.cm_all_weight_labels=allweightLabels;
                    testDB_Obj.cm_all_weight_prices=allweightPrices;
                    testDB_Obj.cm_all_type_values=alltypeValues;
                    testDB_Obj.cm_all_type_labels=alltypeLabels;
                    testDB_Obj.cm_all_type_prices=alltypePrices;
                    
                    [cartDbArray addObject:testDB_Obj];
                    [testDB_Obj checkCartEntry:cartDbArray];
                    
                    
                    
                    
                    
                    NSArray *str=[object fetch_CartDetails];
                    NSLog(@"%@",str);
                    NSString *cartCount=[NSString stringWithFormat:@"%d",[str count]];
                    if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
                        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
                        
                    }
                    else{
                        [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
                        
                        
                    }
                    
                    [ProgressHUD showSuccess:@"Item successfully added to cart" Interaction:NO];
                    
                    
                    //                    [self.pageTableView reloadData];
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    /////End OF if id loop//////
                }
                
                
                
                
                
                ///End of for in loop
            }
            ////End of hist id loop
        }
        [HUD hide:YES];
        CartController *cvc=[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
        [self.navigationController pushViewController:cvc animated:YES];
        

        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    
    

}







//-(NSString *)getSelectedWeightPrice:(NSMutableArray *)inputWeightLabArray AndPriceArray:(NSMutableArray *)inputPriceArray AndSelectedLabel:(NSString *)inputString{
//
//    for (int i=0; i<[inputWeightLabArray count]; i++) {
//        if ([inputString isEqualToString:[inputWeightLabArray objectAtIndex:i]]) {
//            
//            return [inputPriceArray objectAtIndex:i];
//        }
//    }
//    return nil;
//}
//-(NSString *)getSelectedTypePrice:(NSMutableArray *)inputTypeLabArray AndPriceArray:(NSMutableArray *)inputPriceArray AndSelectedLabel:(NSString *)inputString{
//    
//    for (int i=0; i<[inputTypeLabArray count]; i++) {
//        if ([inputString isEqualToString:[inputTypeLabArray objectAtIndex:i]]) {
//            
//            return [inputPriceArray objectAtIndex:i];
//        }
//    }
//    return nil;
//}
//

@end
