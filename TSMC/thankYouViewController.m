//
//  thankYouViewController.m
//  TSMC
//
//  Created by user on 16/10/18.
//  Copyright © 2018 Rahul Lekurwale. All rights reserved.
//

#import "thankYouViewController.h"
#import "AppConstant.h"
#import "ViewController.h"
#import "HelperClass.h"
#import "WebservicesClass.h"
#import "OfflineDb.h"
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface thankYouViewController ()
{
    MBProgressHUD *HUD;

    IBOutlet UILabel *orderIdLbl;
    IBOutlet UIButton *goToHomeBtn;
    OfflineDb *object;

    
}
- (IBAction)goToHomeBtnClicked:(id)sender;
@end

@implementation thankYouViewController
@synthesize orderIDStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    _titleBack.backgroundColor=TopBarColor;
//    _lbl_titleText.font=TopBarFont;
//    _lbl_titleText.textColor=TopBarTextColor;
    self.title = @"Thank You".uppercaseString;
    
    orderIdLbl.text = [NSString stringWithFormat:@"ORDER ID - %@",orderIDStr];
    object=[[OfflineDb alloc] init];
    [self callWebservice];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)goToHomeBtnClicked:(id)sender
{
    ViewController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:viw animated:YES];
}


#pragma mark CheckStatus API

-(void)callWebservice
{
    HelperClass *s=[[HelperClass alloc]init];
    BOOL connection=s.getInternetStatus;
    
    
    if (!connection){
        
        [HUD hide:YES];
        
        [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
    }else{
        @try {
            NSString *url=[NSString stringWithFormat:@"%@",API_GET_ORDER_STATUS];
            NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:orderIDStr,@"order_id", nil];
            
            WebservicesClass *web=[[WebservicesClass alloc] init];
            NSDictionary *dictObj=[web PostTopviewProductWithUrl:url withParameter:dict];
            
            if ([dictObj count] >0) {
                
                BOOL success=[[dictObj valueForKey:@"success"] boolValue];
                
                if (success) {
                    NSString *value = [dictObj valueForKey:@"status"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ORDERID"];
                    [object Delete_Cart_Data:@"ALL" AndCutLab:@"" AndWeightlab:@"" AndTypeLab:@""];
                    
                    NSString *msg=[NSString stringWithFormat:@"Order added successfully.\n Order ID:%@",orderIDStr];
                    [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"ShowCustomiseNote"];
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@""
                                                  message:msg
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   //Do Some action here
                                                                   
                                                                   ViewController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                                   [self.navigationController pushViewController:viw animated:YES];
                                                                   
                                                                   
                                                               }];
                    [alert addAction:ok];
                    
                    //[self presentViewController:alert animated:YES completion:nil];
                }
            }
            [HUD hide:YES];
            
        } @catch (NSException *exception) {
            [ProgressHUD showError:@"Something went wrong.Please try again." Interaction:NO];
        }
    }
}


#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Close App" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    //[alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
