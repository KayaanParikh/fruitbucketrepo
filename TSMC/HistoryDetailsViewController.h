//
//  HistoryDetailsViewController.h
//  TSMC
//
//  Created by user on 05/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface HistoryDetailsViewController : UIViewController
{
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *titleText;
    
    IBOutlet UILabel *or_ID;
    IBOutlet UILabel *or_time1;
    IBOutlet UILabel *or_type;
    IBOutlet UILabel *or_status;
    IBOutlet UILabel *or_time2;
    IBOutlet UILabel *or_add;
    IBOutlet UILabel *or_comment;
    IBOutlet UILabel *or_grandTotal;
   
    
    IBOutlet UILabel *orderIdLbl;
    IBOutlet UILabel *dateTimeLbl;
    IBOutlet UILabel *orderTypeLbl;
    IBOutlet UILabel *orderStatusLbl;
    IBOutlet UILabel *dateTimeLbl2;
    IBOutlet UILabel *orderAddressLbl;
    IBOutlet UILabel *orderCommentLbl;
    
    IBOutlet UITableView *orderDetailsTblView;
    
    IBOutlet UILabel *backLbl;
    IBOutlet UILabel *or_totalLblName;
    IBOutlet UILabel *grandTotalLbl;
    
    IBOutlet NSLayoutConstraint *backLblHeightConstraints;
    IBOutlet NSLayoutConstraint *totalLblNameHeightConstraints;
    IBOutlet NSLayoutConstraint *grandTotalConstraintsHeight;
    
    IBOutlet NSLayoutConstraint *viewConstraintsHeight;
    IBOutlet NSLayoutConstraint *addressConstraintsHeight;
    IBOutlet NSLayoutConstraint *commentConstraintsHeight;
    MBProgressHUD *HUD;
    
    NSString *orderId,*chkAPICall,*decodeStreet,*postcode,*city,*finalAddress;
    
    NSMutableArray *customerArr,*dishNameArr,*basePriceArr,*qtyArr,*basepriceQtyArr,*totalPriceArr,*totalPriceArr2,*basePriceArr2,*qtyArr2;
}
- (IBAction)backBtnClicked:(id)sender;
- (IBAction)reOrderMethod:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_reOrder;
@end
