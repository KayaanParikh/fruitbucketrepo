//
//  PaymentController.m
//  TSMC
//
//  Created by Rahul Lekurwale on 12/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "PaymentController.h"
#import "AppConstant.h"
#import "ViewController.h"
#import "HelperClass.h"
#import "WebservicesClass.h"
#import "OfflineDb.h"
#import "thankYouViewController.h"

@interface PaymentController ()
{
    
    NSString *urlStr;
    NSURL *url;
    OfflineDb *object;
}
@end

@implementation PaymentController
@synthesize fetchedOrderId, orderparaDict;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    NSLog(@"orderparaDict = %@",orderparaDict);
    
//    _titleBack.backgroundColor=TopBarColor;
//    _lbl_titleText.font=TopBarFont;
//    _lbl_titleText.textColor=TopBarTextColor;
    self.title = @"Payment".uppercaseString;
    object=[[OfflineDb alloc] init];
    
    _btn_back.hidden=NO;
    @try
    {
        fetchedOrderId=[[NSUserDefaults standardUserDefaults]valueForKey:@"ORDERID"];
        NSLog(@"%@",fetchedOrderId);
        NSString *url1=[NSString stringWithFormat:@"%@payment.php?order_id=%@",URL_BASE,fetchedOrderId];
        NSURL *conurl = [NSURL URLWithString:url1];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:conurl];
        [_payView loadRequest:requestObj];
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Please wait...";
        //    [HUD showWhileExecuting:@selector(submitData) onTarget:self withObject:Nil animated:YES];
        
        //        self.webView.delegate = self;
        
    }@catch (NSException *exception){
        [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];  
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
dispatch_async(dispatch_get_main_queue(), ^{
//    [HUD hide:YES];
    _btn_back.hidden=NO;
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.dimBackground = YES;
//    HUD.labelText = @"Please wait...";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //    [self updateButtons];
    });
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    HUD.dimBackground = YES;
    //    HUD.labelText = @"Please wait...";
    _btn_back.hidden=NO;

    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    _btn_back.hidden=NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [HUD hide:YES];
    });
    
    NSString *currentURL=_payView.request.URL.absoluteString;
    NSLog(@"%@",currentURL);
    
    if (!([currentURL rangeOfString:@"api4/payu/success.php"].location == NSNotFound))
    {
        
        thankYouViewController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"thankYouViewController"];
        viw.orderIDStr = fetchedOrderId;
        [self.navigationController pushViewController:viw animated:YES];

        
        /*NSString *msg=[NSString stringWithFormat:@"Your order has been successfully submitted.Your order id is %@",fetchedOrderId];
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Success"
                                      message:msg
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       NSLog(@"HEllO");
                                                       HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                                       HUD.dimBackground = YES;
                                                       HUD.labelText = @"Please wait...";
                                                       [HUD showWhileExecuting:@selector(callWebservice) onTarget:self withObject:Nil animated:YES];
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];*/
        
    }
    else if (!([currentURL rangeOfString:@"api3/payu/failure.php"].location == NSNotFound))
    {
        
        NSString *msg=[NSString stringWithFormat:@"Something went wrong.Please try again later."];
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Failure"
                                      message:msg
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       NSLog(@"HEllO");
                                                       ViewController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                       [self.navigationController pushViewController:viw animated:YES];
                                                       
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [HUD hide:YES];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    //    [self updateButtons];
}

#pragma mark CheckStatus API

-(void)callWebservice
{
    HelperClass *s=[[HelperClass alloc]init];
    BOOL connection=s.getInternetStatus;
    
    
    if (!connection){
        
        [HUD hide:YES];
        
        [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
    }else{
        @try {
            NSString *url=[NSString stringWithFormat:@"%@",API_GET_ORDER_STATUS];
            NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:fetchedOrderId,@"order_id", nil];
            
            WebservicesClass *web=[[WebservicesClass alloc] init];
            NSDictionary *dictObj=[web PostTopviewProductWithUrl:url withParameter:dict];
            
            if ([dictObj count] >0) {
                
                BOOL success=[[dictObj valueForKey:@"success"] boolValue];
                
                if (success) {
                    NSString *value = [dictObj valueForKey:@"status"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ORDERID"];
                    [object Delete_Cart_Data:@"ALL" AndCutLab:@"" AndWeightlab:@"" AndTypeLab:@""];
                    
                    NSString *msg=[NSString stringWithFormat:@"Order added successfully.\n Order ID:%@",fetchedOrderId];
                    [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"ShowCustomiseNote"];
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@""
                                                  message:msg
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   //Do Some action here
                                                                   
                                                                   ViewController *viw=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                                   [self.navigationController pushViewController:viw animated:YES];
                                                                   
                                                                   
                                                               }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            [HUD hide:YES];
            
        } @catch (NSException *exception) {
            [ProgressHUD showError:@"Something went wrong.Please try again." Interaction:NO];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Close App" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    //[alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}

@end
