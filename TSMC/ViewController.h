//
//  ViewController.h
//  TSMC
//
//  Created by Rahul Lekurwale on 21/06/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressHUD.h"
#import "MBProgressHUD.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate>
{
    
    MBProgressHUD *HUD;

}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLeft;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnRightCart;
@property (weak, nonatomic) IBOutlet UITableView *homeTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
- (IBAction)menuMethod:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *callUsBtn;
- (IBAction)callUsBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleText;
@property (weak, nonatomic) IBOutlet UIButton *btn_cart;
- (IBAction)cartMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cartCount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleBack;
@property (weak, nonatomic) IBOutlet UIButton *btn_search;
- (IBAction)searchMethod:(id)sender;

/////////ShieldView///////////////////////////
@property (weak, nonatomic) IBOutlet UIView *shieldView;
@property (weak, nonatomic) IBOutlet UIView *locBackView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_locTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_availabilty;
@property (strong, nonatomic) IBOutlet UIImageView *img_locSearch;
@property (strong, nonatomic) IBOutlet UIView *locSearchBackView;
@property (strong, nonatomic) IBOutlet UITextField *txt_locSearch;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
//@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *locationTableView;
///////////////////////////////////////////////


@property(nonatomic,weak)NSString *comingFrom,*logOutWindow;

@property (strong, nonatomic) IBOutlet UIView *progressBackView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMessage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMax;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMin;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *progressBackViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *minHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *maxHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *barHeight;



@end

