//
//  updateAddressViewController.h
//  TSMC
//
//  Created by user on 03/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "ProgressHUD.h"
#import "MBProgressHUD.h"

@interface updateAddressViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
{
     MBProgressHUD *HUD;
    
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *titleText;
    
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
    IBOutlet UITextField *firstNameTF;
    IBOutlet UITextField *lastNameTF;
    IBOutlet UITextField *telephoneTF;
    IBOutlet UITextField *StreetLine1TF;
    IBOutlet UITextField *streetLine2TF;
    IBOutlet UITextField *txtPinCode;
    IBOutlet UIButton *selectLocationBtn;
    IBOutlet UIButton *cityBtn;
    IBOutlet UIButton *cityStateBtn;
    IBOutlet UIButton *defaultAddressBtn;
    IBOutlet UIImageView *chkBoxImgView;
    
    IBOutlet UIButton *editBtn;
    IBOutlet UIButton *addBtn;
    IBOutlet NSLayoutConstraint *editBtnHeight;
    
    NSUserDefaults *defaults;
    NSString *ChkAPICall,*ChkAPICall2,*selectedLocationIndex,*defaultAdd,*ChkBtnClick,*cancelBtnclick;
    NSMutableArray *locationArr,*locationKeyArr,*selectedLocationArr,*locationStateArr;
}

@property (strong,nonatomic)UIPickerView *locationPicker;

@property (strong,nonatomic)NSString *chkAddressCount;

- (IBAction)backBtn_Clicked:(id)sender;
- (IBAction)selectLocationBtn_Clicked:(id)sender;
- (IBAction)defaultAddressBtn_Clicked:(id)sender;
- (IBAction)addBtn_Clicked:(id)sender;
- (IBAction)EditBtnClicked:(id)sender;
- (IBAction)cancelBtn_Clicked:(id)sender;
- (IBAction)selectStateBtn_Clicked:(id)sender;
- (IBAction)selectCityBtn_Clicked:(id)sender;

/////////Location View
@property (strong, nonatomic) IBOutlet UIView *shieldView;
@property (strong, nonatomic) IBOutlet UIView *locBackView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_locTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_availability;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *locationTableView;



@end
