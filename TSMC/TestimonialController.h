//
//  TestimonialController.h
//  TSMC
//
//  Created by Rahul Lekurwale on 04/09/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestimonialController : UIViewController<UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *img_background;
@property (strong, nonatomic) IBOutlet UIImageView *img_logo;
@property (strong, nonatomic) IBOutlet UIButton *btn_skip;
- (IBAction)skipMethod:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *msgBackView;
@property (strong, nonatomic) IBOutlet UIImageView *img_Qutation;
@property (strong, nonatomic) IBOutlet UILabel *lbl_msg;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;

@property(nonatomic,strong)NSArray *nameArray,*imageArray,*messageArray;

@end

