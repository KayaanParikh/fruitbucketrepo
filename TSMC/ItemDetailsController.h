//
//  ItemDetailsController.h
//  Earl of Hindh
//
//  Created by Rahul Lekurwale on 10/05/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "ProgressHUD.h"
#import "MBProgressHUD.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "RTLabel.h"

@interface ItemDetailsController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    MBProgressHUD *HUD;
    //NSString *cartCount;

}

@property (weak, nonatomic) IBOutlet UILabel *titleBack;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleText;
@property (weak, nonatomic) IBOutlet UIButton *btn_back;
- (IBAction)backMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_profile;
- (IBAction)profileMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_badgeCount;
@property (weak, nonatomic) IBOutlet UIImageView *img_Item;
@property (strong, nonatomic) IBOutlet UIImageView *img_item2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_itemName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_itemWeight;
@property (weak, nonatomic) IBOutlet UIImageView *img_foodType;
@property (weak, nonatomic) IBOutlet UILabel *lbl_itemDesc;
@property (weak, nonatomic) IBOutlet UILabel *lbl_itemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbl_spice;
@property (weak, nonatomic) IBOutlet UIImageView *img_spice;
@property (weak, nonatomic) IBOutlet UIImageView *img_dropDown;
@property (weak, nonatomic) IBOutlet UIButton *btn_spice;
- (IBAction)spiceMethod:(id)sender;
//@property (weak, nonatomic) IBOutlet GCPlaceholderTextView *txt_msg;
@property (strong, nonatomic) IBOutlet UIButton *btn_wish;
- (IBAction)wishMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_msg;
//@property (weak, nonatomic) IBOutlet UITextView *txt_msg;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btn_cart;
- (IBAction)cartMethod:(id)sender;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *quantityView;
@property (weak, nonatomic) IBOutlet UIScrollView *innerScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btn_minus;
- (IBAction)minusMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_qty;
@property (weak, nonatomic) IBOutlet UIButton *btn_plus;
- (IBAction)plusMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_addCart;
- (IBAction)addCartMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_history;
- (IBAction)historyMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_vieworder;
- (IBAction)viewOrderMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblOutOfStock;
@property(nonatomic,retain) NSString *outOfStockString;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *itemWeightLblHeight;
@property (nonatomic, strong) UINavigationController *parentNavigationController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spiceLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spiceBtnheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spiceImgHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgLablHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgTxtHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *itemNameContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *outOfStockHeightConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *outOfStockBottomConstriats;

@property (strong,nonatomic)UIPickerView *spicesPicker;
@property(nonatomic)NSString *fetchedItemId,*fetchedItemName,*fetchedItemImage,*fetchedItemDesc,*fetchedBrandVal,*fetchedItemPrice,*fetchedCategoryId,*fetchedBrandLabel,*fetchDropLabelName;
@property(nonatomic)NSString *fetchedShortDesc,*fetchedSmallImg,*fetchedMainImg,*fetchedThumbImg,*fetchedWeight,*fetchedCutOptionId,*fetchedWeightOptionId,*fetchedTypeOptionId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_cut_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_weight_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_type_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_cur_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_weight_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_type_height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lineLblBottomConstraints;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *qtyViewTopConstraints;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblCutBottomConstraints;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblWeightBottomConstraints;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblTypeBottomConstraints;

@property (strong, nonatomic) IBOutlet UIWebView *descWebView;

@property (weak, nonatomic) IBOutlet UILabel *lbl_brand;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cut;
@property (weak, nonatomic) IBOutlet UIButton *btn_cut;
- (IBAction)cutMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_weight;
//@property (weak, nonatomic) IBOutlet UIView *;
@property (weak, nonatomic) IBOutlet UILabel *lbl_type;
@property (weak, nonatomic) IBOutlet UIButton *btn_weight;
- (IBAction)weightMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_type;
- (IBAction)typeMethod:(id)sender;

@property(nonatomic)NSString *fetchedId,*fetchedName,*comingFrom;


@property(nonatomic)NSString *allCutLabel,*allCutPrice,*allCutValues,*allWeightLabel,*allWeightPrice,*allWeightValues,*allTypeLabel,*allTypePrice,*allTypeValues,*fetchedFoodType,*fetchedWeightLbl,*fetchedWishStat;

@property (strong, nonatomic) IBOutlet UIView *progressBackView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMessage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ProgressMax;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_progressMin;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *progressBackHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *maxHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *minHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *barHeight;

@end
