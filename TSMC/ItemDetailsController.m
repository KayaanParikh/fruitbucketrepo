//
//  ItemDetailsController.m
//  Earl of Hindh
//
//  Created by Rahul Lekurwale on 10/05/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "ItemDetailsController.h"
#import "ItemsViewController.h"
#import "SWRevealViewController.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "CartController.h"
#import "HelperClass.h"
#import "wishListHelper.h"
#import "WebservicesClass.h"
#import "ViewController.h"
//#import "LoginController.h"
//#import "HistoryController.h"
@interface ItemDetailsController ()<UITextViewDelegate>
{
    OfflineDb *object;
    
    NSString *cartCount;
    wishListHelper *wishObj;
    NSMutableArray *mirchImagesArr,*mirchIndexArr;
    OfflineDb *obj;
    int quntity;
    NSString *selectedSpiceLevel;
    NSString *selectedTable,*selectedTableName;
    NSMutableArray *pickerWeightLabelArr,*pickerWeightValueArr,*pickerWeightPriceArr;
    NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
    NSMutableArray *pickerTypeLabelArr,*pickerTypeValueArr,*pickerTypePriceArr;
    NSString *selectedLabel,*selectedPrice,*selectedValue;
    NSString *selectedCutLabelIndex,*selectedCutValueIndex,*selectedCutPriceIndex;
    NSString *selectedWeightLabelIndex,*selectedWeightValueIndex,*selectedWeightPriceIndex;
    NSString *selectedTypeLabelIndex,*selectedTypeValueIndex,*selectedTypePriceIndex;
    NSMutableArray *wishlistIdArray;
}
@end

@implementation ItemDetailsController
@synthesize spicesPicker,fetchedId,fetchedName,comingFrom;
@synthesize fetchedItemId,fetchedItemName,fetchedItemImage,fetchedItemDesc,fetchedBrandVal,fetchedItemPrice,fetchedCategoryId,fetchedBrandLabel,allCutLabel,allCutPrice,allCutValues,allWeightLabel,allWeightPrice,allWeightValues,allTypeLabel,allTypePrice,allTypeValues;
@synthesize fetchedShortDesc,fetchedSmallImg,fetchedMainImg,fetchedThumbImg,fetchedWeight,fetchedCutOptionId,fetchedWeightOptionId,fetchedTypeOptionId,fetchedFoodType,fetchDropLabelName,fetchedWishStat;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *border = [UIView new];
    border.backgroundColor = ButtonBlueColor;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(0, 0, self.progressBackView.frame.size.width, 2.0);
    [self.progressBackView addSubview:border];

}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=NO;
    
    [_quantityView.layer setCornerRadius:20.0f];
    _quantityView.layer.borderColor = TopBarTextColor.CGColor;
    _quantityView.layer.borderWidth = 0.5f;
    [_quantityView.layer setMasksToBounds:YES];
    
    quntity=0;
    selectedSpiceLevel=@"0";
    wishObj=[wishListHelper sharedInstance];
    obj=[[OfflineDb alloc] init];
    mirchImagesArr=[[NSMutableArray alloc] initWithObjects:@"1.png",@"2.png",@"3.png", nil];
    mirchIndexArr=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3", nil];
    pickerWeightLabelArr=[[NSMutableArray alloc] init];
    pickerWeightValueArr=[[NSMutableArray alloc] init];
    pickerWeightPriceArr=[[NSMutableArray alloc] init];
    pickerCutLabelArr=[[NSMutableArray alloc] init];
    pickerCutValueArr=[[NSMutableArray alloc] init];
    pickerCutPriceArr=[[NSMutableArray alloc] init];
    pickerTypeLabelArr=[[NSMutableArray alloc] init];
    pickerTypeValueArr=[[NSMutableArray alloc] init];
    pickerTypePriceArr=[[NSMutableArray alloc] init];
    object=[[OfflineDb alloc] init];
    wishlistIdArray=[[NSMutableArray alloc] init];
    
    
    
    
    if ([comingFrom isEqualToString:@"NOTIFICATION"]) {
        [self getNotificationProduct:fetchedItemId];
    }
    
    
//    _titleBack.backgroundColor=TopBarColor;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 480, 44)];
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 2;
        label.font = [UIFont boldSystemFontOfSize: 15.0f];
    //    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = cartIconTextColor;
        label.text = fetchedName.uppercaseString;
        
        self.navigationItem.titleView = label;
//    _lbl_titleText.font=TopBarFont;
//    _lbl_titleText.textColor=TopBarTextColor;
//    _lbl_badgeCount.textColor=cartIconTextColor;
//    _lbl_badgeCount.backgroundColor=cartIconBackColor;
    
    
    _scrollView.contentSize=CGSizeMake(self.view.frame.size.width, 400);
    
    
    _lbl_itemName.text=fetchedItemName;
    
    CGSize maximumLabelSize = CGSizeMake(296,9999);
    CGSize expecteditemNameLblSize = [fetchedItemName sizeWithFont:_lbl_itemName.font
                                                 constrainedToSize:maximumLabelSize
                                                     lineBreakMode:_lbl_itemName.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect itemNameLblFrame = _lbl_itemDesc.frame;
    itemNameLblFrame.size.height = expecteditemNameLblSize.height;
    _lbl_itemName.frame = itemNameLblFrame;
    
    
    
    NSString *descText=[fetchedItemDesc stringByReplacingOccurrencesOfString:@"</br>" withString:@"\n"];
    //    descText=[self stripTags:fetchedItemDesc];
    //    descText=[self stringByDecodingXMLEntities:fetchedItemDesc];
    NSAttributedString *str=[[NSAttributedString alloc] initWithData:[fetchedItemDesc dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    
    descText=[str string];
    
    _lbl_itemDesc.text=descText;
    [_lbl_itemDesc sizeToFit];
    
    _lbl_itemPrice.text=[NSString stringWithFormat:@"₹%.2f",[fetchedItemPrice floatValue]];
    if([fetchedBrandVal isEqualToString:@"0"])
    {
        
    }
    else
    {
        _lbl_brand.text=fetchedBrandVal;
    }
    
    if( ![fetchedWeight isEqual:[NSNull null]] )
    {
        _lbl_itemWeight.text=[NSString stringWithFormat:@"Weight: %.f gm",[fetchedWeight floatValue]];
    }
    else
    {
        _lbl_itemWeight.text=[NSString stringWithFormat:@"Weight: - gm"];
    }
    [_img_Item sd_setImageWithURL:[NSURL URLWithString:fetchedItemImage] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];
    _innerScrollView.contentSize=CGSizeMake(self.view.frame.size.width, 200);
    
    if([_outOfStockString isEqualToString:@"YES"])
    {
        NSDate *currentTime = [NSDate date];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"HH"];
        
        NSString *hourCountString = [timeFormatter stringFromDate:currentTime];
        
        int hourCountInt = [hourCountString intValue];
        
        NSLog(@"hourCountInt=%d",hourCountInt);
        //time between 12PM - 8PM.
        if(hourCountInt >= 12 && hourCountInt < 20)
        {
            //_lineLblBottomConstraints.constant = 0;
            _lblOutOfStock.hidden = YES;
            _outOfStockHeightConstraints.constant = 0;
            _quantityView.hidden = NO;
            _btn_addCart.hidden = NO;
            _btn_wish.hidden = NO;
            _btn_vieworder.hidden = YES;
            
            if ([fetchedWeight isEqual:[NSNull null]] || [fetchedWeight isEqualToString:@"0.0000"] || [fetchedWeight isEqualToString:@"0"])
            {
                _itemWeightLblHeight.constant=0;
            }
            else
            {
                _itemWeightLblHeight.constant=21;
                
            }
            
            if (allCutLabel.length==0)
            {
                _btn_cur_height.constant=0;
                _lbl_cut_height.constant=0;
                _btn_cut.hidden=YES;
                _lbl_cut.hidden=YES;
                selectedCutLabelIndex=@"0";
                selectedCutValueIndex=@"0";
                selectedCutPriceIndex=@"0";
                
                
            }else{
                //        NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
                NSArray *allCutLabArr=[allCutLabel componentsSeparatedByString:@","];
                NSArray *allCutPriArr=[allCutPrice componentsSeparatedByString:@","];
                NSArray *allCutValArr=[allCutValues componentsSeparatedByString:@","];
                
                [pickerCutLabelArr addObjectsFromArray:allCutLabArr];
                [pickerCutPriceArr addObjectsFromArray:allCutPriArr];
                [pickerCutValueArr addObjectsFromArray:allCutValArr];
                
                //        _btn_cur_height.constant=0;
                //        _lbl_cut_height.constant=0;
                _btn_cut.hidden=NO;
                _lbl_cut.hidden=NO;
                selectedCutLabelIndex=[pickerCutLabelArr objectAtIndex:0];
                selectedCutValueIndex=[pickerCutValueArr objectAtIndex:0];
                selectedCutPriceIndex=[pickerCutPriceArr objectAtIndex:0];
                [_btn_cut setTitle:selectedCutLabelIndex forState:UIControlStateNormal];
                _lbl_cut.text=fetchDropLabelName;
                
                
                
            }
            
            if (allWeightLabel.length==0){
                _btn_weight_height.constant=0;
                _lbl_weight_height.constant=0;
                _btn_weight.hidden=YES;
                _lbl_weight.hidden=YES;
                
                selectedWeightLabelIndex=@"0";
                selectedWeightValueIndex=@"0";
                selectedWeightPriceIndex=@"0";
                
                
            }else{
                //        NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
                NSArray *allWeiLabArr=[allWeightLabel componentsSeparatedByString:@","];
                NSArray *allWeiPriArr=[allWeightPrice componentsSeparatedByString:@","];
                NSArray *allWeiValArr=[allWeightValues componentsSeparatedByString:@","];
                
                [pickerWeightLabelArr addObjectsFromArray:allWeiLabArr];
                [pickerWeightPriceArr addObjectsFromArray:allWeiPriArr];
                [pickerWeightValueArr addObjectsFromArray:allWeiValArr];
                
                selectedWeightLabelIndex=[pickerWeightLabelArr objectAtIndex:0];
                selectedWeightValueIndex=[pickerWeightValueArr objectAtIndex:0];
                selectedWeightPriceIndex=[pickerWeightPriceArr objectAtIndex:0];
                
                [_btn_weight setTitle:selectedWeightLabelIndex forState:UIControlStateNormal];
                
                _btn_weight.hidden=NO;
                _lbl_weight.hidden=NO;
                
            }
            if (allTypeLabel.length==0) {
                _btn_type_height.constant=0;
                _lbl_type_height.constant=0;
                _btn_type.hidden=YES;
                _lbl_type.hidden=YES;
                selectedTypeLabelIndex=@"0";
                selectedTypeValueIndex=@"0";
                selectedTypePriceIndex=@"0";
                
                
            }else{
                //        NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
                NSArray *allTypLabArr=[allTypeLabel componentsSeparatedByString:@","];
                NSArray *allTypPriArr=[allTypePrice componentsSeparatedByString:@","];
                NSArray *allTypValArr=[allTypeValues componentsSeparatedByString:@","];
                
                [pickerTypeLabelArr addObjectsFromArray:allTypLabArr];
                [pickerTypePriceArr addObjectsFromArray:allTypPriArr];
                [pickerTypeValueArr addObjectsFromArray:allTypValArr];
                _btn_type.hidden=NO;
                _lbl_type.hidden=NO;
                
                selectedTypeLabelIndex=[pickerTypeLabelArr objectAtIndex:0];
                selectedTypeValueIndex=[pickerTypeValueArr objectAtIndex:0];
                selectedTypePriceIndex=[pickerTypePriceArr objectAtIndex:0];
                [_btn_type setTitle:selectedTypeLabelIndex forState:UIControlStateNormal];
                
                
            }
            
            if(allTypeLabel.length==0 && allWeightLabel.length==0 && allCutLabel.length==0)
            {
                //_lineLblBottomConstraints=_qtyViewTopConstraints;
                _lblCutBottomConstraints.constant=0;
                _lblWeightBottomConstraints.constant=0;
                _lblTypeBottomConstraints.constant=0;
                
                
            }
            
            if ([fetchedWishStat isEqualToString:@"1"]) {
                [_btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
            }
            else{
                [_btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
                
                
            }

        }
        else
        {
            _lbl_type.hidden = YES;
            _btn_type.hidden = YES;
            _lbl_type_height.constant = 0;
            
            _lbl_weight.hidden = YES;
            _btn_weight.hidden = YES;
            _lbl_weight_height.constant = 0;
            
            _lbl_cut.hidden = YES;
            _btn_cut.hidden = YES;
            _lbl_cut_height.constant = 0;
            
            _quantityView.hidden = YES;
            _btn_addCart.hidden = YES;
            _btn_wish.hidden = YES;
            _btn_vieworder.hidden = YES;
            
            _lblOutOfStock.hidden = NO;
            _lblOutOfStock.text = @"Currently out of Stock. Will be available for sale in 12PM to 8PM Hours.";
            _outOfStockHeightConstraints.constant = 50;
//            _outOfStockBottomConstriats.constant = 50;
//            _lineLblBottomConstraints.constant = 50;
        }
        
    }
    else
    {
        //_lineLblBottomConstraints.constant = 0;
        _lblOutOfStock.hidden = YES;
        _outOfStockHeightConstraints.constant = 0;
        _quantityView.hidden = NO;
        _btn_addCart.hidden = NO;
        _btn_wish.hidden = NO;
        _btn_vieworder.hidden = YES;
        
        if ([fetchedWeight isEqual:[NSNull null]] || [fetchedWeight isEqualToString:@"0.0000"] || [fetchedWeight isEqualToString:@"0"])
        {
            _itemWeightLblHeight.constant=0;
        }
        else
        {
            _itemWeightLblHeight.constant=21;
            
        }
        
        if (allCutLabel.length==0)
        {
            _btn_cur_height.constant=0;
            _lbl_cut_height.constant=0;
            _btn_cut.hidden=YES;
            _lbl_cut.hidden=YES;
            selectedCutLabelIndex=@"0";
            selectedCutValueIndex=@"0";
            selectedCutPriceIndex=@"0";
            
            
        }else{
            //        NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
            NSArray *allCutLabArr=[allCutLabel componentsSeparatedByString:@","];
            NSArray *allCutPriArr=[allCutPrice componentsSeparatedByString:@","];
            NSArray *allCutValArr=[allCutValues componentsSeparatedByString:@","];
            
            [pickerCutLabelArr addObjectsFromArray:allCutLabArr];
            [pickerCutPriceArr addObjectsFromArray:allCutPriArr];
            [pickerCutValueArr addObjectsFromArray:allCutValArr];
            
            //        _btn_cur_height.constant=0;
            //        _lbl_cut_height.constant=0;
            _btn_cut.hidden=NO;
            _lbl_cut.hidden=NO;
            selectedCutLabelIndex=[pickerCutLabelArr objectAtIndex:0];
            selectedCutValueIndex=[pickerCutValueArr objectAtIndex:0];
            selectedCutPriceIndex=[pickerCutPriceArr objectAtIndex:0];
            [_btn_cut setTitle:selectedCutLabelIndex forState:UIControlStateNormal];
            _lbl_cut.text=fetchDropLabelName;
            
            
            
        }
        
        if (allWeightLabel.length==0){
            _btn_weight_height.constant=0;
            _lbl_weight_height.constant=0;
            _btn_weight.hidden=YES;
            _lbl_weight.hidden=YES;
            
            selectedWeightLabelIndex=@"0";
            selectedWeightValueIndex=@"0";
            selectedWeightPriceIndex=@"0";
            
            
        }else{
            //        NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
            NSArray *allWeiLabArr=[allWeightLabel componentsSeparatedByString:@","];
            NSArray *allWeiPriArr=[allWeightPrice componentsSeparatedByString:@","];
            NSArray *allWeiValArr=[allWeightValues componentsSeparatedByString:@","];
            
            [pickerWeightLabelArr addObjectsFromArray:allWeiLabArr];
            [pickerWeightPriceArr addObjectsFromArray:allWeiPriArr];
            [pickerWeightValueArr addObjectsFromArray:allWeiValArr];
            
            selectedWeightLabelIndex=[pickerWeightLabelArr objectAtIndex:0];
            selectedWeightValueIndex=[pickerWeightValueArr objectAtIndex:0];
            selectedWeightPriceIndex=[pickerWeightPriceArr objectAtIndex:0];
            
            [_btn_weight setTitle:selectedWeightLabelIndex forState:UIControlStateNormal];
            
            _btn_weight.hidden=NO;
            _lbl_weight.hidden=NO;
            
        }
        if (allTypeLabel.length==0) {
            _btn_type_height.constant=0;
            _lbl_type_height.constant=0;
            _btn_type.hidden=YES;
            _lbl_type.hidden=YES;
            selectedTypeLabelIndex=@"0";
            selectedTypeValueIndex=@"0";
            selectedTypePriceIndex=@"0";
            
            
        }else{
            //        NSMutableArray *pickerCutLabelArr,*pickerCutValueArr,*pickerCutPriceArr;
            NSArray *allTypLabArr=[allTypeLabel componentsSeparatedByString:@","];
            NSArray *allTypPriArr=[allTypePrice componentsSeparatedByString:@","];
            NSArray *allTypValArr=[allTypeValues componentsSeparatedByString:@","];
            
            [pickerTypeLabelArr addObjectsFromArray:allTypLabArr];
            [pickerTypePriceArr addObjectsFromArray:allTypPriArr];
            [pickerTypeValueArr addObjectsFromArray:allTypValArr];
            _btn_type.hidden=NO;
            _lbl_type.hidden=NO;
            
            selectedTypeLabelIndex=[pickerTypeLabelArr objectAtIndex:0];
            selectedTypeValueIndex=[pickerTypeValueArr objectAtIndex:0];
            selectedTypePriceIndex=[pickerTypePriceArr objectAtIndex:0];
            [_btn_type setTitle:selectedTypeLabelIndex forState:UIControlStateNormal];
            
            
        }
        
        if(allTypeLabel.length==0 && allWeightLabel.length==0 && allCutLabel.length==0)
        {
            //_lineLblBottomConstraints=_qtyViewTopConstraints;
            _lblCutBottomConstraints.constant=0;
            _lblWeightBottomConstraints.constant=0;
            _lblTypeBottomConstraints.constant=0;
            
            
        }
        
        if ([fetchedWishStat isEqualToString:@"1"]) {
            [_btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
        }
        else{
            [_btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
            
            
        }
    }
    
    [self checkProgressBar];
    
    
    cartCount=[[NSUserDefaults standardUserDefaults] valueForKey:@"BADGECOUNT"];
    //        [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
    NSLog(@"cartCount=%@",cartCount);
    if ([cartCount isEqualToString:@"0"] || [cartCount isEqualToString:@"(null)"] || [cartCount isEqual:@"(null)"] || cartCount==nil )
    {
        self.navigationItem.rightBarButtonItem.badge.hidden = true;
        //_btn_viewOrder.backgroundColor=[UIColor grayColor];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.badge.hidden = NO;
        //_btn_viewOrder.backgroundColor=[UIColor redColor];
//        _lbl_badgeCount.text=cartCount;
        
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
            [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
        
    }
    
//    self.lbl_badgeCount.text=cartCount;
//    self.lbl_badgeCount.layer.masksToBounds=YES;
//    self.lbl_badgeCount.layer.cornerRadius=self.lbl_badgeCount.frame.size.width/2;
    
}


- (NSString *)stripTags:(NSString *)str
{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backMethod:(id)sender {
    
    if ([comingFrom isEqualToString:@"NOTIFICATION"]) {
        ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
- (IBAction)minusMethod:(id)sender {
    
    NSString *currIndQty=_lbl_qty.text;
    quntity=[currIndQty intValue];
    if (quntity > 0) {
        quntity--;
        _lbl_qty.text=[NSString stringWithFormat:@"%d",quntity];
        
    }
}
- (IBAction)plusMethod:(id)sender {
    NSString *currIndQty=_lbl_qty.text;
    quntity=[currIndQty intValue];
    if (quntity < 99) {
        quntity++;
        NSString *str=[NSString stringWithFormat:@"%d",quntity];
        _lbl_qty.text=str;
        
        
        
    }
}

#pragma mark UIPickerDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// Total rows in our component.

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger i;
    if (pickerView.tag==1) {
        i=[pickerCutLabelArr count];
        
    }
    else if (pickerView.tag==2){
        i=[pickerWeightLabelArr count];
    }
    else if (pickerView.tag==3){
        i=[pickerTypeLabelArr count];
        
    }
    
    //    i=[mirchImagesArr count];
    return i;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 50;
}
// Display each row's data.

//-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    id i;
//    i=[mirchImagesArr objectAtIndex: row];
//    return i;
//}

// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==1) {
        
        selectedLabel=[pickerCutLabelArr objectAtIndex:row];
        selectedValue=[pickerCutValueArr objectAtIndex:row];
        selectedPrice=[pickerCutPriceArr objectAtIndex:row];
        
    }else if (pickerView.tag==2){
        selectedLabel=[pickerWeightLabelArr objectAtIndex:row];
        selectedValue=[pickerWeightValueArr objectAtIndex:row];
        selectedPrice=[pickerWeightPriceArr objectAtIndex:row];
        
    }else if (pickerView.tag==3){
        selectedLabel=[pickerTypeLabelArr objectAtIndex:row];
        selectedValue=[pickerTypeValueArr objectAtIndex:row];
        selectedPrice=[pickerTypePriceArr objectAtIndex:row];
        
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    
    
    UILabel *lblRow = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f,spicesPicker.bounds.size.width, 44.0f)];
    
    
    if (pickerView.tag==1) {
        NSString *rowItem = [pickerCutLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==2){
        NSString *rowItem = [pickerWeightLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    else if (pickerView.tag==3){
        NSString *rowItem = [pickerTypeLabelArr objectAtIndex:row];
        
        [lblRow setTextAlignment:NSTextAlignmentCenter];
        
        [lblRow setTextColor: [UIColor blackColor]];
        
        [lblRow setText:rowItem];
        
        [lblRow setBackgroundColor:[UIColor clearColor]];
        
    }
    return lblRow;
}



- (IBAction)historyMethod:(id)sender {
    
    
}
- (IBAction)viewOrderMethod:(id)sender
{
    NSString *name=fetchedItemName;
    //    NSString *sk=[skuArray objectAtIndex:[sender tag]];
    NSString *idStr=fetchedItemId;
    NSString *catId=fetchedCategoryId;
    NSString *img=fetchedItemImage;
    NSString *imgSmall=fetchedSmallImg;
    NSString *imgMain=fetchedMainImg;
    NSString *imgThumb=fetchedThumbImg;
    NSString *desc=fetchedItemDesc;
    NSString *shortDesc=fetchedShortDesc;
    NSString *weight=fetchedWeight;
    NSString *rsReplacedString=[_lbl_itemPrice.text stringByReplacingOccurrencesOfString:@"₹" withString:@""];
    
    NSString *price=rsReplacedString;
    //    NSString *withotDollarPrice=[price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
    //    long row = [sender tag];
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    //    CustomTableViewCell *cell = [self.pageTableView cellForRowAtIndexPath:indexPath];
    NSString *currIndQty=_lbl_qty.text;
    
    NSString *opid=@"0";
    NSString *brand=fetchedBrandVal;
    NSString *cutopid=fetchedCutOptionId;
    NSString *cutopval=selectedCutValueIndex;
    NSString *cutoppri=selectedCutPriceIndex;
    NSString *weiopid=fetchedWeightOptionId;
    NSString *weiopval=selectedWeightValueIndex;
    NSString *weioppri=selectedWeightPriceIndex;
    NSString *typopid=fetchedTypeOptionId;
    NSString *typopval=selectedTypeValueIndex;
    NSString *typoppri=selectedTypePriceIndex;
    NSString *cutoplabel=selectedCutLabelIndex;
    NSString *weioplabel=selectedWeightLabelIndex;
    NSString *typoplabel=selectedTypeLabelIndex;
    NSString *type=fetchedFoodType;
    
    //For storing All data in database
    NSString *allcutL=allCutLabel;
    NSString *allcutValues=allCutValues;
    NSString *allcutPrices=allCutPrice;
    
    NSString *allweightLabels=allWeightLabel;
    NSString *allweightValues=allWeightValues;
    NSString *allweightPrices=allWeightPrice;
    
    NSString *alltypeLabels=allTypeLabel;
    NSString *alltypeValues=allTypeValues;
    NSString *alltypePrices=allTypePrice;
    
    
    NSString *qty=currIndQty;
    //        NSString *type=[foodTypeArr objectAtIndex:[sender tag]];
    
    if ([qty isEqualToString:@"0"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:@"Please select Quantity."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Do Some action here
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        float qtyInFloat=[qty floatValue];
        float priInFloat=[rsReplacedString floatValue];
        
        float total=qtyInFloat*priInFloat;
        NSString *tot=[NSString stringWithFormat:@"%.2f",total];
        
        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
        
        ///Add in cart
        
        NSMutableArray *cartDbArray=[[NSMutableArray alloc]init];
        testDB_Obj.cart_id=idStr;
        testDB_Obj.cart_category_id=catId;
        testDB_Obj.cart_name=name;
        testDB_Obj.cart_image_url=img;
        testDB_Obj.cart_image_thumb=imgThumb;
        testDB_Obj.cart_image_small=imgSmall;
        testDB_Obj.cart_image_main=imgMain;
        testDB_Obj.cart_price=price;
        testDB_Obj.cart_qty=qty;
        testDB_Obj.cart_description=desc;
        testDB_Obj.cart_short_desc=shortDesc;
        testDB_Obj.cart_food_type=type;
        testDB_Obj.cart_total=tot;
        testDB_Obj.cart_origin=@"";
        
        testDB_Obj.cart_brand=brand;
        testDB_Obj.cart_weight=weight;
        testDB_Obj.cart_option_id=@"";
        testDB_Obj.cart_cut_option_id=cutopid;
        testDB_Obj.cart_cut_option_value=cutopval;
        testDB_Obj.cart_cut_option_price=cutoppri;
        testDB_Obj.cart_weight_option_id=weiopid;
        testDB_Obj.cart_weight_option_value=weiopval;
        testDB_Obj.cart_weight_option_price=weioppri;
        testDB_Obj.cart_type_option_id=typopid;
        testDB_Obj.cart_type_option_value=typopval;
        testDB_Obj.cart_type_option_price=typoppri;
        testDB_Obj.cart_cut_option_label=cutoplabel;
        testDB_Obj.cart_weight_option_label=weioplabel;
        testDB_Obj.cart_type_option_label=typoplabel;
        
        //For cart master
        testDB_Obj.cm_cart_id=idStr;
        testDB_Obj.cm_category_id=catId;
        testDB_Obj.cm_all_cut_values=allcutValues;
        testDB_Obj.cm_all_cut_labels=allcutL;
        testDB_Obj.cm_all_cut_prices=allcutPrices;
        testDB_Obj.cm_all_weight_values=allweightValues;
        testDB_Obj.cm_all_weight_labels=allweightLabels;
        testDB_Obj.cm_all_weight_prices=allweightPrices;
        testDB_Obj.cm_all_type_values=alltypeValues;
        testDB_Obj.cm_all_type_labels=alltypeLabels;
        testDB_Obj.cm_all_type_prices=alltypePrices;
        
        [cartDbArray addObject:testDB_Obj];
        [testDB_Obj checkCartEntry:cartDbArray];
        
       
        
        NSArray *str=[testDB_Obj fetch_CartDetails];
        
        cartCount=[NSString stringWithFormat:@"%lu",(unsigned long)[str count]];
        if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
            self.navigationItem.rightBarButtonItem.badge.hidden = true;
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
            
        }
        else{
            self.navigationItem.rightBarButtonItem.badge.hidden = NO;
            [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
        }
        
        [self checkProgressBar];

        dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
        [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];

        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;
        });
        
//        self.lbl_badgeCount.text=cartCount;
        
        [ProgressHUD showSuccess:@"Item successfully added to cart" Interaction:NO];
        
        _lbl_qty.text=@"1";
        if ([allCutLabel isEqualToString:@"0"] || allCutLabel==nil || allCutLabel.length<=0) {
            //        _lbl_msg.hidden=YES;
            
        }
        else
        {
            selectedCutLabelIndex=[pickerCutLabelArr objectAtIndex:0];
            selectedCutValueIndex=[pickerCutValueArr objectAtIndex:0];
            selectedCutPriceIndex=[pickerCutPriceArr objectAtIndex:0];
            
            [_btn_cut setTitle:[pickerCutLabelArr objectAtIndex:0] forState:UIControlStateNormal];
            
        }
        
        if ([allWeightLabel isEqualToString:@"0"] || allWeightLabel==nil || allWeightLabel.length<=0) {
        }
        else
        {
            
            [_btn_weight setTitle:[pickerWeightLabelArr objectAtIndex:0] forState:UIControlStateNormal];
            
            selectedWeightLabelIndex=[pickerWeightLabelArr objectAtIndex:0];
            selectedWeightValueIndex=[pickerWeightValueArr objectAtIndex:0];
            selectedWeightPriceIndex=[pickerWeightPriceArr objectAtIndex:0];
            
            
        }
        if ([allTypeLabel isEqualToString:@"0"] || allTypeLabel==nil || allTypeLabel.length<=0) {
        }
        else
        {
            [_btn_type setTitle:[pickerTypeLabelArr objectAtIndex:0] forState:UIControlStateNormal];
            selectedTypeLabelIndex=[pickerTypeLabelArr objectAtIndex:0];
            selectedTypeValueIndex=[pickerTypeValueArr objectAtIndex:0];
            selectedTypePriceIndex=[pickerTypePriceArr objectAtIndex:0];
            
        }
       
    }
}
- (IBAction)cutMethod:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=1;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             //                                     [spicelevelArray replaceObjectAtIndex:[sender tag] withObject:selectedSpiceIndex];
                                             
                                             [_btn_cut setTitle:[pickerCutLabelArr objectAtIndex:0] forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                         }else{
                                             
                                             selectedCutLabelIndex=selectedLabel;
                                             selectedCutValueIndex =selectedValue;
                                             selectedCutPriceIndex=selectedPrice;
                                             
                                             [_btn_cut setTitle:selectedCutLabelIndex forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                         }
                                         
                                         int calcPrice=([selectedCutPriceIndex  intValue]+[selectedWeightPriceIndex intValue]+[selectedTypePriceIndex intValue]);
                                         NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                         _lbl_itemPrice.text=[NSString stringWithFormat:@"₹%.2f",[strPrice floatValue]];
                                         //                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                 }];
        action;
    })];
    
    
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
    
}
- (IBAction)weightMethod:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=2;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
         {
             @try {
                 if ([selectedLabel isEqualToString:@"0"] || selectedLabel==nil) {
                     [_btn_weight setTitle:[pickerWeightLabelArr objectAtIndex:0] forState:UIControlStateNormal];
                     selectedLabel=@"0";
                     selectedPrice=@"0";
                     selectedValue=@"0";
                     
                 }
                 else{
                     
                     selectedWeightLabelIndex=selectedLabel;
                     selectedWeightValueIndex=selectedValue;
                     selectedWeightPriceIndex=selectedPrice;
                     
                     
                     [_btn_weight setTitle:selectedWeightLabelIndex forState:UIControlStateNormal];
                     selectedLabel=@"0";
                     selectedPrice=@"0";
                     selectedValue=@"0";
                     
                 }
                 
                 
                 int calcPrice=([selectedCutPriceIndex intValue]+[selectedWeightPriceIndex intValue]+[selectedTypePriceIndex intValue]);
                 NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                 _lbl_itemPrice.text=[NSString stringWithFormat:@"₹%.2f",[strPrice floatValue]];
                 //                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                 
             }
             @catch (NSException *exception) {
                 [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                 
             }
         }];
        action;
    })];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
}

- (IBAction)typeMethod:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alertController.preferredContentSize=CGSizeMake(self.view.frame.size.width, 150.0);
    spicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 200)];
    
    spicesPicker.backgroundColor=[UIColor whiteColor];
    [spicesPicker setDataSource: self];
    [spicesPicker setDelegate: self];
    NSUInteger i=3;
    spicesPicker.tag=i;
    spicesPicker.showsSelectionIndicator = YES;
    [alertController.view addSubview:spicesPicker];
    
    //    [alertController.view addSubview:DateTimePicker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     @try {
                                         NSLog(@"%@",selectedTypeLabelIndex);
                                         
                                         if (selectedLabel == nil || [selectedLabel isEqualToString:@"0"]) {
                                             
                                             [_btn_type setTitle:[pickerTypeLabelArr objectAtIndex:0] forState:UIControlStateNormal];
                                             
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                         }
                                         else{
                                             
                                             selectedTypeLabelIndex =selectedLabel;
                                             selectedTypeValueIndex=selectedValue;
                                             selectedTypePriceIndex=selectedPrice;
                                             
                                             [_btn_type setTitle:selectedTypeLabelIndex forState:UIControlStateNormal];
                                             selectedLabel=@"0";
                                             selectedPrice=@"0";
                                             selectedValue=@"0";
                                             
                                             
                                         }
                                         
                                         int calcPrice=([selectedCutPriceIndex intValue]+[selectedWeightPriceIndex intValue]+[selectedTypePriceIndex intValue]);
                                         if(calcPrice==0)
                                         {
                                         }
                                         else
                                         {
                                             NSString *strPrice=[NSString stringWithFormat:@"%d",calcPrice];
                                             _lbl_itemPrice.text=[NSString stringWithFormat:@"₹%.2f",[strPrice floatValue]];
                                             //                                         [priceArray replaceObjectAtIndex:row withObject:strPrice];
                                         }
                                         
                                         
                                     } @catch (NSException *exception) {
                                         [ProgressHUD showError:@"Something went wrong.Please try again later" Interaction:NO];
                                         
                                     }
                                 }];
        action;
    })];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
    
}

- (IBAction)cartMethod:(id)sender
{
    if ([cartCount isEqualToString:@"0"] || [cartCount isEqualToString:@"(null)"] || [cartCount isEqual:@"(null)"] || cartCount==nil )
    {
        [ProgressHUD showError:@"Cart Empty" Interaction:NO];
    }
    else
    {
        CartController *viewController =[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
        viewController.comingFrom=@"ItemsDetailsVC";
        [self.navigationController pushViewController:viewController animated:YES];
    }
    //    CartController *crt=[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
    //    [self.navigationController pushViewController:crt animated:YES];
    
}
- (IBAction)wishMethod:(id)sender {
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
    
    if ([isLog isEqualToString:@"YES"]) {
        
        NSLog(@"%ld",[sender tag]);
        
        HelperClass *s=[[HelperClass alloc]init];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            BOOL connection=s.getInternetStatus;
            
            if (!connection)
            {
                
                
                NSLog(@"Hello Net connectiion is not pres;ent....");
                
                [HUD hide:YES];
                
                UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Connection Lost" message:@"Please check your internet connection!" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
                
                
                [alrt addAction:ok];
                [self presentViewController:alrt animated:YES completion:nil];
                
                
            }else{
                @try {
                    if ([fetchedWishStat isEqualToString:@"0"]) {
                        NSArray *getdata=[wishObj insertIntoProductList:fetchedItemId];
                        NSLog(@"%@",getdata);
                        if ([getdata count]>0) {
                            
                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
                            if (success) {
                                [_btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
                                fetchedWishStat=@"1";
                            }
                            else{
                                NSString *msg=[getdata valueForKey:@"message"];
                                [ProgressHUD showError:msg Interaction:NO];
                                
                            }
                        }
                        else{
                            
                            [ProgressHUD showError:@"Unable to add to wishlist" Interaction:NO];
                            
                        }
                        [HUD hide:YES];
                        
                    }
                    else if ([fetchedWishStat isEqualToString:@"1"]){
                        
                        NSArray *getdata=[wishObj deleteFromWishList:fetchedItemId];
                        NSLog(@"%@",getdata);
                        if ([getdata count]>0) {
                            BOOL success=[[getdata valueForKey:@"success"] boolValue];
                            if (success) {
                                
                                [_btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
                                fetchedWishStat=@"0";
                            }
                            else{
                                NSString *msg=[getdata valueForKey:@"message"];
                                [ProgressHUD showError:msg Interaction:NO];
                                
                            }
                        }else{
                            
                            [ProgressHUD showError:@"Unable to delete from wishlist" Interaction:NO];
                        }
                        [HUD hide:YES];
                    }
                } @catch (NSException *exception) {
                    [ProgressHUD showError:@"Something went wrong.Please try again later." Interaction:NO];
                }
            }
        });
    }
    else{
        [HUD hide:YES];
        
        UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:@"Alert" message:@"You need to login first." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
        [alrt addAction:ok];
        [self presentViewController:alrt animated:YES completion:nil];
        
        
        
    }
    
}




-(void)getNotificationProduct:(NSString *)prodId{
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    
    HelperClass *s=[[HelperClass alloc]init];
    
    BOOL connection=s.getInternetStatus;
    
    if (!connection){
        [HUD hide:YES];
        
        [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        
    }else{
        @try {
            //                HelperClass *s1=[[HelperClass alloc]init];
            NSString *tempUrl;
            tempUrl=[NSString stringWithFormat:@"%@",API_PRODUCT_DETAIL];
            WebservicesClass *web=[[WebservicesClass alloc] init];
            
            NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
            NSMutableDictionary *dict=[NSMutableDictionary dictionary];
            [dict setObject:prodId forKey:@"product_id"];
            
            if ([isLog isEqualToString:@"YES"]) {
                NSMutableDictionary *wishDict=[wishObj getAllWishData];
                BOOL success=[[wishDict valueForKey:@"success"] boolValue];
                if (success) {
                    [wishlistIdArray addObjectsFromArray:[[wishDict valueForKey:@"message"] valueForKey:@"entity_id"]];
                }
                
            }
            
            //NSDictionary *getJsonData=[web fetchUrl:url PostDataValuesAndKeys:post];
            NSDictionary *jsonDict=[web PostTopviewProductWithUrl:tempUrl withParameter:dict];
            NSLog(@"%@",jsonDict);
            if ([jsonDict count] > 0)
            {
                bool success=[[jsonDict valueForKey:@"success"] boolValue];
                if (success) {
                    NSArray *dataArray=[jsonDict valueForKey:@"data"];
                    NSArray *idArr=[dataArray valueForKey:@"entity_id"];
                    NSArray *name=[dataArray valueForKey:@"name"];
                    //                NSArray *skuArr=[jsonDict valueForKey:@"sku"];
                    NSArray *descriptionArr=[dataArray valueForKey:@"description"];
                    NSArray *shortDescArr=[dataArray valueForKey:@"short_description"];
                    NSArray *priceArr=[dataArray valueForKey:@"final_price_with_tax"];
                    //                NSArray *isSaleableArr=[jsonDict valueForKey:@"is_saleable"];
                    
                    NSArray *foodType=[dataArray valueForKey:@"food_type"];
                    NSArray *imagesArr=[dataArray valueForKey:@"image_url"];
                    NSArray *imagesThumb=[dataArray valueForKey:@"image_thumb"];
                    NSArray *imagesMainUrl=[dataArray valueForKey:@"main_url"];
                    NSArray *imagesSmall=[dataArray valueForKey:@"image_small"];
                    NSArray *brand=[dataArray valueForKey:@"brand"];
                    NSArray *origin=[dataArray valueForKey:@"origin"];
                    NSArray *weight=[dataArray valueForKey:@"weight"];
                    
                    
                    
                    // dropLabelArr,*dropValueArr,*fieldIdArray,*dropDownIdArray;
                    NSArray *attrDict=[dataArray valueForKey:@"attributes"];
                    NSArray *InputTypeArray=[attrDict valueForKey:@"InputType"];
                    NSArray *opIdArr=[attrDict valueForKey:@"option_id"];
                    NSDictionary *attArr=[attrDict valueForKey:@"att"];
                    NSArray *labelArr=[attArr valueForKey:@"label"];
                    NSArray *valueArr=[attArr valueForKey:@"value"];
                    NSArray *priArr=[attArr valueForKey:@"price"];
                    NSArray *InputType=[attArr valueForKey:@"InputType"];
                    [self fillContainwishArray:[idArr objectAtIndex:0] AndWishIdArray:wishlistIdArray];
                    
                    NSArray *getAttr=[object fetch_BrandValue:@"brand" AndValue:[brand objectAtIndex:0]];
                    if ([getAttr count]>0) {
                        fetchedBrandVal=[[getAttr valueForKey:@"attribute_label"] objectAtIndex:0];
                        
                    }else{
                        fetchedBrandVal=@"0";
                        
                    }
                    
                    NSMutableArray *weightlabelArray,*weightValueArray,*weightpriceArray,*cutlabelArray,*cutValueArray,*cutpriceArray,*typelabelArray,*typeValueArray,*typepriceArray,*optionIdArray,*dropLabelArr;
                    weightlabelArray=[[NSMutableArray alloc] init];
                    weightValueArray=[[NSMutableArray alloc] init];
                    weightpriceArray=[[NSMutableArray alloc] init];
                    cutlabelArray=[[NSMutableArray alloc] init];
                    cutValueArray=[[NSMutableArray alloc] init];
                    cutpriceArray=[[NSMutableArray alloc] init];
                    typelabelArray=[[NSMutableArray alloc] init];
                    typelabelArray=[[NSMutableArray alloc] init];
                    typepriceArray=[[NSMutableArray alloc] init];
                    optionIdArray=[[NSMutableArray alloc] init];
                    dropLabelArr=[[NSMutableArray alloc] init];
                    
                    
                    if ([[attrDict objectAtIndex:0] count]>0) {
                        
                        if ([[InputType objectAtIndex:0] count]>0) {
                            NSMutableArray *wlArr=[[NSMutableArray alloc] init];
                            NSMutableArray *wvArr=[[NSMutableArray alloc] init];
                            NSMutableArray *wpArr=[[NSMutableArray alloc] init];
                            NSMutableArray *clArr=[[NSMutableArray alloc] init];
                            NSMutableArray *cvArr=[[NSMutableArray alloc] init];
                            NSMutableArray *cpArr=[[NSMutableArray alloc] init];
                            NSMutableArray *tlArr=[[NSMutableArray alloc] init];
                            NSMutableArray *tvArr=[[NSMutableArray alloc] init];
                            NSMutableArray *tpArr=[[NSMutableArray alloc] init];
                            
                            for (int m=0; m<[[InputType objectAtIndex:0] count]; m++) {
                                NSArray *arr=[[InputType objectAtIndex:0] objectAtIndex:m];
                                for (int n=0; n<[arr count]; n++) {
                                    if ([[arr objectAtIndex:n] isEqualToString:@"Weight Range"] || [[arr objectAtIndex:n] isEqualToString:@"Weight"]) {
                                        NSString *str_wlArr=[[[labelArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_wvArr=[[[valueArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_wpArr=[[[priArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        [wlArr addObject:str_wlArr];
                                        [wvArr addObject:str_wvArr];
                                        [wpArr addObject:str_wpArr];
                                    }
                                    
                                    if ([[arr objectAtIndex:n] isEqualToString:@"Cut"] || [[arr objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                        NSString *str_clArr=[[[labelArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_cvArr=[[[valueArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_cpArr=[[[priArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        [clArr addObject:str_clArr];
                                        [cvArr addObject:str_cvArr];
                                        [cpArr addObject:str_cpArr];
                                        
                                    }
                                    
                                    if ([[arr objectAtIndex:n] isEqualToString:@"Type"]) {
                                        NSString *str_tlArr=[[[labelArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_tvArr=[[[valueArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        NSString *str_tpArr=[[[priArr objectAtIndex:0] objectAtIndex:m] objectAtIndex:n];
                                        [tlArr addObject:str_tlArr];
                                        [tvArr addObject:str_tvArr];
                                        [tpArr addObject:str_tpArr];
                                        
                                    }
                                }
                            }
                            [weightlabelArray addObject:wlArr];
                            [weightValueArray addObject:wvArr];
                            [weightpriceArray addObject:wpArr];
                            [cutlabelArray addObject:clArr];
                            [cutValueArray addObject:cvArr];
                            [cutpriceArray addObject:cpArr];
                            [typelabelArray addObject:tlArr];
                            [typeValueArray addObject:tvArr];
                            [typepriceArray addObject:tpArr];
                            
                        }
                        
                        NSArray *labArr=[opIdArr objectAtIndex:0];
                        if ( (NSArray *)[NSNull null] == labArr ){
                            NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                            [optionIdArray addObject:emptyArray];
                            
                        }else{
                            [optionIdArray addObject:[opIdArr objectAtIndex:0]];
                            
                        }
                        
                        NSArray *valArr=[InputTypeArray objectAtIndex:0];
                        if ( (NSArray *)[NSNull null] == valArr ){
                            
                        }else{
                            [dropLabelArr addObject:valArr];
                        }
                        
                    }else{
                        
                        NSMutableArray *emptyArray=[[NSMutableArray alloc] init];
                        [dropLabelArr addObject:emptyArray];
                        [optionIdArray addObject:emptyArray];
                        [weightlabelArray addObject:emptyArray];
                        [weightValueArray addObject:emptyArray];
                        [weightpriceArray addObject:emptyArray];
                        [cutlabelArray addObject:emptyArray];
                        [cutValueArray addObject:emptyArray];
                        [cutpriceArray addObject:emptyArray];
                        [typelabelArray addObject:emptyArray];
                        [typeValueArray addObject:emptyArray];
                        [typepriceArray addObject:emptyArray];
                        
                    }
                    
                    for (int m=0; m<[optionIdArray count]; m++) {
                        int a,b,c;
                        a=0;b=0;c=0;
                        if([[optionIdArray objectAtIndex:m] count]>0){
                            
                            for (int n=0; n<[[optionIdArray objectAtIndex:m] count]; n++) {
                                if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Cut"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Pieces"]) {
                                    fetchedCutOptionId=[[optionIdArray objectAtIndex:m] objectAtIndex:n];
                                    if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Pieces"])
                                    {
                                        fetchDropLabelName=@"Pieces:";
                                    }
                                    else
                                    {
                                        fetchDropLabelName=@"Cut:";
                                    }
                                    
                                    a=1;
                                }
                                if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight Range"] || [[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Weight"]) {
                                    fetchedWeightOptionId =[[optionIdArray objectAtIndex:m] objectAtIndex:n];
                                    b=1;
                                }
                                if ([[[dropLabelArr objectAtIndex:m] objectAtIndex:n] isEqualToString:@"Type"]) {
                                    fetchedTypeOptionId=[[optionIdArray objectAtIndex:m] objectAtIndex:n];
                                    c=1;
                                }
                            }
                            if (a==0) {
                                fetchedCutOptionId=@"0";
                            }
                            if (b==0) {
                                fetchedWeightOptionId=@"0";
                            }
                            if (c==0) {
                                fetchedTypeOptionId=@"0";
                            }
                        }
                        else{
                            
                            fetchedCutOptionId=@"0";
                            fetchedWeightOptionId=@"0";
                            fetchedTypeOptionId=@"0";
                            
                        }
                    }
                    
                  
                    
                    
                    allCutLabel=[[cutlabelArray objectAtIndex:0] componentsJoinedByString:@","];
                    allCutValues=[[cutValueArray objectAtIndex:0] componentsJoinedByString:@","];
                    allCutPrice=[[cutpriceArray objectAtIndex:0] componentsJoinedByString:@","];
                    
                    allWeightLabel=[[weightlabelArray objectAtIndex:0] componentsJoinedByString:@","];
                    allWeightValues=[[weightValueArray objectAtIndex:0] componentsJoinedByString:@","];
                    allWeightPrice=[[weightpriceArray objectAtIndex:0] componentsJoinedByString:@","];
                    
                    allTypeLabel=[[typelabelArray objectAtIndex:0] componentsJoinedByString:@","];
                    allTypeValues=[[typeValueArray objectAtIndex:0] componentsJoinedByString:@","];
                    allTypePrice=[[typepriceArray objectAtIndex:0] componentsJoinedByString:@","];
                    
                    
                    
                    fetchedName=@"Menu";
                    fetchedId=[idArr objectAtIndex:0];
                    
                    fetchedItemId=[idArr objectAtIndex:0];
                    fetchedItemName=[name objectAtIndex:0];
                    fetchedItemImage=[imagesArr objectAtIndex:0];
                    fetchedItemDesc=[descriptionArr objectAtIndex:0];
                    fetchedCategoryId=@"0";
                    fetchedBrandLabel=[idArr objectAtIndex:0];
                    fetchedShortDesc=[shortDescArr objectAtIndex:0];
                    fetchedSmallImg=[imagesSmall objectAtIndex:0];
                    fetchedMainImg=[imagesMainUrl objectAtIndex:0];
                    fetchedThumbImg=[imagesThumb objectAtIndex:0];
                    fetchedWeight=[weight objectAtIndex:0];
                    fetchedItemPrice=[idArr objectAtIndex:0];
                    fetchedFoodType=[foodType objectAtIndex:0];
                    
                    //allCutLabel,*allCutPrice,*allCutValues,*allWeightLabel,*allWeightPrice,*allWeightValues,*allTypeLabel,*allTypePrice,*allTypeValues,*fetchedFoodType,*fetchedWeightLbl,*fetchedWishStat
                    
                }else{
                    NSString *msg=[jsonDict valueForKey:@"message"];
                    [self UIAlertViewControllerMethodTitle:@"Alert" message:msg];
                    
                }
                [HUD hide:YES];
            }
            
        } @catch (NSException *exception) {
            [ProgressHUD showError:@"Something went wrong,Please try again later." Interaction:NO];
        }
    }
}

-(void)fillContainwishArray:(NSString *)prodIdArr AndWishIdArray:(NSMutableArray *)wIdArr{
    if ([wIdArr containsObject:prodIdArr]) {
        //            [containInWishListArray addObject:@"1"];
        fetchedWishStat=@"1";
        
        [_btn_wish setBackgroundImage:[UIImage imageNamed:@"red-01.png"] forState:UIControlStateNormal];
        
    }else{
        fetchedWishStat=@"0";
        
        [_btn_wish setBackgroundImage:[UIImage imageNamed:@"strock-01.png"] forState:UIControlStateNormal];
        
    }
}

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    
    [alrt addAction:ok];
    [self presentViewController:alrt animated:YES completion:nil];
}


-(void)checkProgressBar{
    
    NSArray *getTotalAmountArray=[object fetch_Total_Amount];
    NSString *amt=[[getTotalAmountArray valueForKey:@"total_cnt"] objectAtIndex:0];
    if (amt.length<=0 || [amt isEqualToString:@"(null)"]) {
        NSLog(@"nill");
        _progressBackHeight.constant=0;
        _progressView.hidden=YES;
        _lbl_ProgressMax.hidden=YES;
        _lbl_progressMin.hidden=YES;
        
        _progressBackView.hidden=YES;
        
    }else{
        _progressBackHeight.constant=60;
        _progressView.hidden=NO;
        _lbl_ProgressMax.hidden=NO;
        _lbl_progressMin.hidden=NO;
        
        _progressBackView.hidden=NO;
        
        _lbl_progressMin.text=@"0";
        //total_cnt
        NSString *storedAmt=[[NSUserDefaults standardUserDefaults] valueForKey:MAXLIMIT];
        _lbl_ProgressMax.text=storedAmt;
        
        if ([amt floatValue]<[_lbl_ProgressMax.text floatValue]) {
            _lbl_progressMessage.text=FREEFORSHIPPING;
            
            
            _progressBackView.hidden=NO;
            float maxValue = 1.0;
            float minValue = 0.0;
            
            CGFloat diff = ([amt floatValue] - 0);
            CGFloat scope = ([_lbl_ProgressMax.text floatValue] - 0);
            CGFloat progress;
            
            if(diff != 0.0) {
                progress = diff / scope;
            } else {
                progress = 0.0f;
            }
            float myValueConstrained = MAX(minValue, MIN(maxValue, progress)); // 1.0
            _progressView.progress=myValueConstrained;
            
        }else{
            //            _progressBackView.hidden=YES;
            _lbl_progressMessage.text=ELIGIBLE;
            _progressBackHeight.constant=30;
            _minHeight.constant=0;
            _maxHeight.constant=0;
            _barHeight.constant=0;
            _progressView.hidden=YES;
            _lbl_ProgressMax.hidden=YES;
            _lbl_progressMin.hidden=YES;
            
        }
    }
}

@end


