//
//  LandingScreenViewController.m
//  TSMC
//
//  Created by Rahul Lekurwale on 22/06/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "LandingScreenViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "ViewController.h"
#import "TestimonialController.h"
#import "HelpController.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define iPadPro12 (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height == 2732)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)




@interface LandingScreenViewController ()

@end

@implementation LandingScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self getBackgroundImage];
    HelperClass *s=[[HelperClass alloc]init];
    BOOL connection=s.getInternetStatus;
    
    
    
    /////Category Webservice
    if (!connection)
    {
        
        
        NSLog(@"Hello Net connectiion is not pres;ent....");
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                        message:@"Please check your internet connection!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else{
        //        [self getCategories];
        [self getLocations];
//        [self getShippingData];
        //        [self demo2Api];
    }
    
}
-(void)getBackgroundImage{
    if (IS_IPHONE_5) {
        _img_background.image=[UIImage imageNamed:@"splash_1136.png"];
    }
    else if(IS_IPHONE_6){
        _img_background.image=[UIImage imageNamed:@"splash_750.png"];
        
    }
    else if(IS_IPHONE_6P){
        _img_background.image=[UIImage imageNamed:@"splash_1080png.png"];
        
    }
    
    
}

/*
 -(void)getCategories
 {
 //[ProgressHUD show:PROGRESS_PLEASE_WAIT_EN Interaction:NO];
 //    HelperClass *s1=[[HelperClass alloc]init];
 NSString *url=API_CATEGORY_DATA;
 
 //NSString *url=@"http://tm.demowebapps.com/test.php";
 
 NSString *post=[NSString stringWithFormat:@"%@", url];
 
 AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
 manager.requestSerializer=[AFJSONRequestSerializer serializer];
 manager.responseSerializer=[AFJSONResponseSerializer serializer];
 manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
 manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
 [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
 @try
 {
 [manager GET:post
 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
 {
 NSLog(@"%@",responseObject);
 BOOL Success=[[responseObject valueForKey:@"success"] boolValue];
 if ([responseObject count] >0) {
 
 if (Success) {
 
 //                     NSDictionary *dictObj=[responseObject valueForKey:@"categories"];
 NSArray *categoryId=[responseObject valueForKey:@"id"];
 NSArray *name=[responseObject valueForKey:@"name"];
 NSArray *parentId=[responseObject valueForKey:@"parentId"];
 NSArray *image=[responseObject valueForKey:@"image"];
 NSArray *isActive=[responseObject valueForKey:@"isActive"];
 NSArray *include_in_menu=[responseObject valueForKey:@"include_in_menu"];
 
 
 
 
 
 
 
 OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
 NSMutableArray *categoryDbArray=[[NSMutableArray alloc]init];
 
 if ([testDB_Obj Delete_Categories_Data]) {
 
 for (int i=0; i<[categoryId count]; i++) {
 
 NSString *catId=[NSString stringWithFormat:@"%@",[categoryId objectAtIndex:i]];
 
 NSString *parent=[NSString stringWithFormat:@"%@",[parentId objectAtIndex:i]];
 
 if ([isActive objectAtIndex:i] == 0 || [[isActive objectAtIndex:i] isEqualToString:@"null"]) {
 
 }
 NSString *imgString=[image objectAtIndex:i];
 
 if ( (NSString *)[NSNull null] == imgString )
 {
 imgString=@" ";
 }
 else
 {
 }
 
 //                             testDB_Obj.category_id=catId;
 //                             testDB_Obj.category_parentId=parent;
 //                             testDB_Obj.category_name=[name objectAtIndex:i];
 //                             testDB_Obj.category_description=[description objectAtIndex:i];
 //                             testDB_Obj.category_image=imgString;
 //                             testDB_Obj.category_postion=pos;
 //
 //                             [categoryDbArray addObject:testDB_Obj];
 //                             [testDB_Obj insert_CategoryDetails_Data:categoryDbArray];
 
 }
 
 }
 
 //                     [self getTables];
 
 
 }
 }
 
 }
 failure:^(AFHTTPRequestOperation *operation, NSError *error)
 {
 // handle ur failure response
 if (error.code==-1009) {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:@"Please check your internet connection!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 }
 else
 {
 
 NSLog(@"%@",error.description);
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:@"Please try again later!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 }
 }];
 }
 @catch (NSException *exception)
 {
 // handle ur exception
 NSLog(@"%@",exception);
 }
 
 }
 
 
 -(void)getCategories
 {
 Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
 
 NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
 
 if (networkStatus == NotReachable)
 {
 
 
 
 /////Category Webservice
 NSLog(@"Hello Net connectiion is not pres;ent....");
 
 //        [HUD hide:YES];
 
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:@"Please check your internet connection!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 }
 else
 {
 
 dispatch_async(dispatch_get_main_queue(), ^{
 
 
 NSString *url=API_CATEGORY_DATA;
 
 
 NSString *post=[NSString stringWithFormat:@"%@", url];
 NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
 manager.responseSerializer=[AFJSONResponseSerializer serializer];
 manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
 
 NSError *error = nil;
 @try {
 dictObj = [manager syncGET:post
 
 parameters:nil
 
 operation:NULL
 
 error:&error];
 
 if (error) {
 
 //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
 //            [ProgressHUD showError:exc Interaction:NO];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:@"Please check your internet connection!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 
 }
 else
 {
 NSLog(@"%@",dictObj);
 if (dictObj.count >0) {
 
 //                     NSDictionary *dictObj=[responseObject valueForKey:@"categories"];
 NSArray *categoryId=[dictObj valueForKey:@"id"];
 NSArray *name=[dictObj valueForKey:@"name"];
 NSArray *parentId=[dictObj valueForKey:@"parentId"];
 NSArray *image=[dictObj valueForKey:@"image"];
 NSArray *isActive=[dictObj valueForKey:@"isActive"];
 NSArray *include_in_menu=[dictObj valueForKey:@"include_in_menu"];
 
 
 
 
 
 
 
 OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
 NSMutableArray *categoryDbArray=[[NSMutableArray alloc]init];
 
 if ([testDB_Obj Delete_Categories_Data]) {
 
 for (int i=0; i<[categoryId count]; i++) {
 NSString *act=[NSString stringWithFormat:@"%@",[isActive objectAtIndex:i]];
 NSString *include_in_main=[NSString stringWithFormat:@"%@",[include_in_menu objectAtIndex:i]];
 NSString *img=[NSString stringWithFormat:@"%@",[image objectAtIndex:i]];
 
 if ([act isEqualToString:@"1"] && [include_in_main isEqualToString:@"1"]) {
 
 
 NSString *parent=[NSString stringWithFormat:@"%@",[parentId objectAtIndex:i]];
 testDB_Obj.category_id=[categoryId objectAtIndex:i];
 testDB_Obj.parent_id=parent;
 testDB_Obj.category_name=[name objectAtIndex:i];
 testDB_Obj.category_include_in_menu=include_in_main;
 testDB_Obj.category_image=img;
 testDB_Obj.is_active=act;
 [categoryDbArray addObject:testDB_Obj];
 [testDB_Obj insert_CategoryDetails_Data:categoryDbArray];
 }
 }
 
 }
 
 [self getLocations];
 }
 }
 
 } @catch (NSException *exception) {
 NSLog(@"%@",exception);
 NSString *exc=[NSString stringWithFormat:@"%@",exception];
 //            [ProgressHUD showError:exc Interaction:NO];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:exc
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 
 }
 
 
 });
 
 }
 
 }
 
 */
-(void)getLocations
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        
        
        
        /////Category Webservice
        NSLog(@"Hello Net connectiion is not pres;ent....");
        
        //        [HUD hide:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                        message:@"Please check your internet connection!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *url=API_SELECT_LOCATIONS;
            
            
            NSString *post=[NSString stringWithFormat:@"%@", url];
            NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer=[AFJSONResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
            
            NSError *error = nil;
            @try {
                dictObj = [manager syncGET:post
                           
                                parameters:nil
                           
                                 operation:NULL
                           
                                     error:&error];
                
                if (error) {
                    
                    //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
                    //            [ProgressHUD showError:exc Interaction:NO];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                    message:@"Please check your internet connection!"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                    
                }
                else
                {
                    NSLog(@"%@",dictObj);
                    if (dictObj.count >0) {
                        
                        //                     NSDictionary *dictObj=[responseObject valueForKey:@"categories"];
                        BOOL success=[dictObj valueForKey:@"success"];
                        NSArray *location_list=[dictObj valueForKey:@"location_list"];
                        //TO DO Message
                        
                      
                        
                        if (success) {
                            OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
                            NSMutableArray *locationDbArray=[[NSMutableArray alloc]init];
                            BOOL del=[testDB_Obj Delete_Locations_Data];
                            if (del) {
                                
                                for (int i=0; i<[location_list count]; i++) {
                                    NSString *location=[NSString stringWithFormat:@"%@",[location_list objectAtIndex:i]];
                                    
                                    testDB_Obj.location_name=location;
                                    [locationDbArray addObject:testDB_Obj];
                                    [testDB_Obj insert_LocationsDetails_Data:locationDbArray];
                                    
                                }
                                
                            }
                        }
                        else{
                            
                            [ProgressHUD showError:@"Something went wrong.Please try agin." Interaction:NO];
                        }
                        
                        
                        [self getShippingData];
                    }
                }
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                message:exc
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
            
            
        });
        
    }
    
}

#pragma mark Shipping data

-(void)getShippingData
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        
        
        
        /////Category Webservice
        NSLog(@"Hello Net connectiion is not pres;ent....");
        
        //        [HUD hide:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                        message:@"Please check your internet connection!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *url=API_SHIPPING_METHOD;
            
            
            NSString *post=[NSString stringWithFormat:@"%@", url];
            NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer=[AFJSONResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
            
            NSError *error = nil;
            @try {
                dictObj = [manager syncGET:post
                           
                                parameters:nil
                           
                                 operation:NULL
                           
                                     error:&error];
                
                if (error) {
                    
                    //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
                    //            [ProgressHUD showError:exc Interaction:NO];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                    message:@"Please check your internet connection!"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                    
                }
                else
                {
                    NSLog(@"%@",dictObj);
                    if (dictObj.count >0) {
                        
                        //                     NSDictionary *dictObj=[responseObject valueForKey:@"categories"];
                        NSArray *value=[dictObj valueForKey:@"value"];
                        NSArray *label=[dictObj valueForKey:@"label"];
                        
                        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
                        NSMutableArray *shippingDbArray=[[NSMutableArray alloc]init];
                        
                        if ([testDB_Obj Delete_Shipping_Data]) {
                            
                            for (int i=0; i<[label count]; i++) {
                                NSString *lab=[NSString stringWithFormat:@"%@",[label objectAtIndex:i]];
                                NSString *val=[NSString stringWithFormat:@"%@",[value objectAtIndex:i]];
                                
                                testDB_Obj.shipping_label=lab;
                                testDB_Obj.shipping_value=val;
                                [shippingDbArray addObject:testDB_Obj];
                                [testDB_Obj insert_ShippingOrder:shippingDbArray];
                                
                            }
                            
                        }
                        
                        [self getAttributes];
                    }
                }
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                message:exc
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
            
            
        });
        
    }
    
}

#pragma mark Attributes
-(void)getAttributes
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        
        
        
        /////Category Webservice
        NSLog(@"Hello Net connectiion is not pres;ent....");
        
        //        [HUD hide:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                        message:@"Please check your internet connection!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *url=API_GET_ATTRIBUTES;
            
            
            NSString *post=[NSString stringWithFormat:@"%@", url];
            NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer=[AFJSONResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
            
            NSError *error = nil;
            @try {
                dictObj = [manager syncGET:post
                           
                                parameters:nil
                           
                                 operation:NULL
                           
                                     error:&error];
                
                if (error) {
                    
                    //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
                    //            [ProgressHUD showError:exc Interaction:NO];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                    message:@"Please check your internet connection!"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                    
                }else{
                    if (dictObj.count >0) {
                        
                        //NSDictionary *dictObj=[responseObject valueForKey:@"categories"];
                        NSArray *brandArray=[dictObj valueForKey:@"brand"];
                        NSArray *brandVal=[brandArray valueForKey:@"value"];
                        NSArray *brandlabel=[brandArray valueForKey:@"label"];
                        
                        NSArray *product_typeArray=[dictObj valueForKey:@"product_type"];
                        NSArray *product_typeVal=[product_typeArray valueForKey:@"value"];
                        NSArray *product_typelabel=[product_typeArray valueForKey:@"label"];
                        
                        NSArray *originArray=[dictObj valueForKey:@"origin"];
                        NSArray *originVal=[originArray valueForKey:@"value"];
                        NSArray *originlabel=[originArray valueForKey:@"label"];
                        NSString *maxLimit=[dictObj valueForKey:@"max_limit"];
                        [[NSUserDefaults standardUserDefaults] setObject:maxLimit forKey:MAXLIMIT];
                        
                        OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
                        NSMutableArray *attributeDbArray=[[NSMutableArray alloc]init];
                        
                        if ([testDB_Obj Delete_Attributes_Data]) {
                            
                            for (int i=0; i<[brandlabel count]; i++) {
                                NSString *brLab=[NSString stringWithFormat:@"%@",[brandlabel objectAtIndex:i]];
                                NSString *brVal=[NSString stringWithFormat:@"%@",[brandVal objectAtIndex:i]];
                                if(![brLab isEqualToString:@""])
                                {
                                    
                                    testDB_Obj.attribute_type=@"brand";
                                    testDB_Obj.attribute_label=brLab;
                                    testDB_Obj.attribute_value=brVal;
                                    [attributeDbArray addObject:testDB_Obj];
                                    [testDB_Obj insert_Attributes:attributeDbArray];
                                }
                            }
                            for (int i=0; i<[product_typelabel count]; i++) {
                                NSString *proLab=[NSString stringWithFormat:@"%@",[product_typelabel objectAtIndex:i]];
                                NSString *proVal=[NSString stringWithFormat:@"%@",[product_typeVal objectAtIndex:i]];
                                if(![proLab isEqualToString:@""])
                                {
                                    testDB_Obj.attribute_type=@"product_type";
                                    testDB_Obj.attribute_label=proLab;
                                    testDB_Obj.attribute_value=proVal;
                                    [attributeDbArray addObject:testDB_Obj];
                                    [testDB_Obj insert_Attributes:attributeDbArray];
                                }
                                
                            }
                            for (int i=0; i<[originlabel count]; i++) {
                                NSString *orLab=[NSString stringWithFormat:@"%@",[originlabel objectAtIndex:i]];
                                NSString *orVal=[NSString stringWithFormat:@"%@",[originVal objectAtIndex:i]];
                                if(![orLab isEqualToString:@""])
                                {
                                    testDB_Obj.attribute_type=@"origin";
                                    testDB_Obj.attribute_label=orLab;
                                    testDB_Obj.attribute_value=orVal;
                                    [attributeDbArray addObject:testDB_Obj];
                                    [testDB_Obj insert_Attributes:attributeDbArray];
                                }
                            }
                            
//                            [self getTestimonialImages];
                            
                            ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                            [self.navigationController pushViewController:vc animated:NO];
                            
                        }
                    }
                }
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                message:exc
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
        });
    }
}

#pragma mark Testimonial API
-(void)getTestimonialImages
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        
        
        
        /////Category Webservice
        NSLog(@"Hello Net connectiion is not pres;ent....");
        
        //        [HUD hide:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                        message:@"Please check your internet connection!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *url=API_TESTIMONIAL;
            
            
            NSString *post=[NSString stringWithFormat:@"%@", url];
            NSMutableDictionary *dictObj=[[NSMutableDictionary alloc]init];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer=[AFJSONResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
            
            NSError *error = nil;
            @try {
                dictObj = [manager syncGET:post
                           
                                parameters:nil
                           
                                 operation:NULL
                           
                                     error:&error];
                
                if (error) {
                    
                    //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
                    //            [ProgressHUD showError:exc Interaction:NO];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                    message:@"Please check your internet connection!"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                    
                }else{
                    
                    
                    if (dictObj.count >0) {
                        BOOL success=[dictObj valueForKey:@"success"];
                        
                        
                        NSString *valueToSave =   [dictObj valueForKey:@"message"];
                        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"startMessage"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        NSLog(@"MESSAGE : %@",valueToSave);
                        
                        if (success) {
                            NSArray *dataArray=[dictObj valueForKey:@"data"];
                            
                            NSArray *nameArray=[dataArray valueForKey:@"name"];
                            NSArray *messageArray=[dataArray valueForKey:@"Message"];
                            NSArray *imageArray=[dataArray valueForKey:@"Image"];
                            
                            
                            
                            
                            NSString *isHelpShowed=[[NSUserDefaults standardUserDefaults] valueForKey:HELPSESSION];
                            if ([isHelpShowed isEqualToString:@"YES"]) {
                                TestimonialController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TestimonialController"];
                                vc.nameArray=nameArray;
                                vc.imageArray=imageArray;
                                vc.messageArray=messageArray;
                                [self.navigationController pushViewController:vc animated:NO];
                                
                            }
                            else{
                                HelpController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"HelpController"];
                                vc.nameArray=nameArray;
                                vc.imageArray=imageArray;
                                vc.messageArray=messageArray;
                                vc.comingFrom=@"Landing";
                                [self.navigationController pushViewController:vc animated:NO];
                            
                            }
                        }
                    }
                }
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                message:exc
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
        });

    }
}




- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
//        // do something here...
//
//        UIApplication *app = [UIApplication sharedApplication];
//        [app performSelector:@selector(suspend)];
//
//        //wait 2 seconds while app is going background
//        [NSThread sleepForTimeInterval:2.0];
//
//        //exit app when app is in background
//        exit(0);
//
    }
}


/*
 -(void)DemoApi
 {
 Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
 
 NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
 
 if (networkStatus == NotReachable)
 {
 
 
 
 /////Category Webservice
 NSLog(@"Hello Net connectiion is not pres;ent....");
 
 //        [HUD hide:YES];
 
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:@"Please check your internet connection!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 }
 else
 {
 
 dispatch_async(dispatch_get_main_queue(), ^{
 
 
 NSString *url=@"http://secure3.convio.net/akdn/site/CRConsAPI";
 
 
 //            NSString *post=[NSString stringWithFormat:@"%@", url];
 
 
 NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
 NSMutableDictionary *paraObj=[NSMutableDictionary dictionary];
 [paraObj setObject:@"wDB09SQODRpVIOuX" forKey:@"api_key"];
 [paraObj setObject:@"login" forKey:@"method"];
 [paraObj setObject:@"12345" forKey:@"password"];
 [paraObj setObject:@"json" forKey:@"response_format"];
 [paraObj setObject:@"test@test.com" forKey:@"user_name"];
 [paraObj setObject:@"1.0" forKey:@"v"];
 
 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
 manager.responseSerializer=[AFJSONResponseSerializer serializer];
 manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
 manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
 [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
 
 
 NSError *error = nil;
 @try {
 dictObj = [manager syncPOST:url parameters:paraObj operation:NULL error:&error];
 
 if (error) {
 
 //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
 //            [ProgressHUD showError:exc Interaction:NO];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:@"Please check your internet connection!"
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 
 } 
 else
 {
 NSLog(@"%@",dictObj);
 if (dictObj.count >0) {
 
 
 
 
 
 
 }
 }
 
 } @catch (NSException *exception) {
 NSLog(@"%@",exception);
 NSString *exc=[NSString stringWithFormat:@"%@",exception];
 //            [ProgressHUD showError:exc Interaction:NO];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
 message:exc
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 
 
 }
 
 
 });
 
 }
 
 }
 
 -(void)demo2Api{
 
 
 NSMutableArray *questiondetailArray=[[NSMutableArray alloc]init];
 
 
 NSString *bodyData = [NSString  stringWithFormat:@"{'api_key':'wDB09SQODRpVIOuX','method':'login','password':'12345','response_format':'json','user_name':'test@test.com','v':'1.0'}"];
 [questiondetailArray addObject:bodyData];
 
 
 NSString *str=[questiondetailArray componentsJoinedByString:@","];
 NSMutableArray *array=[[NSMutableArray alloc]init];
 [array addObject:str];
 str=[NSString stringWithFormat:@"[%@]",[array objectAtIndex:0]];
 //    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
 
 NSString *url=@"http://secure3.convio.net/akdn/site/CRConsAPI";
 
 NSData *httpBody = [NSData dataWithBytes:[str UTF8String] length:[str length]];
 NSDictionary *dictObj = [_connectReq createRequest:url body:httpBody methodType:@"POST" headers:nil synchronous:YES useCache:NO];
 
 NSString *result=[[dictObj objectForKey:@"Result"] stringValue];
 NSString *responce=[dictObj objectForKey:@"Response"];
 NSLog(@"%@ and %@",result,responce);
 
 if ([result isEqualToString:@"0"])
 {
 }
 
 
 }
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
