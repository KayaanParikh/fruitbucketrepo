//
//  FilterController.m
//  TSMC
//
//  Created by Rahul Lekurwale on 09/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "FilterController.h"
#import "CustomTableViewCell.h"
#import "OfflineDb.h"
#import "CartController.h"
#import "AppConstant.h"
#import "ItemsViewController.h"
#import "UIButton+Badge.h"


@interface FilterController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *brandListArr,*brandIdArr;
    long selectedIndex;
    OfflineDb *object;
    CERangeSlider* slider4;
}
@end

@implementation FilterController
@synthesize categoryId,subCategoryId;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    NSArray *str=[object fetch_CartDetails];
    NSLog(@"%@",str);
    NSString *cartCount=[NSString stringWithFormat:@"%lu",(unsigned long)[str count]];
    if ([cartCount isEqualToString:@"0"]|| [cartCount isEqualToString:@"(null)"] ) {
        self.navigationItem.rightBarButtonItem.badge.hidden = YES;
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"BADGECOUNT"];
        
    }else{
        self.navigationItem.rightBarButtonItem.badge.hidden = NO;
        [[NSUserDefaults standardUserDefaults] setObject:cartCount forKey:@"BADGECOUNT"];
//        _lbl_badgeCount.text=cartCount;
//        _lbl_badgeCount.layer.masksToBounds=YES;
//        _lbl_badgeCount.layer.cornerRadius=_lbl_badgeCount.frame.size.width/2;
        UIImage *image = [UIImage imageNamed:@"CartNew"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0,0,image.size.width, image.size.height);
            [button addTarget:self action:@selector(cartMethod:) forControlEvents:UIControlEventTouchDown];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = navLeftButton;
        self.navigationItem.rightBarButtonItem.badgeValue = cartCount;

    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _brandTableView.backgroundColor = [UIColor whiteColor];
    brandListArr=[[NSMutableArray alloc] init];
    brandIdArr=[[NSMutableArray alloc] init];
    object=[[OfflineDb alloc] init];
//    _titleBack.backgroundColor=TopBarColor;
    self.title = @"Filter".uppercaseString;
//    _lbl_titleText.font=TopBarFont;
//    _lbl_titleText.textColor=TopBarTextColor;
//    _lbl_badgeCount.textColor=cartIconTextColor;
//    _lbl_badgeCount.backgroundColor=cartIconBackColor;
    
    NSUInteger margin = 20;

    CGRect     sliderFrame = CGRectMake(margin, margin + 60, self.view.frame.size.width - margin * 2, 30);
    slider4= [[CERangeSlider alloc] initWithFrame:sliderFrame];

    NSString *minn=[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMINPRICE];
    NSString *maxn=[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMAXPRICE ];

    if ([[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMINPRICE] ==nil || minn.length <=0 || [[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMAXPRICE] == nil || maxn.length <=0)
    {
        [self getRangeFilterWithMinVal:[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] AndMaxValue:[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE]];

    }else{
        [self getRangeFilterWithMinVal:[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMINPRICE] AndMaxValue:[[NSUserDefaults standardUserDefaults] valueForKey:SELECTEDMAXPRICE]];

    }

    [self getBrandOfflineList];
    
}

- (void)slideValueChanged:(id)control{
    
    CERangeSlider* slider = (CERangeSlider*)control;
    NSLog(@"Slider value changed: (%.2f,%.2f)",
          slider.lowerValue, slider.upperValue);
    _lbl_min.text=[NSString stringWithFormat:@"%d",(int)slider.lowerValue];
    _lbl_max.text=[NSString stringWithFormat:@"%d",(int)slider.upperValue];
    _lbl_min.text=[NSString stringWithFormat:@"%d - %d",(int)slider.lowerValue,(int)slider.upperValue] ;
    
}

-(void)getRangeFilterWithMinVal:(NSString *)minVal AndMaxValue:(NSString *)maxVal{

    int min=[minVal intValue];
    int max=[maxVal intValue];

    _lbl_minConstant.text=[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] ;
    _lbl_maxConstant.text=[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE];

    _lbl_min.text=[NSString stringWithFormat:@"%@ - %@",minVal,maxVal] ;
    
    slider4.maximumValue = [[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE] intValue];
    slider4.minimumValue = [[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] intValue];
    slider4.upperValue = max;
    slider4.lowerValue =min;
    slider4.curvatiousness = 1;
    slider4.trackHighlightColour = ButtonBlueColor;
    slider4.knobColour =  [UIColor redColor];
    [self.rangeBackView addSubview:slider4];
    
    //    [self.rangeBackView addSubview:slider3];
    
    
    [slider4 addTarget:self
                action:@selector(slideValueChanged:)
      forControlEvents:UIControlEventValueChanged];

}

-(void)getBrandOfflineList{

    NSArray *brandData=[object fetch_AttributeValue:@"brand"];
    brandListArr=[brandData valueForKey:@"attribute_label"];
    brandIdArr=[brandData valueForKey:@"attribute_value"];
    APP_DELEGATE.selectBarndArray=[[NSMutableArray alloc] init];
    [APP_DELEGATE.selectBarndArray addObjectsFromArray:brandIdArr];

    NSString *brandVal=[[NSUserDefaults standardUserDefaults] valueForKey:BRANDVALUE];
    if (brandVal ==nil ||  brandVal.length <=0 || [brandVal isEqualToString:@"0"]) {
        selectedIndex=-1;

    }else{
//        APP_DELEGATE.selectBarndArray=[[NSMutableArray alloc] init];

        
        for(int i=0;i<[brandIdArr count];i++){
            if ([brandVal isEqualToString:[brandIdArr objectAtIndex:i]]) {
                selectedIndex=i;
                [APP_DELEGATE.selectBarndArray replaceObjectAtIndex:i withObject:[brandIdArr objectAtIndex:i]];
            }else{
                [APP_DELEGATE.selectBarndArray replaceObjectAtIndex:i withObject:@"0"];

            }
        }
    }
    [_brandTableView reloadData];
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (brandIdArr.count >0)
    {
        // return [[[NSUserDefaults standardUserDefaults]objectForKey:@"SELECTED_ADDRESS"] count];
        return [brandIdArr count];
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"CustomTableViewCell";
    CustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    //UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CELLID"];
    if (cell==nil)
    {
        cell=[[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    cell.addressLbl.text=[NSString stringWithFormat:@"%@",[brandListArr objectAtIndex:indexPath.row]];
    
    cell.selectedAddresChkBox.tag=indexPath.row;
//    [cell.selectedAddresChkBox addTarget:self action:@selector(chkBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(selectedIndex == indexPath.row)
    {
        [cell.selectedAddresChkBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];

    }else{
        [cell.selectedAddresChkBox setImage:[UIImage imageNamed:@"square.png"] forState:UIControlStateNormal];

    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedIndex=indexPath.row;
    [_brandTableView reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cartMethod:(id)sender
{
    CartController *viewController =[self.storyboard instantiateViewControllerWithIdentifier:@"CartController"];
    viewController.comingFrom=@"FilterVC";
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)backMethod:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)resetMethod:(id)sender {
    selectedIndex=-1;
    [self getRangeFilterWithMinVal:[[NSUserDefaults standardUserDefaults] valueForKey:MINPRICE] AndMaxValue:[[NSUserDefaults standardUserDefaults] valueForKey:MAXPRICE]];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:SELECTEDMAXPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:BRANDVALUE];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ISFILTER];

    [self getBrandOfflineList];
  
}

- (IBAction)filterMethod:(id)sender {

    NSArray *arr=[_lbl_min.text componentsSeparatedByString:@" - "];
    NSString *selMin=[arr objectAtIndex:0];
    NSString *selMax=[arr objectAtIndex:1];
    [[NSUserDefaults standardUserDefaults] setObject:selMin forKey:SELECTEDMINPRICE];
    [[NSUserDefaults standardUserDefaults] setObject:selMax forKey:SELECTEDMAXPRICE];

    
    if (selectedIndex == -1) {
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:BRANDVALUE];

    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[brandIdArr objectAtIndex:selectedIndex] forKey:BRANDVALUE];
        for (int i=0; i<[brandIdArr count]; i++) {
            if (selectedIndex ==i) {
                [APP_DELEGATE.selectBarndArray replaceObjectAtIndex:i withObject:[brandIdArr objectAtIndex:selectedIndex]];
            }
            else{
                
                [APP_DELEGATE.selectBarndArray replaceObjectAtIndex:i withObject:@"0"];
                
            }
        }
    }

    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:ISFILTER];
    
    [self.navigationController popViewControllerAnimated:YES];
//    ItemsViewController *itms=[self.storyboard instantiateViewControllerWithIdentifier:@"ItemsViewController"];
//    itms.fetchedId=categoryId;
//    [self.navigationController pushViewController:itms animated:YES];
}

@end
