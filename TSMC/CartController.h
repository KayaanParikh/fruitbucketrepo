//
//  CartController.h
//  EarlOfHindh
//
//  Created by Rahul Lekurwale on 18/02/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "DYAlertPickView.h"

@interface CartController : UIViewController<UITextFieldDelegate,DYAlertPickViewDataSource, DYAlertPickViewDelegate>
{
    MBProgressHUD *HUD;
    int minimumValueOfSelectedLocation,totalValue;

}
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleText;
@property (weak, nonatomic) IBOutlet UIView *cartBottomView;
@property (weak, nonatomic) IBOutlet UILabel *titleBack;
@property (weak, nonatomic) IBOutlet UIButton *btn_Empty;
@property (weak, nonatomic) IBOutlet UIButton *btnRedeem;
- (IBAction)emptyClocheMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_total;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu;
- (IBAction)menuMethod:(id)sender;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_titleText;
@property (weak, nonatomic) IBOutlet UITableView *cartTableView;
@property (weak, nonatomic) IBOutlet UIButton *btn_Back;
- (IBAction)backMethod:(id)sender;

- (IBAction)continueMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_continue;
@property (strong,nonatomic)UIPickerView *spicesPicker;
@property (weak, nonatomic) IBOutlet UITextField *txt_couponCode;
@property (weak, nonatomic) IBOutlet UIButton *btn_apply;
- (IBAction)applyMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_noOfItems;

///
@property (weak, nonatomic) IBOutlet UIView *shieldView;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_popTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_popTitleName;
@property (weak, nonatomic) IBOutlet UIButton *btn_popClose;
- (IBAction)closeMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_popMinus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_popQty;
@property (weak, nonatomic) IBOutlet UIButton *btn_popPlus;
- (IBAction)subMethod:(id)sender;
- (IBAction)addMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_popRemove;
@property (weak, nonatomic) IBOutlet UIButton *btn_popUpdate;
- (IBAction)removeMethod:(id)sender;
- (IBAction)updateMethod:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *min_Order_Suggestion_Lbl;

@property (weak, nonatomic) IBOutlet UIView *attrPopView;

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleName;
@property (weak, nonatomic) IBOutlet UIButton *btn_close;
@property (weak, nonatomic) IBOutlet UIButton *btn_minus;
@property (weak, nonatomic) IBOutlet UIButton *btn_plus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Qty;
@property (weak, nonatomic) IBOutlet UIButton *btn_remove;
@property (weak, nonatomic) IBOutlet UIButton *btn_update;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cut;
@property (weak, nonatomic) IBOutlet UIButton *btn_cut;
@property (weak, nonatomic) IBOutlet UILabel *lbl_weightRange;
@property (weak, nonatomic) IBOutlet UIButton *btn_weightRange;
@property (weak, nonatomic) IBOutlet UILabel *lbl_type;
@property (weak, nonatomic) IBOutlet UIButton *btn_type;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_cut_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_cut_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_WeightRange_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_weightRange_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_type_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_type_height;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_cut_height1;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_cut_height1;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_WeightRange_height1;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_weightRange_height1;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_type_height1;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_type_height1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pop_view_height;

- (IBAction)cutMethod:(id)sender;
- (IBAction)weightMethod:(id)sender;
- (IBAction)typeMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_couponName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_couponVal;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_subTotHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_subTotValHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_cupNameheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_copValHeight;


@property(nonatomic)NSString *comingFromItem,*comingFrom;


@end
