//
//  SignUpViewController.h
//  TSMC
//
//  Created by user on 01/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UILabel *_titleBack;
    IBOutlet UILabel *_titleText;
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UITextField *EmailTF;
    IBOutlet UITextField *txtMobileNumber;
    IBOutlet UITextField *passwordTF;
    IBOutlet UITextField *confirmPassTF;
    IBOutlet UITextField *FirstNameTF;
    IBOutlet UITextField *LastNameTF;
    
     MBProgressHUD *HUD;
    
    NSUserDefaults *defaults;
}
- (IBAction)BackBtn_Clicked:(id)sender;
- (IBAction)CancelBtn_Clicked:(id)sender;
- (IBAction)SignUpBtn_Clicked:(id)sender;

- (IBAction)EmailTFDidEndEditing:(id)sender;
- (IBAction)PasswordTFDidEndEditing:(id)sender;
- (IBAction)confirmPasswordTFDidEndEditing:(id)sender;
- (IBAction)FirstNameDidEndEditing:(id)sender;
- (IBAction)LastNameTFDidEndEditing:(id)sender;
@property (nonatomic) NSString *comingFrom,*goBackTo;

@end
