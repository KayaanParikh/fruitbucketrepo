//
//  AppDelegate.m
//  TSMC
//
//  Created by Rahul Lekurwale on 21/06/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "AppDelegate.h"
#import <sqlite3.h>
#import "OfflineDb.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"
#import "ItemsViewController.h"
#import "ItemDetailsController.h"
#import "ViewController.h"
#import "SWRevealViewController.h"

#import "SidebarViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@import Firebase;
@import FirebaseMessaging;
@import FirebaseInstanceID;
@import UserNotifications;

@interface AppDelegate ()<FIRMessagingDelegate,UNUserNotificationCenterDelegate>
{
    NSString *msgId,*type;
    
    
}
@end
static sqlite3 *database;

@implementation AppDelegate
@synthesize databasePath,cartToItem,selectBarndArray;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    selectBarndArray=[[NSMutableArray alloc] init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GoogleService-Info" ofType:@"plist"];
    FIROptions *options = [[FIROptions alloc] initWithContentsOfFile:filePath];
    [FIRApp configureWithOptions:options];
    
    
    /*Facebook Code*/
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    NSLog(@"### Running FB SDK Version: %@", [FBSDKSettings sdkVersion]);
    
    [self createdb];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:UNAuthorizationOptionAlert completionHandler:^(BOOL granted, NSError * _Nullable error) {
        
        if (granted) {
            NSLog(@"Granted");
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            
        } else {
            NSLog(@"Error registering: %@", error);
        }
        
    }];
    
    
    
    
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
    } else
    {
        // iOS 8 or later
        // [END_EXCLUDE]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
    // here we configure firebase for Push notification
    
    //    [FIRApp configure];
    
    // Add observer for InstanceID token refresh callback.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                        message:@"Please check your internet connection!"
                                                       delegate:self
                                              cancelButtonTitle:@"Close App"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        NSString *url=API_CATEGORY_DATA;
        
        NSString *post=[NSString stringWithFormat:@"%@", url];
        NSMutableDictionary *dictObj=[NSMutableDictionary dictionary];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer=[AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"video/mpeg", nil];
        
        NSError *error = nil;
        @try {
            dictObj = [manager syncGET:post
                       
                            parameters:nil
                       
                             operation:NULL
                       
                                 error:&error];
            
            if (error) {
                
                //                [ProgressHUD showError:@"Please check your internet connection!" Interaction:NO];
                //            [ProgressHUD showError:exc Interaction:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                                message:@"Please check your internet connection!"
                                                               delegate:self
                                                      cancelButtonTitle:@"Close App"
                                                      otherButtonTitles:nil];
                [alert show];
                
                
            }
            else
            {
                NSLog(@"%@",dictObj);
                if (dictObj.count >0) {
                    
                    //                     NSDictionary *dictObj=[responseObject valueForKey:@"categories"];
                    NSArray *categoryId=[dictObj valueForKey:@"id"];
                    NSArray *name=[dictObj valueForKey:@"name"];
                    NSArray *parentId=[dictObj valueForKey:@"parentId"];
                    NSArray *image=[dictObj valueForKey:@"image"];
                    NSArray *isActive=[dictObj valueForKey:@"isActive"];
                    NSArray *include_in_menu=[dictObj valueForKey:@"include_in_menu"];
                       
                    OfflineDb *testDB_Obj=[[OfflineDb alloc]init];
                    NSMutableArray *categoryDbArray=[[NSMutableArray alloc]init];
                    
                    if ([testDB_Obj Delete_Categories_Data]) {
                        
                        for (int i=0; i<[categoryId count]; i++) {
                            NSString *act=[NSString stringWithFormat:@"%@",[isActive objectAtIndex:i]];
                            NSString *include_in_main=[NSString stringWithFormat:@"%@",[include_in_menu objectAtIndex:i]];
                            NSString *img=[NSString stringWithFormat:@"%@",[image objectAtIndex:i]];
                            
                            if ([act isEqualToString:@"1"] && [include_in_main isEqualToString:@"1"]) {
                                
                                
                                NSString *parent=[NSString stringWithFormat:@"%@",[parentId objectAtIndex:i]];
                                testDB_Obj.category_id=[categoryId objectAtIndex:i];
                                testDB_Obj.parent_id=parent;
                                testDB_Obj.category_name=[name objectAtIndex:i];
                                testDB_Obj.category_include_in_menu=include_in_main;
                                testDB_Obj.category_image=img;
                                testDB_Obj.is_active=act;
                                [categoryDbArray addObject:testDB_Obj];
                                [testDB_Obj insert_CategoryDetails_Data:categoryDbArray];
                            }
                        }
                        
                    }
                    
                }
            }
            
        } @catch (NSException *exception) {
            NSLog(@"%@",exception);
            NSString *exc=[NSString stringWithFormat:@"%@",exception];
            //            [ProgressHUD showError:exc Interaction:NO];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost"
                                                            message:exc
                                                           delegate:self
                                                  cancelButtonTitle:@"Close App"
                                                  otherButtonTitles:nil];
            [alert show];
            
            
        }
    }
    
    if (launchOptions != nil)
    {
        //opened from a push notification when the app is closed
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo != nil)
        {
            NSString *notifType=userInfo[@"gcm.notification.notification_type"];
            NSString *notId=userInfo[@"gcm.notification.id"];
            
            msgId=notId;
            type=notifType;
            
            if (msgId !=nil) {
                [self openViewController:type AndId:msgId];
            }
            
            
            NSLog(@"userInfo->%@",[userInfo objectForKey:@"aps"]);
            //write you push handle code here
            
        }
        
    }

    return YES;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {
        // do something here...
        
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:2.0];
        
        //exit app when app is in background
        exit(0);
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
    NSLog(@"applicationDidBecomeActive");
    application.applicationIconBadgeNumber = 0;
    
    //    if (msgId !=nil) {
    //        [self openViewController:type AndId:msgId];
    //    }
    
    // jst for now we will hide this and we call our code directly instade of push .. hope u gpt this..
    [self connectToFcm];// wht hpn ?push zala ek min mi push var run karto ok kr
    
    
    [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)createdb
{
    NSString *docsDir;
    
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = dirPaths[0];
    
    // Build the path to the database file/var/mobile/Containers/Data/Application/91A280FB-8116-432E-9622-08CB029041CE/Documents/
    
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    databasePath=[NSString stringWithFormat:@"%@/TSMC.sqlite",[dir lastObject]];
    
    NSLog(@"%@",databasePath);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:databasePath forKey:@"databasePath"];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    
    
    const NSString *category=@"CREATE TABLE category(category_id INT PRIMARY KEY,parent_id text,category_name text,category_include_in_menu text,category_image text,is_active text)";
    
    const NSString *LocationTable=@"CREATE TABLE LocationTable(location_id INTEGER PRIMARY KEY   AUTOINCREMENT,location_name text)";
    
    const NSString *ShippingMethods=@"CREATE TABLE ShippingMethods(shipping_id INTEGER PRIMARY KEY   AUTOINCREMENT,value text,label text)";
    
    const NSString *Attributes=@"CREATE TABLE Attributes(attribute_id INTEGER PRIMARY KEY   AUTOINCREMENT,type text,value text,label text)";
    
    const NSString *cartDetails=@"CREATE TABLE cartDetails(cid INTEGER PRIMARY KEY   AUTOINCREMENT,cart_id text,category_id text,name text,image_url text,image_thumb text, image_small text,image_main text,price text,qty text,description text,short_desc text,food_type text,total text,brand text,weight text,origin text,option_id text,cut_option_id text,cut_option_value text,cut_option_price text,weight_option_id text,weight_option_value text,weight_option_price text,type_option_id text,type_option_value text,type_option_price text,cut_option_label text,weight_option_label text,type_option_label text,all_cut_values text,all_cut_labels text,all_cut_prices text,all_weight_values text,all_weight_labels text,all_weight_prices text,all_type_values text,all_type_labels text,all_type_prices text)";
    
    //    const NSString *cartMaster=@"CREATE TABLE cartMaster(cmid INTEGER PRIMARY KEY   AUTOINCREMENT,cart_id text,category_id text,all_cut_values text,all_cut_labels text,all_cut_prices text,all_weight_values text,all_weight_labels text,all_weight_prices text,all_type_values text,all_type_labels text,all_type_prices text)";
    //
    //    const NSString *ShippingOrder=@"CREATE TABLE shippingOrder(label text,value text)";
    
    const char *categoryTable=[category UTF8String];
    const char *Loc=[LocationTable UTF8String];
    const char *ShippingMethodsTable=[ShippingMethods UTF8String];
    const char *AttributesTable=[Attributes UTF8String];
    const char *cartDetailsTable=[cartDetails UTF8String];

    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            
            sqlite3_exec(database, categoryTable, NULL, NULL, NULL);
            sqlite3_exec(database, Loc, NULL, NULL, NULL);
            sqlite3_exec(database, ShippingMethodsTable, NULL, NULL, NULL);
            sqlite3_exec(database, AttributesTable, NULL, NULL, NULL);
            sqlite3_exec(database, cartDetailsTable, NULL, NULL, NULL);
            //            sqlite3_exec(database, cartMasterTable, NULL, NULL, NULL);
            
            
            //            sqlite3_exec(database, ShippingOrderTable, NULL, NULL, NULL);
            
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Failed to open/create database");
        }
    }
}

#pragma mark FBSDK_STUFF
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}



#pragma Mark FCM_STUFF


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Print full message.
    NSLog(@"%@", userInfo);
    NSString *notifType=userInfo[@"gcm.notification.notification_type"];
    NSString *Desc=userInfo[@"gcm.notification.Description"];
    NSString *image=userInfo[@"gcm.notification.image"];
    NSString *notId=userInfo[@"gcm.notification.id"];
    
    NSString *notTitle=userInfo[@"Title"];
    //    NSString *notImage=userInfo[@"image"];
    msgId=notId;
    type=notifType;
    
    //    if (application.applicationState == UIApplicationStateBackground) {
    //
    //        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    //
    //        NSLog(@"Background Mode");
    //        NSString *title = userInfo[@"title"];
    //
    //        //        [self openViewController:notifType AndId:notId];
    //
    //    }
    
    
    
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    
    // Print message ID.
    
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    
    //    final String type = data.get("notification_type");
    //    final String description = data.get("Description");
    //    final String dataId = data.get("id");
    //    final String title = data.get("Title");
    //    final String image = data.get("image");
    //    {
    //        aps =     {
    //            alert =         {
    //                body = ashishi;
    //                title = Test;
    //            };
    //            "content-available" = 1;
    //        };
    //        "gcm.message_id" = "0:1505208654970840%ac8cb35dac8cb35d";
    //        "gcm.notification.Description" = hiiiiiii;
    //        "gcm.notification.alert" = TSMC;
    //        "gcm.notification.id" = 3;
    //        "gcm.notification.image" = "http://seafoodandmeatco.in/media/notifications/image/2/3/23845793844_58839e0654_o.png";
    //        "gcm.notification.notification_type" = Category;
    //    }
    
    NSString *notifType=userInfo[@"gcm.notification.notification_type"];
    NSString *Desc=userInfo[@"gcm.notification.Description"];
    NSString *image=userInfo[@"gcm.notification.image"];
    NSString *notId=userInfo[@"gcm.notification.id"];
    
    NSString *notTitle=userInfo[@"Title"];
    //    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:10 repeats:NO];
    //    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Reminder 1"
    //                                                                          content:notificationContent
    //                                                                          trigger:trigger];
    
    
    //    NSString *notImage=userInfo[@"image"];
    msgId=notId;
    type=notifType;
    
    //    if (application.applicationState == UIApplicationStateBackground) {
    //
    //        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    //
    //        NSLog(@"Background Mode");
    //        NSString *title = userInfo[@"title"];
    //
    ////        [self openViewController:notifType AndId:notId];
    //
    //    }else if(application.applicationState == UIApplicationStateActive){
    //
    //        NSLog(@"Active Mode");
    //
    //    }
    //    if(application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground){
    //
    //        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
    //
    //        [self openViewController:notifType AndId:notId];
    //
    //    }
    
    
    
    
    
    //////    NSArray *content=[message componentsSeparatedByString:@"-"];
    //////    NSString *userID;
    //////    NSString *msg=[content objectAtIndex:0];
    //////    NSString *msg_date=[content objectAtIndex:1];
    ////
    ////
    ////    if (content.count>2)
    ////    {
    ////        userID=[content objectAtIndex:2];
    ////
    ////    }
    //    NSInteger badgeNo=[[content objectAtIndex:3] integerValue];
    
    //    [UIApplication sharedApplication].applicationIconBadgeNumber =badgeNo;
    
    //    if (application.applicationState == UIApplicationStateActive) {
    //        // Nothing to do if applicationState is Inactive, the iOS already displayed an alert view.
    //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Did receive a Remote Notification" message:[NSString stringWithFormat:@"Your App name received this notification while it was running:\n%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //        [alertView show];
    //    }
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else
    {
        NSLog( @"FOREGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    
}



- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", [remoteMessage appData]);
    NSString *title=[[remoteMessage appData] valueForKey:@"Title"];
    NSString *Description=[[remoteMessage appData] valueForKey:@"Description"];
    
    
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
    //                                                    message:Description
    //                                                   delegate:self
    //                                          cancelButtonTitle:@"OK"
    //                                          otherButtonTitles:nil];
    //    [alert show];
    
}

-(void)application: (UIApplication *)application didRegisterUserNotificationSettings:(nonnull UIUserNotificationSettings *)notificationSettings
{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        
        UNAuthorizationOptions authOptions =
        
        UNAuthorizationOptionAlert
        
        | UNAuthorizationOptionSound
        
        | UNAuthorizationOptionBadge;
        
        [[UNUserNotificationCenter currentNotificationCenter]
         
         requestAuthorizationWithOptions:authOptions
         
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             
         }
         
         ];
        
        
        
        // For iOS 10 display notification (sent via APNS)
        
        [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
        
        // For iOS 10 data message (sent via FCM)
        
        [[FIRMessaging messaging] setRemoteMessageDelegate:self];
        
#endif
        // iOS 10 or later
        //#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        //        UNAuthorizationOptions authOptions =
        //        UNAuthorizationOptionAlert
        //        | UNAuthorizationOptionSound
        //        | UNAuthorizationOptionBadge;
        //        [[UNUserNotificationCenter currentNotificationCenter]
        //         requestAuthorizationWithOptions:authOptions
        //         completionHandler:^(BOOL granted, NSError * _Nullable error) {
        //         }
        //         ];
        //
        //        // For iOS 10 display notification (sent via APNS)
        //        [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
        // For iOS 10 data message (sent via FCM)
        
        [[FIRMessaging messaging] setRemoteMessageDelegate:self];
        //#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSString *deviceToken1 = [[[[deviceToken description]
                                stringByReplacingOccurrencesOfString:@"<"withString:@""]
                               stringByReplacingOccurrencesOfString:@">" withString:@""]
                              stringByReplacingOccurrencesOfString: @" " withString: @""];
    //    NSLog(@"%@", deviceToken1);
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                        type:FIRInstanceIDAPNSTokenTypeSandbox];
    
    // fPrO2JTxeVo:APA91bEKqTYoa2Z3k1QB_SMZtYTwVWCKaufGi8anq-MKKR2iSw8hWlIb_QGqMmiiBJTYeNx4OrTqunGtu6p_5RUZNSoNam3nRcisi_ZyHDNObs7NYOi5l3e_hcWt41GUUfDPEnoSY37y
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"content- %@", refreshedToken);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:refreshedToken forKey:@"TOKEN"];
    
    if (refreshedToken!=nil) {
        NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
        if ([isLog isEqualToString:@"YES"]) {
            HelperClass *helpObj=[[HelperClass alloc] init];
            [helpObj addFCMTokenToServer];
        }
        
    }
    
    
    //    NSLog(@"%@",deviceToken);
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"Error %@",str);
    NSLog(@"Error %@",err);
    
}

- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}


//-(void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
//
//    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
//
//    // Pring full message.
//
//    NSLog(@"%@", userInfo);
//}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:refreshedToken forKey:@"TOKEN"];
    if (refreshedToken!=nil) {
        NSString *isLog=[[NSUserDefaults standardUserDefaults] valueForKey:ISLOGIN];
        if ([isLog isEqualToString:@"YES"]) {
            HelperClass *helpObj=[[HelperClass alloc] init];
            [helpObj addFCMTokenToServer];
        }
        
    }
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to appliation server.
}



////////////////////////////////
/*
 - (void)openViewController:(NSString *)notificationTitle{
 
 NSDictionary * userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"loginUser"];
 
 if([notificationTitle isEqualToString:@"Exercise"]){
 
 UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
 
 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
 
 Excercise *excerciseController = (Excercise*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Excercise"];
 [navigationController pushViewController:excerciseController animated:YES];
 //[navigationController presentViewController:excerciseController animated:YES completion:nil];
 
 }else if([notificationTitle isEqualToString:@"Weight"]){
 
 
 UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
 
 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
 
 Weight *weightController = (Weight*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Weight"];
 [navigationController pushViewController:weightController animated:YES];
 
 //[navigationController presentViewController:weightController animated:YES completion:nil];
 
 }else if([notificationTitle isEqualToString:@"MCQ"]){
 
 
 UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
 
 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
 
 QuestionOfTheDay *questionController = (QuestionOfTheDay*)[mainStoryboard instantiateViewControllerWithIdentifier: @"QuestionOfTheDay"];
 questionController.self.dictUser = userDict;
 [navigationController pushViewController:questionController animated:YES]
 }
 
 }
 
 -(void)sendAckRequest:(NSString *)alertID {
 
 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
 manager.requestSerializer = [AFHTTPRequestSerializer serializer];
 manager.responseSerializer = [AFHTTPResponseSerializer serializer];
 
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 
 NSString *userID =[[defaults objectForKey:@"loginUser"]objectForKey:@"UserId"];
 NSString *serverRegistrationID = [defaults objectForKey:@"server_registration_id"];
 
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
 
 [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
 NSString *currentDateString = [dateFormatter stringFromDate:[NSDate date]];
 
 //    NSDate *currentDate = [dateFormatter dateFromString:currentDateString];
 
 //NSDictionary *parameter = @{@"ReminderDateTime":currentDateString};
 
 NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@/?ReminderDateTime=%@",sendNotificationAck,userID,serverRegistrationID,alertID,currentDateString];
 
 
 NSLog(@"url: %@",url);
 
 [manager GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
 
 if(operation.response.statusCode == 200)
 NSLog(@"Notification Acknowledged");
 else
 NSLog(@"Notification failed to acknowledge");
 
 } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
 
 NSLog(@"error: %@",[error localizedDescription]);
 
 }];
 
 }
 
 - (NSString *)stringWithDeviceToken:(NSData *)deviceToken {
 
 const char *data = [deviceToken bytes];
 
 NSMutableString *token = [NSMutableString string];
 
 for (int i = 0; i < [deviceToken length]; i++) {
 
 [token appendFormat:@"%02.2hhX", data[i]];
 }
 
 return token;
 }
 */

//- (void)applicationDidBecomeActive:(UIApplication *)application {
//    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//
//    if([UIApplication sharedApplication].applicationIconBadgeNumber!=0)
//        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber - 1;
//
//}
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#pragma Mark FOREGROUND_DISPLAY
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo]; simula.. push yetat ka re ? device wr ch run kr...push yetoa
    
    // Print message ID.
    if (userInfo[@"gcm.message_id"]) {
        NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionAlert);
}

#pragma Mark FOREGROUND_CLICK_ACTION
// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[@"gcm.message_id"]) {
        NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
        
        
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    NSString *notifType=userInfo[@"gcm.notification.notification_type"];
    NSString *Desc=userInfo[@"gcm.notification.Description"];
    NSString *image=userInfo[@"gcm.notification.image"];
    NSString *notId=userInfo[@"gcm.notification.id"];
    [self openViewController:notifType AndId:notId];
    
    
    completionHandler();
}
#endif


//- (void)userNotificationCenter:(UNUserNotificationCenter* )center willPresentNotification:(UNNotification* )notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
//    NSLog( @"Here handle push notification in foreground");
//    //For notification Banner - when app in foreground
//
//    completionHandler(UNNotificationPresentationOptionAlert);
////    isActive=@"YES";
//    // Print Notification info
//    NSLog(@"Userinfo %@",notification.request.content.userInfo);
//}




- (void)openViewController:(NSString *)notificationTitle AndId:(NSString *)catId{
    
    NSDictionary * userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"loginUser"];
    
    
    // category_name
    OfflineDb *object=[[OfflineDb alloc]init];
    
    
    if([notificationTitle isEqualToString:@"Category"]){
        NSArray *dataArray=[object fetch_categoryName:catId];
        NSArray *nArr=[dataArray valueForKey:@"category_name"];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ItemsViewController *weightController = (ItemsViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ItemsViewController"];
        weightController.fetchedId=catId;
        weightController.fetchedName=[nArr objectAtIndex:0];
        UINavigationController  *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:weightController];
        SidebarViewController *rearViewController = (SidebarViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"SidebarController"];
        
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
        
        mainRevealController.rearViewController = rearViewController;
        mainRevealController.frontViewController= frontNavigationController;
        
        self.window.rootViewController = mainRevealController;
        
        
        
        
    }else if([notificationTitle isEqualToString:@"Product"]){
        
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ItemDetailsController *weightController = (ItemDetailsController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ItemDetailsController"];
        weightController.fetchedItemId=catId;
        weightController.fetchedItemName=@"Menu";
        weightController.comingFrom=@"NOTIFICATION";
        UINavigationController  *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:weightController];
        SidebarViewController *rearViewController = (SidebarViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"SidebarController"];
        
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
        
        mainRevealController.rearViewController = rearViewController;
        mainRevealController.frontViewController= frontNavigationController;
        
        self.window.rootViewController = mainRevealController;
        
    }
    
    
    
    // amit code  he try kr na
    
    
    
    
    
}
//- (void)downloadFile:(void (^)(UIBackgroundFetchResult))completionHandler {
//    NSURLSessionDownloadTask *task = [AFHTTPSessionManager_Instance downloadTaskWithRequest:request progress:nil destination:nil completionHandler:nil];
//    [task resume];
//    NSLog(@"Starting download of %@", request.URL.absoluteString);
//
//    // Some additional time to start the download
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        NSLog(@"Completion handler!");
//        completionHandler(UIBackgroundFetchResultNewData);
//    });
//
//


@end
