//
//  CustomTableViewCell2.h
//  TSMC
//
//  Created by user on 13/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell2 : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *contentBackView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_shortDesc;
@property (weak, nonatomic) IBOutlet UIImageView *img_product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_price;
@property (weak, nonatomic) IBOutlet UIView *qtyBackView;
@property (weak, nonatomic) IBOutlet UIButton *btn_sub;
@property (weak, nonatomic) IBOutlet UIButton *btn_add;
@property (weak, nonatomic) IBOutlet UILabel *label_qty;
@property (weak, nonatomic) IBOutlet UIButton *btn_addCart;
@property (weak, nonatomic) IBOutlet UILabel *lbl_brandName;



@property (weak, nonatomic) IBOutlet UILabel *lbl_cut;
@property (weak, nonatomic) IBOutlet UIButton *btn_cut;
@property (weak, nonatomic) IBOutlet UILabel *lbl_weight;
@property (weak, nonatomic) IBOutlet UIButton *btn_weight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_type;
@property (weak, nonatomic) IBOutlet UIButton *btn_type;
@property (weak, nonatomic) IBOutlet UILabel *lbl_noAttrWeight;
@property (strong, nonatomic) IBOutlet UILabel *weight_Lbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentBackViewConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_cutHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_cutHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_weightHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_weightHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_typeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_typeHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shortDescHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartWeightHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *weightLblHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_noAttrWeightHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qtyBackViewConstraint;


@end
