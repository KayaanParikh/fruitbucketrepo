//
//  LoginViewController.h
//  TSMC
//
//  Created by user on 01/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ProgressHUD.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController : UIViewController<UIActionSheetDelegate, UITextFieldDelegate>
{
    
    IBOutlet UILabel *_titleBack;
    IBOutlet UILabel *_titleText;
    IBOutlet UIButton *btn_Back;
    IBOutlet UITextField *UNameTF;
    IBOutlet UITextField *PasswordTF;
    IBOutlet UIButton *Btn_SignUp;
    IBOutlet UIButton *Btn_Forgot;
    IBOutlet UIScrollView *scrollView;
    
    
    //FORGOT PASSWORD OUTLETS
    IBOutlet UIView *subView_ForgotPassword;
    IBOutlet UIView *forgotPasswordView;
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *titleText;
    IBOutlet UITextField *forgotPasswordTF;
    IBOutlet UIButton *cancelBtn;
    IBOutlet UIButton *submitBtn;
    
    IBOutlet UIImageView *img_background;
    IBOutlet UIView *loginBackView;
    IBOutlet FBSDKLoginButton *btn_fbLogin;
    
    
    
    //FORGOT FBCONTACT OUTLETS

    IBOutlet UIView *contactBackView;
    IBOutlet UILabel *lbl_contactTitle;
    IBOutlet UITextField *txt_contact;
    IBOutlet UIButton *btn_contactCancel;
    IBOutlet UIButton *btn_conatactSubmit;
//    UITextField *txt_contact;

    
     MBProgressHUD *HUD;
    
    NSUserDefaults *defaults;
}
- (IBAction)Back_Btn_Clicked:(id)sender;
- (IBAction)Cancel_Btn_Clicked:(id)sender;
- (IBAction)SignIn_Btn_Clicked:(id)sender;
- (IBAction)SignUpBtnClicked:(id)sender;
- (IBAction)ForgotPass_Btn_Clicked:(id)sender;

- (IBAction)EmailTFDidEndEditing:(id)sender;
- (IBAction)PasswordTFDidEndEditing:(id)sender;

- (IBAction)cancelBtn_Clicked:(id)sender;
- (IBAction)submitBtn_Clicked:(id)sender;
//- (IBAction)fbLoginMethod:(id)sender;
- (IBAction)facebookLoginMehothod:(id)sender;

- (IBAction)contactSubmitMethod:(id)sender;
- (IBAction)contactCancelMethod:(id)sender;

#pragma mark POPUP
-(void) CreateSlideOut;
-(void) removePopUp;
-(void) slidePopup;


@property (nonatomic) NSString *comingFrom,*goBackTo;

@end
