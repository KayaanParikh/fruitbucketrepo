//
//  FilterController.h
//  TSMC
//
//  Created by Rahul Lekurwale on 09/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CERangeSlider.h"
@interface FilterController : UIViewController{
    NSString * _sliderRangeText;

}
@property (weak, nonatomic) IBOutlet UILabel *lbl_max;
@property (weak, nonatomic) IBOutlet UILabel *lbl_min;
@property (weak, nonatomic) IBOutlet UILabel *lbl_badgeCount;
@property (weak, nonatomic) IBOutlet UIButton *btn_cart;
- (IBAction)cartMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleText;
@property (weak, nonatomic) IBOutlet UILabel *titleBack;
@property (weak, nonatomic) IBOutlet UIButton *btn_back;
- (IBAction)backMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *brandTableView;
@property (weak, nonatomic) IBOutlet UIButton *btn_reset;
- (IBAction)resetMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_filter;
- (IBAction)filterMethod:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *rangeBackView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_minConstant;
@property (strong, nonatomic) IBOutlet UILabel *lbl_maxConstant;

@property(weak,nonatomic)NSString *categoryId,*subCategoryId;
@end
