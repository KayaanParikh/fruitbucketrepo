//
//  ObjInvitedPeopleData.m
//
//  Created by IT-kinnarparikhL  on 6/30/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ObjStateData.h"


NSString *const kObjInvitedPeopleDataTitle = @"title";
NSString *const kObjInvitedPeopleDataValue = @"value";
NSString *const kObjInvitedPeopleDataLabel = @"label";


@interface ObjStateData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ObjStateData

@synthesize title = _title;
@synthesize value = _value;
@synthesize label = _label;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kObjInvitedPeopleDataTitle fromDictionary:dict];
            self.value = [self objectOrNilForKey:kObjInvitedPeopleDataValue fromDictionary:dict];
            self.label = [self objectOrNilForKey:kObjInvitedPeopleDataLabel fromDictionary:dict];
           
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kObjInvitedPeopleDataTitle];
    [mutableDict setValue:self.value forKey:kObjInvitedPeopleDataValue];
    [mutableDict setValue:self.label forKey:kObjInvitedPeopleDataLabel];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.title = [aDecoder decodeObjectForKey:kObjInvitedPeopleDataTitle];
    self.value = [aDecoder decodeObjectForKey:kObjInvitedPeopleDataValue];
    self.label = [aDecoder decodeObjectForKey:kObjInvitedPeopleDataLabel];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_title forKey:kObjInvitedPeopleDataTitle];
    [aCoder encodeObject:_value forKey:kObjInvitedPeopleDataValue];
    [aCoder encodeObject:_label forKey:kObjInvitedPeopleDataLabel];
    
}

- (id)copyWithZone:(NSZone *)zone
{
    ObjStateData *copy = [[ObjStateData alloc] init];
    
    if (copy) {

        copy.title = [self.title copyWithZone:zone];
        copy.value = [self.value copyWithZone:zone];
        copy.label = [self.label copyWithZone:zone];
        
    }
    
    return copy;
}


@end
