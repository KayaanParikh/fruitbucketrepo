//
//  AppDelegate.h
//  TSMC
//
//  Created by Rahul Lekurwale on 21/06/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(strong,nonatomic)NSString *databasePath;
@property(strong,nonatomic)NSString *cartToItem;

@property(strong,nonatomic)NSMutableArray *selectBarndArray;

@end

