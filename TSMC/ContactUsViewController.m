//
//  ContactUsViewController.m
//  TSMC
//
//  Created by user on 14/07/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import "ContactUsViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "WebservicesClass.h"
#import "HelperClass.h"
#import "OfflineDb.h"
#import "AppConstant.h"


@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Full Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
       EmailTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
       TelephoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Telephone" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
       CommentTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Comments" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
       
    
    
    [self initObjects];
    [self InitAttributeFrame];
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init Methods

-(void)initObjects
{
    NameTF.tag=104;
}
-(void)InitAttributeFrame{
    
    [scrollView1 setContentSize:CGSizeMake(375, 800)];
    
    self.navigationController.navigationBarHidden=NO;
    
//    titleLbl.backgroundColor=TopBarColor;
    self.title = @"Contact Us".uppercaseString;
//    titleText.font=TopBarFont;
//    titleText.textColor=TopBarTextColor;
    
}

#pragma mark - Button Clicked Methods

- (IBAction)backBtn_Clicked:(id)sender{
    
    [self loadStoryBoardWithIdentifier:@"ViewController"];
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitBtn_Clicked:(id)sender{
    
    [CommentTF resignFirstResponder];
    
    if(NameTF.text.length>0)
    {
        if([self validEmail:EmailTF.text])
        {
            if(TelephoneTF.text.length==0 || TelephoneTF.text.length==10)
            {
                if(CommentTF.text.length>0)
                {
                    [self ContactUsAPI];
                }else{
                    [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Comment!"];
                    //enter cp
                }
            }else{
                [self UIAlertViewControllerMethodTitle:@"" message:@"Number must have 10 digit!"];
                //need 10 character long password
            }
        }else{
            [self UIAlertViewControllerMethodTitle:@"" message:@"Enter valid Email Address!"];
            //enter password
        }
    }else{
        [self UIAlertViewControllerMethodTitle:@"" message:@"Enter Name"];
        //invalid email
    }
    
}
#pragma mark - OtherMethods
-(void)loadStoryBoardWithIdentifier:(NSString *)identifier
{
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:vc animated:NO];
}
#pragma mark - API Call Methods

-(void)ContactUsAPI
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.hidden=NO;
    HUD.dimBackground = YES;
    HUD.labelText = @"Please wait...";
    
    HelperClass *s=[[HelperClass alloc]init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        BOOL connection=s.getInternetStatus;
        
        if (!connection)
        {
            NSLog(@"Hello Net connection is not present....");
            
            //[HUD hide:YES];
            HUD.hidden=YES;
            [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:@"Please check your internet connection!"];
        
        }else{
            @try
            {
                NSString *email=EmailTF.text;
                NSString *fname=NameTF.text;
                NSString *comment=CommentTF.text;
                NSString *telephone=TelephoneTF.text;
                
                NSString *url=@"https://seafoodandmeatco.in/api/contacts.php";
                
                NSMutableDictionary *parameterDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:email,@"email",fname,@"name",comment,@"comment",telephone,@"telephone", nil];
                
                WebservicesClass *web=[[WebservicesClass alloc] init];
                
                NSDictionary *getJsonData=[web PostTopviewProductWithUrl:url withParameter:parameterDict];
                
                NSLog(@"%@",getJsonData);
                
                
                
                NSString *success=[getJsonData valueForKey:@"value"];
                
                if ([getJsonData count] >0)
                {
                    if ([success isEqualToString:@"Success"])
                    {
                        HUD.hidden=YES;
                        
                        [ProgressHUD showSuccess:@"Your Contact Request Has Been Submitted,We will Get Back To You Shortly." Interaction:NO];
                        [self loadStoryBoardWithIdentifier:@"ViewController"];
                        
                    }else{
                        [ProgressHUD showError:@"error" Interaction:NO];
                        
                    }
                }
            } @catch (NSException *exception){
                NSLog(@"%@",exception);
                NSString *exc=[NSString stringWithFormat:@"%@",exception];
                //            [ProgressHUD showError:exc Interaction:NO];
                [self UIAlertViewControllerMethodTitle:@"Connection Lost" message:exc];
                
            }
        }
    });
}


#pragma mark - ValidationMethods
- (BOOL) validEmail:(NSString*) emailString
{
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - UIAlertViewControllerMethod

-(void)UIAlertViewControllerMethodTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController  *alrt=[UIAlertController alertControllerWithTitle:title message:NSLocalizedString(msg, @"Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                       {
                           HUD.hidden=YES;
                           [self dismissViewControllerAnimated:YES completion:nil];
                       }];
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                           {
                               HUD.hidden=YES;
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }];
    
    
    [alrt addAction:ok];
    [alrt addAction:cancel];
    [self presentViewController:alrt animated:YES completion:nil];
}


#pragma mark - TextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    ///Restrict user to enter numbers in First Name and Last Name
    if (textField.tag==104) {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
            else{
                return YES;
            }
        }
    }
    return YES;
}

@end
